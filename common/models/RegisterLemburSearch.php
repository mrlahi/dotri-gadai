<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RegisterLembur;

/**
 * RegisterLemburSearch represents the model behind the search form of `common\models\RegisterLembur`.
 */
class RegisterLemburSearch extends RegisterLembur
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'pegawai_id', 'jam_lembur', 'created_by'], 'integer'],
            [['tgl_lembur', 'status_lembur', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RegisterLembur::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'pegawai_id' => $this->pegawai_id,
            'tgl_lembur' => $this->tgl_lembur,
            'jam_lembur' => $this->jam_lembur,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'status_lembur', $this->status_lembur]);

        return $dataProvider;
    }
}
