<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LogLogin;

/**
 * LogLoginSearch represents the model behind the search form of `common\models\LogLogin`.
 */
class LogLoginSearch extends LogLogin
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'screen_width', 'screen_height', 'created_by'], 'integer'],
            [['user_agent', 'device_type', 'browser_type', 'created_at', 'OSName', 'OSVersion'], 'safe'],
            [['latitude', 'longitude'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogLogin::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'screen_width' => $this->screen_width,
            'screen_height' => $this->screen_height,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'user_agent', $this->user_agent])
            ->andFilterWhere(['like', 'device_type', $this->device_type])
            ->andFilterWhere(['like', 'browser_type', $this->browser_type])
            ->andFilterWhere(['like', 'OSName', $this->OSName])
            ->andFilterWhere(['like', 'OSVersion', $this->OSVersion]);

        return $dataProvider;
    }
}
