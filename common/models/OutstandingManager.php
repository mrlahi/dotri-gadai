<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "outstanding_manager".
 *
 * @property int $id_outstanding_manager
 * @property string $tgl_start
 * @property string $tgl_end
 * @property string $status_generator
 * @property int $created_by
 * @property string $created_at
 */
class OutstandingManager extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'outstanding_manager';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl_start', 'tgl_end', 'created_by', 'created_at'], 'required'],
            [['tgl_start', 'tgl_end', 'created_at'], 'safe'],
            [['created_by'], 'integer'],
            [['status_generator'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_outstanding_manager' => 'Id Outstanding Manager',
            'tgl_start' => 'Tgl Start',
            'tgl_end' => 'Tgl End',
            'status_generator' => 'Status Generator',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
        ];
    }

    public function getStatus(){
        if($this->status_generator == "ONPROCESS"){
            return '<span class="label label-warning">ON PROCESS</span>';
        }
        else if($this->status_generator == "EXPORTING"){
            return '<span class="label label-warning">EXPORTING</span>';
        }
        else{
            return '<span class="label label-success">SUCCESS</span>';
        }
    }
}
