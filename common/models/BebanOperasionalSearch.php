<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BebanOperasional;

/**
 * BebanOperasionalSearch represents the model behind the search form of `common\models\BebanOperasional`.
 */
class BebanOperasionalSearch extends BebanOperasional
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_beban', 'jumlah_pengeluaran', 'created_by', 'id_cabang'], 'integer'],
            [['nama_pengeluaran', 'type_pencairan', 'type_operasional', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BebanOperasional::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_beban' => $this->id_beban,
            'jumlah_pengeluaran' => $this->jumlah_pengeluaran,
            'created_by' => $this->created_by,
            'id_cabang' => $this->id_cabang,
        ]);

        $query->andFilterWhere(['like', 'nama_pengeluaran', $this->nama_pengeluaran])
            ->andFilterWhere(['like', 'type_pencairan', $this->type_pencairan])
            ->andFilterWhere(['like', 'type_operasional', $this->type_operasional]);

        if(isset ($this->created_at)&&$this->created_at!=''){ //you dont need the if function if yourse sure you have a not null date
            $date_explode=explode(" - ",$this->created_at);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);
            $query->andFilterWhere(['between','created_at',$date1,$date2]);
        }

        return $dataProvider;
    }
}
