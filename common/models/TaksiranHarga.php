<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "taksiran_harga".
 *
 * @property int $id
 * @property int $merk_id
 * @property int $type_barang_id
 * @property string $tgl_aktif
 * @property string $tgl_non_aktif
 * @property int $is_active
 * @property string $created_at
 * @property int $created_by
 *
 * @property MerkBarang $merk
 * @property TypeBarang $typeBarang
 */
class TaksiranHarga extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'taksiran_harga';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['merk_id', 'type_barang_id', 'tgl_aktif', 'is_active', 'created_at', 'created_by'], 'required'],
            [['merk_id', 'type_barang_id', 'is_active', 'created_by'], 'integer'],
            [['tgl_aktif', 'tgl_non_aktif', 'created_at'], 'safe'],
            [['merk_id'], 'exist', 'skipOnError' => true, 'targetClass' => MerkBarang::className(), 'targetAttribute' => ['merk_id' => 'id']],
            [['type_barang_id'], 'exist', 'skipOnError' => true, 'targetClass' => TypeBarang::className(), 'targetAttribute' => ['type_barang_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'merk_id' => 'Merk ID',
            'type_barang_id' => 'Type Barang ID',
            'tgl_aktif' => 'Tgl Aktif',
            'tgl_non_aktif' => 'Tgl Non Aktif',
            'is_active' => 'Is Active',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[Merk]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMerk()
    {
        return $this->hasOne(MerkBarang::className(), ['id' => 'merk_id']);
    }

    /**
     * Gets query for [[TypeBarang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTypeBarang()
    {
        return $this->hasOne(TypeBarang::className(), ['id' => 'type_barang_id']);
    }
}
