<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Inventory;

/**
 * InventorySearch represents the model behind the search form of `common\models\Inventory`.
 */
class InventorySearch extends Inventory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'harga_perolehan', 'persen_penyusutan', 'umur_aset', 'created_by'], 'integer'],
            [['no_inventaris', 'nama_barang', 'tgl_perolehan', 'created_at', 'merk_barang', 'type_barang'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Inventory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'harga_perolehan' => $this->harga_perolehan,
            'persen_penyusutan' => $this->persen_penyusutan,
            'umur_aset' => $this->umur_aset,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'no_inventaris', $this->no_inventaris])
            ->andFilterWhere(['like', 'nama_barang', $this->nama_barang])
            ->andFilterWhere(['like', 'merk_barang', $this->merk_barang])
            ->andFilterWhere(['like', 'type_barang', $this->type_barang]);

        if(isset ($this->tgl_perolehan)&&$this->tgl_perolehan!=''){
            $date_explode=explode(" - ",$this->tgl_perolehan);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);
            $query->andFilterWhere(['between','tgl_perolehan',$date1,$date2]);
        }

        return $dataProvider;
    }
}
