<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TambahanJual;

/**
 * TambahanJualSearch represents the model behind the search form of `common\models\TambahanJual`.
 */
class TambahanJualSearch extends TambahanJual
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tambah_jual', 'id_barang', 'biaya_tambah', 'created_by'], 'integer'],
            [['desk_tambahan', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TambahanJual::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_tambah_jual' => $this->id_tambah_jual,
            'id_barang' => $this->id_barang,
            'biaya_tambah' => $this->biaya_tambah,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'desk_tambahan', $this->desk_tambahan]);

        return $dataProvider;
    }
}
