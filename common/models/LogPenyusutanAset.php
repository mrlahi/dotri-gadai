<?php

namespace common\models;

use DateTime;
use Yii;

/**
 * This is the model class for table "log_penyusutan_aset".
 *
 * @property int $id
 * @property int $inventaris_id
 * @property float $nilai_penyusutan
 * @property string $tgl_penyusutan
 * @property int $is_penyusutan
 * @property string $created_at
 * @property int $created_by
 *
 * @property Inventory $inventaris
 */
class LogPenyusutanAset extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_penyusutan_aset';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inventaris_id', 'nilai_penyusutan', 'tgl_penyusutan', 'is_penyusutan', 'created_at', 'created_by'], 'required'],
            [['inventaris_id', 'is_penyusutan', 'created_by', 'penyusutan_ke'], 'integer'],
            [['nilai_penyusutan'], 'number'],
            [['tgl_penyusutan', 'created_at'], 'safe'],
            [['inventaris_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inventory::className(), 'targetAttribute' => ['inventaris_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inventaris_id' => 'Inventaris ID',
            'nilai_penyusutan' => 'Nilai Penyusutan',
            'tgl_penyusutan' => 'Tgl Penyusutan',
            'is_penyusutan' => 'Is Penyusutan',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[Inventaris]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInventaris()
    {
        return $this->hasOne(Inventory::className(), ['id' => 'inventaris_id']);
    }

    public function inLogPenyusutan($aset_id){
        $dataAset = Inventory::findOne($aset_id);

        $bulanPenyusutan = $dataAset->umur_aset * 12;
        $nilaiPenyusutan = round($dataAset->harga_perolehan/$bulanPenyusutan,2);

        $tglPengadaan = date("d",strtotime($dataAset->tgl_perolehan));

        if($tglPengadaan > 15){

            $date = date('Y-m-d', strtotime('+1 month', strtotime($dataAset->tgl_perolehan)));
            $startPenyusutan = date("Y-m-t", strtotime($date));
        }
        else{
            $startPenyusutan = date("Y-m-t", strtotime($dataAset->tgl_perolehan));
        }

        $start = 1;
        while($start <= $bulanPenyusutan){

            $mLog = new LogPenyusutanAset();

            $mLog->inventaris_id = $aset_id;
            $mLog->penyusutan_ke = $start;
            $mLog->nilai_penyusutan = $nilaiPenyusutan;
            $mLog->tgl_penyusutan = $startPenyusutan;
            $mLog->is_penyusutan = 0;
            $mLog->created_at = date('Y-m-d');
            $mLog->created_by = Yii::$app->user->id;
            $mLog->save();

            $start++;

            $jangka = "+".$start." month";
            $date = date('Y-m-d', strtotime($jangka, strtotime($dataAset->tgl_perolehan)));
            $startPenyusutan = date("Y-m-t", strtotime($date));

        }

    }

}
