<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "periode_penggajian_honor".
 *
 * @property int $id
 * @property int $pegawai_id
 * @property int $honor_id
 * @property int $periode_id
 * @property string $created_at
 * @property int $created_by
 *
 * @property HonorPegawai $honor
 * @property Profile $pegawai
 * @property PeriodePenggajian $periode
 */
class PeriodePenggajianHonor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'periode_penggajian_honor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pegawai_id', 'honor_id', 'periode_id', 'created_at', 'created_by'], 'required'],
            [['pegawai_id', 'honor_id', 'periode_id', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['honor_id'], 'exist', 'skipOnError' => true, 'targetClass' => HonorPegawai::className(), 'targetAttribute' => ['honor_id' => 'id']],
            [['pegawai_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['pegawai_id' => 'id_user']],
            [['periode_id'], 'exist', 'skipOnError' => true, 'targetClass' => PeriodePenggajian::className(), 'targetAttribute' => ['periode_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai_id' => 'Pegawai ID',
            'honor_id' => 'Honor ID',
            'periode_id' => 'Periode ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[Honor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHonor()
    {
        return $this->hasOne(HonorPegawai::className(), ['id' => 'honor_id']);
    }

    /**
     * Gets query for [[Pegawai]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Profile::className(), ['id_user' => 'pegawai_id']);
    }

    /**
     * Gets query for [[Periode]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPeriode()
    {
        return $this->hasOne(PeriodePenggajian::className(), ['id' => 'periode_id']);
    }
}
