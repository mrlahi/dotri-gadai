<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "inventory".
 *
 * @property int $id
 * @property string $no_inventaris
 * @property string $nama_barang
 * @property int $harga_perolehan
 * @property string $tgl_perolehan
 * @property double $persen_penyusutan
 * @property int $umur_aset
 * @property string $created_at
 * @property int $created_by
 */
class Inventory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inventory';
    }

    /**
     * {@inheritdoc}
     */

    public $akun_kode;

    public function rules()
    {
        return [
            [['no_inventaris', 'nama_barang', 'harga_perolehan', 'tgl_perolehan', 'persen_penyusutan', 'umur_aset', 'created_at', 'created_by', 'akun_kode', 'merk_barang', 'type_barang'], 'required'],
            [['harga_perolehan', 'umur_aset', 'created_by', 'akun_kode'], 'integer'],
            //[['persen_penyusutan'], 'double'],
            [['tgl_perolehan', 'created_at', 'spesifikasi', 'deskripsi'], 'safe'],
            [['no_inventaris', 'jenis_inventaris'], 'string', 'max' => 50],
            [['nama_barang'], 'string', 'max' => 225],
            [['no_inventaris'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_inventaris' => 'No Inventaris',
            'nama_barang' => 'Nama Barang',
            'harga_perolehan' => 'Harga Perolehan',
            'tgl_perolehan' => 'Tgl Perolehan',
            'persen_penyusutan' => 'Persen Penyusutan',
            'umur_aset' => 'Umur Aset',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    public function getPenyusutanAsets()
    {
        return $this->hasMany(LogPenyusutanAset::className(), ['inventaris_id' => 'id']);
    }

    public function noInventaris(){
        $data = $this->find()->count();
        $no = $data + 1;

        $prefix = "INV-".date_format(date_create($this->tgl_perolehan), 'ym');

        $noInventaris = $prefix.str_pad($no, 4, '0', STR_PAD_LEFT);
        return $noInventaris;
    }

    public function nilaiSisa(){
        $susut = $this->getPenyusutanAsets()->andWhere(['is_penyusutan' => 1])->sum('nilai_penyusutan');
        $sisa = $this->harga_perolehan - $susut;

        return $sisa;
    }

}
