<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "merk_barang".
 *
 * @property int $id
 * @property string $nama_merek
 * @property string $deskripsi
 * @property string $created_at
 * @property int $created_by
 *
 * @property TaksiranHarga[] $taksiranHargas
 * @property TypeBarang[] $typeBarangs
 */
class MerkBarang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'merk_barang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_merek', 'deskripsi', 'created_at', 'created_by'], 'required'],
            [['deskripsi'], 'string'],
            [['created_at'], 'safe'],
            [['created_by'], 'integer'],
            [['nama_merek'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_merek' => 'Nama Merek',
            'deskripsi' => 'Deskripsi',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[TaksiranHargas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTaksiranHargas()
    {
        return $this->hasMany(TaksiranHarga::className(), ['merk_barang_id' => 'id']);
    }

    /**
     * Gets query for [[TypeBarangs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTypeBarangs()
    {
        return $this->hasMany(TypeBarang::className(), ['merk_barang_id' => 'id']);
    }

    public static function listMerk(){
        return ArrayHelper::map(MerkBarang::find()->asArray()->all(),'id','nama_merek');
    }

}
