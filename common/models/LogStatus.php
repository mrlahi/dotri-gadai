<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "log_status".
 *
 * @property int $id_status
 * @property int $id_barang
 * @property string $status
 * @property int $created_at
 * @property int $created_by
 */
class LogStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_barang', 'status', 'created_at', 'created_by'], 'required'],
            [['id_barang', 'created_by'], 'integer'],
            [['status'], 'string', 'max' => 50],
        ];
    }

    public function getBarang(){
        return $this->hasOne(Barang::className(),['id_barang'=>'id_barang']);
    }

    public function myBarang(){
        return Barang::find()->where(['id_barang' => $this->id_barang])->one();
    }

    public function getCabang(){
        $id_cabang = $this->myBarang()->id_cabang;
        return Cabang::find()->where(['id_cabang' => $id_cabang])->one();
    }

    public function getJenisBarang(){
        $id_jenis = $this->myBarang()->id_jenis;
        return JenisBarang::find()->where(['id_jenis_barang' => $id_jenis])->one();
    }

    public function getNasabah(){
        $id_nasabah = $this->myBarang()->id_nasabah;
        return Profile::find()->where(['id_user'=>$id_nasabah])->one();
    }

    public function getStatbar(){
        if($this->status == "Terjual"){
            $status = '<span class="label label-warning">Terjual</span>';
        }
        else if($this->status == "Lunas"){
            $status = '<span class="label label-success">Lunas</span>';
        }
        else{
            $status = '<span class="label label-info">Outstanding</span>';
        }

        return $status;
    }

    public function getPegawai(){
        return $this->hasOne(Profile::className(),['id_user'=>'created_by']);
    }

    public function listStatus(){
        return $dataStatus = ['Terjual' => 'Terjual','Lunas' => 'Lunas','Outstanding' => 'Outstanding'];
    }

    public function inStatus($id_barang,$status){
        $this->id_barang = $id_barang;
        $this->status = $status;
        $this->created_at = date('Y-m-d');
        $this->created_by = Yii::$app->user->identity->id;
        $this->save(false);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_status' => 'Id Status',
            'id_barang' => 'Id Barang',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }
}
