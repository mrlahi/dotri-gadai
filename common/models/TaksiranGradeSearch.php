<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TaksiranGrade;

/**
 * TaksiranGradeSearch represents the model behind the search form of `common\models\TaksiranGrade`.
 */
class TaksiranGradeSearch extends TaksiranGrade
{
    /**
     * {@inheritdoc}
     */

    public $merk;
    public $type;
    public $desk_barang;

    public function rules()
    {
        return [
            [['id', 'taksiran_id', 'grade_id', 'lengkap', 'batangan', 'kotak', 'charger', 'created_by'], 'integer'],
            [['created_at', 'merk', 'type', 'desk_barang'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaksiranGrade::find();
        $query->joinWith(['taksiran', 'taksiran.typeBarang']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'taksiran_id' => $this->taksiran_id,
            'grade_id' => $this->grade_id,
            'lengkap' => $this->lengkap,
            'batangan' => $this->batangan,
            'kotak' => $this->kotak,
            'charger' => $this->charger,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'merk_id' => $this->merk
        ]);

        $query->andFilterWhere(['like', 'nama_type', $this->type])
        ->andFilterWhere(['like', 'deskripsi', $this->desk_barang]);

        return $dataProvider;
    }
}
