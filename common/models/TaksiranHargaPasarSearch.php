<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TaksiranHargaPasar;

/**
 * TaksiranHargaPasarSearch represents the model behind the search form of `common\models\TaksiranHargaPasar`.
 */
class TaksiranHargaPasarSearch extends TaksiranHargaPasar
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_taksiran', 'taksiran_harga', 'created_by'], 'integer'],
            [['merk_barang', 'type_barang', 'nama_barang', 'kelengkapan', 'kondisi', 'created_at', 'spesifikasi'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaksiranHargaPasar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id_taksiran' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_taksiran' => $this->id_taksiran,
            'taksiran_harga' => $this->taksiran_harga,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'merk_barang', $this->merk_barang])
            ->andFilterWhere(['like', 'type_barang', $this->type_barang])
            ->andFilterWhere(['like', 'nama_barang', $this->nama_barang])
            ->andFilterWhere(['like', 'kondisi', $this->kondisi])
            ->andFilterWhere(['like', 'kelengkapan', $this->kelengkapan])
            ->andFilterWhere(['like', 'spesifikasi', $this->spesifikasi]);

        return $dataProvider;
    }
}
