<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "face_user".
 *
 * @property int $id
 * @property int $profile_id
 * @property string $face_decrypt
 * @property string $created_at
 * @property int $created_by
 */
class FaceUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'face_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['profile_id', 'face_decrypt', 'created_at', 'created_by'], 'required'],
            [['profile_id', 'created_by'], 'integer'],
            [['face_decrypt'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Profile ID',
            'face_decrypt' => 'Face Decrypt',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Profile::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[Profile]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['id' => 'profile_id']);
    }

    public static function compressImage($source, $destination, $maxSize)
    {
        $quality = 90; // Awalnya kualitas tinggi
        do {
            // Buat versi terkompresi
            $image = imagecreatefrompng($source);
            ob_start();
            imagepng($image, null, round($quality / 10)); // Kompresi untuk PNG
            $compressedData = ob_get_clean();
            imagedestroy($image);

            // Cek ukuran
            if (strlen($compressedData) <= $maxSize || $quality <= 10) {
                file_put_contents($destination, $compressedData);
                return true;
            }

            // Kurangi kualitas
            $quality -= 10;
        } while ($quality > 0);

        return false; // Jika gagal mencapai ukuran
    }
}
