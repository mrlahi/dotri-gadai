<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pasaran_harga_emas".
 *
 * @property int $id
 * @property int $nilai_karat
 * @property int $harga
 * @property string $created_at
 * @property int $created_by
 * @property int $is_active
 */
class PasaranHargaEmas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasaran_harga_emas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nilai_karat', 'harga', 'created_at', 'created_by', 'is_active'], 'required'],
            [['nilai_karat', 'harga', 'created_by', 'is_active'], 'integer'],
            [['created_at','deskripsi'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nilai_karat' => 'Nilai Karat',
            'harga' => 'Harga',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'is_active' => 'Is Active',
            'deskripsi' => 'Deskripsi'
        ];
    }
}
