<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "kas_umum".
 *
 * @property int $id_kas_umum
 * @property int|null $jumlah_uang_masuk
 * @property int|null $jumlah_uang_keluar
 * @property int $id_cabang
 * @property string $created_at
 * @property int $created_by
 */
class KasUmum extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kas_umum';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jumlah_uang_masuk', 'jumlah_uang_keluar', 'id_cabang', 'created_by'], 'integer'],
            [['id_cabang', 'created_at', 'created_by'], 'required'],
            [['created_at'], 'safe'],
        ];
    }

    public function getCabang(){
        return $this->hasOne(Cabang::className(),['id_cabang'=>'id_cabang']);
    }

    public static function listCabang(){
        $datacabang = ArrayHelper::map(Cabang::find()->asArray()->all(),'id_cabang','nama_cabang');
        return $datacabang;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kas_umum' => 'Id Kas Umum',
            'jumlah_uang_masuk' => 'Jumlah Uang Masuk',
            'jumlah_uang_keluar' => 'Jumlah Uang Keluar',
            'id_cabang' => 'Id Cabang',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }
}
