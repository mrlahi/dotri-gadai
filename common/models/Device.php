<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "device".
 *
 * @property string $id
 * @property string $token
 * @property string $nama_device
 * @property string $created_at
 * @property int $created_by
 * @property int $cabang_id
 * @property string $no_device
 *
 * @property Cabang $cabang
 * @property LogNotifikasi[] $logNotifikasis
 */
class Device extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'device';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'token', 'nama_device', 'created_at', 'created_by', 'cabang_id', 'no_device'], 'required'],
            [['token'], 'string'],
            [['created_at'], 'safe'],
            [['created_by', 'cabang_id'], 'integer'],
            [['id', 'nama_device', 'no_device'], 'string', 'max' => 225],
            [['id'], 'unique'],
            [['cabang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cabang::className(), 'targetAttribute' => ['cabang_id' => 'id_cabang']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'token' => 'Token',
            'nama_device' => 'Nama Device',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'cabang_id' => 'Cabang ID',
            'no_device' => 'No Device',
        ];
    }

    /**
     * Gets query for [[Cabang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCabang()
    {
        return $this->hasOne(Cabang::className(), ['id_cabang' => 'cabang_id']);
    }

    /**
     * Gets query for [[LogNotifikasis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLogNotifikasis()
    {
        return $this->hasMany(LogNotifikasi::className(), ['device_id' => 'id']);
    }

    //MY FUNCTIONS START HERE

    public function getStatus(){
        $pureToken = explode('.',$this->token);
        $token = $pureToken[0];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,  "https://deu.wablas.com/api/device/info?token=$token");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $result = curl_exec($curl);
        curl_close($curl);
        $lapor = json_decode($result);
        return $lapor->data;

    }

    //MY FUNCTIONS END HERE

}
