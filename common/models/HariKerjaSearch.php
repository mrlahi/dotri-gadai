<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\HariKerja;

/**
 * HariKerjaSearch represents the model behind the search form of `common\models\HariKerja`.
 */
class HariKerjaSearch extends HariKerja
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by'], 'integer'],
            [['tgl_kerja', 'status_hari', 'deskripsi', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HariKerja::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['tgl_kerja' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 30,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            //'tgl_kerja' => $this->tgl_kerja,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'status_hari', $this->status_hari])
            ->andFilterWhere(['like', 'deskripsi', $this->deskripsi]);

        if(isset ($this->tgl_kerja) && $this->tgl_kerja !=''){ //you dont need the if function if yourse sure you have a not null date
            $date_explode=explode(" - ",$this->tgl_kerja);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);
            $query->andFilterWhere(['between','tgl_kerja',$date1,$date2]);
        }

        return $dataProvider;
    }
}
