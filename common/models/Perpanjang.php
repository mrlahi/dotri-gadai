<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "perpanjang".
 *
 * @property int $id_perpanjang
 * @property int $id_barang
 * @property int $denda
 * @property int $bunga
 * @property string $created_at
 * @property int $created_by
 */
class Perpanjang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'perpanjang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'created_by'], 'required'],
            [['id_barang', 'denda', 'created_by'], 'integer'],
            [['bunga'], 'double'],
            [
                ['id_barang','created_at'],'unique',
                'targetAttribute' => ['id_barang','created_at'],
            ],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_perpanjang' => 'Id Perpanjang',
            'id_barang' => 'Id Barang',
            'denda' => 'Denda',
            'bunga' => 'Bunga',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    public function myDetailPerpanjang($id_barang){
        return $this->find()->where(['id_barang' => $id_barang])->all();
    }

    public function cekPerpanjangNow($id_barang){
        $dataPerpanjang = $this->find()->where(['id_barang' => $id_barang, 'created_at' => date('Y-m-d')])->one();

        if(empty($dataPerpanjang->id_perpanjang)){
            return false;
        }
        else{
            return true;
        }
    }

    public function getBarang(){
        return $this->hasOne(Barang::className(),['id_barang'=>'id_barang']);
    }
}
