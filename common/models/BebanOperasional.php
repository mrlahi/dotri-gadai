<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "beban_operasional".
 *
 * @property int $id_beban
 * @property string $nama_pengeluaran
 * @property int $jumlah_pengeluaran
 * @property string $type_pencairan
 * @property string $type_operasional
 * @property int $created_by
 * @property string $created_at
 * @property int $id_cabang
 */
class BebanOperasional extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'beban_operasional';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_pengeluaran', 'jumlah_pengeluaran', 'type_pencairan', 'type_operasional', 'created_by', 'id_cabang', 'kas_bank_id'], 'required'],
            [['jumlah_pengeluaran', 'created_by', 'id_cabang', 'type_operasional', 'kas_bank_id'], 'integer'],
            [['created_at'], 'safe'],
            [['nama_pengeluaran'], 'string', 'max' => 225],
            [['type_pencairan'], 'string', 'max' => 20],
        ];
    }

    public function listOperasional() {
        $dataJenis = ArrayHelper::map(JenisBiayaOperasional::find()->asArray()->all(),'id','nama_jenis');
        return $dataJenis;
    }

    public function getCabang(){
        return $this->hasOne(Cabang::className(),['id_cabang'=>'id_cabang']);
    }

    public function getKasBank(){
        return $this->hasOne(KasBank::className(),['id_kas_bank'=>'kas_bank_id']);
    }

    public function getJenisBiaya(){
        return $this->hasOne(JenisBiayaOperasional::className(),['id'=>'type_operasional']);
    }

    public static function listCabang(){
        $datacabang = ArrayHelper::map(Cabang::find()->asArray()->all(),'id_cabang','nama_cabang');
        $datacabang['1000000'] = 'Semua Cabang';
        return $datacabang;
    }

    public function dailyOperasional($date1,$date2,$id_cabang){
        if($id_cabang == 1000000){
            return $this->find()->where(['between','created_at', $date1,$date2])->orderBy([ 'created_at' => SORT_ASC, 'id_cabang' => SORT_ASC])->all();
        }else{
            return $this->find()->where(['between','created_at', $date1,$date2])->andWhere(['id_cabang' => $id_cabang])->orderBy(['created_at' => SORT_ASC])->all();
        }
        
    }

    public function myCabang($id_cabang){
        return Cabang::findOne($id_cabang);
    }

    public function cekType($kas_id){
        if($kas_id == 1){
            return "Cash";
        }
        else{
            return "Transfer";
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_beban' => 'Id Beban',
            'nama_pengeluaran' => 'Nama Pengeluaran',
            'jumlah_pengeluaran' => 'Jumlah Pengeluaran',
            'type_pencairan' => 'Type Pencairan',
            'type_operasional' => 'Type Operasional',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'id_cabang' => 'Id Cabang',
        ];
    }
}
