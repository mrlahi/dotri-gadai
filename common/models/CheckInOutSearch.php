<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CheckInOut;

/**
 * CheckInOutSearch represents the model behind the search form of `common\models\CheckInOut`.
 */
class CheckInOutSearch extends CheckInOut
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['absen_id', 'tgl_absen', 'jam_absen', 'cabang_id', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CheckInOut::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'jam_absen' => $this->jam_absen,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'absen_id', $this->absen_id])
            ->andFilterWhere(['like', 'cabang_id', $this->cabang_id]);

        if(isset ($this->tgl_absen)&&$this->tgl_absen!=''){ //you dont need the if function if yourse sure you have a not null date
            $date_explode=explode(" - ",$this->tgl_absen);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);
            $query->andFilterWhere(['between','tgl_absen',$date1,$date2]);
        }

        return $dataProvider;
    }
}
