<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "barang".
 *
 * @property int $id_barang
 * @property string $no_kontrak
 * @property int $id_nasabah
 * @property string $nama_barang
 * @property string $merk
 * @property string $tipe
 * @property string $serial
 * @property int $id_jenis
 * @property string $tgl_masuk
 * @property string $jatuh_tempo
 * @property string $spek
 * @property string $kelengkapan
 * @property string $kondisi
 * @property int $harga_pasar
 * @property int $harga_taksir
 * @property int $nilai_pinjam
 * @property float $bunga
 * @property int $biaya_admin
 * @property int|null $biaya_simpan
 * @property int $jangka
 * @property int $created_by
 * @property string $status
 * @property int $id_cabang
 * @property string $kata_sandi
 *
 * @property Cabang $cabang
 * @property LogStatus[] $logStatuses
 * @property LogStatusTemp[] $logStatusTemps
 * @property TambahanJual[] $tambahanJuals
 */
class Barang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'barang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_kontrak', 'id_nasabah', 'nama_barang', 'merk', 'tipe', 'serial', 'id_jenis', 'tgl_masuk', 'jatuh_tempo', 'spek', 'kelengkapan', 'kondisi', 'harga_pasar', 'harga_taksir', 'nilai_pinjam', 'bunga', 'biaya_admin', 'jangka', 'created_by', 'status', 'id_cabang', 'no_fak', 'kata_sandi'], 'required'],
            [['id_nasabah', 'id_jenis', 'harga_pasar', 'harga_taksir', 'nilai_pinjam', 'biaya_admin', 'biaya_simpan', 'jangka', 'created_by', 'id_cabang', 'lokasi_simpan_id'], 'integer'],
            [['tgl_masuk', 'jatuh_tempo', 'tgl_status'], 'safe'],
            [['spek', 'kelengkapan', 'kondisi'], 'string'],
            [['bunga'], 'double'],
            [['no_kontrak', 'nama_barang', 'merk', 'tipe', 'serial', 'status'], 'string', 'max' => 225],
            [['id_cabang'], 'exist', 'skipOnError' => true, 'targetClass' => Cabang::className(), 'targetAttribute' => ['id_cabang' => 'id_cabang']],

            ['nilai_pinjam', 'compare', 'compareAttribute' => 'harga_pasar', 'operator' => '<', 'type' => 'number', 'message' => 'Nilai Pinjam harus dibawah Harga Pasar.'],

            [['serial', 'spek', 'kelengkapan', 'kondisi'], 'compare', 'compareValue' => '-', 'operator' => '!==', 'message' => 'Isi Data Ini dengan Benar'],

            ['nilai_pinjam', 'match', 'pattern' => '/^\d*000$/', 'message' => 'Nilai ini harus berkelipatan 1000.'],

        ];
    }

    public static function noKontrak(){
        $maxid = Barang::find()->orderBy(['id_barang' => SORT_DESC])->one();
        $id_cabang = Yii::$app->session['id_cabang'];
        $no_kontrak = date('Ym').$maxid->id_barang;
        $no_kontrak = $no_kontrak + 1;
        $no_kontrak = $no_kontrak.rand(0,9).$id_cabang;

        return $no_kontrak;
    }

    public static function noKontrakLangsung($tgl,$cabang){
        $maxid = Barang::find()->where(['tgl_masuk' => $tgl])->orderBy(['id_barang' => SORT_DESC])->one();
        if(empty($maxid->no_kontrak)){
            $no_kontrak = date_format(date_create($tgl),'Ym')."1001".$cabang;
        }
        else{
            $no_kontrak = $maxid->no_kontrak+1;
            $no_kontrak = $no_kontrak.$cabang;
        }
        return $no_kontrak;
    }

    public function getBarang(){
        return $this->hasOne(Profile::className(),['id_user'=>'id_nasabah']);
    }

    public function getNasabah(){
        return $this->hasOne(Profile::className(),['id_user'=>'id_nasabah']);
    }

    public function getCreatedBy(){
        return $this->hasOne(Profile::className(),['id_user'=>'created_by']);
    }

    public function myNasabah(){
        $id_nasabah = $this->id_nasabah;
        return Profile::find()->where(['id_user' => $id_nasabah])->one();
    }

    public function getJumlahPinjam(){
        return "Rp. ".number_format($this->nilai_pinjam,0,",",".");
    }

    public function getHargaPasar(){
        return "Rp. ".number_format($this->harga_pasar,0,",",".");
    }

    public function getPegawai(){
        return $this->hasOne(Profile::className(),['id_user'=>'created_by']);
    }

    public function myBunga(){
        $bunga = $this->nilai_pinjam * $this->bunga/100;
        return $bunga;
    }

    public function myBungaPerpanjang($sisa_hutang){
        $bunga = $sisa_hutang * $this->bunga/100;
        return $bunga;
    }

    public function myTerimabersih(){
        return $this->nilai_pinjam - ($this->myBunga() + $this->biaya_admin)-$this->biaya_simpan;
    }

    public function getHargaJual(){
        $dataJual = Jual::find()->where(['id_barang' => $this->id_barang])->one();
        $dataJual = "Rp. ".number_format($dataJual->harga_jual,0,",",".");
        return $dataJual;
    }

    public function getHargaLapor(){
        $dataJual = Jual::find()->where(['id_barang' => $this->id_barang])->one();
        $dataJual = "Rp. ".number_format($dataJual->harga_lapor,0,",",".");
        return $dataJual;
    }

    public function myTerjual(){
        $dataJual = Jual::find()->where(['id_barang' => $this->id_barang])->one();
        $sisa_kembali = $dataJual->harga_pasar - $dataJual->nilai_pinjam;
        $dataJual = ['harga_jual' => $dataJual->harga_jual,'sisa' => $sisa_kembali];

        return $dataJual;
    }

    public function getNoKontrak(){
        return "<a href=\"#\" class=\"btn-pop\" value=\"/barang/detail?id=".$this->id_barang."\">".$this->no_kontrak."</a>";
    }

    public function jumBarangAll(){
        return $this->find()->count();
    }

    public static function listCabang(){
        $datacabang = ArrayHelper::map(Cabang::find()->where(['!=', 'id_cabang', '10000'])->asArray()->all(),'id_cabang','nama_cabang');
        return $datacabang;
    }

    public function jumBarang($status){
        return $this->find()->where(['status' => $status])->count();
    }

    public function jumBarangPercabang($status,$id_cabang){
        return $this->find()->andWhere(['status' => $status, 'id_cabang' => $id_cabang])->count();
    }

    public function getCabang(){
        return $this->hasOne(Cabang::className(),['id_cabang'=>'id_cabang']);
    }

    public function getJenisBarang(){
        return $this->hasOne(JenisBarang::className(),['id_jenis_barang'=>'id_jenis']);
    }

    public function getStatusBarang(){
        return '<span class="label label-success">'.$this->status.'</span>';
    }

    public function getJual(){
        return $this->hasOne(Jual::className(),['id_barang'=>'id_barang']);
    }

    public function myDetail($sbg){
        return $this->find()->where(['no_kontrak' => $sbg])->one();
    }

    public function myCabang($id_cabang){
        return Cabang::find()->where(['id_cabang' => $id_cabang])->one();
    }

    public function myJenisBarang($sbg){
        return JenisBarang::find()->where(['id_jenis_barang' => $this->myDetail($sbg)])->one();
    }

    public function myDiskonBunga($sbg){
        $id_barang = $this->myDetail($sbg)->id_barang;
        $jangka = $this->myDetail($sbg)->jangka;
        $dataBayar = Bayar::find()->where(['id_barang' => $id_barang])->one();
        
        if(!empty($dataBayar)){
            $tgl_lunas = date_create($dataBayar->created_at);
            $tgl_masuk = date_create($this->myDetail($sbg)->tgl_masuk);
            $waktu_pinjam = date_diff($tgl_masuk,$tgl_lunas);
            $waktu_pinjam = $waktu_pinjam->format("%a");

            if($waktu_pinjam <= 15){
                if($jangka > 15){
                    $diskon_bunga = 0.05*$this->myDetail($sbg)->nilai_pinjam;
                }
                else{
                    $diskon_bunga = 0;
                }
            }
            else{
                $diskon_bunga = 0;
            }

            return $diskon_bunga;
        }
    }

    public function myNasabah2($id_nasabah){
        return Profile::find()->where(['id_user' => $id_nasabah])->one();
    }

    public function myDataLog($sbg){
        $id_barang = $this->myDetail($sbg)->id_barang;
        return LogStatus::find()->where(['id_barang' => $id_barang])->all();
    }

    public function getDenda(){
        $tgl_now = date_create(date('Y-m-d'));
        $jatuh_tempo = date_create($this->jatuh_tempo);
            $telat = date_diff($tgl_now,$jatuh_tempo);
            $telat = $telat->format("%a");
            $denda = "Rp.". number_format($telat * 5000,0,",",".");
        
        return $denda;
    }

    public function listTagihNow2(){
        $tgl_now = date('Y-m-d');
        $besok = date('Y-m-d', strtotime($tgl_now .' +1 day'));
        return $this->find()->where(['jatuh_tempo' => $besok, 'status' => 'Aktif'])->all();
    }

    public function listPasif($bunga){
        if($bunga == 5 or $bunga == 1.2){
            $lelang = '-6 day';
        }
        else{
            $lelang = '-12 day';
            //$lelang = '-6 day'; //disamakan rencananya
        }
        $tgl_now = date('Y-m-d');
        $tglLelang = date('Y-m-d', strtotime($tgl_now .$lelang));
        return $this->find()->where(['jatuh_tempo' => $tglLelang, 'bunga' => $bunga])->andWhere(['or', ['status' => 'JatuhTempo'], ['status' => 'Pasif']])->all();
    }

    public function listLelang($bunga){
        if($bunga == 5 or $bunga == 1.2){
            $lelang = '-9 day';
        }
        else{
            $lelang = '-15 day';
            //$lelang = '-9 day'; //disamakan rencananya
        }
        $tgl_now = date('Y-m-d');
        $tglLelang = date('Y-m-d', strtotime($tgl_now .$lelang));
        return $this->find()->where(['jatuh_tempo' => $tglLelang, 'bunga' => $bunga])->andWhere(['or', ['status' => 'JatuhTempo'], ['status' => 'Pasif']])->all();
    }

    public function listTagihNow($id_cabang,$jangka){
        $tgl_now = date('Y-m-d');
        $besok = date('Y-m-d', strtotime($tgl_now .' +1 day'));
        return $this->find()->where(['id_cabang'=>$id_cabang, 'jangka' => $jangka,'jatuh_tempo' => $besok])->andWhere(['or', ['status'=> 'Aktif'],['status' => 'Lelang']])->orderBy(['jatuh_tempo' => SORT_DESC])->all();
    }

    public function listTagihAll($id_cabang,$jangka){
        $tgl = date('Y-m-d');
        return $this->find()->where(['id_cabang' => $id_cabang, 'jangka' => $jangka])->andWhere(['or', ['status'=> 'JatuhTempo'],['status' => 'Lelang'],['status' => 'Pasif'], ['status' => 'Aktif']])->andWhere(['<=', 'jatuh_tempo', $tgl])->orderBy(['jatuh_tempo' => SORT_ASC])->all();
    }

    public function pencairanNow(){
        $tgl_now = date('Y-m-d');
        return $this->find()->where(['tgl_masuk' => $tgl_now])->sum('nilai_pinjam');
    }

    public function biayaAdminNow(){
        $tgl_now = date('Y-m-d');
        return $this->find()->where(['tgl_masuk' => $tgl_now])->sum('biaya_admin');
    }

    public function biayaSimpanNow(){
        $tgl_now = date('Y-m-d');
        return $this->find()->where(['tgl_masuk' => $tgl_now])->sum('biaya_admin');
    }

    public function outstandingUang($id_cabang,$status){
        $data = $this->find()->where(['id_cabang' => $id_cabang,'status' => $status])->sum('nilai_pinjam');
        return $data;
    }

    public function outstandingUangAll($id_cabang){
        $uang_aktif = $this->outstandingUang($id_cabang,'Aktif');
        $uang_jatuh_tempo = $this->outstandingUang($id_cabang,'JatuhTempo');
        $uang_pasif = $this->outstandingUang($id_cabang,'Pasif');
        $uang_lelang = $this->outstandingUang($id_cabang,'Lelang');
        $uang_belum_terjual = $this->outstandingUang($id_cabang,'Belum Terjual');
        $total = $uang_aktif+$uang_jatuh_tempo+$uang_pasif+$uang_lelang+$uang_belum_terjual;

        return $total;
    }

    public static function listPegawai(){
        $datapegawai = ArrayHelper::map(Profile::find()->where(['or',['akses' => 'defaultStaff'], ['akses' => 'Penafsir'], ['akses' => 'gudang']])->asArray()->all(),'id_user','nama');
        return $datapegawai;
    }

    public function getSisapinjam(){
        $id_barang = $this->id_barang;
        $dataLogStatus = LogStatus::find()->where(['id_barang' => $id_barang])->orderBy(['id_status' => SORT_DESC])->one();

        if($dataLogStatus->status == "Aktif" || $dataLogStatus->status == "JatuhTempo" || $dataLogStatus->status == "Lelang"){
            $sisabayar = $this->nilai_pinjam;
        }
        else{
            $sisabayar = 0;
        }

        return $sisabayar;
    }
    
    public function getTgllunas(){
        $id_barang = $this->id_barang;
        $dataLogStatus = LogStatus::find()->where(['id_barang' => $id_barang])->orderBy(['id_status' => SORT_DESC])->one();

        if($dataLogStatus->status == "Lunas"){
            $tgl_lunas = $dataLogStatus->created_at;
        }
        else{
            $tgl_lunas = "";
        }
        return $tgl_lunas;
    }

    public function jatuhTempoNew(){
        $jangka = ' +'.$this->jangka.' days';
        $tempo = date('Y-m-d', strtotime($this->jatuh_tempo. $jangka));
        return $tempo;
    }

    public function tglJual(){
        if($this->jangka < 30){
            $tglJual = '+11 day';
        }
        else{
            $tglJual = '+17 day';
            //$tglJual = '+11 day'; //rencananya disamakan
        }

        return $tglJual;
    }

    public function getSisapinjam2(){

        if($this->status == "Lunas"){
            $sisa = 0;
        }
        else{
            $cicil = $this->getLogFinances()->where(['status' => 'Angsuran'])->sum('jumlah_uang');

            $sisa = $this->nilai_pinjam - $cicil;
        }
       
        return $sisa;
    }

    public function getForjual(){
        $jual = date("Y-m-d", strtotime($this->jatuh_tempo .$this->tglJual()));
        return $jual;
    }

    public function logBunga($bunga){
        if($bunga == $this->bunga){
            return LogFinance::find()->where(['id_barang' => $this->id_barang, 'status' => 'Bunga'])->sum('jumlah_uang');
        }
        else{
            return 0;
        }
    }

    public function pinjamBersih(){
        $nilai_pinjam = $this->nilai_pinjam;
        $bunga = $this->bunga;
        $bersih = $nilai_pinjam - ($nilai_pinjam*$bunga/100) - $this->biaya_admin;
        return $bersih;
    }

    public function getDevice(){
        return $this->hasOne(Device::className(),['cabang_id'=>'id_cabang']);
    }

    public function nilaiBunga(){
        $bunga = $this->bunga;
        $pinjaman = $this->nilai_pinjam;
        $nilaiBunga = $bunga*$pinjaman/100;
        return $nilaiBunga;
    }

    public function nilaiBungaPerpanjang(){
        $bunga = $this->bunga;
        $pinjaman = $this->getSisapinjam2();
        $nilaiBunga = $bunga*$pinjaman/100;
        return $nilaiBunga;
    }

    public function getJurnalJuals()
    {
        return $this->hasMany(LogJurnalPenjualan::className(), ['barang_id' => 'id_barang']);
    }

    public function getLogFinances()
    {
        return $this->hasMany(LogFinance::className(), ['id_barang' => 'id_barang']);
    }

    public function getCicils()
    {
        return $this->hasMany(Cicil::className(), ['id_barang' => 'id_barang']);
    }

    public function getNotes()
    {
        return $this->hasMany(NoteBarang::className(), ['barang_id' => 'id_barang']);
    }

    public function getLokasi(){
        return $this->hasOne(LokasiSimpan::className(),['id'=>'lokasi_simpan_id']);

    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_barang' => 'Id Barang',
            'no_kontrak' => 'No Kontrak',
            'id_nasabah' => 'Id Nasabah',
            'nama_barang' => 'Nama Barang',
            'tipe' => 'Tipe',
            'serial' => 'Serial',
            'id_jenis' => 'Id Jenis',
            'tgl_masuk' => 'Tgl Masuk',
            'jatuh_tempo' => 'Jatuh Tempo',
            'spek' => 'Spek',
            'kelengkapan' => 'Kelengkapan',
            'kondisi' => 'Kondisi',
            'harga_pasar' => 'Harga Pasar',
            'harga_taksir' => 'Harga Taksir',
            'nilai_pinjam' => 'Nilai Pinjam',
            'bunga' => 'Bunga',
            'biaya_admin' => 'Biaya Admin',
            'biaya_simpan' => 'Biaya Simpan',
            'jangka' => 'Jangka',
            'created_by' => 'Created By',
            'no_fak' => 'No. FAK'
        ];
    }
}
