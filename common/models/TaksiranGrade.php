<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "taksiran_grade".
 *
 * @property int $id
 * @property int $taksiran_id
 * @property int $grade_id
 * @property int $lengkap
 * @property int $batangan
 * @property int $kotak
 * @property int $charger
 * @property string $created_at
 * @property int $created_by
 *
 * @property GradeBarang $grade
 * @property TaksiranHarga $taksiran
 */
class TaksiranGrade extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'taksiran_grade';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['taksiran_id', 'grade_id', 'lengkap', 'batangan', 'kotak', 'charger', 'created_at', 'created_by'], 'required'],
            [['taksiran_id', 'grade_id', 'lengkap', 'batangan', 'kotak', 'charger', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['grade_id'], 'exist', 'skipOnError' => true, 'targetClass' => GradeBarang::className(), 'targetAttribute' => ['grade_id' => 'id']],
            [['taksiran_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaksiranHarga::className(), 'targetAttribute' => ['taksiran_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'taksiran_id' => 'Taksiran ID',
            'grade_id' => 'Grade ID',
            'lengkap' => 'Lengkap',
            'batangan' => 'Batangan',
            'kotak' => 'Kotak',
            'charger' => 'Charger',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[Grade]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGrade()
    {
        return $this->hasOne(GradeBarang::className(), ['id' => 'grade_id']);
    }

    /**
     * Gets query for [[Taksiran]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTaksiran()
    {
        return $this->hasOne(TaksiranHarga::className(), ['id' => 'taksiran_id']);
    }


    public function generateGrade($taksiran_id){
        $dataGrade = GradeBarang::find()->all();
        foreach($dataGrade as $grade){
            $model = new TaksiranGrade();
            $model->taksiran_id = $taksiran_id;
            $model->grade_id = $grade->id;
            $model->lengkap = 0;
            $model->batangan = 0;
            $model->kotak = 0;
            $model->charger = 0;
            $model->created_at = date('Y-m-d');
            $model->created_by = Yii::$app->user->id;
            $model->save();
        }
    }

}
