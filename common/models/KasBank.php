<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "kas_bank".
 *
 * @property int $id_kas_bank
 * @property int $akun_kode
 * @property string $nama_kas_bank
 * @property string $created_at
 * @property int $created_by
 *
 * @property Akun $akunKode
 */
class KasBank extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kas_bank';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['akun_kode', 'nama_kas_bank', 'created_at', 'created_by'], 'required'],
            [['akun_kode', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['nama_kas_bank'], 'string', 'max' => 225],
            [['akun_kode'], 'exist', 'skipOnError' => true, 'targetClass' => Akun::className(), 'targetAttribute' => ['akun_kode' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kas_bank' => 'Id Kas Bank',
            'akun_kode' => 'Akun Kode',
            'nama_kas_bank' => 'Nama Kas Bank',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[AkunKode]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAkunKode()
    {
        return $this->hasOne(Akun::className(), ['id' => 'akun_kode']);
    }

    //MY FUNCTION START HERE

    public static function listKas(){
        $kas = KasBank::find()->where(['or', ['jenis_pilihan' => 'SEMUA'], ['jenis_pilihan' => 'OUTLET']])->all();
        $dataKas = ArrayHelper::map($kas,'id_kas_bank',function($kas){
            return "(".$kas->akun_kode.") ".$kas->nama_kas_bank;
        });

        return $dataKas;
    }

    public static function listKasJual(){
        $kas = KasBank::find()->where(['or', ['jenis_pilihan' => 'SEMUA'], ['jenis_pilihan' => 'KANTOR']])->all();
        $dataKas = ArrayHelper::map($kas,'id_kas_bank',function($kas){
            return "(".$kas->akun_kode.") ".$kas->nama_kas_bank;
        });

        return $dataKas;
    }

    //MY FUNCTION END HERE

}
