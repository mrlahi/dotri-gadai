<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "log_modal".
 *
 * @property int $id_log_modal
 * @property int $jumlah_masuk
 * @property int $id_cabang
 * @property string $created_at
 * @property string $keterangan
 */
class LogModal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_modal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jumlah_masuk', 'id_cabang', 'created_at', 'keterangan'], 'required'],
            [['jumlah_masuk', 'id_cabang'], 'integer'],
            [['created_at'], 'safe'],
            [['keterangan'], 'string'],
        ];
    }

    public function getJumlah(){
        return "Rp. ".number_format($this->jumlah_masuk,0,",",".");
    }

    public function modalNow(){
        $tgl_now = date('Y-m-d');
        return $this->find()->andWhere(['created_at' => $tgl_now,'id_cabang' => Yii::$app->session['id_cabang']])->sum('jumlah_masuk');
    }

    public function getCabang(){
        return $this->hasOne(Cabang::className(),['id_cabang'=>'id_cabang']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_log_modal' => 'Id Log Modal',
            'jumlah_masuk' => 'Jumlah Masuk',
            'id_cabang' => 'Id Cabang',
            'created_at' => 'Created At',
            'keterangan' => 'Keterangan',
        ];
    }
}
