<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "check_in_out".
 *
 * @property int $id
 * @property string $absen_id
 * @property string $tgl_absen
 * @property string $jam_absen
 * @property string $cabang_id
 * @property string $created_at
 */
class CheckInOut extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'check_in_out';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['absen_id', 'tgl_absen', 'jam_absen', 'cabang_id', 'created_at'], 'required'],
            [['tgl_absen', 'jam_absen', 'created_at'], 'safe'],
            [['absen_id'], 'string', 'max' => 255],
            [['cabang_id'], 'string', 'max' => 50],
            [['absen_id', 'tgl_absen', 'jam_absen'], 'unique', 'targetAttribute' => ['absen_id', 'tgl_absen', 'jam_absen']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'absen_id' => 'Absen ID',
            'tgl_absen' => 'Tgl Absen',
            'jam_absen' => 'Jam Absen',
            'cabang_id' => 'Cabang ID',
            'created_at' => 'Created At',
        ];
    }

    public function getPegawai(){
        return $this->hasOne(Profile::className(),['absensi_id'=>'absen_id']);
    }

    public static function calculateFaceDistance($descriptor1, $descriptor2)
    {
        // Implementasikan algoritma perhitungan jarak antara dua descriptor wajah
        $distance = 0;
        for ($i = 0; $i < count($descriptor1); $i++) {
            $distance += pow($descriptor1[$i] - $descriptor2[$i], 2);
        }
        return sqrt($distance);
    }

    public static function compressImage($source, $destination, $maxSize)
    {
        $quality = 90; // Awalnya kualitas tinggi
        do {
            // Buat versi terkompresi
            $image = imagecreatefrompng($source);
            ob_start();
            imagepng($image, null, round($quality / 10)); // Kompresi untuk PNG
            $compressedData = ob_get_clean();
            imagedestroy($image);

            // Cek ukuran
            if (strlen($compressedData) <= $maxSize || $quality <= 10) {
                file_put_contents($destination, $compressedData);
                return true;
            }

            // Kurangi kualitas
            $quality -= 10;
        } while ($quality > 0);

        return false; // Jika gagal mencapai ukuran
    }

}
