<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "akun".
 *
 * @property int $id
 * @property string $nama_akun
 * @property int $parent_kode
 * @property int $created_by
 * @property string $created_at
 * @property string $saldo_normal
 *
 * @property Akun $parentKode
 * @property Akun[] $akuns
 * @property LogJurnal[] $logJurnals
 */
class Akun extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'akun';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','nama_akun', 'parent_kode', 'created_by', 'created_at'], 'required'],
            [['id','parent_kode', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['saldo_normal', 'status_laporan'], 'string'],
            [['nama_akun'], 'string', 'max' => 100],
            [['parent_kode'], 'exist', 'skipOnError' => true, 'targetClass' => Akun::className(), 'targetAttribute' => ['parent_kode' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_akun' => 'Nama Akun',
            'parent_kode' => 'Parent Kode',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'saldo_normal' => 'Saldo Normal',
        ];
    }

    /**
     * Gets query for [[ParentKode]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParentKode()
    {
        return $this->hasOne(Akun::className(), ['id' => 'parent_kode']);
    }

    /**
     * Gets query for [[Akuns]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAkuns()
    {
        return $this->hasMany(Akun::className(), ['parent_kode' => 'id']);
    }

    /**
     * Gets query for [[LogJurnals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLogJurnals()
    {
        return $this->hasMany(LogJurnal::className(), ['akun_kode' => 'id']);
    }

    //MY FUNCTION START HERE

    public function listParent(){
        $akun = Akun::find()->all();
        $dataAkun = ArrayHelper::map($akun,'id',function($akun){
            return "(".$akun->id.") ".$akun->nama_akun;
        });

        return $dataAkun;
    }

    public static function listAkun(){
        $akun = Akun::find()->all();
        $dataAkun = ArrayHelper::map($akun,'id',function($akun){
            return "(".$akun->id.") ".$akun->nama_akun;
        });

        return $dataAkun;
    }

    public static function listAkunAktiva(){
        $akun = Akun::find()->where(['status_laporan' => 'AKTIVA'])->all();
        $dataAkun = ArrayHelper::map($akun,'id',function($akun){
            return "(".$akun->id.") ".$akun->nama_akun;
        });

        return $dataAkun;
    }

    public function saldoJurnal($tgl,$cabang_id){
        $kode_akun = $this->id;
        if($cabang_id == 1000000){
            $debet = LogJurnal::find()->where(['akun_kode' => $kode_akun])->andWhere(['<=', 'created_at', $tgl])->sum('debet');
            $kredit = LogJurnal::find()->where(['akun_kode' => $kode_akun])->andWhere(['<=', 'created_at', $tgl])->sum('kredit');
        }else{
            $debet = LogJurnal::find()->where(['akun_kode' => $kode_akun, 'cabang_id' => $cabang_id])->andWhere(['<=', 'created_at', $tgl])->sum('debet');
            $kredit = LogJurnal::find()->where(['akun_kode' => $kode_akun, 'cabang_id' => $cabang_id])->andWhere(['<=', 'created_at', $tgl])->sum('kredit');
        }
       

        if($this->saldo_normal == "DEBET"){
            return $debet - $kredit;
        }

        if($this->saldo_normal == "KREDIT"){
            return $kredit - $debet;
        }

    }

    public function saldoJurnalRange($date1,$date2,$cabang_id){
        $kode_akun = $this->id;
        if($cabang_id == 1000000){
            $debet = LogJurnal::find()->where(['akun_kode' => $kode_akun])->andWhere(['between', 'created_at', $date1, $date2])->sum('debet');
            $kredit = LogJurnal::find()->where(['akun_kode' => $kode_akun])->andWhere(['between', 'created_at', $date1, $date2])->sum('kredit');
        }
        else{
            $debet = LogJurnal::find()->where(['akun_kode' => $kode_akun, 'cabang_id' => $cabang_id])->andWhere(['between', 'created_at', $date1, $date2])->sum('debet');
            $kredit = LogJurnal::find()->where(['akun_kode' => $kode_akun, 'cabang_id' => $cabang_id])->andWhere(['between', 'created_at', $date1, $date2])->sum('kredit');
        }
        

        if($this->saldo_normal == "DEBET"){
            return $debet - $kredit;
        }

        if($this->saldo_normal == "KREDIT"){
            return $kredit - $debet;
        }

    }

    public function saldoArusKasRange($date1,$date2,$cabang_id,$posisi){
        $kode_akun = $this->id;
        if($cabang_id == 1000000){
            $saldo = LogJurnal::find()->where(['akun_kode' => $kode_akun])->andWhere(['between', 'created_at', $date1, $date2])->sum($posisi);
            if(!$saldo){
                $saldo = 0;
            }
        }
        else{
            $saldo = LogJurnal::find()->where(['akun_kode' => $kode_akun, 'cabang_id' => $cabang_id])->andWhere(['between', 'created_at', $date1, $date2])->sum($posisi);
            if(!$saldo){
                $saldo = 0;
            }
        }
        return $saldo;
    }

    public function kodeAkhir(){
        $parentKode = $this->parent_kode;
        $kodeAkhir = Akun::find()->where(['parent_kode' => $parentKode])->orderBy(['id' => SORT_DESC])->one();

        return $kodeAkhir->id;

    }

    //MY FUNCTION END HERE

}
