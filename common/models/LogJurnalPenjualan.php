<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "log_jurnal_penjualan".
 *
 * @property int $id
 * @property int $barang_id
 * @property int $akun_id
 * @property int $jumlah_uang
 * @property string $posisi_keuangan
 * @property string $created_at
 * @property int $created_by
 *
 * @property Akun $akun
 * @property Barang $barang
 */
class LogJurnalPenjualan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_jurnal_penjualan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['barang_id', 'akun_id', 'jumlah_uang', 'posisi_keuangan', 'created_at', 'created_by', 'deskripsi'], 'required'],
            [['barang_id', 'akun_id', 'jumlah_uang', 'created_by'], 'integer'],
            [['posisi_keuangan', 'deskripsi'], 'string'],
            [['created_at'], 'safe'],
            [['akun_id'], 'exist', 'skipOnError' => true, 'targetClass' => Akun::className(), 'targetAttribute' => ['akun_id' => 'id']],
            [['barang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Barang::className(), 'targetAttribute' => ['barang_id' => 'id_barang']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'barang_id' => 'Barang ID',
            'akun_id' => 'Akun ID',
            'jumlah_uang' => 'Jumlah Uang',
            'posisi_keuangan' => 'Posisi Keuangan',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[Akun]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAkun()
    {
        return $this->hasOne(Akun::className(), ['id' => 'akun_id']);
    }

    /**
     * Gets query for [[Barang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBarang()
    {
        return $this->hasOne(Barang::className(), ['id_barang' => 'barang_id']);
    }

    //MY FUNCTIONS START HERE

    public function listJurnal($kas_bank_id){

        $dataKas = KasBank::findOne($kas_bank_id);

        $jurnal = [
            [
                'akun_id' => $dataKas->akun_kode,
                'posisi' => 'DEBET',
                'deskripsi' => 'PENJUALAN BARANG'
            ],
            [
                'akun_id' => '1113',
                'posisi' => 'DEBET',
                'deskripsi' => 'PENJUALAN BARANG'
            ],
            [
                'akun_id' => '5015',
                'posisi' => 'DEBET',
                'deskripsi' => 'KERUGIAN PENJUALAN BARANG'
            ],
            [
                'akun_id' => '1106',
                'posisi' => 'KREDIT',
                'deskripsi' => 'PINJAMAN YANG DIBERIKAN'
            ],
            [
                'akun_id' => '4017',
                'posisi' => 'KREDIT',
                'deskripsi' => 'BUNGA PENJUALAN BARANG'
            ],
            [
                'akun_id' => '4018',
                'posisi' => 'KREDIT',
                'deskripsi' => 'DENDA PENJUALAN BARANG'
            ],
            [
                'akun_id' => '4019',
                'posisi' => 'KREDIT',
                'deskripsi' => 'LABA PENJUALAN BARANG'
            ],
            [
                'akun_id' => '2109',
                'posisi' => 'KREDIT',
                'deskripsi' => 'KELEBIHAN PENJUALAN BARANG'
            ],
        ];

        return $jurnal;
    }

    public function generateLog($barang_id,$kas_bank_id){
        $list = $this->listJurnal($kas_bank_id);

        foreach($list as $jurnal){
            $mLog = new LogJurnalPenjualan();

            $mLog->barang_id = $barang_id;
            $mLog->akun_id = $jurnal['akun_id'];
            $mLog->jumlah_uang = 0;
            $mLog->posisi_keuangan = $jurnal['posisi'];
            $mLog->deskripsi = $jurnal['deskripsi'];
            $mLog->created_at = date('Y-m-d');
            $mLog->created_by = Yii::$app->user->id;
            $mLog->save();
        }
    }

    //MY FUNCTIONS END HERE

}
