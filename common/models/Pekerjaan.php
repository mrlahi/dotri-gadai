<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "pekerjaan".
 *
 * @property int|null $id_pekerjaan
 * @property string $nama_pekerjaan
 * @property string $keterangan
 * @property string $created_at
 * @property int $created_by
 */
class Pekerjaan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pekerjaan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pekerjaan', 'created_by'], 'integer'],
            [['nama_pekerjaan', 'created_at', 'created_by'], 'required'],
            [['keterangan'], 'string'],
            [['created_at'], 'safe'],
            [['nama_pekerjaan'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pekerjaan' => 'Id Pekerjaan',
            'nama_pekerjaan' => 'Nama Pekerjaan',
            'keterangan' => 'Keterangan',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    //MY FUNCTIONS START HERE

    public static function listPekerjaan(){
       return ArrayHelper::map(Pekerjaan::find()->asArray()->all(),'id_pekerjaan','nama_pekerjaan');

    }

    //MY FUNCTIONS END HERE

}
