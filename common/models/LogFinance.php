<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "log_finance".
 *
 * @property int $id_log_finance
 * @property int $jumlah_uang
 * @property string $status
 * @property string $created_at
 * @property int $created_by
 * @property int $id_cabang
 * @property int $id_barang
 */
class LogFinance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_finance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jumlah_uang', 'status', 'created_at', 'created_by', 'id_cabang', 'id_barang'], 'required'],
            [['created_by', 'id_cabang', 'id_barang', 'jumlah_uang'], 'integer'],
            [['created_at'], 'safe'],
            [['status'], 'string', 'max' => 225],
        ];
    }

    public function getCabang(){
        return $this->hasOne(Cabang::className(),['id_cabang'=>'id_cabang']);
    }

    public function pinjamanNow($id_cabang){
        $tgl_now = date('Y-m-d');
        return $this->find()->andWhere(['status' => 'Pinjaman','id_cabang' => $id_cabang,'created_at' => $tgl_now])->sum('jumlah_uang');
    }

    public function modalNow($id_cabang){
        $tgl_now = date('Y-m-d');
        return $this->find()->andWhere(['status' => 'Modal','id_cabang' => $id_cabang,'created_at' => $tgl_now])->sum('jumlah_uang');
    }

    public function pembayaranNow($id_cabang){
        $tgl_now = date('Y-m-d');
        return $this->find()->andWhere(['status' => 'Angsuran','id_cabang' => $id_cabang,'created_at' => $tgl_now])->sum('jumlah_uang');
    }

    public function pelunasanNow($id_cabang){
        $tgl_now = date('Y-m-d');
        return $this->find()->andWhere(['status' => 'Pelunasan','id_cabang' => $id_cabang,'created_at' => $tgl_now])->sum('jumlah_uang');
    }

    public function bungaPerpanjangNow($id_cabang){
        $tgl_now = date('Y-m-d');
        return $this->find()->andWhere(['status' => 'BungaPerpanjang','id_cabang' => $id_cabang,'created_at' => $tgl_now])->sum('jumlah_uang');
    }

    public function bungaPerpanjangNow2($id_cabang,$bunga){
        $tgl_now = date('Y-m-d');
        return $this->find()->joinWith('barang')->where(['log_finance.status' => 'BungaPerpanjang', 'log_finance.id_cabang' => $id_cabang,'created_at' => $tgl_now])->andWhere(['like', 'barang.bunga',$bunga])->sum('jumlah_uang');
    }

    public function biayaAdminNow($id_cabang){
        $tgl_now = date('Y-m-d');
        return $this->find()->andWhere(['status' => 'Admin'])->andWhere(['id_cabang' => $id_cabang,'created_at' => $tgl_now])->sum('jumlah_uang');
    }

    public function biayaSimpanNow($id_cabang){
        $tgl_now = date('Y-m-d');
        return $this->find()->andWhere(['status' => 'Simpan'])->andWhere(['id_cabang' => $id_cabang,'created_at' => $tgl_now])->sum('jumlah_uang');
    }

    public function biayaBungaNow($id_cabang){
        $tgl_now = date('Y-m-d');
        return $this->find()->andWhere(['status' => 'Bunga'])->andWhere(['id_cabang' => $id_cabang,'created_at' => $tgl_now])->sum('jumlah_uang');
    }

    public function biayaBungaNow2($id_cabang,$bunga){
        $tgl_now = date('Y-m-d');
        return $this->find()->joinWith('barang')->where(['log_finance.status' => 'Bunga', 'log_finance.id_cabang' => $id_cabang,'created_at' => $tgl_now])->andWhere(['like', 'barang.bunga',$bunga])->sum('jumlah_uang');
    }

    public function dendaPelunasanNow($id_cabang){
        $tgl_now = date('Y-m-d');
        return $this->find()->andWhere(['status' => 'DendaPelunasan'])->andWhere(['id_cabang' => $id_cabang,'created_at' => $tgl_now])->sum('jumlah_uang');
    }
    
    public function bungaPelunasanNow($id_cabang){
        $tgl_now = date('Y-m-d');
        return $this->find()->andWhere(['status' => 'BungaPelunasanPasif'])->andWhere(['id_cabang' => $id_cabang,'created_at' => $tgl_now])->sum('jumlah_uang');
    }

    public function dendaPerpanjangNow($id_cabang){
        $tgl_now = date('Y-m-d');
        return $this->find()->andWhere(['status' => 'DendaPerpanjang'])->andWhere(['id_cabang' => $id_cabang,'created_at' => $tgl_now])->sum('jumlah_uang');
    }

    public function bebanOperasionalNow($id_cabang){
        $tgl_now = date('Y-m-d');
        return $this->find()->andWhere(['status' => 'Operasional'])->andWhere(['id_cabang' => $id_cabang,'created_at' => $tgl_now])->sum('jumlah_uang');
    }

    public function kembaliBungaNow($id_cabang,$status){
        $tgl_now = date('Y-m-d');
        return $this->find()->andWhere(['status' => $status])->andWhere(['id_cabang' => $id_cabang,'created_at' => $tgl_now])->sum('jumlah_uang');
    }

    public function lelangNow($id_cabang){
        $tgl_now = date('Y-m-d');
        return $this->find()->andWhere(['status' => 'Lelang','id_cabang' => $id_cabang,'created_at' => $tgl_now])->sum('jumlah_uang');
    }

    public function pengeluaranNow($id_cabang){
        $tgl_now = date('Y-m-d');
        return $this->find()->andWhere(['status' => 'Pengeluaran'])->andWhere(['id_cabang' => $id_cabang,'created_at' => $tgl_now])->sum('jumlah_uang');
    }

    public function viaTransferNow($id_cabang){
        $tgl_now = date('Y-m-d');
        return $this->find()->andWhere(['id_cabang' => $id_cabang,'created_at' => $tgl_now,'type_pembayaran' => 'Transfer'])->andWhere(['<>','status', 'KembaliBunga5Persen'])->andWhere(['<>','status', 'KembaliBunga1.2Persen'])->sum('jumlah_uang');
    }

    public function kasNow($id_cabang){
        return $this->modalNow($id_cabang)+
        $this->pelunasanNow($id_cabang)+
        $this->pembayaranNow($id_cabang)+
        $this->biayaBungaNow($id_cabang)+
        $this->bungaPerpanjangNow($id_cabang)+
        $this->dendaPelunasanNow($id_cabang)+
        $this->bungaPelunasanNow($id_cabang)+
        $this->dendaPerpanjangNow($id_cabang)+
        $this->biayaAdminNow($id_cabang)+
        $this->biayaSimpanNow($id_cabang)+
        $this->lelangNow($id_cabang)-
        $this->bebanOperasionalNow($id_cabang)-
        $this->pinjamanNow($id_cabang)-
        $this->pengeluaranNow($id_cabang)-
        $this->viaTransferNow($id_cabang);
    }

    public function myRiwayatFinance($id_barang){
        return $this->find()->andWhere(['id_barang' => $id_barang])->andWhere(['<>','status', 'Pinjaman'])->all();
    }

    public function myCabang($id_cabang){
        return Cabang::findOne($id_cabang);
    }

    public function inLogFinance($id_barang,$jumlah_uang,$status,$kas_id){
        if($kas_id == 1){
            $this->type_pembayaran = "Cash";
        }
        else{
            $this->type_pembayaran = "Transfer";
        }
        $this->jumlah_uang = $jumlah_uang;
        $this->status = $status;
        $this->created_at = date('Y-m-d');
        $this->created_by = Yii::$app->user->identity->id;
        $this->id_cabang = Yii::$app->session['id_cabang'];
        $this->id_barang = $id_barang;
        $this->kas_bank_id = $kas_id;
        $this->save(false);
    }

    public static function listCabang(){
        $datacabang = ArrayHelper::map(Cabang::find()->where(['!=', 'id_cabang', '10000'])->asArray()->all(),'id_cabang','nama_cabang');
        $datacabang['1000000'] = 'Semua Cabang';
        return $datacabang;
    }

    public function dailyGadai($date1,$date2,$id_cabang){
        
        //return $this->find()->where(['or',['status' => 'Pinjaman'], ['status' => 'Bunga']])->andWhere(['between','created_at', $date1,$date2])->andWhere(['id_cabang' => $id_cabang])->groupBy('id_barang')->all();

        return LogFinance::find()->where(['status' => 'Pinjaman', 'id_cabang' => $id_cabang])->andWhere(['between','created_at', $date1,$date2])->all();
        
    }

    public function dailyPerpanjang($date1,$date2,$id_cabang){
        return $this->find()->where(['or',['status' => 'BungaPerpanjang'], ['status' => 'DendaPerpanjang']])->andWhere(['between','created_at', $date1,$date2])->andWhere(['id_cabang' => $id_cabang])->groupBy('id_barang')->all();
    }

    public function dailyMenebus($date1,$date2,$id_cabang){
        //return $this->find()->where(['or',['status' => 'Pelunasan'], ['status' => 'DendaPelunasan'], ['status' => 'KembaliBunga5Persen']])->andWhere(['between','created_at', $date1,$date2])->andWhere(['id_cabang' => $id_cabang])->groupBy('id_barang')->all();

        return LogFinance::find()->where(['status' => 'Pelunasan', 'id_cabang' => $id_cabang])->andWhere(['between','created_at', $date1,$date2])->all();

    }

    public function dailyAngsuran($date1,$date2,$id_cabang){
        return $this->find()->where(['status' => 'Angsuran'])->andWhere(['between','created_at', $date1,$date2])->andWhere(['id_cabang' => $id_cabang])->all();
    }

    public function dailyLelang($date1,$date2,$id_cabang){
        return $this->find()->where(['status' => 'Lelang'])->andWhere(['between','created_at', $date1,$date2])->andWhere(['id_cabang' => $id_cabang])->groupBy('id_barang')->orderBy(['created_at' => SORT_ASC])->all();
    }

    // public function dailyEach($id_barang,$status,$date1,$date2,$id_cabang){
    //     $data = LogFinance::find()->where(['between','created_at',$date1,$date2])->andWhere(['id_cabang' => $id_cabang,'id_barang' => $id_barang,'status' => $status])->one();

    //     if(empty($data)){
    //         return 0;
    //     }
    //     else{
    //         return $data->jumlah_uang;
    //     }
    // }

    public function dailyEach($id_barang, $status, $date1, $date2, $id_cabang)
    {
        // Generate a unique cache key
        $cacheKey = "dailyEach_{$id_barang}_{$status}_{$date1}_{$date2}_{$id_cabang}";

        // Attempt to get the data from cache
        $data = Yii::$app->cache->get($cacheKey);

        if ($data === false) {
            // Data is not cached, fetch from the database
            $data = LogFinance::find()
                ->where(['between', 'created_at', $date1, $date2])
                ->andWhere(['id_cabang' => $id_cabang, 'id_barang' => $id_barang, 'status' => $status])
                ->one();

            // Cache the result
            Yii::$app->cache->set($cacheKey, $data, 3600); // Cache for 1 hour
        }

        if (empty($data)) {
            return 0;
        } else {
            return $data->jumlah_uang;
        }
    }


    public function dailyModal($date1,$date2,$id_cabang){
        return $this->find()->andWhere(['status' => 'Modal','id_cabang' => $id_cabang])->andWhere(['between','created_at', $date1,$date2])->sum('jumlah_uang');
    }

    public function dailyOperasional($date1,$date2,$id_cabang){
        return BebanOperasional::find()->andWhere(['id_cabang' => $id_cabang])->andWhere(['type_pencairan' => 'Cash'])->andWhere(['between','created_at',$date1,$date2])->sum('jumlah_pengeluaran');
    }

    public function dailyKembaliBunga($date1,$date2,$id_cabang){
        return $this->find()->andWhere(['or',['status' => 'KembaliBunga1.2Persen'],['status' => 'KembaliBunga5Persen']])->andWhere(['id_cabang' => $id_cabang])->andWhere(['between', 'created_at',$date1,$date2])->sum('jumlah_uang');
    }

    public function myBarang($id_barang){
        return Barang::find()->where(['id_barang' => $id_barang])->one();
    }

    public function myStatusLelang($id_barang){
        $dataJual = Jual::find()->where(['id_barang' => $id_barang])->one();
        if($dataJual->status == "Rugi"){
            return "<span class=\"label label-warning\">".$dataJual->status."</span>";
        }
        else{
            return "<span class=\"label label-info\">".$dataJual->status."</span>";
        }
        
    }

    public function dailyPengeluaran($date1,$date2,$id_cabang){
        return $this->find()->andWhere(['status' => 'Pengeluaran','id_cabang' => $id_cabang])->andWhere(['between','created_at', $date1,$date2])->sum('jumlah_uang');
    }

    public function dailyViaTransfer($date1,$date2,$id_cabang,$status){
        return $this->find()->andWhere(['status' => $status,'id_cabang' => $id_cabang,'type_pembayaran' => 'Transfer'])->andWhere(['between','created_at',$date1,$date2])->sum('jumlah_uang');
    }

    public function countFinance($date,$status,$id_cabang){
        return $this->find()->where(['id_cabang' => $id_cabang, 'status' => $status, 'created_at' => $date])->count();
    }

    public function countFinance2($date,$status,$id_cabang,$bunga){
        return $this->find()->joinWith('barang')->where(['log_finance.id_cabang' => $id_cabang, 'log_finance.status' => $status, 'created_at' => $date])->andWhere(['like', 'barang.bunga', $bunga])->count();
    }

    public function countTransfer($date,$id_cabang){
        return $this->find()->where(['id_cabang' => $id_cabang, 'type_pembayaran' => 'Transfer','created_at' => $date])->groupBy('id_barang')->count();
    }

    public function jumAngsuran($id_cabang){
        return $this->find()->where(['id_cabang' => $id_cabang, 'status' =>'Angsuran'])->sum('jumlah_uang');
    }

    public function getBarang(){
        return $this->hasOne(Barang::className(),['id_barang'=>'id_barang']);
    }

    public function dendaPerpanjang(){
        $tgl = $this->created_at;
        $barang_id = $this->id_barang;
        $cek = LogFinance::find()->where(['id_barang' => $barang_id, 'created_at' => $tgl, 'status' => 'DendaPerpanjang'])->sum('jumlah_uang');

        return $cek;
    }

    public function bungaPerpanjang(){
        $tgl = $this->created_at;
        $barang_id = $this->id_barang;
        $cek = LogFinance::find()->where(['id_barang' => $barang_id, 'created_at' => $tgl, 'status' => 'BungaPerpanjang'])->sum('jumlah_uang');

        return $cek;
    }

    public function dendaPelunasan(){
        $tgl = $this->created_at;
        $barang_id = $this->id_barang;
        $cek = LogFinance::find()->where(['id_barang' => $barang_id, 'created_at' => $tgl, 'status' => 'DendaPelunasan'])->sum('jumlah_uang');

        return $cek;
    }

    public function bungaPelunasan(){
        $tgl = $this->created_at;
        $barang_id = $this->id_barang;
        $cek = LogFinance::find()->where(['id_barang' => $barang_id, 'created_at' => $tgl, 'status' => 'BungaPelunasanPasif'])->sum('jumlah_uang');

        if(empty($cek)){
            $cek = 0;
        }

        return $cek;
    }

    public function nilaiCashback1_2(){
        $tgl = $this->created_at;
        $barang_id = $this->id_barang;
        $cek = LogFinance::find()->where(['id_barang' => $barang_id, 'created_at' => $tgl, 'status' => 'KembaliBunga1.2Persen'])->sum('jumlah_uang');

        return $cek;
    }

    public function nilaiCashback5(){
        $tgl = $this->created_at;
        $barang_id = $this->id_barang;
        $cek = LogFinance::find()->where(['id_barang' => $barang_id, 'created_at' => $tgl, 'status' => 'KembaliBunga5Persen'])->sum('jumlah_uang');

        return $cek;
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_log_finance' => 'Id Log Finance',
            'jumlah_uang' => 'Jumlah Uang',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'id_cabang' => 'Id Cabang',
            'id_barang' => 'Id Barang',
        ];
    }
}
