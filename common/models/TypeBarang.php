<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "type_barang".
 *
 * @property int $id
 * @property string $nama_type
 * @property string $deskripsi
 * @property string $created_at
 * @property int $created_by
 * @property int $merk_barang_id
 *
 * @property TaksiranHarga[] $taksiranHargas
 * @property MerkBarang $merkBarang
 */
class TypeBarang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'type_barang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_type', 'deskripsi', 'created_at', 'created_by', 'merk_barang_id', 'kategori_barang_id'], 'required'],
            [['deskripsi'], 'string'],
            [['created_at'], 'safe'],
            [['created_by', 'merk_barang_id', 'kategori_barang_id'], 'integer'],
            [['nama_type'], 'string', 'max' => 225],
            [['merk_barang_id'], 'exist', 'skipOnError' => true, 'targetClass' => MerkBarang::className(), 'targetAttribute' => ['merk_barang_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_type' => 'Nama Type',
            'deskripsi' => 'Deskripsi',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'merk_barang_id' => 'Merk Barang ID',
        ];
    }

    /**
     * Gets query for [[TaksiranHargas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTaksiranHargas()
    {
        return $this->hasMany(TaksiranHarga::className(), ['type_barang_id' => 'id']);
    }

    /**
     * Gets query for [[MerkBarang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMerkBarang()
    {
        return $this->hasOne(MerkBarang::className(), ['id' => 'merk_barang_id']);
    }

    public function getKategoriBarang()
    {
        return $this->hasOne(KategoriBarang::className(), ['id' => 'kategori_barang_id']);
    }

    public function listType($merk_id){
        $out = $this->find()->where(['merk_barang_id' => $merk_id])->select(['id as id','nama_type AS name'])->asArray()->all();
        
        return $out;
    }
    
}
