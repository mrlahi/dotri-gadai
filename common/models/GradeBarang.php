<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "grade_barang".
 *
 * @property int $id
 * @property string $nama_grade
 * @property string $deskripsi
 * @property string $created_at
 * @property int $created_by
 */
class GradeBarang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grade_barang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_grade', 'deskripsi', 'created_at', 'created_by'], 'required'],
            [['deskripsi'], 'string'],
            [['created_at'], 'safe'],
            [['created_by'], 'integer'],
            [['nama_grade'], 'string', 'max' => 50],
            [['nama_grade'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_grade' => 'Nama Grade',
            'deskripsi' => 'Deskripsi',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    public static function listGrade(){
        return ArrayHelper::map(GradeBarang::find()->asArray()->all(),'id','nama_grade');
    }
}
