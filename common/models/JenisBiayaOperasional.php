<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jenis_biaya_operasional".
 *
 * @property int $id
 * @property string $nama_jenis
 * @property int $akun_id
 * @property string $created_at
 * @property int $created_by
 *
 * @property BebanOperasional[] $bebanOperasionals
 * @property Akun $akun
 */
class JenisBiayaOperasional extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenis_biaya_operasional';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_jenis', 'akun_id', 'created_at', 'created_by'], 'required'],
            [['akun_id', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['nama_jenis'], 'string', 'max' => 255],
            [['akun_id'], 'exist', 'skipOnError' => true, 'targetClass' => Akun::className(), 'targetAttribute' => ['akun_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_jenis' => 'Nama Jenis',
            'akun_id' => 'Akun ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[BebanOperasionals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBebanOperasionals()
    {
        return $this->hasMany(BebanOperasional::className(), ['type_operasional' => 'id']);
    }

    /**
     * Gets query for [[Akun]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAkun()
    {
        return $this->hasOne(Akun::className(), ['id' => 'akun_id']);
    }
}
