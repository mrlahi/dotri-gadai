<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bayar".
 *
 * @property int $id_bayar
 * @property int $id_barang
 * @property int $id_nasabah
 * @property int $telat
 * @property int $denda
 * @property string $created_at
 * @property int $created_by
 */
class Bayar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bayar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_barang', 'id_nasabah', 'telat', 'denda', 'created_by'], 'required'],
            [['id_barang', 'id_nasabah', 'telat', 'denda', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    public function myDenda($id_barang){
        $dataBarang = Barang::find()->where(['id_barang' => $id_barang])->one();
        $tgl_now = date_create(date('Y-m-d'));
        $jatuh_tempo = date_create($dataBarang->jatuh_tempo);
        if($jatuh_tempo < $tgl_now){
            $telat = date_diff($tgl_now,$jatuh_tempo);
            $telat = $telat->format("%a");
            $denda = $telat * 5000;

            $denda_telat = ["telat" => $telat,"denda" => $denda];
        }
        else{
            $denda_telat = ["telat" => 0,"denda" => 0];
        }
        return $denda_telat;
    }

    public function myBunga($id_barang){
        $dataBarang = Barang::find()->where(['id_barang' => $id_barang])->one();
        $tgl_now = date_create(date('Y-m-d'));
        $tgl_masuk = date_create($dataBarang->tgl_masuk);
        $waktu_pinjam = date_diff($tgl_masuk,$tgl_now);
        $waktu_pinjam = $waktu_pinjam->format("%a");
        $bunga = $dataBarang->nilai_pinjam * ($dataBarang->bunga/100);
        $bungaBaru = $dataBarang->getSisapinjam2() * ($dataBarang->bunga/100);
        $statusLogFinance = "";

        if($waktu_pinjam <= 15){
            
            if($dataBarang->jangka >15){
                $potongan_bunga = ($dataBarang->bunga/100)/2;//SUPAYA DIBAGI 2 DARI BUNGA SEHARUSNYA JIKA DIA TEBUS SEBELUM 15 HARI
                $diskon_bunga = $potongan_bunga*$dataBarang->nilai_pinjam;
                $label = $dataBarang->bunga/2;
                $statusLogFinance = "KembaliBunga".$label."Persen";
            }
            else{
                $diskon_bunga = 0;
            }
        }
        else{
            $diskon_bunga = 0;
        }

        $bunga = ["diskon_bunga" => $diskon_bunga, "bunga" => $bunga, 'statusLogFinance' => $statusLogFinance, "bunga_baru" => $bungaBaru];

        return $bunga;
    }

    public function dendaNow(){
        $tgl_now = date('Y-m-d');
        return $this->find()->where(['created_at' => $tgl_now])->sum('denda');
    }

    public function inBayar($id_barang,$id_nasabah){
        $this->id_barang = $id_barang;
        $this->id_nasabah = $id_nasabah;
        $this->telat = $this->myDenda($id_barang)['telat'];
        $this->denda = $this->myDenda($id_barang)['denda'];
        $this->created_at = date('Y-m-d');
        $this->created_by = Yii::$app->user->identity->id;
        $this->save(false);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_bayar' => 'Id Bayar',
            'id_barang' => 'Id Barang',
            'id_nasabah' => 'Id Nasabah',
            'telat' => 'Telat',
            'denda' => 'Denda',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }
}
