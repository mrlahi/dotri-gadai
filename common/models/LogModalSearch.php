<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LogModal;

/**
 * LogModalSearch represents the model behind the search form of `common\models\LogModal`.
 */
class LogModalSearch extends LogModal
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_log_modal', 'jumlah_masuk', 'id_cabang'], 'integer'],
            [['created_at', 'keterangan'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogModal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_log_modal' => $this->id_log_modal,
            'jumlah_masuk' => $this->jumlah_masuk,
            'id_cabang' => $this->id_cabang,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
