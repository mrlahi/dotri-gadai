<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "koordinator_kunci".
 *
 * @property int $id
 * @property int $koordinator_id
 * @property string $username
 * @property string $key_code
 * @property string $created_at
 * @property int $created_by
 *
 * @property Profile $koordinator
 * @property User $username0
 */
class KoordinatorKunci extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'koordinator_kunci';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['koordinator_id', 'username', 'key_code', 'created_at', 'created_by'], 'required'],
            [['koordinator_id', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['username', 'key_code'], 'string', 'max' => 255],
            [['koordinator_id'], 'unique'],
            [['koordinator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['koordinator_id' => 'id_user']],
            [['username'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['username' => 'username']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'koordinator_id' => 'Koordinator ID',
            'username' => 'Username',
            'key_code' => 'Key Code',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[Koordinator]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKoordinator()
    {
        return $this->hasOne(Profile::className(), ['id_user' => 'koordinator_id']);
    }

    /**
     * Gets query for [[Username0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsername0()
    {
        return $this->hasOne(User::className(), ['username' => 'username']);
    }
}
