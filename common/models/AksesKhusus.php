<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "akses_khusus".
 *
 * @property int $id
 * @property int $profile_id
 * @property string $deskripsi
 * @property string $created_at
 * @property int $created_by
 *
 * @property Profile $profile
 */
class AksesKhusus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'akses_khusus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['profile_id', 'deskripsi', 'created_at', 'created_by'], 'required'],
            [['profile_id', 'created_by'], 'integer'],
            [['deskripsi'], 'string'],
            [['created_at'], 'safe'],
            [['profile_id'], 'unique'],
            [['profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['profile_id' => 'id_user']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Profile ID',
            'deskripsi' => 'Deskripsi',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[Profile]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id_user' => 'profile_id']);
    }
}
