<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LogJurnal;

/**
 * LogJurnalSearch represents the model behind the search form of `common\models\LogJurnal`.
 */
class LogJurnalSearch extends LogJurnal
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'akun_kode', 'debet', 'kredit', 'cabang_id', 'created_by', 'asal_id'], 'integer'],
            [['created_at', 'jenis_asal_jurnal'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogJurnal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'akun_kode' => $this->akun_kode,
            'debet' => $this->debet,
            'kredit' => $this->kredit,
            'cabang_id' => $this->cabang_id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'asal_id' => $this->asal_id,
        ]);

        $query->andFilterWhere(['like', 'jenis_asal_jurnal', $this->jenis_asal_jurnal]);

        return $dataProvider;
    }
}
