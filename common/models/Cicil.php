<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cicil".
 *
 * @property int $id_cicil
 * @property int $id_barang
 * @property int $jumlah_cicil
 * @property string $created_at
 * @property int $created_by
 */
class Cicil extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cicil';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_barang', 'jumlah_cicil', 'created_by'], 'required'],
            [['id_barang', 'jumlah_cicil', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    public function myCicilan($id_barang){
        return $this->find()->where(['id_barang' => $id_barang])->sum('jumlah_cicil');
    }

    public function myRiwayatCicil($id_barang){
        return $this->find()->where(['id_barang' => $id_barang])->all();
    }

    public function myJumCicil($id_barang){
        return $this->find()->where(['id_barang' => $id_barang])->sum('jumlah_cicil');
    }

    public function cicilNow(){
        $tgl_now = date('Y-m-d');
        return $this->find()->where(['created_at' => $tgl_now])->sum('jumlah_cicil');
    }

    public function inCicil($id_barang,$jumlah_cicil){
        $this->id_barang = $id_barang;
        $this->jumlah_cicil = $jumlah_cicil;
        $this->created_at = date('Y-m-d');
        $this->created_by = Yii::$app->user->identity->id;
        $this->save(false);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_cicil' => 'Id Cicil',
            'id_barang' => 'Id Barang',
            'jumlah_cicil' => 'Jumlah Cicil',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }
}
