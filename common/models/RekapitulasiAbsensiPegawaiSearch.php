<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RekapitulasiAbsensiPegawai;

/**
 * RekapitulasiAbsensiPegawaiSearch represents the model behind the search form of `common\models\RekapitulasiAbsensiPegawai`.
 */
class RekapitulasiAbsensiPegawaiSearch extends RekapitulasiAbsensiPegawai
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'pegawai_id', 'hari_kerja_pegawai_id', 'waktu_telat', 'waktu_cepat', 'created_by'], 'integer'],
            [['log_in', 'log_out', 'status_log_in', 'status_log_out', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RekapitulasiAbsensiPegawai::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'pegawai_id' => $this->pegawai_id,
            'hari_kerja_pegawai_id' => $this->hari_kerja_pegawai_id,
            'log_in' => $this->log_in,
            'log_out' => $this->log_out,
            'waktu_telat' => $this->waktu_telat,
            'waktu_cepat' => $this->waktu_cepat,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'status_log_in', $this->status_log_in])
            ->andFilterWhere(['like', 'status_log_out', $this->status_log_out]);

        return $dataProvider;
    }
}
