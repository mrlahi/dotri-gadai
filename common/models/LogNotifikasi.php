<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "log_notifikasi".
 *
 * @property int $id
 * @property int|null $barang_id
 * @property string $status_wa
 * @property string $status_sms
 * @property string $created_at
 * @property string $jenis_notifikasi
 * @property string $device_id
 * @property string $message_id
 * @property string $message
 * @property string $no_hp_nasabah
 *
 * @property Barang $barang
 * @property Device $device
 */
class LogNotifikasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_notifikasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['barang_id'], 'integer'],
            [['status_wa', 'created_at', 'jenis_notifikasi', 'device_id', 'no_hp_nasabah'], 'required'],
            [['status_wa', 'status_sms', 'jenis_notifikasi', 'message'], 'string'],
            [['created_at'], 'safe'],
            [['device_id', 'message_id', 'no_hp_nasabah'], 'string', 'max' => 225],
            [['barang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Barang::className(), 'targetAttribute' => ['barang_id' => 'id_barang']],
            [['device_id'], 'exist', 'skipOnError' => true, 'targetClass' => Device::className(), 'targetAttribute' => ['device_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'barang_id' => 'Barang ID',
            'status_wa' => 'Status Wa',
            'status_sms' => 'Status Sms',
            'created_at' => 'Created At',
            'jenis_notifikasi' => 'Jenis Notifikasi',
            'device_id' => 'Device ID',
            'message_id' => 'Message ID',
            'message' => 'Message',
            'no_hp_nasabah' => 'No Hp Nasabah',
        ];
    }

    /**
     * Gets query for [[Barang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBarang()
    {
        return $this->hasOne(Barang::className(), ['id_barang' => 'barang_id']);
    }

    /**
     * Gets query for [[Device]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDevice()
    {
        return $this->hasOne(Device::className(), ['id' => 'device_id']);
    }
}
