<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "profile".
 *
 * @property int $id_user
 * @property string $no_ktp
 * @property string $nama
 * @property string $alamat
 * @property string $email
 * @property string $no_hp
 * @property int $created_by
 * @property string|null $created
 * @property string $akses
 * @property int $id_cabang
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{profile}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_ktp', 'nama', 'alamat', 'email', 'no_hp', 'created_by', 'akses', 'id_cabang', 'domisili', 'pekerjaan_id'], 'required'],
            [['alamat','alasan_blacklist', 'domisili', 'absensi_id'], 'string'],
            [['no_hp', 'no_hp2'], 'k-phone', 'countryValue' => 'ID'],
            [['created_by', 'id_cabang', 'is_blacklist', 'pekerjaan_id'], 'integer'],
            [['created'], 'safe'],
            /*[
                'email','unique',
                'targetClass'=>'\common\models\Profile',
                'message' => 'Email Sudah Digunakan.'
            ],*/
            [['no_ktp'],'unique','message' => 'No KTP Sudah Digunakan/Nasabah Diblacklist.'],
            [['absensi_id'],'unique','message' => 'ID Absen Ini Sudah Digunakan'],
            [['no_ktp', 'nama', 'email'], 'string', 'max' => 225],
            [['no_hp', 'no_hp2', 'akses'], 'string', 'max' => 50],
        ];
    }

    public static function listNasabah(){
        $nasabah = Profile::find()->where(['akses' => 'defaultNasabah', 'is_blacklist' => FALSE])->all();
        $dataNasabah = ArrayHelper::map($nasabah,'id_user',function($nasabah){
            return $nasabah->nama.' ('.$nasabah->no_ktp.')';
        });

        return $dataNasabah;
    
    }

    public static function listCabang(){
        $datacabang = ArrayHelper::map(Cabang::find()->where(['!=', 'id_cabang', '10000'])->asArray()->all(),'id_cabang','nama_cabang');
        return $datacabang;
    }

    public static function listPegawai(){
        $pegawai = Profile::find()->where(['or',['akses' => 'defaultStaff'], ['akses' => 'Penafsir'], ['akses' => 'gudang'], ['akses' => 'defaultFinance']])->all();

        $dataPegawai = ArrayHelper::map($pegawai,'id_user',function($pegawai){
            return $pegawai->nama.' - '.$pegawai->cabanguser->nama_cabang.' ('.$pegawai->email.')';
        });

        return $dataPegawai;
    }

    public static function listPegawaiAbsen(){
        $datapegawai = ArrayHelper::map(Profile::find()->where(['or',['akses' => 'defaultStaff'], ['akses' => 'Penafsir'], ['akses' => 'gudang'], ['akses' => 'defaultFinance']])->asArray()->all(),'absensi_id','nama');
        return $datapegawai;
    }


    public static function listAdmin(){
        $datapegawai = ArrayHelper::map(Profile::find()->where(['akses' => 'admin'])->asArray()->all(),'id_user','nama');
        return $datapegawai;
    }

    public function getCabanguser(){
        return $this->hasOne(Cabang::className(),['id_cabang'=>'id_cabang']);
    }

    public function getPekerjaan(){
        return $this->hasOne(Pekerjaan::className(),['id_pekerjaan'=>'pekerjaan_id']);
    }

    public function myProfile(){
        $id = Yii::$app->user->identity->id;
        return $this->findOne($id);
    }

    public static function myProfile2(){
        $id = Yii::$app->user->identity->id;
        return Profile::findOne($id);
    }

    public function pureHp($no){
        if($no == 1){
            $hp = $this->no_hp;
            $newHp = str_replace(' ', '', $hp);
            $newHp = str_replace('+', '', $newHp);
            $newHp = str_replace('-', '', $newHp);
            
            return $newHp;
        }else{
            $hp = $this->no_hp2;
            $newHp = str_replace(' ', '', $hp);
            $newHp = str_replace('+', '', $newHp);
            $newHp = str_replace('-', '', $newHp);

            return $newHp;
        }
    }

    public function insNasabah($date1,$date2){
        $profil_id = $this->id_user;
        $dataBarang = Barang::find()->where(['between', 'tgl_masuk', $date1, $date2])->andWhere(['created_by' => $profil_id])->count();

        if($dataBarang){
            return $dataBarang;
        }
        else{
            return 0;
        }

    }

    /**
     * Gets query for [[Positions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFaceUsers()
    {
        return $this->hasMany(FaceUser::class, ['profile_id' => 'id_user']);
    }

    public function getCekFaceRecorded(){
        return $this->getFaceUsers()->count();
    }

    public static function myCabang(){
        empty(Profile::myProfile2()->cabangActive->nama_cabang) ? $cabangActive = "Outlet Not Set" : $cabangActive = Profile::myProfile2()->cabangActive->nama_cabang;

        return $cabangActive;

    }

    public function getCabangActive(){
        return $this->hasOne(Cabang::class, ['id_cabang' => 'id_cabang']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user' => 'Id User',
            'no_ktp' => 'No Ktp',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'email' => 'Email',
            'no_hp' => 'No Hp',
            'created_by' => 'Created By',
            'created' => 'Created',
            'akses' => 'Akses',
            'id_cabang' => 'Id Cabang',
        ];
    }
}
