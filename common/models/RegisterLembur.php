<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "register_lembur".
 *
 * @property int $id
 * @property int $pegawai_id
 * @property string $tgl_lembur
 * @property int $jam_lembur
 * @property string $status_lembur
 * @property string $created_at
 * @property int $created_by
 *
 * @property Profile $pegawai
 */
class RegisterLembur extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'register_lembur';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pegawai_id', 'tgl_lembur', 'jam_lembur', 'status_lembur', 'created_at', 'created_by'], 'required'],
            [['pegawai_id', 'jam_lembur', 'created_by'], 'integer'],
            [['tgl_lembur', 'created_at'], 'safe'],
            [['status_lembur'], 'string'],
            [['pegawai_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['pegawai_id' => 'id_user']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai_id' => 'Pegawai ID',
            'tgl_lembur' => 'Tgl Lembur',
            'jam_lembur' => 'Jam Lembur',
            'status_lembur' => 'Status Lembur',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[Pegawai]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Profile::className(), ['id_user' => 'pegawai_id']);
    }
}
