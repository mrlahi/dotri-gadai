<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OutstandingManager;

/**
 * OutstandingManagerSearch represents the model behind the search form of `common\models\OutstandingManager`.
 */
class OutstandingManagerSearch extends OutstandingManager
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_outstanding_manager', 'status_generator', 'created_by'], 'integer'],
            [['tgl_start', 'tgl_end', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OutstandingManager::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_outstanding_manager' => $this->id_outstanding_manager,
            'tgl_start' => $this->tgl_start,
            'tgl_end' => $this->tgl_end,
            'status_generator' => $this->status_generator,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
        ]);

        return $dataProvider;
    }
}
