<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Barang;

/**
 * BarangSearch represents the model behind the search form of `common\models\Barang`.
 */
class BarangSearch extends Barang
{
    /**
     * {@inheritdoc}
     */
    public $nama;
    public $no_ktp; 
    public $pekerjaan_id;
    public function rules()
    {
        return [
            [['id_barang', 'id_nasabah', 'id_jenis', 'harga_pasar', 'harga_taksir', 'nilai_pinjam', 'biaya_admin', 'biaya_simpan', 'jangka', 'created_by','id_cabang', 'pekerjaan_id', 'no_fak', 'lokasi_simpan_id'], 'integer'],
            [['bunga'], 'double'],
            [['no_kontrak', 'nama_barang','merk', 'tipe', 'serial', 'tgl_masuk', 'jatuh_tempo', 'spek', 'kelengkapan', 'kondisi', 'status', 'nama', 'no_ktp', 'tgl_status', 'kata_sandi'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Barang::find();
        $query->joinWith(['nasabah']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id_barang' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_barang' => $this->id_barang,
            'id_nasabah' => $this->id_nasabah,
            'id_jenis' => $this->id_jenis,
            //'tgl_masuk' => $this->tgl_masuk,
            // 'jatuh_tempo' => $this->jatuh_tempo,
            'harga_pasar' => $this->harga_pasar,
            'harga_taksir' => $this->harga_taksir,
            'nilai_pinjam' => $this->nilai_pinjam,
            //'bunga' => $this->bunga,
            'biaya_admin' => $this->biaya_admin,
            'biaya_simpan' => $this->biaya_simpan,
            'jangka' => $this->jangka,
            'barang.created_by' => $this->created_by,
            'barang.id_cabang' => $this->id_cabang,
            'pekerjaan_id' => $this->pekerjaan_id, 
            'lokasi_simpan_id' => $this->lokasi_simpan_id
        ]);

        $query->andFilterWhere(['like', 'no_kontrak', $this->no_kontrak])
            ->andFilterWhere(['like', 'nama_barang', $this->nama_barang])
            ->andFilterWhere(['like', 'tipe', $this->tipe])
            ->andFilterWhere(['like', 'serial', $this->serial])
            ->andFilterWhere(['like', 'spek', $this->spek])
            ->andFilterWhere(['like', 'kelengkapan', $this->kelengkapan])
            ->andFilterWhere(['like', 'kondisi', $this->kondisi])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'no_ktp', $this->no_ktp])
            ->andFilterWhere(['like', 'bunga', $this->bunga])
            ->andFilterWhere(['like', 'merk', $this->merk]);

            if(isset ($this->tgl_masuk)&&$this->tgl_masuk!=''){ //you dont need the if function if yourse sure you have a not null date
                $date_explode=explode(" - ",$this->tgl_masuk);
                $date1=trim($date_explode[0]);
                $date2=trim($date_explode[1]);
                $query->andFilterWhere(['between','tgl_masuk',$date1,$date2]);
            }

            if(isset ($this->tgl_status)&&$this->tgl_status!=''){ //you dont need the if function if yourse sure you have a not null date
                $date_explode=explode(" - ",$this->tgl_status);
                $date1=trim($date_explode[0]);
                $date2=trim($date_explode[1]);
                $query->andFilterWhere(['between','tgl_status',$date1,$date2]);
            }

            if(isset ($this->jatuh_tempo)&&$this->jatuh_tempo!=''){ //you dont need the if function if yourse sure you have a not null date
                $date_explode=explode(" - ",$this->jatuh_tempo);
                $date1=trim($date_explode[0]);
                $date2=trim($date_explode[1]);
                $query->andFilterWhere(['between','jatuh_tempo',$date1,$date2]);
            }

        return $dataProvider;
    }
}
