<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hari_kerja".
 *
 * @property int $id
 * @property string $tgl_kerja
 * @property string $status_hari
 * @property string $deskripsi
 * @property string $created_at
 * @property int $created_by
 */
class HariKerja extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hari_kerja';
    }

    /**
     * {@inheritdoc}
     */

    public $bulan;

    const SCENARIO_GENERATE_HARI = 'generate-hari';

    public function rules()
    {
        return [
            [['tgl_kerja', 'status_hari', 'deskripsi', 'created_at', 'created_by'], 'required'],
            [['tgl_kerja', 'created_at'], 'safe'],
            [['status_hari', 'deskripsi','bulan'], 'string'],
            [['created_by'], 'integer'],
            [['tgl_kerja'], 'unique'],
            [['bulan'], 'required', 'on' => self::SCENARIO_GENERATE_HARI]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tgl_kerja' => 'Tgl Kerja',
            'status_hari' => 'Status Hari',
            'deskripsi' => 'Deskripsi',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }
}
