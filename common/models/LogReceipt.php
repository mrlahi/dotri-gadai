<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "log_receipt".
 *
 * @property int $id_log_receipt
 * @property int $id_barang
 * @property int $biaya_admin
 * @property int $biaya_simpan
 * @property int $bunga
 * @property int $denda
 * @property int $angsuran
 * @property string $type_pembayaran
 * @property int $cashback
 * @property string $type_receipt
 * @property string $created_at
 * @property int $created_by
 */
class LogReceipt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_receipt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_barang', 'biaya_admin', 'biaya_simpan', 'bunga', 'denda', 'angsuran', 'type_receipt', 'created_at', 'created_by'], 'required'],
            [['id_barang', 'biaya_admin', 'biaya_simpan',  'denda', 'angsuran', 'created_by', 'cashback'], 'integer'],
            [['bunga'], 'double'],
            [['created_at'], 'safe'],
            [['type_receipt', 'type_pembayaran'], 'string', 'max' => 20],
        ];
    }

    public function getBarang(){
        return $this->hasOne(Barang::className(),['id_barang'=>'id_barang']);
    }

    public function myKasir(){
        return Profile::find()->where(['id_user' => $this->created_by])->one();
    }

    public function myCabang(){
        return Cabang::find()->where(['id_cabang' => $this->myKasir()->id_cabang])->one();
    }

    public function myBarang(){
        return Barang::find()->where(['id_barang' => $this->id_barang])->one();
    }

    public function myNasabah(){
        return Profile::find()->where(['id_user' => $this->myBarang()->id_nasabah])->one();
    }

    public function inReceipt($id_barang,$biaya_admin,$biaya_simpan,$bunga,$type_receipt, $type_pembayaran, $cashback){
        $this->id_barang = $id_barang;
        $this->biaya_admin = $biaya_admin;
        $this->biaya_simpan = $biaya_simpan;
        $this->bunga = $bunga;
        $this->denda = 0;
        $this->angsuran = 0;
        $this->type_receipt = $type_receipt;
        $this->type_pembayaran = $type_pembayaran;
        $this->cashback = $cashback;
        $this->created_at = date('Y-m-d H:m:i');
        $this->created_by = Yii::$app->user->identity->id;
        $this->save(false);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_log_receipt' => 'Id Log Receipt',
            'id_barang' => 'Id Barang',
            'biaya_admin' => 'Biaya Admin',
            'biaya_simpan' => 'Biaya Simpan',
            'bunga' => 'Bunga',
            'denda' => 'Denda',
            'angsuran' => 'Angsuran',
            'type_receipt' => 'Type Receipt',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    public function typePembayaran($kas_id){
        if($kas_id == 1){
            return "Cash";
        }
        else{
            return "Transfer";
        }
    }

}
