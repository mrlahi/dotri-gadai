<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LogUangKeluar;

/**
 * LogUangKeluarSearch represents the model behind the search form of `common\models\LogUangKeluar`.
 */
class LogUangKeluarSearch extends LogUangKeluar
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_log_uang_keluar', 'jumlah_pengeluaran', 'created_by', 'id_cabang'], 'integer'],
            [['nama_pengeluaran', 'type_pengeluaran', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogUangKeluar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_log_uang_keluar' => $this->id_log_uang_keluar,
            'jumlah_pengeluaran' => $this->jumlah_pengeluaran,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'id_cabang' => $this->id_cabang,
        ]);

        $query->andFilterWhere(['like', 'nama_pengeluaran', $this->nama_pengeluaran])
            ->andFilterWhere(['like', 'type_pengeluaran', $this->type_pengeluaran]);

        return $dataProvider;
    }
}
