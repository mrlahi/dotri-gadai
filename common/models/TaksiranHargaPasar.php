<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "taksiran_harga_pasar".
 *
 * @property int $id_taksiran
 * @property string $merk_barang
 * @property string $nama_barang
 * @property string $type_barang
 * @property string $spesifikasi
 * @property string $kelengkapan
 * @property string $kondisi
 * @property int $taksiran_harga
 * @property int $created_by
 * @property string $created_at
 */
class TaksiranHargaPasar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'taksiran_harga_pasar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['merk_barang', 'nama_barang', 'type_barang', 'spesifikasi', 'kelengkapan', 'kondisi', 'taksiran_harga', 'created_by', 'created_at'], 'required'],
            [['spesifikasi'], 'string'],
            [['taksiran_harga', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['merk_barang', 'nama_barang', 'type_barang', 'kelengkapan', 'kondisi'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_taksiran' => 'Id Taksiran',
            'merk_barang' => 'Merk Barang',
            'nama_barang' => 'Nama Barang',
            'type_barang' => 'Type Barang',
            'spesifikasi' => 'Spesifikasi',
            'kelengkapan' => 'Kelengkapan',
            'kondisi' => 'Kondisi',
            'taksiran_harga' => 'Taksiran Harga',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
        ];
    }
}
