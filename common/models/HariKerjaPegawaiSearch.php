<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\HariKerjaPegawai;

/**
 * HariKerjaPegawaiSearch represents the model behind the search form of `common\models\HariKerjaPegawai`.
 */
class HariKerjaPegawaiSearch extends HariKerjaPegawai
{
    /**
     * {@inheritdoc}
     */

    public $range_tgl;

    public function rules()
    {
        return [
            [['id', 'pegawai_id', 'hari_kerja_id', 'shift_id', 'created_by'], 'integer'],
            [['status_absensi', 'created_at', 'range_tgl'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HariKerjaPegawai::find();
        $query->joinWith(['hariKerja']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'pegawai_id' => $this->pegawai_id,
            'hari_kerja_id' => $this->hari_kerja_id,
            'shift_id' => $this->shift_id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'status_absensi', $this->status_absensi]);

        if(isset ($this->range_tgl) && $this->range_tgl !=''){ //you dont need the if function if yourse sure you have a not null date
            $date_explode=explode(" - ",$this->range_tgl);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);
            $query->andFilterWhere(['between','tgl_kerja',$date1,$date2]);
        }

        return $dataProvider;
    }
}
