<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "jenis_barang".
 *
 * @property int $id_jenis_barang
 * @property int $nama_barang
 * @property string $desk_barang
 */
class JenisBarang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenis_barang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_barang'], 'required'],
            [['nama_barang','desk_barang'], 'string'],
        ];
    }

    public static function listJenisBarang(){
        $dataJenisBarang = ArrayHelper::map(JenisBarang::find()->asArray()->all(),'id_jenis_barang','nama_barang');

        return $dataJenisBarang;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_jenis_barang' => 'Id Jenis Barang',
            'nama_barang' => 'Nama Barang',
            'desk_barang' => 'Desk Barang',
        ];
    }
}
