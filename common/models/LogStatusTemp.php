<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "log_status_temp".
 *
 * @property int $id_status_temp
 * @property int $id_barang
 * @property string $status
 * @property string $created_at
 * @property int $created_by
 */
class LogStatusTemp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_status_temp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_status_temp', 'id_barang', 'status', 'created_at', 'created_by'], 'required'],
            [['id_status_temp', 'id_barang', 'created_by','outstanding_id'], 'integer'],
            [['created_at'], 'safe'],
            [['status'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_status_temp' => 'Id Status Temp',
            'id_barang' => 'Id Barang',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    public function sisaPinjam($id_barang,$start,$end){
        $dataBarang = Barang::findOne($id_barang);
        $cicil = $this->myCicilanRange($id_barang,$end);
        $pinjam = $dataBarang->nilai_pinjam;
        $sisapinjam = $pinjam - $cicil;

        return $sisapinjam;
    }
    
    public function getTgllunas(){
        $id_barang = $this->myBarang()->id_barang;
        $dataLogStatus = LogStatus::find()->where(['id_barang' => $id_barang])->orderBy(['id_status' => SORT_DESC])->one();

        if($dataLogStatus->status == "Lunas"){
            $tgl_lunas = $dataLogStatus->created_at;
        }
        else{
            $tgl_lunas = "";
        }
        return $tgl_lunas;
    }
    public function getBarang(){
        return $this->hasOne(Barang::className(),['id_barang'=>'id_barang']);
    }

    public function myBarang(){
        return Barang::find()->where(['id_barang' => $this->id_barang])->one();
    }

    public function myCicilan($id_barang){
        return LogFinance::find()->where(['id_barang' => $id_barang, 'status' => 'Angsuran'])->sum('jumlah_uang');
    }

    public function myCicilanRange($id_barang,$end){
        return LogFinance::find()->where(['id_barang' => $id_barang])->andWhere(['or', ['status' => 'Angsuran'], ['status' => 'Pelunasan']])->andWhere(['<=', 'created_at', $end])->sum('jumlah_uang');
    }

    public function getCabang(){
        $id_cabang = $this->myBarang()->id_cabang;
        return Cabang::find()->where(['id_cabang' => $id_cabang])->one();
    }

    public function getJenisBarang(){
        $id_jenis = $this->myBarang()->id_jenis;
        return JenisBarang::find()->where(['id_jenis_barang' => $id_jenis])->one();
    }

    public function getNasabah(){
        $id_nasabah = $this->myBarang()->id_nasabah;
        return Profile::find()->where(['id_user'=>$id_nasabah])->one();
    }

    public function getStatbar(){
        if($this->status == "Terjual"){
            $status = '<span class="label label-warning">Terjual</span>';
        }
        else if($this->status == "Lunas"){
            $status = '<span class="label label-success">Lunas</span>';
        }
        else{
            $status = '<span class="label label-info">Outstanding</span>';
        }

        return $status;
    }

    public function getPegawai(){
        return $this->hasOne(Profile::className(),['id_user'=>'created_by']);
    }

    public function listStatus(){
        return $dataStatus = ['Terjual' => 'Terjual','Lunas' => 'Lunas','Outstanding' => 'Outstanding'];
    }
}
