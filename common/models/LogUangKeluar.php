<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "log_uang_keluar".
 *
 * @property int $id_log_uang_keluar
 * @property string $nama_pengeluaran
 * @property int $jumlah_pengeluaran
 * @property string $type_pengeluaran
 * @property int $created_by
 * @property string $created_at
 * @property int $id_cabang
 */
class LogUangKeluar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_uang_keluar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_pengeluaran', 'jumlah_pengeluaran', 'type_pengeluaran', 'created_by', 'created_at', 'id_cabang'], 'required'],
            [['jumlah_pengeluaran', 'created_by', 'id_cabang'], 'integer'],
            [['created_at'], 'safe'],
            [['nama_pengeluaran'], 'string', 'max' => 225],
            [['type_pengeluaran'], 'string', 'max' => 20],
        ];
    }

    public function getCabang(){
        return $this->hasOne(Cabang::className(),['id_cabang'=>'id_cabang']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_log_uang_keluar' => 'Id Log Uang Keluar',
            'nama_pengeluaran' => 'Nama Pengeluaran',
            'jumlah_pengeluaran' => 'Jumlah Pengeluaran',
            'type_pengeluaran' => 'Type Pengeluaran',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'id_cabang' => 'Id Cabang',
        ];
    }
}
