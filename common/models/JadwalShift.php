<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "jadwal_shift".
 *
 * @property int $id
 * @property string $nama_shift
 * @property string $jam_masuk
 * @property string $jam_pulang
 * @property string $hitung_lembur
 * @property string $created_at
 * @property int $created_by
 */
class JadwalShift extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jadwal_shift';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_shift', 'jam_masuk', 'jam_pulang', 'hitung_lembur', 'created_at', 'created_by'], 'required'],
            [['jam_masuk', 'jam_pulang', 'created_at'], 'safe'],
            [['hitung_lembur'], 'string'],
            [['created_by'], 'integer'],
            [['nama_shift'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_shift' => 'Nama Shift',
            'jam_masuk' => 'Jam Masuk',
            'jam_pulang' => 'Jam Pulang',
            'hitung_lembur' => 'Hitung Lembur',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    public static function listShift(){
        $shift = JadwalShift::find()->all();
        $dataShift = ArrayHelper::map($shift,'id',function($shift){
            return $shift->nama_shift.' ('.$shift->jam_masuk." - ".$shift->jam_pulang.')';
        });

        return $dataShift;
    
    }

}
