<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\KasUmum;

/**
 * KasUmumSearch represents the model behind the search form of `common\models\KasUmum`.
 */
class KasUmumSearch extends KasUmum
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kas_umum', 'jumlah_uang_masuk', 'jumlah_uang_keluar', 'id_cabang', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KasUmum::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_kas_umum' => $this->id_kas_umum,
            'jumlah_uang_masuk' => $this->jumlah_uang_masuk,
            'jumlah_uang_keluar' => $this->jumlah_uang_keluar,
            'id_cabang' => $this->id_cabang,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        return $dataProvider;
    }
}
