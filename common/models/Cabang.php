<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cabang".
 *
 * @property int $id_cabang
 * @property string $nama_cabang
 * @property string $alamat
 * @property string $telp
 */
class Cabang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cabang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_cabang', 'alamat', 'telp'], 'required'],
            [['alamat', 'latitude', 'longitude'], 'string'],
            [['nama_cabang', 'telp'], 'string', 'max' => 225],
        ];
    }

    public function myCabang($id_cabang){
        return $this->findOne($id_cabang);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_cabang' => 'Id Cabang',
            'nama_cabang' => 'Nama Cabang',
            'alamat' => 'Alamat',
            'telp' => 'Telp',
        ];
    }


    //MY FUNCTIONS START HERE

    public static function listCabang(){
        $datacabang = ArrayHelper::map(Cabang::find()->where(['!=', 'id_cabang', '10000'])->andWhere(['!=', 'id_cabang', '10003'])->asArray()->all(),'id_cabang','nama_cabang');
        return $datacabang;
    }

    public static function listCabangKeuangan(){
        $datacabang = ArrayHelper::map(Cabang::find()->where(['!=', 'id_cabang', '10000'])->andWhere(['!=', 'id_cabang', '10003'])->asArray()->all(),'id_cabang','nama_cabang');
        $datacabang['1000000'] = 'Semua Cabang';
        return $datacabang;
    }

    //MY FUNCTIONS END HERE

}
