<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "log_login".
 *
 * @property int $id
 * @property string|null $user_agent
 * @property int|null $screen_width
 * @property int|null $screen_height
 * @property string|null $device_type
 * @property string|null $browser_type
 * @property float|null $latitude
 * @property float|null $longitude
 * @property string $created_at
 * @property int $created_by
 * @property string|null $OSName
 * @property string|null $OSVersion
 *
 * @property Profile $createdBy
 */
class LogLogin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_login';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['screen_width', 'screen_height', 'created_by'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['created_at'], 'safe'],
            [['created_by'], 'required'],
            [['user_agent'], 'string', 'max' => 255],
            [['device_type', 'browser_type'], 'string', 'max' => 50],
            [['OSName', 'OSVersion'], 'string', 'max' => 100],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['created_by' => 'id_user']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_agent' => 'User Agent',
            'screen_width' => 'Screen Width',
            'screen_height' => 'Screen Height',
            'device_type' => 'Device Type',
            'browser_type' => 'Browser Type',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'OSName' => 'Os Name',
            'OSVersion' => 'Os Version',
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Profile::className(), ['id_user' => 'created_by']);
    }
}
