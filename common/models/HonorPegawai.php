<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "honor_pegawai".
 *
 * @property int $id
 * @property int $pegawai_id
 * @property int $honor
 * @property int $is_active
 * @property string $created_at
 * @property int $created_by
 *
 * @property Profile $pegawai
 */
class HonorPegawai extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'honor_pegawai';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pegawai_id', 'honor', 'is_active', 'created_at', 'created_by'], 'required'],
            [['pegawai_id', 'honor', 'is_active', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['pegawai_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['pegawai_id' => 'id_user']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai_id' => 'Pegawai ID',
            'honor' => 'Honor',
            'is_active' => 'Is Active',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[Pegawai]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Profile::className(), ['id_user' => 'pegawai_id']);
    }
}
