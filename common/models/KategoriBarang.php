<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "kategori_barang".
 *
 * @property int $id
 * @property string $nama_kategori
 * @property string $deskripsi
 * @property string $created_at
 * @property int $created_by
 *
 * @property TaksiranHarga[] $taksiranHargas
 */
class KategoriBarang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kategori_barang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_kategori', 'deskripsi', 'created_at', 'created_by'], 'required'],
            [['deskripsi'], 'string'],
            [['created_at'], 'safe'],
            [['created_by'], 'integer'],
            [['nama_kategori'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_kategori' => 'Nama Kategori',
            'deskripsi' => 'Deskripsi',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[TaksiranHargas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTaksiranHargas()
    {
        return $this->hasMany(TaksiranHarga::className(), ['kategori_barang_id' => 'id']);
    }

    public static function listKategori(){
        return ArrayHelper::map(KategoriBarang::find()->asArray()->all(),'id','nama_kategori');
    }

}
