<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "periode_penggajian".
 *
 * @property int $id
 * @property string $tahun
 * @property int $bulan
 * @property string $start_date
 * @property string $end_date
 * @property string $deskripsi
 * @property string $created_at
 * @property int $created_by
 */
class PeriodePenggajian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'periode_penggajian';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tahun', 'bulan', 'start_date', 'end_date', 'deskripsi', 'created_at', 'created_by'], 'required'],
            [['tahun', 'start_date', 'end_date', 'created_at'], 'safe'],
            [['bulan', 'created_by'], 'integer'],
            [['deskripsi'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tahun' => 'Tahun',
            'bulan' => 'Bulan',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'deskripsi' => 'Deskripsi',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }
}
