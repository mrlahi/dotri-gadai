<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "lokasi_simpan".
 *
 * @property int $id
 * @property string $nama_lokasi
 * @property string $deskripsi
 * @property string $created_at
 * @property int $created_by
 *
 * @property Barang[] $barangs
 */
class LokasiSimpan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lokasi_simpan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_lokasi', 'deskripsi', 'created_at', 'created_by'], 'required'],
            [['deskripsi'], 'string'],
            [['created_at'], 'safe'],
            [['created_by'], 'integer'],
            [['nama_lokasi'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_lokasi' => 'Nama Lokasi',
            'deskripsi' => 'Deskripsi',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[Barangs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBarangs()
    {
        return $this->hasMany(Barang::className(), ['lokasi_simpan_id' => 'id']);
    }

    //MY FUNCTION START HERE

    public static function listLokasi(){
        return ArrayHelper::map(LokasiSimpan::find()->asArray()->all(),'id','nama_lokasi');
    }

    //MY FUNCTION END HERE
}
