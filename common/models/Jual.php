<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jual".
 *
 * @property int $id_jual
 * @property int $id_barang
 * @property int $id_nasabah
 * @property int $nilai_pinjam
 * @property int $harga_pasar
 * @property int|null $harga_jual
 * @property string $status
 * @property string $created_at
 * @property int $created_by
 */
class Jual extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jual';
    }

    /**
     * {@inheritdoc}
     */

    public function rules()
    {
        return [
            [['id_barang', 'id_nasabah', 'nilai_pinjam', 'harga_pasar', 'status', 'created_by','harga_jual'], 'required'],
            [['id_barang', 'id_nasabah', 'nilai_pinjam', 'harga_pasar', 'harga_jual', 'created_by', 'is_jurnal'], 'integer'],
            [['created_at'], 'safe'],
            [['status'], 'string', 'max' => 225],
        ];
    }

    public function cekRugi($harga_jual,$nilai_pinjam){
        if($harga_jual < $nilai_pinjam){
            return "Rugi";
        }
        else{
            return "Untung";
        }
    }

    public function getBarang(){
        return $this->hasOne(Barang::className(),['id_barang'=>'id_barang']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_jual' => 'Id Jual',
            'id_barang' => 'Id Barang',
            'id_nasabah' => 'Id Nasabah',
            'nilai_pinjam' => 'Nilai Pinjam',
            'harga_pasar' => 'Harga Pasar',
            'harga_jual' => 'Harga Jual',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }
}
