<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\KoordinatorKunci;

/**
 * KoordinatorKunciSearch represents the model behind the search form of `common\models\KoordinatorKunci`.
 */
class KoordinatorKunciSearch extends KoordinatorKunci
{
    /**
     * {@inheritdoc}
     */

    public $cabang_id;

    public function rules()
    {
        return [
            [['id', 'koordinator_id', 'created_by', 'cabang_id'], 'integer'],
            [['username', 'key_code', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KoordinatorKunci::find();
        $query->joinWith(['koordinator']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'koordinator_id' => $this->koordinator_id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'id_cabang' => $this->cabang_id
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'key_code', $this->key_code]);

        return $dataProvider;
    }
}
