<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hari_kerja_pegawai".
 *
 * @property int $id
 * @property int $pegawai_id
 * @property int|null $hari_kerja_id
 * @property int|null $shift_id
 * @property string $status_absensi
 * @property string $created_at
 * @property int $created_by
 *
 * @property HariKerja $hariKerja
 * @property Profile $pegawai
 * @property JadwalShift $shift
 */
class HariKerjaPegawai extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hari_kerja_pegawai';
    }

    /**
     * {@inheritdoc}
     */

    public $range_tgl_kerja;

    public function rules()
    {
        return [
            [['pegawai_id', 'status_absensi', 'created_at', 'created_by'], 'required'],
            [['pegawai_id', 'hari_kerja_id', 'shift_id', 'created_by'], 'integer'],
            [['status_absensi', 'range_tgl_kerja', 'deskripsi'], 'string'],
            [['created_at'], 'safe'],
            [['hari_kerja_id'], 'exist', 'skipOnError' => true, 'targetClass' => HariKerja::className(), 'targetAttribute' => ['hari_kerja_id' => 'id']],
            [['pegawai_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['pegawai_id' => 'id_user']],
            [['shift_id'], 'exist', 'skipOnError' => true, 'targetClass' => JadwalShift::className(), 'targetAttribute' => ['shift_id' => 'id']],
            [['pegawai_id', 'hari_kerja_id'], 'unique', 'targetAttribute' => ['pegawai_id', 'hari_kerja_id']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai_id' => 'Pegawai ID',
            'hari_kerja_id' => 'Hari Kerja ID',
            'shift_id' => 'Shift ID',
            'status_absensi' => 'Status Absensi',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[HariKerja]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHariKerja()
    {
        return $this->hasOne(HariKerja::className(), ['id' => 'hari_kerja_id']);
    }

    /**
     * Gets query for [[Pegawai]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Profile::className(), ['id_user' => 'pegawai_id']);
    }

    /**
     * Gets query for [[Shift]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShift()
    {
        return $this->hasOne(JadwalShift::className(), ['id' => 'shift_id']);
    }

    public static function listStatus(){
        return [
            'HARI KERJA' => 'HARI KERJA',
            'LIBUR NASIONAL' => 'LIBUR NASIONAL',
            'CUTI' => 'CUTI',
            'SAKIT' => 'SAKIT',
            'OFF' => 'OFF'
        ];
    }
}
