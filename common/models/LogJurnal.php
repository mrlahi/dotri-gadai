<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "log_jurnal".
 *
 * @property int $id
 * @property int $akun_kode
 * @property double $debet
 * @property double $kredit
 * @property int $cabang_id
 * @property string $created_at
 * @property int $created_by
 * @property string $jenis_asal_jurnal
 * @property int $asal_id
 *
 * @property Akun $akunKode
 * @property Cabang $cabang
 */
class LogJurnal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_jurnal';
    }

    const SCENARIO_SALDO = 'saldo_awal';
    const SCENARIO_PENYESUAIAN = 'penyesuaian';
    const SCENARIO_MUTASI = 'mutasi';

    public $asal_cabang;
    public $asal_akun;
    public $tujuan_cabang;
    public $tujuan_akun;
    public $jumlah_mutasi;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['akun_kode', 'debet', 'kredit', 'created_at', 'created_by', 'jenis_asal_jurnal'], 'required'],
            [['cabang_id', 'log_transaksi_id', 'asal_id'], 'required', 'except' => ['saldo_awal', 'penyesuaian']],
            [['log_transaksi_id','akun_kode', 'cabang_id', 'created_by', 'asal_id'], 'integer'],
            [['created_at'], 'safe'],
            [['debet', 'kredit',],'double'],
            [['jenis_asal_jurnal'], 'string'],
            [['akun_kode'], 'exist', 'skipOnError' => true, 'targetClass' => Akun::className(), 'targetAttribute' => ['akun_kode' => 'id']],
            [['cabang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cabang::className(), 'targetAttribute' => ['cabang_id' => 'id_cabang']],
            [['asal_cabang', 'asal_akun', 'tujuan_cabang', 'tujuan_akun', 'jumlah_mutasi'], 'required', 'on' => self::SCENARIO_MUTASI]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'akun_kode' => 'Akun Kode',
            'debet' => 'Debet',
            'kredit' => 'Kredit',
            'cabang_id' => 'Cabang ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'jenis_asal_jurnal' => 'Jenis Asal Jurnal',
            'asal_id' => 'Asal ID',
        ];
    }

    /**
     * Gets query for [[AkunKode]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAkunKode()
    {
        return $this->hasOne(Akun::className(), ['id' => 'akun_kode']);
    }

    /**
     * Gets query for [[Cabang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCabang()
    {
        return $this->hasOne(Cabang::className(), ['id_cabang' => 'cabang_id']);
    }


    //MY FUNCTION START HERE

    public function getBarang()
    {
        if($this->jenis_asal_jurnal != "OPERASIONAL" and $this->jenis_asal_jurnal != "MUTASI KAS" and $this->jenis_asal_jurnal != "PENYESUAIAN" and $this->jenis_asal_jurnal != "INVENTORY"){
            return $this->hasOne(Barang::className(), ['id_barang' => 'asal_id']);
        }
    }

    public function noKontrak(){
        if(isset($this->barang->no_kontrak)){
            return $this->barang->no_kontrak;
        }
        else{
            return "TIDAK ADA SBG";
        }
    }

    public function namaNasabah(){
        if(isset($this->barang->nasabah)){
            return $this->barang->nasabah->nama;
        }
        else{
            return "TIDAK ADA DATA";
        }
    }

    public function ktpNasabah(){
        if(isset($this->barang->nasabah)){
            return $this->barang->nasabah->no_ktp;
        }
        else{
            return "TIDAK ADA DATA";
        }
    }

    public function getLogTransaksi()
    {
        return $this->hasOne(LogTransaksi::className(), ['id_log_transaksi' => 'log_transaksi_id']);
    }

    public function inLogJurnal($transaksi_id,$kode_akun,$debet,$kredit,$asal,$asal_id, $ket=""){
        $model = new LogJurnal();
        $model->log_transaksi_id = $transaksi_id;
        $model->akun_kode = $kode_akun;
        $model->debet = $debet;
        $model->kredit = $kredit;
        $model->cabang_id = Profile::myProfile2()->id_cabang;
        $model->created_at = date('Y-m-d');
        $model->created_by = Yii::$app->user->id;
        $model->jenis_asal_jurnal = $asal;
        $model->asal_id = $asal_id;
        $model->keterangan = $ket;
        $model->save(false);
    }

    public function inLogJurnal2($transaksi_id,$kode_akun,$debet,$kredit,$asal,$asal_id, $ket="",$tgl){
        $model = new LogJurnal();
        $model->log_transaksi_id = $transaksi_id;
        $model->akun_kode = $kode_akun;
        $model->debet = $debet;
        $model->kredit = $kredit;
        $model->cabang_id = Profile::myProfile2()->id_cabang;
        $model->created_at = $tgl;
        $model->created_by = Yii::$app->user->id;
        $model->jenis_asal_jurnal = $asal;
        $model->asal_id = $asal_id;
        $model->keterangan = $ket;
        $model->save(false);
    }

    public function inLogJurnalMutasi($transaksi_id,$kode_akun,$debet,$kredit,$asal,$asal_id, $ket="",$cabang_id,$tgl){
        $model = new LogJurnal();
        $model->log_transaksi_id = $transaksi_id;
        $model->akun_kode = $kode_akun;
        $model->debet = $debet;
        $model->kredit = $kredit;
        $model->cabang_id = $cabang_id;
        $model->created_at = $tgl;
        $model->created_by = Yii::$app->user->id;
        $model->jenis_asal_jurnal = $asal;
        $model->asal_id = $asal_id;
        $model->keterangan = $ket;
        $model->save(false);
    }

    public function inLogJurnalPenyusutan($transaksi_id,$kode_akun,$debet,$kredit,$asal,$asal_id, $ket, $created_at, $created_by){
        $model = new LogJurnal();
        $model->log_transaksi_id = $transaksi_id;
        $model->akun_kode = $kode_akun;
        $model->debet = $debet;
        $model->kredit = $kredit;
        $model->cabang_id = "";
        $model->created_at = $created_at;
        $model->created_by = $created_by;
        $model->jenis_asal_jurnal = $asal;
        $model->asal_id = $asal_id;
        $model->keterangan = $ket;
        $model->save(false);
    }

    public function inLogJurnalJual($transaksi_id,$kode_akun,$debet,$kredit,$asal,$asal_id,$tgl_jual,$ket=""){
        $model = new LogJurnal();
        $model->log_transaksi_id = $transaksi_id;
        $model->akun_kode = $kode_akun;
        $model->debet = $debet;
        $model->kredit = $kredit;
        $model->cabang_id = 999;
        $model->created_at = $tgl_jual;
        $model->created_by = Yii::$app->user->id;
        $model->jenis_asal_jurnal = $asal;
        $model->asal_id = $asal_id;
        $model->keterangan = $ket;
        $model->save(false);
    }

    public function inLogJurnalFix($transaksi_id,$kode_akun,$debet,$kredit,$asal,$asal_id, $ket,$cabang_id,$created_at,$created_by){//INI CUMA UNTUK MEMPERBAIKI LOG BEBAN OPERASIONAL
        $model = new LogJurnal();
        $model->log_transaksi_id = $transaksi_id;
        $model->akun_kode = $kode_akun;
        $model->debet = $debet;
        $model->kredit = $kredit;
        $model->cabang_id = $cabang_id;
        $model->created_at = $created_at;
        $model->created_by = $created_by;
        $model->jenis_asal_jurnal = $asal;
        $model->asal_id = $asal_id;
        $model->keterangan = $ket;
        $model->save(false);
    }

    public function inLogBunga($transaksi_id,$id_barang){
        $barang = Barang::findOne($id_barang);
        $bunga = $barang->nilai_pinjam*$barang->bunga/100;
        if($barang->bunga == 1.2){
            $this->inLogJurnal($transaksi_id,4001,0,$bunga,"BARANG",$id_barang,"BUNGA PINJAM");
        }
        else if($barang->bunga == 2.4){
            $this->inLogJurnal($transaksi_id,4002,0,$bunga,"BARANG",$id_barang,"BUNGA PINJAM");
        }
        else if($barang->bunga == 5){
            $this->inLogJurnal($transaksi_id,4003,0,$bunga,"BARANG",$id_barang,"BUNGA PINJAM");
        }
        else if($barang->bunga == 10){
            $this->inLogJurnal($transaksi_id,4004,0,$bunga,"BARANG",$id_barang,"BUNGA PINJAM");
        }
    }
    
    public function inLogBungaPerpanjang($transaksi_id,$id_barang,$bunga){
        $barang = Barang::findOne($id_barang);
        if($barang->bunga == 1.2){
            $this->inLogJurnal($transaksi_id,4005,0,$bunga,"BARANG",$id_barang,"BUNGA PERPANJANG");
        }
        else if($barang->bunga == 2.4){
            $this->inLogJurnal($transaksi_id,4006,0,$bunga,"BARANG",$id_barang,"BUNGA PERPANJANG");
        }
        else if($barang->bunga == 5){
            $this->inLogJurnal($transaksi_id,4007,0,$bunga,"BARANG",$id_barang,"BUNGA PERPANJANG");
        }
        else if($barang->bunga == 10){
            $this->inLogJurnal($transaksi_id,4008,0,$bunga,"BARANG",$id_barang,"BUNGA PERPANJANG");
        }
    }
    
    public function inLogDendaPerpanjang($transaksi_id,$id_barang,$denda,$status){
        if($denda > 0){
            $keterangan = "DENDA ".$status;
            $this->inLogJurnal($transaksi_id,4009,0,$denda,"BARANG",$id_barang,$keterangan);
        }
    }

    public function inLogDendaPelunasan($transaksi_id,$id_barang,$denda,$status){
        if($denda > 0){
            $keterangan = "DENDA ".$status;
            $this->inLogJurnal($transaksi_id,4015,0,$denda,"BARANG",$id_barang,$keterangan);
        }
    }

    public function inLogCashback($transaksi_id,$id_barang,$cashback){
        $dataBarang = Barang::findOne($id_barang);

        if($dataBarang->bunga == 10){
            $noAkun = 4013;
            $keterangan = "CASHBACK 5%";
        }
        else{
            $noAkun = 4014;
            $keterangan = "CASHBACK 1.2%";
        }

        if($cashback > 0){
            $minusCb = $cashback * -1;
            $this->inLogJurnal($transaksi_id,$noAkun,$minusCb,0,"BARANG",$id_barang,$keterangan);
        }

    }

    public function inLogAdmin($transaksi_id,$id_barang){
        $barang = Barang::findOne($id_barang);
        if($barang->biaya_admin > 0){
            $this->inLogJurnal($transaksi_id,4011,0,$barang->biaya_admin,"BARANG",$id_barang,"BIAYA ADMIN");
        }
    }   

    public function inBarang($id_barang){
        $mTransaksi = new LogTransaksi();
        $mTransaksi->inTransaksi("GADAI","GADAI BARANG BARU");
        $transaksi_id = $mTransaksi->id_log_transaksi;

        $barang = Barang::findOne($id_barang);
        $this->inLogJurnal($transaksi_id,1106,$barang->nilai_pinjam,0,"BARANG",$id_barang);
        $this->inLogJurnal($transaksi_id,1101,0,$barang->pinjamBersih(),"BARANG",$id_barang);
        $this->inLogBunga($transaksi_id,$id_barang);
        $this->inLogAdmin($transaksi_id,$id_barang);
    }

    public function inAngsuran($id_barang,$jumlah_cicil,$kas_id){
        $mTransaksi = new LogTransaksi();
        $mTransaksi->inTransaksi("ANGSURAN","PEMBAYARAN ANGSURAN");
        $transaksi_id = $mTransaksi->id_log_transaksi;

        $dataKas = KasBank::findOne($kas_id);
        $kode_akun = $dataKas->akun_kode;
        $this->inLogJurnal($transaksi_id,$kode_akun,$jumlah_cicil,0,"BARANG",$id_barang, "ANGSURAN");
        $this->inLogJurnal($transaksi_id,1106,0,$jumlah_cicil,"BARANG",$id_barang,"ANGSURAN");
    }

    public function inPerpanjang($id_barang,$bunga,$denda,$kas_id){
        $mTransaksi = new LogTransaksi();
        $mTransaksi->inTransaksi("PERPANJANG","PERPANJANG BARANG");
        $transaksi_id = $mTransaksi->id_log_transaksi;

        $dataKas = KasBank::findOne($kas_id);
        $kode_akun = $dataKas->akun_kode;
        $total = $bunga + $denda;
        $this->inLogJurnal($transaksi_id,$kode_akun,$total,0,"BARANG",$id_barang, "PERPANJANGAN");
        $this->inLogBungaPerpanjang($transaksi_id,$id_barang,$bunga);
        $this->inLogDendaPerpanjang($transaksi_id,$id_barang,$denda,"PERPANJANGAN");
    }

    public function inPelunasan($id_barang,$jumlah_uang,$denda,$cashback,$kas_id){
        $mTransaksi = new LogTransaksi();
        $mTransaksi->inTransaksi("PELUNASAN","PELUNASAN BARANG");
        $transaksi_id = $mTransaksi->id_log_transaksi;

        $dataKas = KasBank::findOne($kas_id);
        $kode_akun = $dataKas->akun_kode;

        $bayarHutang = $jumlah_uang;
        $masukKas = $jumlah_uang;

        if($cashback > 1){
            $bayarHutang = $jumlah_uang + $cashback;
            $masukKas = $jumlah_uang;
        }

        if($denda > 1){
            $bayarHutang = $jumlah_uang;
            $masukKas = $jumlah_uang + $denda;
        }

        $this->inLogJurnal($transaksi_id,$kode_akun,$masukKas,0,"BARANG",$id_barang, "PELUNASAN");
        $this->inLogCashback($transaksi_id,$id_barang,$cashback);
        $this->inLogJurnal($transaksi_id,1106,0,$bayarHutang,"BARANG",$id_barang,"POKOK PINJAMAN");
        $this->inLogDendaPelunasan($transaksi_id,$id_barang,$denda,"PELUNASAN");
    }

    public function inPelunasanPasif($id_barang,$jumlah_uang,$denda,$bunga,$kas_id){
        $mTransaksi = new LogTransaksi();
        $mTransaksi->inTransaksi("PELUNASAN","PELUNASAN BARANG");
        $transaksi_id = $mTransaksi->id_log_transaksi;
        $dataKas = KasBank::findOne($kas_id);
        $kode_akun = $dataKas->akun_kode;
        $bayarHutang = $jumlah_uang;
        $masukKas = $jumlah_uang + $bunga + $denda;
        $this->inLogJurnal($transaksi_id,$kode_akun,$masukKas,0,"BARANG",$id_barang, "PELUNASAN");
        $this->inLogJurnal($transaksi_id,1106,0,$bayarHutang,"BARANG",$id_barang,"POKOK PINJAMAN");
        $this->inLogJurnal($transaksi_id,4016,0,$bunga,"BARANG",$id_barang,"BUNGA PELUNASAN");
        $this->inLogDendaPelunasan($transaksi_id,$id_barang,$denda,"PELUNASAN");
    }

    // public function inPenyesuaian($id_penyesuaian){
    //     $dataPenyesuaian = LogPenyesuaian::findOne($id_penyesuaian);
    //     $kode_akun = $dataPenyesuaian->akun_id;

    //     if($dataPenyesuaian->jenis_penyesuaian == "DEBET"){
    //         $this->inLogJurnal($kode_akun,$dataPenyesuaian->jumlah_uang,0,"PENYESUAIAN",$id_penyesuaian);
    //     }
    //     else{
    //         $this->inLogJurnal($kode_akun,0,$dataPenyesuaian->jumlah_uang,"PENYESUAIAN",$id_penyesuaian);
    //     }
    // }

    // public function inLogJual($id_barang){
    //     $mTransaksi = new LogTransaksi();
    //     $mTransaksi->inTransaksi("PENJUALAN","PENJUALAN BARANG");
    //     $transaksi_id = $mTransaksi->id_log_transaksi;

    //     $jual = Jual::find()->where(['id_barang' => $id_barang])->one();
    //     $selisih = $jual->harga_jual - $jual->nilai_pinjam;

    //     $this->inLogJurnalJual($transaksi_id,1102,$jual->harga_jual,0,"BARANG",$id_barang,"PENJUALAN BARANG");
        
    //     if($jual->harga_jual < $jual->nilai_pinjam){//jika penjualan rugi
    //         $positifSelisih = $selisih * -1;
    //         $this->inLogJurnalJual($transaksi_id,5015,$positifSelisih,0,"BARANG",$id_barang,"KERUGIAN PENJUALAN BARANG");
    //     }

    //     $this->inLogJurnalJual($transaksi_id,1106,0,$jual->nilai_pinjam,"BARANG",$id_barang,"PENJUALAN BARANG");
        
    //     if($selisih > 0){//jika penjualan untung
    //         $this->inLogJurnalJual($transaksi_id,4012,0,$selisih,"BARANG",$id_barang,"KEUNTUNGAN PENJUALAN BARANG");
    //     }

    // }

    public function inOperasional($operasional_id){
        $operasional = BebanOperasional::findOne($operasional_id);
        $debet = $operasional->jenisBiaya->akun_id;
        $kredit = $operasional->kasBank->akun_kode;
        $jumlah = $operasional->jumlah_pengeluaran;

        $mTransaksi = new LogTransaksi();
        $mTransaksi->inTransaksi("OPERASIONAL","PENGELUARAN BIAYA OPERASIONAL");
        $transaksi_id = $mTransaksi->id_log_transaksi;

        $keterangan = "PENGELUARAN OPERASIONAL - ".$operasional->jenisBiaya->nama_jenis;

        $this->inLogJurnal($transaksi_id,$debet,$jumlah,0,"OPERASIONAL",$operasional_id,$keterangan);
        $this->inLogJurnal($transaksi_id,$kredit,0,$jumlah,"OPERASIONAL",$operasional_id,$keterangan);
        
    }

    public function inOperasionalFix($operasional_id){
        $operasional = BebanOperasional::findOne($operasional_id);
        $debet = $operasional->jenisBiaya->akun_id;
        $kredit = $operasional->kasBank->akun_kode;
        $jumlah = $operasional->jumlah_pengeluaran;

        $mTransaksi = new LogTransaksi();
        $mTransaksi->inTransaksiFix("OPERASIONAL","PENGELUARAN BIAYA OPERASIONAL",$operasional->id_cabang,$operasional->created_at,$operasional->created_by);
        $transaksi_id = $mTransaksi->id_log_transaksi;

        $keterangan = "PENGELUARAN OPERASIONAL - ".$operasional->jenisBiaya->nama_jenis;

        $this->inLogJurnalFix($transaksi_id,$debet,$jumlah,0,"OPERASIONAL",$operasional_id,$keterangan,$operasional->id_cabang,$operasional->created_at,$operasional->created_by);
        $this->inLogJurnalFix($transaksi_id,$kredit,0,$jumlah,"OPERASIONAL",$operasional_id,$keterangan,$operasional->id_cabang,$operasional->created_at,$operasional->created_by);
        
    }

    public function deleteOperasional($id){
        $dataJurnal = $this->find()->where(['asal_id' => $id, 'jenis_asal_jurnal' => 'OPERASIONAL'])->one();
        $log_transaksi_id = $dataJurnal->log_transaksi_id;
        
        $transaksi = LogTransaksi::findOne($log_transaksi_id);
        $transaksi->delete();
    }

    public function mutasiKas(){
        $mTransaksi = new LogTransaksi();

        $cabangAsal = Cabang::findOne($this->asal_cabang);
        $cabangTujuan = Cabang::findOne($this->tujuan_cabang);
        $asalAkun = $this->asal_akun;
        $tujuanAkun = $this->tujuan_akun;
        $jumlahMutasi = $this->jumlah_mutasi;
        $deskripsi = "MUTASI DARI ".$cabangAsal->nama_cabang." KE ".$cabangTujuan->nama_cabang;

        $mTransaksi->inTransaksi("MUTASI KAS",$deskripsi,$this->created_at);
        $transaksi_id = $mTransaksi->id_log_transaksi;

        $this->inLogJurnalMutasi($transaksi_id,$tujuanAkun,$jumlahMutasi,0,"MUTASI KAS",$transaksi_id,$deskripsi,$this->tujuan_cabang,$this->created_at);
        $this->inLogJurnalMutasi($transaksi_id,$asalAkun,0,$jumlahMutasi,"MUTASI KAS",$transaksi_id,$deskripsi,$this->asal_cabang,$this->created_at);

        // $mUangKeluar = new LogUangKeluar();
        // $mModal = new LogModal();

        // $mUangKeluar->nama_pengeluaran = "PEMBERIAN KAS KE ".$cabangTujuan->nama_cabang;
        // $mUangKeluar->jumlah_pengeluaran = $jumlahMutasi;
        

    }

    public function tDebetSebelumnya($cabang_id){
        $id = $this->id;
        $akun_kode = $this->akun_kode;
        $created_at = $this->created_at;

        if($cabang_id == 1000000){
            $debet = LogJurnal::find()->where(['akun_kode' => $akun_kode])->andWhere(['<=', 'created_at', $created_at])->andWhere(['<=', 'id', $id])->orderBy(['created_at' => SORT_ASC, 'id' => SORT_ASC])->sum('debet');
        }
        else{
            $debet = LogJurnal::find()->where(['akun_kode' => $akun_kode, 'cabang_id' => $cabang_id])->andWhere(['<=', 'created_at', $created_at])->andWhere(['<=', 'id', $id])->orderBy(['created_at' => SORT_ASC, 'id' => SORT_ASC])->sum('debet');
        }
        

        return $debet;
    }

    public function tKreditSebelumnya($cabang_id){
        $id = $this->id;
        $akun_kode = $this->akun_kode;
        $created_at = $this->created_at;

        if($cabang_id == 1000000){
            $kredit = LogJurnal::find()->where(['akun_kode' => $akun_kode])->andWhere(['<=', 'created_at', $created_at])->andWhere(['<=', 'id', $id])->orderBy(['created_at' => SORT_ASC, 'id' => SORT_ASC])->sum('kredit');
        }
        else{
            $kredit = LogJurnal::find()->where(['akun_kode' => $akun_kode, 'cabang_id' => $cabang_id])->andWhere(['<=', 'created_at', $created_at])->andWhere(['<=', 'id', $id])->orderBy(['created_at' => SORT_ASC, 'id' => SORT_ASC])->sum('kredit');
        }

        return $kredit;

    }

    public function saldoSebelumnya($tgl,$akun_kode){
        $dataAkun = Akun::findOne($akun_kode);
        $debet = LogJurnal::find()->where(['akun_kode' => $akun_kode])->andWhere(['<', 'created_at', $tgl])->sum('debet');
        $kredit = LogJurnal::find()->where(['akun_kode' => $akun_kode])->andWhere(['<', 'created_at', $tgl])->sum('kredit');

        if($dataAkun->saldo_normal == "DEBET"){
            $saldo = $debet - $kredit;
        }
        else{
            $saldo = $kredit - $debet;
        }

        return $saldo;
    }

    public function labaTahunBerjalan($tgl,$jumlah,$cabang_id,$labaRugi){
        $cekData = LogJurnal::find()->where(['cabang_id' => $cabang_id, 'created_at' => $tgl, 'akun_kode' => 3003])->one();
        if($cekData){
            LogJurnal::deleteAll(['id' => $cekData->id]);
        }
        if($labaRugi == "laba"){
            $transaksi_id = "";
            $debet = 0;
            $kredit = $jumlah;
            $operasional_id = "";
            $keterangan = "LABA";

            $model = new LogJurnal();
            $model->log_transaksi_id = "";
            $model->akun_kode = 3003;
            $model->debet = $debet;
            $model->kredit = $kredit;
            $model->cabang_id = $cabang_id;
            $model->created_at = $tgl;
            $model->created_by = 1;
            $model->jenis_asal_jurnal = "LABA TAHUN BERJALAN";
            $model->asal_id = "";
            $model->keterangan = $keterangan;
            $model->save(false);

        }
        else{
            $transaksi_id = "";
            $debet = $jumlah;
            $kredit = 0;
            $operasional_id = "";
            $keterangan = "RUGI";
            
            $model = new LogJurnal();
            $model->log_transaksi_id = "";
            $model->akun_kode = 3003;
            $model->debet = $debet;
            $model->kredit = $kredit;
            $model->cabang_id = $cabang_id;
            $model->created_at = $tgl;
            $model->created_by = 1;
            $model->jenis_asal_jurnal = "LABA TAHUN BERJALAN";
            $model->asal_id = "";
            $model->keterangan = $keterangan;
            $model->save(false);

        }

    }

    public function saldoAwal($cabang_id){
        if($cabang_id == 1000000){
            $saldo = LogJurnal::find()->where(['or',['akun_kode' => 1101],['akun_kode' => 1102], ['akun_kode' => 1103], ['akun_kode' => 1104], ['akun_kode' => 1105]])->andWhere(['jenis_asal_jurnal' => 'SALDO AWAL'])->sum('debet');
        }
        else{
            $saldo = LogJurnal::find()->where(['or',['akun_kode' => 1101], ['akun_kode' => 1102], ['akun_kode' => 1103], ['akun_kode' => 1104], ['akun_kode' => 1105]])->andWhere(['jenis_asal_jurnal' => 'SALDO AWAL','cabang_id' => $cabang_id])->sum('debet');
        }
        return $saldo;
    }

    public function inInventory($id_inventory,$kode_akun){
        $mTransaksi = new LogTransaksi();

        $dataInventory = Inventory::findOne($id_inventory);

        $keterangan = "Pengadaan Barang : ".$dataInventory->nama_barang." ".$dataInventory->merk_barang." ".$dataInventory->type_barang.". NO. INVENTORI : ".$dataInventory->no_inventaris;

        $mTransaksi->inTransaksiInventory("INVENTORY",$keterangan,$dataInventory->tgl_perolehan,$dataInventory->created_by);
        $transaksi_id = $mTransaksi->id_log_transaksi;

        if($dataInventory->jenis_inventaris == "KANTOR"){
            $kode_akun1 = 1201;
            $deskripsi = "PENGADAAN ASET KANTOR";
        }
        else{
            $kode_akun1 = 1203;
            $deskripsi = "PENGADAAN ASET KENDARAAN";
        }

        $kode_akun2 = $kode_akun;

        $this->inLogJurnal2($transaksi_id,$kode_akun1,$dataInventory->harga_perolehan,0,"INVENTORY",$id_inventory, $deskripsi, $dataInventory->tgl_perolehan);
        $this->inLogJurnal2($transaksi_id,$kode_akun,0,$dataInventory->harga_perolehan,"INVENTORY",$id_inventory, $deskripsi, $dataInventory->tgl_perolehan);
    }

    public function inPenyusutanInventory($log_penyusutan_id){
        $dataLog = LogPenyusutanAset::findOne($log_penyusutan_id);
        $keterangan = "NO INVENTORI : ".$dataLog->inventaris->no_inventaris;
        $mTransaksi = new LogTransaksi();
        $mTransaksi->inTransaksiInventory("INVENTORY",$keterangan, $dataLog->tgl_penyusutan, $dataLog->created_by);
        $transaksi_id = $mTransaksi->id_log_transaksi;

        $jenis_inventaris = $dataLog->inventaris->jenis_inventaris;

        if($jenis_inventaris == "KANTOR"){
            $kode_akun1 = 5016;
            $kode_akun2 = 1202;
            $deskripsi = "PENYUSUTAN ASET KANTOR";
        }
        else{
            $kode_akun1 = 5017;
            $kode_akun2 = 1204;
            $deskripsi = "PENYUSUTAN ASET KENDARAAN";
        }

        $this->inLogJurnalPenyusutan($transaksi_id,$kode_akun1,$dataLog->nilai_penyusutan,0,"INVENTORY",$log_penyusutan_id, $deskripsi, $dataLog->tgl_penyusutan, $dataLog->tgl_penyusutan, $dataLog->created_by);
        $this->inLogJurnalPenyusutan($transaksi_id,$kode_akun2,0,$dataLog->nilai_penyusutan,"INVENTORY",$log_penyusutan_id, $deskripsi, $dataLog->tgl_penyusutan, $dataLog->tgl_penyusutan, $dataLog->created_by);

        $dataLog->is_penyusutan = 1;
        $dataLog->save();

    }

    //MY FUNCTION END HERE
}
