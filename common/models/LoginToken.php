<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "login_token".
 *
 * @property int $id
 * @property int $pegawai_id
 * @property int $koordinator_id
 * @property int $cabang_id
 * @property string $token
 * @property string $created_at
 *
 * @property Cabang $cabang
 * @property Profile $koordinator
 * @property Profile $pegawai
 */
class LoginToken extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'login_token';
    }

    /**
     * {@inheritdoc}
     */

    public $username;
    public $key;

    public function rules()
    {
        return [
            [['pegawai_id', 'koordinator_id', 'cabang_id', 'token', 'created_at'], 'required'],
            [['pegawai_id', 'koordinator_id', 'cabang_id'], 'integer'],
            [['created_at'], 'safe'],
            [['token', 'username', 'key'], 'string', 'max' => 255],
            [['cabang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cabang::className(), 'targetAttribute' => ['cabang_id' => 'id_cabang']],
            [['koordinator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['koordinator_id' => 'id_user']],
            [['pegawai_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['pegawai_id' => 'id_user']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai_id' => 'Pegawai ID',
            'koordinator_id' => 'Koordinator ID',
            'cabang_id' => 'Cabang ID',
            'token' => 'Token',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Cabang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCabang()
    {
        return $this->hasOne(Cabang::className(), ['id_cabang' => 'cabang_id']);
    }

    /**
     * Gets query for [[Koordinator]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKoordinator()
    {
        return $this->hasOne(Profile::className(), ['id_user' => 'koordinator_id']);
    }

    /**
     * Gets query for [[Pegawai]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Profile::className(), ['id_user' => 'pegawai_id']);
    }
}
