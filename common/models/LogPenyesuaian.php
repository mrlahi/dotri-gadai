<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "log_penyesuaian".
 *
 * @property int $id
 * @property int $akun_id
 * @property string $jenis_penyesuaian
 * @property int $jumlah_uang
 * @property string $deskripsi
 * @property int $cabang_id
 * @property string $created_at
 * @property int $created_by
 *
 * @property Akun $akun
 */
class LogPenyesuaian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_penyesuaian';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['akun_id', 'jenis_penyesuaian', 'jumlah_uang', 'deskripsi', 'cabang_id', 'created_at', 'created_by'], 'required'],
            [['akun_id', 'jumlah_uang', 'cabang_id', 'created_by'], 'integer'],
            [['jenis_penyesuaian', 'deskripsi'], 'string'],
            [['created_at'], 'safe'],
            [['akun_id'], 'exist', 'skipOnError' => true, 'targetClass' => Akun::className(), 'targetAttribute' => ['akun_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'akun_id' => 'Akun ID',
            'jenis_penyesuaian' => 'Jenis Penyesuaian',
            'jumlah_uang' => 'Jumlah Uang',
            'deskripsi' => 'Deskripsi',
            'cabang_id' => 'Cabang ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[Akun]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAkun()
    {
        return $this->hasOne(Akun::className(), ['id' => 'akun_id']);
    }
}
