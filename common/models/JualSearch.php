<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Jual;

/**
 * JualSearch represents the model behind the search form of `common\models\Jual`.
 */
class JualSearch extends Jual
{
    /**
     * {@inheritdoc}
     */

    public $no_kontrak;
    public $nama_barang;
    public $merk;
    public $tipe;
    public $tgl_masuk;
    public $jatuh_tempo;
    public $id_pegawai;
    public $id_cabang;
    public $tgl_jual;
    public $serial;
    public $id_jenis;

    public function rules()
    {
        return [
            [['id_jual', 'id_barang', 'id_nasabah', 'nilai_pinjam', 'harga_pasar', 'harga_jual', 'created_by', 'is_jurnal'], 'integer'],
            [['status', 'created_at', 'no_kontrak', 'nama_barang', 'merk', 'tipe', 'tgl_masuk', 'jatuh_tempo', 'id_pegawai', 'id_cabang', 'tgl_jual', 'serial', 'id_jenis'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Jual::find();
        $query->joinWith(['barang']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_jual' => $this->id_jual,
            'id_barang' => $this->id_barang,
            'id_nasabah' => $this->id_nasabah,
            'nilai_pinjam' => $this->nilai_pinjam,
            'harga_pasar' => $this->harga_pasar,
            'harga_jual' => $this->harga_jual,
            'created_by' => $this->created_by,
            'barang.created_by' => $this->id_pegawai,
            'barang.id_cabang' => $this->id_cabang,
            'barang.id_jenis' => $this->id_jenis,
            'is_jurnal' => $this->is_jurnal
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
        ->andFilterWhere(['like', 'no_kontrak', $this->no_kontrak])
        ->andFilterWhere(['like', 'nama_barang', $this->nama_barang])
        ->andFilterWhere(['like', 'merk', $this->merk])
        ->andFilterWhere(['like', 'tipe', $this->tipe])
        ->andFilterWhere(['like', 'jatuh_tempo', $this->jatuh_tempo])
        ->andFilterWhere(['like', 'serial', $this->serial]);

        if(isset ($this->tgl_masuk) && $this->tgl_masuk!=''){
            $date_explode=explode(" - ",$this->tgl_masuk);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);
            $query->andFilterWhere(['between','tgl_masuk',$date1,$date2]);
        }

        if(isset ($this->tgl_jual) && $this->tgl_jual!=''){
            $date_explode=explode(" - ",$this->tgl_jual);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);
            $query->andFilterWhere(['between','created_at',$date1,$date2]);
        }

        return $dataProvider;
    }
}
