<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LogNotifikasi;

/**
 * LogNotifikasiSearch represents the model behind the search form of `common\models\LogNotifikasi`.
 */
class LogNotifikasiSearch extends LogNotifikasi
{
    /**
     * {@inheritdoc}
     */

    public $no_sbg;
    public $cabang_id;
    public $no_hp_nasabah;

    public function rules()
    {
        return [
            [['id', 'barang_id'], 'integer'],
            [['status_wa', 'status_sms', 'created_at', 'jenis_notifikasi', 'device_id', 'message_id', 'message', 'no_sbg', 'cabang_id', 'no_hp_nasabah'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogNotifikasi::find();
        $query->joinWith('barang');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'barang_id' => $this->barang_id,
            'created_at' => $this->created_at,
            'barang.id_cabang' => $this->cabang_id,
        ]);

        $query->andFilterWhere(['like', 'status_wa', $this->status_wa])
            ->andFilterWhere(['like', 'status_sms', $this->status_sms])
            ->andFilterWhere(['like', 'jenis_notifikasi', $this->jenis_notifikasi])
            ->andFilterWhere(['like', 'device_id', $this->device_id])
            ->andFilterWhere(['like', 'message_id', $this->message_id])
            ->andFilterWhere(['like', 'message', $this->message])
            ->andFilterWhere(['like', 'barang.no_kontrak', $this->no_sbg])
            ->andFilterWhere(['like', 'no_hp_nasabah', $this->no_hp_nasabah]);

        return $dataProvider;
    }
}
