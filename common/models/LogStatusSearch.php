<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LogStatus;

/**
 * LogStatusSearch represents the model behind the search form of `common\models\LogStatus`.
 */
class LogStatusSearch extends LogStatus
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_status', 'id_barang', 'created_by'], 'integer'],
            [['status', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogStatus::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_status' => $this->id_status,
            'id_barang' => $this->id_barang,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'log_status.status', $this->status]);

        /*if(isset ($this->created_at)&&$this->created_at!=''){ 
            $date_explode=explode("-",$this->created_at);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);
            $query->andFilterWhere(['between','created_at',$date1,$date2]);
          }
          */

        return $dataProvider;
    }
}
