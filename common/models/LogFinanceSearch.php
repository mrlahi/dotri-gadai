<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LogFinance;

/**
 * LogFinanceSearch represents the model behind the search form of `common\models\LogFinance`.
 */
class LogFinanceSearch extends LogFinance
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_log_finance', 'jumlah_uang', 'created_by', 'id_cabang', 'id_barang'], 'integer'],
            [['status', 'created_at', 'type_pembayaran'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogFinance::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_log_finance' => $this->id_log_finance,
            'jumlah_uang' => $this->jumlah_uang,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'id_cabang' => $this->id_cabang,
            'id_barang' => $this->id_barang,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'type_pembayaran', $this->type_pembayaran]);

        return $dataProvider;
    }
}
