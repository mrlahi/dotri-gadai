<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tambahan_jual".
 *
 * @property int $id_tambah_jual
 * @property int $id_barang
 * @property string $desk_tambahan
 * @property int $biaya_tambah
 * @property int $created_by
 * @property string $created_at
 *
 * @property Barang $barang
 */
class TambahanJual extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tambahan_jual';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_barang', 'desk_tambahan', 'biaya_tambah', 'created_by', 'created_at', 'jenis_tambahan'], 'required'],
            [['id_barang', 'biaya_tambah', 'created_by'], 'integer'],
            [['desk_tambahan', 'jenis_tambahan'], 'string'],
            [['created_at'], 'safe'],
            [['id_barang'], 'exist', 'skipOnError' => true, 'targetClass' => Barang::className(), 'targetAttribute' => ['id_barang' => 'id_barang']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_tambah_jual' => 'Id Tambah Jual',
            'id_barang' => 'Id Barang',
            'desk_tambahan' => 'Desk Tambahan',
            'biaya_tambah' => 'Biaya Tambah',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Barang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBarang()
    {
        return $this->hasOne(Barang::className(), ['id_barang' => 'id_barang']);
    }

    public function myTotal($id_barang){
        return $this->find()->where(['id_barang' => $id_barang])->sum('biaya_tambah');
    }

    public function listTambah($id_barang){
        $data = $this->find()->where(['id_barang' => $id_barang])->all();
        $total = $this->find()->where(['id_barang' => $id_barang])->sum('biaya_tambah');
        $list = "";

        $biaya_penjualan = 0;
        $biaya_service = 0;
        $denda = 0;
        $bunga = 0;

        if($data){
            foreach($data as $datas){
                $list = $list.$datas->desk_tambahan." : ".number_format($datas->biaya_tambah,'0',',','.')."<br>";

                if($datas->jenis_tambahan == "BIAYA PENJUALAN"){
                    $biaya_penjualan += $datas->biaya_tambah;
                }
                else if($datas->jenis_tambahan == "BIAYA SERVICE"){
                    $biaya_service += $datas->biaya_tambah;
                }
                else if($datas->jenis_tambahan == "DENDA"){
                    $denda += $datas->biaya_tambah;
                }
                else if($datas->jenis_tambahan == "BUNGA"){
                    $bunga += $datas->biaya_tambah;
                }
            }
        }
        else{
            $list = "Tidak Ada Biaya";
        }
        return ['list' => $list, 'total' => $total, 'biaya_penjualan' => $biaya_penjualan, 'biaya_service' => $biaya_service, 'denda' => $denda, 'bunga' => $bunga];
    }
}
