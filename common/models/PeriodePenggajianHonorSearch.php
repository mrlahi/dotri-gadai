<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PeriodePenggajianHonor;

/**
 * PeriodePenggajianHonorSearch represents the model behind the search form of `common\models\PeriodePenggajianHonor`.
 */
class PeriodePenggajianHonorSearch extends PeriodePenggajianHonor
{
    /**
     * {@inheritdoc}
     */

    public $nama_pegawai;

    public function rules()
    {
        return [
            [['id', 'pegawai_id', 'honor_id', 'periode_id', 'created_by'], 'integer'],
            [['created_at', 'nama_pegawai'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PeriodePenggajianHonor::find();
        $query->joinWith(['pegawai']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'pegawai_id' => $this->pegawai_id,
            'honor_id' => $this->honor_id,
            'periode_id' => $this->periode_id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama_pegawai]);

        return $dataProvider;
    }
}
