<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ListLaporan;

/**
 * ListLaporanSearch represents the model behind the search form of `common\models\ListLaporan`.
 */
class ListLaporanSearch extends ListLaporan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'akun_id', 'created_by'], 'integer'],
            [['jenis_laporan', 'created_at', 'posisi_perhitungan_khusus'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ListLaporan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'akun_id' => $this->akun_id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'posisi_perhitungan_khusus' => $this->posisi_perhitungan_khusus,
        ]);

        $query->andFilterWhere(['like', 'jenis_laporan', $this->jenis_laporan]);

        return $dataProvider;
    }
}
