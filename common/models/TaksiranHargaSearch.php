<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TaksiranHarga;

/**
 * TaksiranHargaSearch represents the model behind the search form of `common\models\TaksiranHarga`.
 */
class TaksiranHargaSearch extends TaksiranHarga
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'merk_id', 'type_barang_id', 'is_active', 'created_by'], 'integer'],
            [['tgl_aktif', 'tgl_non_aktif', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaksiranHarga::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'merk_id' => $this->merk_id,
            'type_barang_id' => $this->type_barang_id,
            'tgl_aktif' => $this->tgl_aktif,
            'tgl_non_aktif' => $this->tgl_non_aktif,
            'is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        return $dataProvider;
    }
}
