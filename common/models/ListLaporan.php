<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "list_laporan".
 *
 * @property int $id
 * @property string $jenis_laporan
 * @property int $akun_id
 * @property string $created_at
 * @property int $created_by
 *
 * @property Akun $akun
 */
class ListLaporan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'list_laporan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis_laporan', 'akun_id', 'created_at', 'created_by'], 'required'],
            [['jenis_laporan', 'posisi_perhitungan_khusus'], 'string'],
            [['akun_id', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['akun_id'], 'exist', 'skipOnError' => true, 'targetClass' => Akun::className(), 'targetAttribute' => ['akun_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_laporan' => 'Jenis Laporan',
            'akun_id' => 'Akun ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[Akun]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAkun()
    {
        return $this->hasOne(Akun::className(), ['id' => 'akun_id']);
    }
}
