<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rekapitulasi_absensi_pegawai".
 *
 * @property int $id
 * @property int $pegawai_id
 * @property int $hari_kerja_pegawai_id
 * @property string $tgl_kerja
 * @property string $status_absen
 * @property string|null $log_in
 * @property string|null $log_out
 * @property string $status_log_in
 * @property string $status_log_out
 * @property int|null $waktu_telat
 * @property int|null $waktu_cepat
 * @property string $created_at
 * @property int $created_by
 *
 * @property HariKerjaPegawai $hariKerjaPegawai
 * @property Profile $pegawai
 */
class RekapitulasiAbsensiPegawai extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rekapitulasi_absensi_pegawai';
    }

    /**
     * {@inheritdoc}
     */

    public $range_hari_kerja;
    public $absensi_id;

    public function rules()
    {
        return [
            [['pegawai_id', 'hari_kerja_pegawai_id', 'tgl_kerja', 'status_absen', 'status_log_in', 'status_log_out', 'created_at', 'created_by'], 'required'],
            [['pegawai_id', 'hari_kerja_pegawai_id', 'waktu_telat', 'waktu_cepat', 'created_by', 'waktu_lebur'], 'integer'],
            [['tgl_kerja', 'log_in', 'log_out', 'created_at', 'range_hari_kerja', 'absensi_id'], 'safe'],
            [['status_absen', 'status_log_in', 'status_log_out'], 'string'],
            [['hari_kerja_pegawai_id'], 'exist', 'skipOnError' => true, 'targetClass' => HariKerjaPegawai::className(), 'targetAttribute' => ['hari_kerja_pegawai_id' => 'id']],
            [['pegawai_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['pegawai_id' => 'id_user']],
            [['hari_kerja_pegawai_id'],'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai_id' => 'Pegawai ID',
            'hari_kerja_pegawai_id' => 'Hari Kerja Pegawai ID',
            'tgl_kerja' => 'Tgl Kerja',
            'status_absen' => 'Status Absen',
            'log_in' => 'Log In',
            'log_out' => 'Log Out',
            'status_log_in' => 'Status Log In',
            'status_log_out' => 'Status Log Out',
            'waktu_telat' => 'Waktu Telat',
            'waktu_cepat' => 'Waktu Cepat',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[HariKerjaPegawai]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHariKerjaPegawai()
    {
        return $this->hasOne(HariKerjaPegawai::className(), ['id' => 'hari_kerja_pegawai_id']);
    }

    /**
     * Gets query for [[Pegawai]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(Profile::className(), ['id_user' => 'pegawai_id']);
    }

    public function cekLog($pegawai_id,$tgl,$jenis){
        $dataPegawai = Profile::findOne($pegawai_id);
        $absensi_id = $dataPegawai->absensi_id;


        if($jenis == "IN"){
            $cek = CheckInOut::find()->where(['absen_id' => $absensi_id, 'tgl_absen' => $tgl])->orderBy(['jam_absen' => SORT_ASC])->one();

            if($cek){
                return $cek->jam_absen;
            }
            else{
                return NULL;
            }
        }
        else{
            $cek = CheckInOut::find()->where(['absen_id' => $absensi_id, 'tgl_absen' => $tgl])->orderBy(['jam_absen' => SORT_DESC])->one();

            if($cek){
                return $cek->jam_absen;
            }
            else{
                return NULL;
            }
        }
    }

    public function selisih($jam1,$jam2){
        $t1 = strtotime($jam1);
        $t2 = strtotime($jam2);
        $diff = $t1 - $t2;
        
        return $diff/60;
    }

    public function rekapAbsen($pegawai_id,$date1,$date2){

        RekapitulasiAbsensiPegawai::deleteAll(['AND', ['pegawai_id' => $pegawai_id],['between', 'tgl_kerja', $date1, $date2]]); //MENGHAPUS REKAP SEBELUMNYA

        $dataRange = HariKerjaPegawai::find()->joinWith(['hariKerja'])->where(['pegawai_id' => $pegawai_id])->andWhere(['between', 'tgl_kerja', $date1, $date2])->all();

        foreach($dataRange as $kerja){
            $jamLogIn = $this->cekLog($pegawai_id,$kerja->hariKerja->tgl_kerja,"IN");
            $jamLogOut = $this->cekLog($pegawai_id,$kerja->hariKerja->tgl_kerja,"OUT");

            $status_absen = $kerja->status_absensi;

            $shiftIn = $kerja->shift->jam_masuk;
            $shiftOut = $kerja->shift->jam_pulang;

            $telat = 0;
            $cepat = 0;

            $lembur = 0;

            $status_log_in = "TEPAT";
            $status_log_out = "TEPAT";

            if(empty($jamLogIn) && empty($jamLogOut)){
                $jamLogIn = NULL;
                $status_log_in = "TIDAK LOG IN";
                $jamLogIn = NULL;
                $status_log_out = "TIDAK LOG OUT";

                $telat = "";
                $cepat = "";
                
                if($status_absen == "HARI KERJA"){
                    $status_absen = "TIDAK HADIR";
                }
            }
            else if($jamLogIn > $shiftIn){
                $telat = $this->selisih($jamLogIn,$shiftIn);
                if($telat >= 30){
                    $jamLogIn = NULL;
                    $status_log_in = "TIDAK LOG IN";
                    $telat = 0;
                }
                else{
                    $status_log_in = "TELAT";
                }
            }
            else if($shiftOut > $jamLogOut){
                $cepat  = $this->selisih($shiftOut,$jamLogOut);
                if($cepat >= 30){
                    $jamLogOut = NULL;
                    $status_log_out = "TIDAK LOG OUT";
                    $cepat = 30;
                }
                else{
                    $status_log_out = "CEPAT";
                }
            }

            if($kerja->shift->hitung_lembur == "SETELAH PULANG" && $jamLogOut > $shiftOut){
                $lembur = $this->selisih($jamLogOut,$shiftOut);
            }

            if($kerja->shift->hitung_lembur == "SEBELUM DATANG" && $jamLogIn < $shiftIn){
                $lembur = $this->selisih($shiftIn,$jamLogIn);
            }

            if($status_absen == "LIBUR NASIONAL" && (!empty($jamLogIn) or !empty($jamLogOut))){
                $status_absen = "LEMBUR LIBUR";
            }

            $mRekap = new RekapitulasiAbsensiPegawai();

            $mRekap->pegawai_id = $pegawai_id;
            $mRekap->hari_kerja_pegawai_id = $kerja->id;
            $mRekap->tgl_kerja = $kerja->hariKerja->tgl_kerja;
            $mRekap->status_absen = $status_absen;
            $mRekap->log_in = $jamLogIn;
            $mRekap->log_out = $jamLogOut;
            $mRekap->status_log_in = $status_log_in;
            $mRekap->status_log_out = $status_log_out;
            $mRekap->waktu_telat = $telat;
            $mRekap->waktu_cepat = $cepat;
            $mRekap->waktu_lebur = $lembur;
            $mRekap->created_at = date('Y-m-d');
            $mRekap->created_by = Yii::$app->user->id;
            $mRekap->save();

        }

    }

    public function getCekLembur(){
        $pegawai_id = $this->pegawai_id;
        $tglAbsen = $this->tgl_kerja;
        $menitLembur = $this->waktu_lebur;

        $dataLembur = RegisterLembur::find()->where(['pegawai_id' => $pegawai_id, 'tgl_lembur' => $tglAbsen, 'status_lembur' => 'DISETUJUI'])->one();

        if($dataLembur){
            $jamRealisasi = round($menitLembur/60,0);

            if($jamRealisasi > $dataLembur->jam_lembur){
                return $dataLembur->jam_lembur;
            }
            else{
                return $jamRealisasi;
            }

        }
        else{
            return 0;
        }

    }
}
