<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "log_transaksi".
 *
 * @property int $id_log_transaksi
 * @property string $jenis_transaksi
 * @property string $deskripsi
 * @property int $cabang_id
 * @property string $created_at
 * @property int $created_by
 *
 * @property LogJurnal[] $logJurnals
 * @property Cabang $cabang
 */
class LogTransaksi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_transaksi';
    }

    const SCENARIO_PENYESUAIAN = 'penyesuaian';

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis_transaksi', 'deskripsi', 'created_at', 'created_by'], 'required'],
            [['cabang_id'], 'required', 'except' => 'penyesuaian'],
            [['jenis_transaksi', 'deskripsi'], 'string'],
            [['cabang_id', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['cabang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cabang::className(), 'targetAttribute' => ['cabang_id' => 'id_cabang']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_log_transaksi' => 'Id Log Transaksi',
            'jenis_transaksi' => 'Jenis Transaksi',
            'deskripsi' => 'Deskripsi',
            'cabang_id' => 'Cabang ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[LogJurnals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLogJurnals()
    {
        return $this->hasMany(LogJurnal::className(), ['log_transaksi_id' => 'id_log_transaksi']);
    }

    /**
     * Gets query for [[Cabang]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCabang()
    {
        return $this->hasOne(Cabang::className(), ['id_cabang' => 'cabang_id']);
    }

    public function myCabang(){
        $cabang = $this->getCabang()->one();
        if($cabang){
            return $cabang->nama_cabang;
        }
        else{
            return "TIDAK ADA CABANG";
        }
    }

    //MY FUNCTIONS START HERE

    public function inTransaksi($jenis_transaksi,$deskripsi,$tgl=""){
        if($tgl == ""){
            $tgl = date('Y-m-d');
        }
        $this->jenis_transaksi = $jenis_transaksi;
        $this->deskripsi = $deskripsi;
        $this->cabang_id = Profile::myProfile2()->id_cabang;
        $this->created_at = $tgl;
        $this->created_by = Yii::$app->user->id;
        $this->save();
    }

    public function inTransaksiInventory($jenis_transaksi,$deskripsi,$created_at,$created_by){
        $this->jenis_transaksi = $jenis_transaksi;
        $this->deskripsi = $deskripsi;
        $this->cabang_id = "";
        $this->created_at = $created_at;
        $this->created_by = $created_by;
        $this->save(false);
    }

    public function inTransaksiFix($jenis_transaksi,$deskripsi,$cabang_id,$created_at,$created_by){
        $this->jenis_transaksi = $jenis_transaksi;
        $this->deskripsi = $deskripsi;
        $this->cabang_id = $cabang_id;
        $this->created_at = $created_at;
        $this->created_by = $created_by;
        $this->save();
    }

    public function getTglJurnal(){
        $data = $this->getLogJurnals()->one();
        return $data->created_at;
    }

    //MY FUNCTIONS END HERE

}
