<?php
return [
    'adminEmail' => 'silalahi.hardiman@gmail.com',
    'supportEmail' => 'it.support@dotrigadai.com',
    'senderEmail' => 'it.support@dotrigadai.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
];
