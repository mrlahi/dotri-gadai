<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'name' => 'Dotri Gadai Jaya',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
        ],
        'gridviewKartik' =>  [
            'class' => '\kartik\grid\Module',
            // your other grid module settings
        ],
        
        //kartik-v gridView
        'gridview' =>  [
                'class' => '\kartik\grid\Module',
                // your other grid module settings
        ],
        'api' => [
            'class' => 'backend\modules\api\Api',
        ], 
    ],
    'timeZone' => 'Asia/Jakarta',
    'components' => [
        'backup' => [
            'class' => 'demi\backup\Component',
            // The directory for storing backups files
            'backupsFolder' => dirname(dirname(__DIR__)) . '/backups', // <project-root>/backups
            // Directories that will be added to backup
            'directories' => [
                // 'images' => '@frontend/web/images',
                // 'uploads' => '@backend/uploads',
            ],
        ],
        'authManager' => [//component authmanager untuk RBAC
            'class' => 'yii\rbac\DbManager',
            'defaultRoles'=>['guest'],
            // uncomment if you want to cache RBAC items hierarchy
            // 'cache' => 'cache',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'transport' => [
                    'class' => 'Swift_SmtpTransport',
                    'host' => 'smtp.hostinger.com',
                    'username' => 'it.support@dotrigadai.com',//isikan email google untuk mencoba
                    'password' => 'Silalahi2718!',//isikan password email
                    'port' => '465',
                    'encryption' => 'ssl',
                    'streamOptions' => [
                            'ssl' => [
                                    'allow_self_signed' => true,
                                    'verify_peer' => false,
                                    'verify_peer_name' => false,
                            ],
                    ]
                    
            ],
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
    ],
    'formatter' => [
        'dateFormat' => 'php:D, d M Y',
        'datetimeFormat' => 'd-M-Y H:i:s',
        'timeFormat' => 'H:i:s',
        'thousandSeparator' => '.',
        'decimalSeparator' => ',',
        'currencyCode' => 'Rp.',
        'numberFormatterOptions' => [
            NumberFormatter::MIN_FRACTION_DIGITS => 0,
            NumberFormatter::MAX_FRACTION_DIGITS => 0,
        ]
   ],
    ],  
];
