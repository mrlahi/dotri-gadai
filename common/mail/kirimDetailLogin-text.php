<?php
$loginLink = Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
?>

KLik link dibawah untuk reset password Anda:
Kepada Yth, <?php echo $this->params['firstname'];?>.
    	
    	Berikut ini kami informasikan detail login anda untuk mengakses Aplikasi E-Presensi, Username: <?= $user->username ?> dan Password: <?php echo $this->params['randomPass'];?>. Anda dapat langsung login ke aplikasi dengan klik link  <?= $loginLink ?>. 


    Jika anda mengalami kendala dalam mengakses Aplikasi, jangan ragu untuk menghubungi kami
    secara langsung dengan mengirimkan email ke: dotrigadai.it@gmail.com.


    Terimakasih,


    Admin



