<?php
use yii\helpers\Html;

$frontEnd = Yii::$app->params['frontendWeb'];
$loginLink = $frontEnd.'site/login';
?>
<div class="login-detail">
    <p>Kepada Yth, <br/>
    	<b><?php echo $this->params['firstname'];?></b><br/>
    	di tempat <br/><br/>
    	
    	Berikut ini kami informasikan detail login anda untuk mengakses Aplikasi Dotri Gadai Medan<br/><br/>
    	
    	<b>Username: <?= Html::encode($user->username) ?><br/>
    	Password: <?php echo $this->params['randomPass'];?><br/><br/></b>
    	
    	Anda dapat langsung login ke aplikasi dengan klik link di bawah:<br/>

     <?= Html::a(Html::encode($loginLink), $loginLink) ?> <br/><br/><br/>

    Jika anda mengalami kendala dalam mengakses Aplikasi, jangan ragu untuk menghubungi kami
    secara langsung dengan mengirimkan email ke: <b>dotrigadai.it@gmail.com</b><br/><br/><br/>


    Terimakasih,<br/><br/>


    Admin

    
</div>
