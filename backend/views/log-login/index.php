<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogLoginSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute' => 'created_by',
                    'label' => 'Nama Pegawai',
                    'value' => 'createdBy.nama'
                ],
                'user_agent',
                'screen_width',
                'screen_height',
                'device_type',
                'browser_type',
                //'latitude',
                //'longitude',
                [
                    'attribute' => 'created_at',
                    'label' => 'Waktu Login',
                ],
                //'created_by',
                'OSName',
                'OSVersion',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{map}',
                    'buttons' => [
                        'map' => function($url, $model){
                            return Html::button('<span class="fa fa-map"></span> Map',['value' => Url::to([$url]),'class'=>'btn btn-info btn-pop btn-sm']);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>

<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-lg',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");