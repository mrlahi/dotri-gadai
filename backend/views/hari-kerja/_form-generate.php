<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HariKerja */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hari-kerja-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'bulan')->dropDownList(['01' => 'JAN', '02' => 'FEB', '03' => 'MAR', '04' => 'APR', '05' => 'MEI', '06' => 'JUN', '07' => 'JUL', '08' => 'AGT', '09' => 'SEP', '10' => 'OKT', '11' => 'NOV', '12' => 'DES'], ['prompt' => 'PILIH BULAN', 'require' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
