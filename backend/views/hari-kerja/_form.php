<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HariKerja */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hari-kerja-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tgl_kerja')->textInput() ?>

    <?= $form->field($model, 'status_hari')->dropDownList([ 'HARI KERJA' => 'HARI KERJA', 'LIBUR NASIONAL' => 'LIBUR NASIONAL', 'CUTI' => 'CUTI', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
