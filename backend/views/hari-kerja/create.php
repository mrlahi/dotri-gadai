<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HariKerja */

$this->title = 'Create Hari Kerja';
$this->params['breadcrumbs'][] = ['label' => 'Hari Kerjas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hari-kerja-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
