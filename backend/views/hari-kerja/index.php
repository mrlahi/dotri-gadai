<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use kartik\editable\Editable;

/* @var $this yii\web\View */
/* @var $searchModel common\models\HariKerjaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hari Kerja';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::button('<span class="fa fa-refresh"></span> Generate Hari Kerja',['value'=>'generate','class'=>'btn btn-success btn-pop']); ?>
           
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax' => true,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute' => 'tgl_kerja',
                    'format' => 'date',
                    'filterType' => GridView::FILTER_DATE_RANGE,
                    'filterWidgetOptions' => [       
                        'attribute' => 'tgl_masuk',
                        'pluginOptions' => [
                            'separator' => ' - ',
                            'format' => 'YYYY-MM-DD',
                            'locale' => [
                                'format' => 'YYYY-MM-DD'
                            ],
                        ],
                    ]
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'status_hari',
                    'editableOptions' =>[
                        'asPopover' => true,
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => ['HARI KERJA' => 'HARI KERJA', 'LIBUR NASIONAL' => 'LIBUR NASIONAL', 'CUTI' => 'CUTI'],
                        'size'=>'md',
                        //'options' => ['class'=>'form-control']
                    ],
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ['HARI KERJA' => 'HARI KERJA', 'LIBUR NASIONAL' => 'LIBUR NASIONAL', 'CUTI' => 'CUTI'],
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                [
                    'attribute' => 'deskripsi',
                    'class' => 'kartik\grid\EditableColumn',
                    'editableOptions' =>[
                        'asPopover' => true,
                        'inputType' => Editable::INPUT_TEXTAREA,
                        'size'=>'md',
                        //'options' => ['class'=>'form-control']
                    ]
                ],
                //'created_at',
                //'created_by',

                //['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>

<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-lg',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");

?>