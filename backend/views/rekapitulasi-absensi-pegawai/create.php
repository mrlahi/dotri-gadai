<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RekapitulasiAbsensiPegawai */

$this->title = 'Create Rekapitulasi Absensi Pegawai';
$this->params['breadcrumbs'][] = ['label' => 'Rekapitulasi Absensi Pegawais', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rekapitulasi-absensi-pegawai-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
