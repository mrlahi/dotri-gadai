<?php

use common\models\Profile;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\RekapitulasiAbsensiPegawai */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rekapitulasi-absensi-pegawai-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'pegawai_id' => [
                'label' => 'Pegawai',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => Profile::listPegawai()
                ]
            ],
            'range_hari_kerja' =>[
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => DateRangePicker::classname(),
            ],
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
