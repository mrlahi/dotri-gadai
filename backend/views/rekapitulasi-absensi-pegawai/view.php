<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\RekapitulasiAbsensiPegawai */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rekapitulasi Absensi Pegawais', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="rekapitulasi-absensi-pegawai-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'pegawai_id',
            'hari_kerja_pegawai_id',
            'log_in',
            'log_out',
            'status_log_in',
            'status_log_out',
            'waktu_telat',
            'waktu_cepat',
            'created_at',
            'created_by',
        ],
    ]) ?>

</div>
