<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RekapitulasiAbsensiPegawai */

$this->title = 'Update Rekapitulasi Absensi Pegawai: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rekapitulasi Absensi Pegawais', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rekapitulasi-absensi-pegawai-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
