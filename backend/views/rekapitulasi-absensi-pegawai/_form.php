<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RekapitulasiAbsensiPegawai */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rekapitulasi-absensi-pegawai-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pegawai_id')->textInput() ?>

    <?= $form->field($model, 'hari_kerja_pegawai_id')->textInput() ?>

    <?= $form->field($model, 'log_in')->textInput() ?>

    <?= $form->field($model, 'log_out')->textInput() ?>

    <?= $form->field($model, 'status_log_in')->dropDownList([ 'TEPAT' => 'TEPAT', 'TELAT' => 'TELAT', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'status_log_out')->dropDownList([ 'TEPAT' => 'TEPAT', 'CEPAT' => 'CEPAT', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'waktu_telat')->textInput() ?>

    <?= $form->field($model, 'waktu_cepat')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
