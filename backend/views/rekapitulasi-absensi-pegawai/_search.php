<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RekapitulasiAbsensiPegawaiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rekapitulasi-absensi-pegawai-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'pegawai_id') ?>

    <?= $form->field($model, 'hari_kerja_pegawai_id') ?>

    <?= $form->field($model, 'log_in') ?>

    <?= $form->field($model, 'log_out') ?>

    <?php // echo $form->field($model, 'status_log_in') ?>

    <?php // echo $form->field($model, 'status_log_out') ?>

    <?php // echo $form->field($model, 'waktu_telat') ?>

    <?php // echo $form->field($model, 'waktu_cepat') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
