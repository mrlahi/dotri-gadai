<?php

$this->title = "Rekapitulasi Absensi Pegawai";

?>
<h3 align="center">Rekapitulasi Absensi Pegawai</h3>

<table>
    <tr>
        <td>Nama Pegawai</td>
        <td>:</td>
        <td><?= $dataPegawai->nama; ?></td>
    </tr>
    <tr>
        <td>Cabang</td>
        <td>:</td>
        <td><?= $dataPegawai->cabanguser->nama_cabang; ?></td>
    </tr>
    <tr>
        <td>Jabatan</td>
        <td>:</td>
        <td><?= $dataPegawai->akses; ?></td>
    </tr>
    <tr>
        <td>Periode</td>
        <td>:</td>
        <td>
            <?= Yii::$app->formatter->asDate($date1)." s/d ". Yii::$app->formatter->asDate($date2); ?>
        </td>
    </tr>
</table>

<table align="center" border="1" cellspacing="0" cellpadding="2" width="100%">
    <thead>
        <tr>
            <th align="center">No.</th>
            <th>Tgl.</th>
            <th>Status Absen</th>
            <th>Shift</th>
            <th>Check In</th>
            <th>Check Out</th>
            <th>Status Check In</th>
            <th>Status Check Out</th>
            <th>Waktu Telat</th>
            <th>Waktu Cepat</th>
            <th>Waktu Lembur</th>
            <th>Deskripsi</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $no=1;
            $tSakit = 0;
            $tCutiPribadi = 0;
            $tAbsen = 0;
            $tTelat = 0;
            $tPulangCepat = 0;
            $tLemburLibur = 0;
            $tTdkIn = 0;
            $tTdkOut = 0;
            $tLemburHarian = 0;
            foreach($dataRekap as $rekap){

                if($rekap->status_absen == "TIDAK HADIR"){
                    $bg = "orange";
                    $tAbsen++;
                }
                else if($rekap->status_absen == "SAKIT"){
                    $bg = "yellow";
                    $tSakit++;
                }
                else if($rekap->status_absen == "CUTI"){
                    $bg = "green";
                    $tCutiPribadi++;
                }
                else if($rekap->status_absen == "LIBUR NASIONAL"){
                    $bg = "cyan";
                }
                else if($rekap->status_absen == "OFF"){
                    $bg = "grey";
                }
                else if($rekap->status_absen == "LEMBUR LIBUR"){
                    $bg = "magenta";
                    $tLemburLibur++;
                }
                else{
                    $bg = "";
                }

                if($rekap->waktu_telat > 0){
                    $tTelat++;
                }

                if($rekap->waktu_cepat > 0){
                    $tPulangCepat ++;
                }

                if($rekap->status_absen == "HARI KERJA"){
                    if($rekap->status_log_in == "TIDAK LOG IN" AND $rekap->status_log_out != "TIDAK LOG OUT"){
                        $tTdkIn++;
                    }
    
                    if($rekap->status_log_in != "TIDAK LOG IN" AND $rekap->status_log_out == "TIDAK LOG OUT"){
                        $tTdkOut++;
                    }
                }

        ?>
        <tr bgcolor="<?= $bg; ?>">
            <td align="center"><?= $no++; ?></td>
            <td><?= Yii::$app->formatter->asDate($rekap->tgl_kerja) ?></td>
            <td><?= $rekap->hariKerjaPegawai->shift->nama_shift; ?></td>
            <td><?= $rekap->status_absen; ?></td>
            <td><?= $rekap->log_in; ?></td>
            <td><?= $rekap->log_out; ?></td>
            <td><?= $rekap->status_log_in; ?></td>
            <td><?= $rekap->status_log_out; ?></td>
            <td><?= $rekap->waktu_telat; ?></td>
            <td><?= $rekap->waktu_cepat; ?></td>
            <td>
                <?= $rekap->waktu_lebur; ?>
                <?php
                    if($rekap->cekLembur >= 1){
                        echo "(DISETUJUI : ".$rekap->cekLembur." jam)";
                        $tLemburHarian += $rekap->cekLembur;
                    }
                ?>
            </td>
            <td><?= $rekap->hariKerjaPegawai->deskripsi; ?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>

<hr>

<?php

$potTelat = $tTelat * 25000;
$potAbsen = $tAbsen * 150000;
$potTdkLogin = $tTdkIn * 50000;
$potTdkLogout = $tTdkOut * 50000;
$potPlgCepat = $tPulangCepat * 25000;
$liburLembur = $tLemburLibur * 150000;
$lemburHarian = $tLemburHarian * 19000;


?>

<table>
    <tr>
        <td>Gaji Pokok</td>
        <td>:</td>
        <td>
            <?= Yii::$app->formatter->asCurrency($dataHonor->honor); ?>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>SAKIT</td>
        <td>:</td>
        <td>
            <strong>
                <?= $tSakit; ?> hari.
            </strong>
        </td>
    </tr>
    <tr>
        <td>CUTI</td>
        <td>:</td>
        <td>
            <strong>
                <?= $tCutiPribadi; ?> hari.
            </strong>
        </td>
    </tr>
    <tr>
        <td>Telat</td>
        <td>:</td>
        <td>
            <strong>
                <?= $tTelat; ?> x Rp. 25.000 = <?= Yii::$app->formatter->asCurrency($potTelat); ?>.
            </strong>
        </td>
    </tr>
    <tr>
        <td>Pulang Cepat</td>
        <td>:</td>
        <td>
            <strong>
                <?= $tPulangCepat; ?> x Rp. 25.000 =  <?= Yii::$app->formatter->asCurrency($potPlgCepat); ?>.
            </strong>
        </td>
    </tr>
    <tr>
        <td>TIDAK HADIR</td>
        <td>:</td>
        <td>
            <strong>
                <?= $tAbsen; ?> x Rp. 150.000 = <?= Yii::$app->formatter->asCurrency($potAbsen); ?>.
            </strong>
        </td>
    </tr>
    <tr>
        <td>TIDAK LOG IN</td>
        <td>:</td>
        <td>
            <strong>
                <?= $tTdkIn; ?> x Rp. 50.000 = <?= Yii::$app->formatter->asCurrency($potTdkLogin); ?>
            </strong>
        </td>
    </tr>
    <tr>
        <td>TIDAK LOG OUT</td>
        <td>:</td>
        <td>
            <strong>
                <?= $tTdkOut; ?> x Rp. 50.000 = <?= Yii::$app->formatter->asCurrency($potTdkLogout); ?>
            </strong>
        </td>
    </tr>
    <tr>
        <td>LEMBUR HARIAN</td>
        <td>:</td>
        <td>
            <strong>
                <?= $tLemburHarian; ?> x Rp. 19.000 = <?= Yii::$app->formatter->asCurrency($lemburHarian); ?>
            </strong>
        </td>
    </tr>
    <tr>
        <td>LEMBUR LIBUR</td>
        <td>:</td>
        <td>
            <strong>
                <?= $tLemburLibur; ?> x Rp. 150.000 = <?= Yii::$app->formatter->asCurrency($liburLembur); ?>
            </strong>
        </td>
    </tr>
    <tr>
        <td>Ins. Nasabah</td>
        <td>:</td>
        <td>
            <strong>
                <?= $dataPegawai->insNasabah($date1,$date2); ?> x Rp. 500,00 = <?= $insNasabah = $dataPegawai->insNasabah($date1,$date2) * 500; ?>
            </strong>
        </td>
    </tr>
</table>

<hr>


<?php
    $gajiBersih = $dataHonor->honor - $potTelat - $potAbsen - $potTdkLogin - $potTdkLogout - $potPlgCepat + $liburLembur + $insNasabah + $lemburHarian;
?>

<h3>
    Gaji Bersih : <?= Yii::$app->formatter->asCurrency($gajiBersih); ?>
</h3>

<script>
    print();
</script>