<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\editable\Editable;

/* @var $this yii\web\View */
/* @var $searchModel common\models\RekapitulasiAbsensiPegawaiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rekapitulasi Absensi Pegawai : '.$dataPegawai->nama;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                //'pegawai_id',
                [
                    'attribute' => 'tgl_kerja',
                    'format' => 'date'
                ],
                [
                    'attribute' => 'status_absen'
                ],
                [
                    'label' => 'Shift',
                    'value' => 'hariKerjaPegawai.shift.nama_shift'
                ],
                'log_in',
                'log_out',
                'status_log_in',
                'status_log_out',
                'waktu_telat',
                'waktu_cepat',
                [
                    'label' => 'Deskripsi',
                    'value' => 'hariKerjaPegawai.deskripsi'
                ],
                //'created_at',
                //'created_by',

                //['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

        <?= Html::a('<span class="fa fa-print"></span> Cetak Rekapitulasi', ['print-rekapitulasi', 'periode_penggajian_honor_id' => $periode_penggajian_honor_id], ['class' => 'btn btn-info btn-block', 'target' => '_blank']); ?>

    </div>
</div>
