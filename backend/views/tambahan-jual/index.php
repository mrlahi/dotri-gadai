<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TambahanJualSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tambahan Juals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tambahan-jual-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Tambahan Jual', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_tambah_jual',
            'id_barang',
            'desk_tambahan:ntext',
            'biaya_tambah',
            'created_by',
            //'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
