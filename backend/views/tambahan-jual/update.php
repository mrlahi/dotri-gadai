<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TambahanJual */

$this->title = 'Update Tambahan Jual: ' . $model->id_tambah_jual;
$this->params['breadcrumbs'][] = ['label' => 'Tambahan Juals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_tambah_jual, 'url' => ['view', 'id' => $model->id_tambah_jual]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tambahan-jual-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
