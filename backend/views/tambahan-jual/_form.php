<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $model common\models\TambahanJual */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tambahan-jual-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'biaya_tambah' => [
                'label' => 'Biaya',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required'
                    ]
                ]
            ],
            'jenis_tambahan' => [
                'label' => 'Kategori Biaya',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => ['BIAYA SERVICE' => 'BIAYA SERVICE', 'BIAYA PENJUALAN' => 'BIAYA PENJUALAN', 'DENDA' => 'DENDA', 'BUNGA' => 'BUNGA'],
                    'options' => ['placeholder' => 'Pilih Kategori Biaya']
                ],
            ]
        ]
    ]);
    ?>


    <?= $form->field($model, 'desk_tambahan')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
