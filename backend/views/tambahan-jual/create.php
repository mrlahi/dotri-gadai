<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TambahanJual */

$this->title = 'Tambahan Jual';
$this->params['breadcrumbs'][] = ['label' => 'Tambahan Juals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
