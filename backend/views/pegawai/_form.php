<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Profile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="profile-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'no_ktp' => ['label' =>'Nomor KTP', 'options' => ['placeholder' => 'Nomor KTP']],
            'nama' => ['label' =>'Nama', 'options' => ['placeholder' => 'Nama Lengkap']],
            'email' => ['label' => 'Email','options' => ['placeholder' =>'Email Aktif','type' => 'email']],
            'no_hp' => ['label' => 'No. HP','options' => ['placeholder' =>'Nomor Handphone']],
            'alamat' => [
                'label'=> 'Alamat Lengkap',
                'type' => Form::INPUT_TEXTAREA,

            ],
            'domisili' => [
                'label'=> 'Domisili',
                'type' => Form::INPUT_TEXTAREA,

            ],
            'id_cabang' =>[
                'label' => 'Cabang',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => $model->listCabang()
                ]
            ],
            'akses' => [
                'label' => 'Hak Akses',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => ['admin' => 'admin', 'defaultStaff' => 'Kasir', 'defaultFinance' => 'Finance','Penafsir' => 'Penafsir', 'gudang' => 'gudang']
                ]
            ]
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
