<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\editable\Editable;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pegawai';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">

    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a('<span class="fa fa-plus-circle"></span> Tambah Pegawai', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_user',
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'absensi_id',
                'editableOptions' =>[
                    'asPopover' => true,
                    'inputType' => Editable::INPUT_TEXT,
                    'size'=>'md',
                    //'options' => ['class'=>'form-control']
                ],
            ],
            'no_ktp',
            'nama',
            //'alamat:ntext',
            'email:email',
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'no_hp',
                'editableOptions' =>[
                    'asPopover' => true,
                    'inputType' => Editable::INPUT_TEXT,
                    'size'=>'md',
                    //'options' => ['class'=>'form-control']
                ],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'domisili',
                'editableOptions' =>[
                    'asPopover' => true,
                    'inputType' => Editable::INPUT_TEXT,
                    'size'=>'md',
                    //'options' => ['class'=>'form-control']
                ],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'alamat',
                'editableOptions' =>[
                    'asPopover' => true,
                    'inputType' => Editable::INPUT_TEXT,
                    'size'=>'md',
                    //'options' => ['class'=>'form-control']
                ],
            ],
            //'created_by',
            //'created',
            //'akses',
            [
                'attribute' => 'id_cabang',
                'label' => 'Cabang',
                'value' => 'cabanguser.nama_cabang'
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{ubah-password} {block}',
                'contentOptions' => ['style' => ['white-space' => 'nowrap']],
                'buttons' => [
                    'ubah-password' => function($url,$model){
                        return Html::a('<span class="fa fa-lock"></span> Ubah Password', [$url], ['class' => 'btn btn-info']);
                    },
                    'block' => function ($url, $model){
                        $targetUrl = '/pegawai/block?id='.$model->id_user;
                        return Html::a('<span class="fa fa-close"></span> Block', [$targetUrl], ['class' => 'btn btn-warning','data' => ['confirm' => 'Yakin ingin block pegawai ini..??','method' => 'post',]]);
                    },
                ]
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>
    </div>
</div>
