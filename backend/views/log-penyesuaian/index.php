<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogPenyesuaianSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jurnal Penyesuaian';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a('Tambah Transaksi Penyesuaian', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'akun_id',
                'label' => 'Nama Akun',
                'value' => 'akun.nama_akun'
            ],
            'jenis_penyesuaian',
            [
                'attribute' => 'jumlah_uang',
                'format' => 'currency'
            ],
            'deskripsi:ntext',
            //'cabang_id',
            [
                'attribute' => 'created_at',
                'label' => 'Tgl. Input',
                'format' => 'date'
            ],
            //'created_by',
            [
                'class' => ActionColumn::className(),
                'template' => '{delete}',
                'visibleButtons' => [
                    'delete' => function($model){
                        if($model->created_at == date('Y-m-d')){
                            return TRUE;
                        }
                    }
                ],
                'contentOptions' => ['style' => ['white-space' => 'nowrap']],
                 'buttons' => [
                    'update' => function($url, $model){
                        return Html::a('<span class="fa fa-edit"></span> Update', [$url], ['class' => 'btn btn-sm btn-outline-warning', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Update Transaksi']);
                    },
                    'delete' => function($url, $model){
                        return Html::a('<span class="fa fa-trash"></span>', [$url], ['class' => 'btn btn-sm btn-outline-danger', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Hapus Transaksi', 'data' => ['confirm' => 'Yakin ingin menghapus transaksi..??'], 'data-method' => 'post']);
                    },
                 ]
            ],
        ],
    ]); ?>
    </div>
</div>
