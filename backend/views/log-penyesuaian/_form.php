<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use common\models\Akun;

/** @var yii\web\View $this */
/** @var frontend\modules\koperasi\models\FKspLogPenyesuaian $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="fksp-log-penyesuaian-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' =>[
                'akun_id' => [
                    'label' => 'Nama/Kode Akun',
                    'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::classname(),
                        'options' =>[
                            'data' => Akun::listAkun(),
                            'options' => ['placeholder' => 'Pilih Akun']
                        ]
                ],
                'jenis_penyesuaian' => [
                    'label' => 'Jenis Transaksi',
                    'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::classname(),
                        'options' =>[
                            'data' => [ 'DEBET' => 'DEBET', 'KREDIT' => 'KREDIT', ],
                            'options' => ['placeholder' => 'Pilih Jenis Transaksi']
                        ]
                ],
                'jumlah_uang' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => NumberControl::classname(),
                    'options' => [
                        'maskedInputOptions' =>[
                            'prefix' => 'Rp.  ',
                            'groupSeparator' => '.',
                            'radixPoint' => ',',
                            'allowMinus' => false
                        ],
                        'options' => [
                            'placeholder' => '',
                            'required'=>'required'
                        ]
                    ]
                ],
                'created_at' => [
                    'label' => 'Tgl. Transaksi',
                    'options' => [
                        'type' => 'date'
                    ]
                ]
            ]
        ]);
    ?>

    <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
