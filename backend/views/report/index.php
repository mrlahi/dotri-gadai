<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\BarangExtSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Outstanding';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">

    <?= Html::a('<span class="fa fa-search"></span> Filter Tanggal', ['filter'], ['class' => 'btn btn-success pull-right']) ?>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php
//Table Yang Mau diexport
    $gridExport = [
        ['class' => 'yii\grid\SerialColumn'],
        'no_kontrak',
        [
            'attribute' => 'id_cabang',
            'label' => 'Outlet',
            'value' => 'cabang.nama_cabang'
        ],
        [
            'attribute' => 'id_jenis',
            'label' => 'Tipe SBG',
            'value' => 'jenisBarang.nama_barang'
        ],
        [
            'attribute' => 'tgl_masuk',
            'label' => 'Tgl SBG',
            'format' => 'date',
        ],
        [
            'attribute' => 'jatuh_tempo',
            'label' => 'Jth Tempo',
            'format' => 'date',
        ],
        [
            'attribute' => 'id_nasabah',
            'label' => 'Nama Nasabah',
            'value' => 'nasabah.nama',
        ],
        'nama_barang',
        'merk',
        'tipe',
        'kelengkapan',
        'harga_taksir',
        'nilai_pinjam',
        [
            'label' => 'Sisa Pinjaman',
            'value' => 'sisaPinjam'
        ],
        [
            'label' => 'Tgl Pelunasan',
            'value' => 'tgllunas',
            'format' => 'date'
        ],
        [
            'attribute' => 'status',
            'label' => 'Status',
            'value' => 'statusBarang',
            'format' => 'html'
        ],
        [
            'label' => 'Outstanding',
            'value' => 'outstanding'
        ],
    ];
?>

<?php
//Tombol Exportnya
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridExport,
    'showColumnSelector' => false,
    'exportConfig' => [
        ExportMenu::FORMAT_HTML => false,
        ExportMenu::FORMAT_CSV => false,
        ExportMenu::FORMAT_TEXT => false
    ],
    'filename' => $judulFile
]);
?>

<div class="clearfix"></div>
<?php
/*
echo \kartik\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridExport
]);
*/
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table-hover table-responsive'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'no_kontrak',
            [
                'attribute' => 'id_cabang',
                'label' => 'Outlet',
                'value' => 'cabang.nama_cabang'
            ],
            [
                'attribute' => 'id_jenis',
                'label' => 'Tipe SBG',
                'value' => 'jenisBarang.nama_barang'
            ],
            [
                'attribute' => 'tgl_masuk',
                'label' => 'Tgl SBG',
                'format' => 'date',
            ],
            [
                'attribute' => 'jatuh_tempo',
                'label' => 'Jth Tempo',
                'format' => 'date',
            ],
            [
                'attribute' => 'id_nasabah',
                'label' => 'Nama Nasabah',
                'value' => 'nasabah.nama',
            ],
            'nama_barang',
            'merk',
            [
                'attribute' => 'status',
                'label' => 'Status',
                'value' => 'statusBarang',
                'format' => 'html'
            ],
            [
                'label' => 'Outstanding',
                'value' => 'outstanding'
            ]
            //'serial',
            //'id_jenis',
            //'tgl_masuk',
            //'jatuh_tempo',
            //'spek:ntext',
            //'kelengkapan:ntext',
            //'kondisi:ntext',
            //'harga_pasar',
            //'harga_taksir',
            //'nilai_pinjam',
            //'bunga',
            //'biaya_admin',
            //'biaya_simpan',
            //'jangka',
            //'created_by',
            //'status',
        ],
    ]); ?>

    <?php Pjax::end(); ?>
    </div>
</div>
