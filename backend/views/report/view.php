<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\BarangExt */

$this->title = $model->id_barang;
$this->params['breadcrumbs'][] = ['label' => 'Barang Exts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="barang-ext-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_barang], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_barang], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_barang',
            'no_kontrak',
            'id_nasabah',
            'nama_barang',
            'merk',
            'tipe',
            'serial',
            'id_jenis',
            'tgl_masuk',
            'jatuh_tempo',
            'spek:ntext',
            'kelengkapan:ntext',
            'kondisi:ntext',
            'harga_pasar',
            'harga_taksir',
            'nilai_pinjam',
            'bunga',
            'biaya_admin',
            'biaya_simpan',
            'jangka',
            'created_by',
            'status',
        ],
    ]) ?>

</div>
