<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\BarangExt */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="barang-ext-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'no_kontrak')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_nasabah')->textInput() ?>

    <?= $form->field($model, 'nama_barang')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'merk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipe')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'serial')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_jenis')->textInput() ?>

    <?= $form->field($model, 'tgl_masuk')->textInput() ?>

    <?= $form->field($model, 'jatuh_tempo')->textInput() ?>

    <?= $form->field($model, 'spek')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'kelengkapan')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'kondisi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'harga_pasar')->textInput() ?>

    <?= $form->field($model, 'harga_taksir')->textInput() ?>

    <?= $form->field($model, 'nilai_pinjam')->textInput() ?>

    <?= $form->field($model, 'bunga')->textInput() ?>

    <?= $form->field($model, 'biaya_admin')->textInput() ?>

    <?= $form->field($model, 'biaya_simpan')->textInput() ?>

    <?= $form->field($model, 'jangka')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
