<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BarangExt */

$this->title = 'Create Barang Ext';
$this->params['breadcrumbs'][] = ['label' => 'Barang Exts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="barang-ext-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
