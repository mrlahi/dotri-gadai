<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LogJurnal */

$this->title = 'Update Log Jurnal: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Log Jurnals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="log-jurnal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
