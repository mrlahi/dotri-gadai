<?php

use common\models\Akun;
use common\models\Cabang;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\number\NumberControl;

$this->title = 'Mutasi Kas';

?>

<div class="box box-success">
    <div class="box-header">
        <h3>Mutasi Kas</h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

        <?= Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 2,
                'attributes' =>[
                    'asal_cabang' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::classname(),
                        'options' =>[
                            'data' => Cabang::listCabang(),
                            'options' => ['placeholder' => 'Pilih Cabang']
                        ]
                    ],
                    'asal_akun' =>[
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::classname(),
                        'options' =>[
                            'data' => Akun::listAkun(),
                            'options' => ['placeholder' => 'Pilih Akun']
                        ]
                    ],
                    'tujuan_cabang' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::classname(),
                        'options' =>[
                            'data' => Cabang::listCabang(),
                            'options' => ['placeholder' => 'Pilih Cabang']
                        ]
                    ],
                    'tujuan_akun' =>[
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::classname(),
                        'options' =>[
                            'data' => Akun::listAkun(),
                            'options' => ['placeholder' => 'Pilih Akun']
                        ]
                    ],
                    'created_at' => [
                        'label' => 'Tgl. Mutasi',
                        'options' => [
                            'type' => 'date'
                        ]
                    ],
                    'jumlah_mutasi' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => NumberControl::classname(),
                        'options' => [
                            'maskedInputOptions' =>[
                                'prefix' => 'Rp.  ',
                                'groupSeparator' => '.',
                                'radixPoint' => ',',
                                'allowMinus' => false
                            ],
                            'options' => [
                                'placeholder' => '',
                                'required'=>'required'
                            ]
                        ]
                    ]
                ]
            ]);
        ?>

        <div class="form-group">
            <?= Html::submitButton('Mutasi', ['class' => 'btn btn-success btn-sm']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>