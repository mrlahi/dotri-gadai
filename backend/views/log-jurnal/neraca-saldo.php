<?php

$this->title = "Neraca Saldo";
?>

<div class="box box-success">
    <div class="box-header">
        <h3><?= $this->title ?></h3>
    </div>
    <div class="box-body">
        <div class="box-sub">
            <h3>
                Per Tanggal : <?= Yii::$app->formatter->asDate($tgl); ?>
            </h3>
        </div>
        <table class="table table-hover table-stripped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>No. Akun</th>
                    <th>Nama Akun</th>
                    <th>Debet</th>
                    <th>Kredit</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                    $t_debet = 0;
                    $t_kredit = 0;
                    foreach($dataAkun as $akun){
                        $debet = 0;
                        $kredit = 0;

                        if($akun->saldo_normal == "DEBET"){
                            $debet = $akun->saldoJurnal($tgl,$cabang_id);
                            $t_debet += $debet;
                        }

                        if($akun->saldo_normal == "KREDIT"){
                            $kredit = $akun->saldoJurnal($tgl,$cabang_id);
                            $t_kredit += $kredit;
                        }

                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $akun->id; ?></td>
                    <td><?= $akun->nama_akun; ?></td>
                    <td><?= Yii::$app->formatter->asCurrency($debet); ?></td>
                    <td><?= Yii::$app->formatter->asCurrency($kredit); ?></td>
                </tr>
                <?php
                    }
                ?>
                <tr>
                    <td colspan="3">
                        <strong>
                            TOTAL
                        </strong>
                    </td>
                    <td><?= Yii::$app->formatter->asCurrency($t_debet); ?></td>
                    <td><?= Yii::$app->formatter->asCurrency($t_kredit); ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>