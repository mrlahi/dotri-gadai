<?php

use yii\helpers\Html;

$this->title = "Laba Rugi";
?>

<div class="box box-info">
    <div class="box-header">
        <h3><?= $this->title; ?></h3>
    </div>
    <div class="box-body">
            <strong>
                Range Tanggal : <?= Yii::$app->formatter->asDate($date1); ?> - <?= Yii::$app->formatter->asDate($date2); ?>
            </strong>
            <hr>
        <h4>PENDAPATAN OPERASIONAL USAHA</h4>
        <table class="table table-hover table-stripped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>No. Akun</th>
                    <th>Nama Akun</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                    $t_pendapatan_op = 0;
                    $t_biaya_op = 0;
                    $listPendapatan = $mListLaporan->find()->where(['jenis_laporan' => 'PENDAPATAN'])->orderBy(['akun_id' => SORT_ASC])->all();
                    foreach($listPendapatan as $lapPendapatan){
                        $pendapatan = $lapPendapatan->akun->saldoJurnalRange($date1,$date2,$cabang_id);
                        $akun = $lapPendapatan->akun;
                        $t_pendapatan_op += $pendapatan;

                        if($akun->parent_kode == $akun->id){
                            $labelAkun = "<b>".$akun->nama_akun."</b>";
                        }
                        else{
                            $labelAkun = $akun->nama_akun;
                        }
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td>
                        <?= Html::a($akun->id, ['buku-besar-linked', 'start' => $date1, 'end' => $date2, 'cabang_id' => $cabang_id, 'akun_kode' => $akun->id],['target' => '_blank']); ?>
                    </td>
                    <td><?= $labelAkun; ?></td>
                    <td><?= Yii::$app->formatter->asCurrency($pendapatan); ?></td>
                </tr>
                <?php
                    }
                ?>
                <tr>
                    <td colspan="3">
                        <strong>
                            TOTAL PENDAPATAN OPERASIONAL USAHA
                        </strong>
                    </td>
                    <td>
                        <strong>
                            <?= Yii::$app->formatter->asCurrency($t_pendapatan_op); ?>
                        </strong>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <h4>
                            BIAYA OPERASIONAL USAHA
                        </h4>
                    </td>
                </tr>
                <?php
                    $no = 1;
                    $listBiaya = $mListLaporan->find()->where(['jenis_laporan' => 'BIAYA'])->all();
                    foreach($listBiaya as $lapBiaya){
                        $biaya = $lapBiaya->akun->saldoJurnalRange($date1,$date2,$cabang_id);
                        $akun = $lapBiaya->akun;
                        $t_biaya_op += $biaya;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td>
                        <?= Html::a($akun->id, ['buku-besar', 'start' => $date1, 'end' => $date2, 'cabang_id' => $cabang_id, 'akun_kode' => $akun->id],['target' => '_blank']); ?>
                    </td>
                    <td><?= $akun->nama_akun; ?></td>
                    <td><?= Yii::$app->formatter->asCurrency($biaya); ?></td>
                </tr>
                <?php
                    }
                ?>
                <tr>
                    <td colspan="3">
                        <strong>
                            TOTAL BIAYA OPERASIONAL USAHA
                        </strong>
                    </td>
                    <td>
                        <strong>
                            <?= Yii::$app->formatter->asCurrency($t_biaya_op); ?>
                        </strong>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <h4>
                            PENDAPATAN NON OPERASIONAL USAHA
                        </h4>
                    </td>
                </tr>
                <?php
                $no = 1;
                $t_pendapatan_non = 0;
                $listPendapatanNonOperasional = $mListLaporan->find()->where(['jenis_laporan' => 'PENDAPATAN NON OPERASIONAL'])->orderBy(['akun_id' => SORT_ASC])->all();
                    foreach($listPendapatanNonOperasional as $lapPendapatan){
                        $pendapatan = $lapPendapatan->akun->saldoJurnalRange($date1,$date2,$cabang_id);
                        $akun = $lapPendapatan->akun;
                        $t_pendapatan_non += $pendapatan;

                        if($akun->parent_kode == $akun->id){
                            $labelAkun = "<b>".$akun->nama_akun."</b>";
                        }
                        else{
                            $labelAkun = $akun->nama_akun;
                        }
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td>
                        <?= Html::a($akun->id, ['buku-besar', 'start' => $date1, 'end' => $date2, 'cabang_id' => $cabang_id, 'akun_kode' => $akun->id],['target' => '_blank']); ?>
                    </td>
                    <td><?= $labelAkun; ?></td>
                    <td><?= Yii::$app->formatter->asCurrency($pendapatan); ?></td>
                </tr>
                <?php
                    }
                ?>
                <tr>
                    <td colspan="3">
                        <strong>
                            TOTAL PENDAPATAN NON OPERASIONAL USAHA
                        </strong>
                    </td>
                    <td>
                        <strong>
                            <?= Yii::$app->formatter->asCurrency($t_pendapatan_non); ?>
                        </strong>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <h4>
                            BIAYA NON OPERASIONAL USAHA
                        </h4>
                    </td>
                </tr>
                <?php
                    $no = 1;
                    $t_biaya_non = 0;
                    $listBiayaNon = $mListLaporan->find()->where(['jenis_laporan' => 'BIAYA NON OPERASIONAL'])->all();
                    foreach($listBiayaNon as $lapBiaya){
                        $biaya = $lapBiaya->akun->saldoJurnalRange($date1,$date2,$cabang_id);
                        $akun = $lapBiaya->akun;
                        $t_biaya_non += $biaya;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td>
                        <?= Html::a($akun->id, ['buku-besar', 'start' => $date1, 'end' => $date2, 'cabang_id' => $cabang_id, 'akun_kode' => $akun->id],['target' => '_blank']); ?>
                    </td>
                    <td><?= $akun->nama_akun; ?></td>
                    <td><?= Yii::$app->formatter->asCurrency($biaya); ?></td>
                </tr>
                <?php
                    }
                ?>
                <tr>
                    <td colspan="3">
                        <strong>
                            TOTAL BIAYA NON OPERASIONAL
                        </strong>
                    </td>
                    <td>
                        <strong>
                            <?= Yii::$app->formatter->asCurrency($t_biaya_non); ?>
                        </strong>
                    </td>
                </tr>
                <tr>
                    <?php
                        $t_pendapatan = $t_pendapatan_op + $t_pendapatan_non;
                        $t_biaya = $t_biaya_op + $t_biaya_non;
                        if($t_pendapatan >= $t_biaya){
                            $jumlah = $t_pendapatan - $t_biaya;
                            $status = "LABA BERSIH";
                        }
                        else{
                            $jumlah = $t_biaya - $t_pendapatan;
                            $status = "RUGI BERSIH";
                        }
                    ?>
                    <td colspan="3">
                        <strong>
                            <?= $status; ?>
                        </strong>
                    </td>
                    <td><?= Yii::$app->formatter->asCurrency($jumlah); ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

