<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use common\models\Akun;

/* @var $this yii\web\View */
/* @var $model common\models\LogJurnal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-jurnal-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' =>[
                'akun_kode' => [
                    'label' => 'Nama/Kode Akun',
                    'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::classname(),
                        'options' =>[
                            'data' => Akun::listAkun(),
                            'options' => ['placeholder' => 'Pilih Akun']
                        ]
                ],
                'debet' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => NumberControl::classname(),
                    'options' => [
                        'maskedInputOptions' =>[
                            'prefix' => 'Rp.  ',
                            'groupSeparator' => '.',
                            'radixPoint' => ',',
                            'allowMinus' => true
                        ],
                        'options' => [
                            'placeholder' => '',
                            'required'=>'required'
                        ]
                    ]
                ],
                'kredit' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => NumberControl::classname(),
                    'options' => [
                        'maskedInputOptions' =>[
                            'prefix' => 'Rp.  ',
                            'groupSeparator' => '.',
                            'radixPoint' => ',',
                            'allowMinus' => true
                        ],
                        'options' => [
                            'placeholder' => '',
                            'required'=>'required'
                        ]
                    ]
                ],
                'created_at' => [
                    'options' => [
                        'type' => 'date'
                    ]
                ]
            ]
        ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
