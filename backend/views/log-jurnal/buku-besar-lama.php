<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'Buku Besar Lama';
$this->params['breadcrumbs'][] = ['label' => 'Log Jurnal', 'url' => ['index']];

?>
<div class="box box-success">
    <div class="box-header">
        <h3>
            <?= $akun->nama_akun ." (".$akun->id.")"; ?>
        </h3>
        <strong>
            <?= "Per Tanggal".Yii::$app->formatter->asDate($tgl); ?>
        </strong>
    </div>
    <div class="box-body">
        <!-- <strong>
            <?php
                //$saldo = $model->saldoSebelumnya($tgl,$akun->id);
            ?>
            Saldo Sebelumnya : <?php //Yii::$app->formatter->asCurrency($saldo); ?>
        </strong> -->
        <table class="table table-hover table-stripped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tgl</th>
                    <th>Nasabah</th>
                    <th>KTP</th>
                    <th>SBG</th>
                    <th>Uraian</th>
                    <th>Cabang</th>
                    <th>Debit</th>
                    <th>Kredit</th>
                    <th>Saldo</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                    $t_debit = 0;
                    $t_kredit = 0;
                
                    foreach($dataJurnal as $jurnal){
                        $t_debit += $jurnal->debet;
                        $t_kredit += $jurnal->kredit;

                        // $t_debit = $jurnal->tDebetSebelumnya();
                        // $t_kredit = $jurnal->tKreditSebelumnya();

                        if($akun->saldo_normal == "DEBET"){
                            $saldo = $t_debit - $t_kredit;
                        }
                        else{
                            $saldo = $t_kredit - $t_debit;
                        }

                        if(isset($jurnal->logTransaksi->deskripsi)){
                            $uraian = $jurnal->logTransaksi->deskripsi;
                        }
                        else{
                            $uraian = $jurnal->keterangan;
                        }

                ?>

                <tr>
                    <td><?= $no++; ?></td>
                    <td>
                        <?= Yii::$app->formatter->asDate($jurnal->created_at); ?>
                    </td>
                    <td><?= $jurnal->namaNasabah(); ?></td>
                    <td><?= $jurnal->ktpNasabah();; ?></td>
                    <td><?= $jurnal->noKontrak(); ?></td>
                    <td><?= $uraian; ?></td>
                    <td><?= $jurnal->cabang->nama_cabang; ?></td>
                    <td>
                        <?= Yii::$app->formatter->asCurrency($jurnal->debet); ?>
                    </td>
                    <td>
                        <?= Yii::$app->formatter->asCurrency($jurnal->kredit); ?>
                    </td>
                    <td>
                        <strong>
                            <?= Yii::$app->formatter->asCurrency($saldo); ?>
                        </strong>
                    </td>
                </tr>

                <?php } ?>
            </tbody>
        </table>

        <hr>
        <h3>Saldo Normal : <?= Yii::$app->formatter->asCurrency($saldo); ?></h3>
    </div>
</div>