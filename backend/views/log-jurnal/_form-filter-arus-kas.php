<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\daterange\DateRangePicker;
use common\models\Akun;
use common\models\Cabang;

/** @var yii\web\View $this */
/** @var frontend\modules\koperasi\models\FKspLogJurnal $model */
/** @var yii\widgets\ActiveForm $form */

$this->title = 'Filter Arus Kas';

?>

<div class="box box-info">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

        <?= Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 2,
                'attributes' =>[
                    'created_at' =>[
                        'label' => 'Range Tanggal',
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => DateRangePicker::classname(),
                        'filterWidgetOptions' => [       
                            'attribute' => 'created',
                            'presetDropdown' => true,
                            'convertFormat' => false,
                        ],
                        'options' => [
                            'options' => ['required' => true, 'autocomplete'=>'off']]
                    ],
                    'cabang_id' => [
                        'label' => 'Cabang',
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::classname(),
                        'options' =>[
                            'data' => Cabang::listCabangKeuangan(),
                            'options' => ['placeholder' => 'Pilih Cabang']
                        ]
                    ]
                ]
            ]);
        ?>

        <div class="form-group">
            <?= Html::submitButton('Tampilkan', ['class' => 'btn btn-success btn-sm']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
