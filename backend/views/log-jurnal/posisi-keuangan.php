<?php

use yii\helpers\Html;

$this->title = "Laporan Posisi Keuangan";
?>

<div class="box">
    <div class="box-body">
        <div class="card-sub">
            <h3><?= $this->title; ?></h3>
            <h3>
                Range Tanggal : <?= Yii::$app->formatter->asDate($date1); ?> - <?= Yii::$app->formatter->asDate($date2); ?>
            </h3>
        </div>
        
        <div class="col-lg-6">
            <div class="box box-success">
                <div class="box-body">
                    <div class="box-sub">
                        <h3>AKTIVA</h3>
                    </div>
                    <table class="table table-hover table-stripped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No. Akun</th>
                                <th>Nama Akun</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $no = 1;
                                $t_aktiva = 0;
                                $t_biaya = 0;
                                $listAktiva = $mListLaporan->find()->where(['jenis_laporan' => 'AKTIVA'])->orderBy(['akun_id' => SORT_ASC])->all();
                                $t_parent_aktiva = 0 ;
                                foreach($listAktiva as $lapAktiva){
                                    $aktiva = $lapAktiva->akun->saldoJurnalRange($date1,$date2,$cabang_id);
                                    $akun = $lapAktiva->akun;
                                    if($akun->parent_kode == $akun->id){
                                        $labelAkun = "<b>".$akun->nama_akun."</b>";
                                    }
                                    else{
                                        $labelAkun = $akun->nama_akun;
                                    }
                            ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td>
                                    <?= Html::a($akun->id, ['buku-besar-linked', 'start' => $date1, 'end' => $date2, 'cabang_id' => $cabang_id, 'akun_kode' => $akun->id],['target' => '_blank']); ?>
                                </td>
                                <td><?= $labelAkun; ?></td>
                                <td><?= Yii::$app->formatter->asCurrency($aktiva); ?></td>
                            </tr>
                            <?php
                                if($akun->id == $akun->kodeAkhir() && $akun->parent_kode != $akun->id){
                            ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <b>TOTAL <?= $akun->parentKode->nama_akun; ?></b>
                                    </td>
                                    <td>
                                        <b>
                                            <?= Yii::$app->formatter->asCurrency($t_parent_aktiva); ?>
                                        </b>
                                    </td>
                                </tr>
                            <?php
                                $t_parent_aktiva = 0; //RESET TOTAL PARENT
                            }
                                $t_aktiva += $aktiva;
                                $t_parent_aktiva += $aktiva;
                                }
                            ?>
                            <tr>
                                <td colspan="3">
                                    <strong>
                                        TOTAL AKTIVA
                                    </strong>
                                </td>
                                <td><?= Yii::$app->formatter->asCurrency($t_aktiva); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <div class="col-lg-6">
            <div class="box box-warning">
                <div class="box-body">
                    <div class="box-sub">
                        <h3>PASIVA</h3>
                    </div>
                    <table class="table table-hover table-stripped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No. Akun</th>
                                <th>Nama Akun</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $no = 1;
                                $t_pasiva = 0;
                                $t_biaya = 0;
                                $t_parent_pasiva = 0;
                                $listPasiva = $mListLaporan->find()->where(['jenis_laporan' => 'PASIVA'])->orderBy(['akun_id' => SORT_ASC])->all();
                                foreach($listPasiva as $lapPasiva){
                                    $pasiva = $lapPasiva->akun->saldoJurnalRange($date1,$date2,$cabang_id);
                                    $akun = $lapPasiva->akun;

                                    if($akun->parent_kode == $akun->id){
                                        $labelAkun = "<b>".$akun->nama_akun."</b>";
                                    }
                                    else{
                                        $labelAkun = $akun->nama_akun;
                                    }
                            ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td>
                                    <?= Html::a($akun->id, ['buku-besar', 'start' => $date1, 'end' => $date2, 'cabang_id' => $cabang_id, 'akun_kode' => $akun->id],['target' => '_blank']); ?>
                                </td>
                                <td><?= $labelAkun; ?></td>
                                <td><?= Yii::$app->formatter->asCurrency($pasiva); ?></td>
                            </tr>
                            <?php
                                $t_parent_pasiva += $pasiva;
                                if($akun->id == $akun->kodeAkhir() && $akun->parent_kode != $akun->id){
                            ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <b>TOTAL <?= $akun->parentKode->nama_akun; ?></b>
                                    </td>
                                    <td>
                                        <b>
                                            <?= Yii::$app->formatter->asCurrency($t_parent_pasiva); ?>
                                        </b>
                                    </td>
                                </tr>
                                <?php
                                    $t_parent_pasiva = 0; //RESET TOTAL PARENT
                                }
                                    $t_pasiva += $pasiva;
                                }
                            ?>
                            <tr>
                                <td colspan="3">
                                    <strong>
                                        TOTAL PASIVA
                                    </strong>
                                </td>
                                <td><?= Yii::$app->formatter->asCurrency($t_pasiva); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>