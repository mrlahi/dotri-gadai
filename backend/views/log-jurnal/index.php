<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\models\Akun;
use common\models\Cabang;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogJurnalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log Jurnal';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?php //Html::a('Tambah Jurnal', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute' => 'akun_kode'
                ],
                [
                    'attribute' => 'akun_kode',
                    'label' => 'Nama Akun',
                    'value' => 'akunKode.nama_akun',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => Akun::listAkun(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                [
                    'attribute' => 'cabang_id',
                    'label' => 'Cabang',
                    'value' => 'cabang.nama_cabang',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => Cabang::listCabang(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                [
                    'attribute' => 'created_at', 
                    'label' => 'Tgl. Input',
                    'format' => 'date',
                    'filterType' => GridView::FILTER_DATE,
                    'filterWidgetOptions' => [
                        'pluginOptions' => [
                            'autoClear' => true,
                            'format' => 'yyyy-mm-d'
                        ]
                    ]
                ],
                [
                    'label' => 'No. SBG',
                    'value' => 'barang.no_kontrak'
                ],
                [
                    'attribute' => 'debet',
                    'format' => 'currency',
                    'filter' => false
                ],
                [
                    'attribute' => 'kredit',
                    'format' => 'currency',
                    'filter' => false
                ],
                'keterangan',
                //'created_at',
                //'created_by',
                //'jenis_asal_jurnal',
                //'asal_id',

                // [
                //     'class' => 'yii\grid\ActionColumn',
                //     'template' => '{delete-jurnal}',
                //     'buttons' => [
                //         'delete-jurnal' => function($url,$model){
                //             return Html::a('<span class="fa fa-trash"></span> Hapus',[$url], ['class' => 'btn btn-danger btn-sm', 'data' => ['confirm' => 'Yakin Ingin Menghapus Jurnal Ini..?? Jurnal yang terhapus tidak dapat dikembalikan..!!', 'method' => 'post']]);
                //         }
                //     ]
                // ],
            ],
        ]); ?>
    </div>
</div>
