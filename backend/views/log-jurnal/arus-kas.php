<?php

$this->title = "Arus Kas";
?>

<div class="box box-info">
    <div class="box-header">
        <h3><?= $this->title; ?></h3>
    </div>
    <div class="box-body">
            <strong>
                Range Tanggal : <?= Yii::$app->formatter->asDate($date1); ?> - <?= Yii::$app->formatter->asDate($date2); ?>
            </strong>
            <hr>
        <h4>Arus Kas Masuk Dari Aktivitas Operasional</h4>
        <table class="table table-hover table-stripped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>No. Akun</th>
                    <th>Nama Akun</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                    $t_masuk_operasional = 0;
                    $t_keluar_operasional = 0;
                    $t_masuk_investasi = 0;
                    $t_keluar_investasi = 0;
                    $t_masuk_pendanaan = 0;
                    $t_keluar_pendanaan = 0;
                    $listMasukOperasional = $mListLaporan->find()->where(['or',['jenis_laporan' => 'ARUS KAS MASUK DARI AKTIVITAS OPERASIONAL']])->orderBy(['akun_id' => SORT_ASC])->all();
                    foreach($listMasukOperasional as $lapMasukOperasional){
                        $masukOperasional = $lapMasukOperasional->akun->saldoArusKasRange($date1,$date2,$cabang_id,$lapMasukOperasional->posisi_perhitungan_khusus);
                        $akun = $lapMasukOperasional->akun;
                        $t_masuk_operasional += $masukOperasional;

                        if($akun->parent_kode == $akun->id){
                            $labelAkun = "<b>".$akun->nama_akun."</b>";
                        }
                        else{
                            $labelAkun = $akun->nama_akun;
                        }
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $akun->id; ?></td>
                    <td><?= $labelAkun; ?></td>
                    <td><?= Yii::$app->formatter->asCurrency($masukOperasional); ?></td>
                </tr>
                <?php
                    }
                ?>
                <tr>
                    <td colspan="3">
                        <strong>
                            Total Arus Kas Masuk Dari Aktivitas Operasional
                        </strong>
                    </td>
                    <td><?= Yii::$app->formatter->asCurrency($t_masuk_operasional); ?></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <h4>
                            Arus Kas Keluar  Dari Aktivitas Operasional 
                        </h4>
                    </td>
                </tr>
                <?php
                    $no = 1;
                    $listKeluarOperasional = $mListLaporan->find()->where(['or', ['jenis_laporan' => 'ARUS KAS KELUAR DARI AKTIVITAS OPERASIONAL']])->all();
                    foreach($listKeluarOperasional as $lapKeluarOperasional){
                        $keluarOperasional = $lapKeluarOperasional->akun->saldoArusKasRange($date1,$date2,$cabang_id,$lapKeluarOperasional->posisi_perhitungan_khusus);
                        $akun = $lapKeluarOperasional->akun;
                        $t_keluar_operasional += $keluarOperasional;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $akun->id; ?></td>
                    <td><?= $akun->nama_akun; ?></td>
                    <td><?= Yii::$app->formatter->asCurrency($keluarOperasional); ?></td>
                </tr>
                <?php
                    }
                ?>
                <tr>
                    <td colspan="3">
                        <strong>
                            Total Arus Kas Keluar  Dari Aktivitas Operasional 
                        </strong>
                    </td>
                    <td><?= Yii::$app->formatter->asCurrency($t_keluar_operasional); ?></td>
                </tr>

                <tr>
                    <td colspan="4">
                        <h4>
                            Arus Kas Masuk Dari Aktivitas Investasi 
                        </h4>
                    </td>
                </tr>
                <?php
                    $no = 1;
                    $listMasukInvestasi = $mListLaporan->find()->where(['or', ['jenis_laporan' => 'ARUS KAS MASUK DARI AKTIVITAS INVESTASI']])->all();
                    foreach($listMasukInvestasi as $lapMasukInvestasi){
                        $masukInvestasi = $lapMasukInvestasi->akun->saldoArusKasRange($date1,$date2,$cabang_id,$lapMasukInvestasi->posisi_perhitungan_khusus);
                        $akun = $lapMasukInvestasi->akun;
                        $t_masuk_investasi += $masukInvestasi;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $akun->id; ?></td>
                    <td><?= $akun->nama_akun; ?></td>
                    <td><?= Yii::$app->formatter->asCurrency($masukInvestasi); ?></td>
                </tr>
                <?php
                    }
                ?>
                <tr>
                    <td colspan="3">
                        <strong>
                            Total Arus Kas Masuk  Dari Aktivitas Investasi 
                        </strong>
                    </td>
                    <td><?= Yii::$app->formatter->asCurrency($t_masuk_investasi); ?></td>
                </tr>

                <tr>
                    <td colspan="4">
                        <h4>
                            Arus Kas Keluar Dari Aktivitas Investasi 
                        </h4>
                    </td>
                </tr>
                <?php
                    $no = 1;
                    $listKeluarInvestasi = $mListLaporan->find()->where(['or', ['jenis_laporan' => 'ARUS KAS KELUAR DARI AKTIVITAS INVESTASI']])->all();
                    foreach($listKeluarInvestasi as $lapKeluarInvestasi){
                        $keluarInvestasi = $lapKeluarInvestasi->akun->saldoArusKasRange($date1,$date2,$cabang_id,$lapKeluarInvestasi->posisi_perhitungan_khusus);
                        $akun = $lapKeluarInvestasi->akun;
                        $t_keluar_investasi += $keluarInvestasi;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $akun->id; ?></td>
                    <td><?= $akun->nama_akun; ?></td>
                    <td><?= Yii::$app->formatter->asCurrency($keluarInvestasi); ?></td>
                </tr>
                <?php
                    }
                ?>
                <tr>
                    <td colspan="3">
                        <strong>
                            Total Arus Kas Keluar  Dari Aktivitas Investasi 
                        </strong>
                    </td>
                    <td><?= Yii::$app->formatter->asCurrency($t_keluar_investasi); ?></td>
                </tr>

                <tr>
                    <td colspan="4">
                        <h4>
                            Arus Kas Masuk Dari Aktivitas Pendanaan  
                        </h4>
                    </td>
                </tr>
                <?php
                    $no = 1;
                    $listMasukPendanaan = $mListLaporan->find()->where(['or', ['jenis_laporan' => 'ARUS KAS MASUK DARI AKTIVITAS PENDANAAN']])->all();
                    foreach($listMasukPendanaan as $lapMasukPendanaan){
                        $masukPendanaan = $lapMasukPendanaan->akun->saldoArusKasRange($date1,$date2,$cabang_id,$lapMasukPendanaan->posisi_perhitungan_khusus);
                        $akun = $lapMasukPendanaan->akun;
                        $t_masuk_pendanaan += $masukPendanaan;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $akun->id; ?></td>
                    <td><?= $akun->nama_akun; ?></td>
                    <td><?= Yii::$app->formatter->asCurrency($masukPendanaan); ?></td>
                </tr>
                <?php
                    }
                ?>
                <tr>
                    <td colspan="3">
                        <strong>
                            Total Arus Kas Masuk  Dari Aktivitas Pendanaan 
                        </strong>
                    </td>
                    <td><?= Yii::$app->formatter->asCurrency($t_masuk_pendanaan); ?></td>
                </tr>

                <tr>
                    <td colspan="4">
                        <h4>
                            Arus Kas Keluar Dari Aktivitas Pendanaan  
                        </h4>
                    </td>
                </tr>
                <?php
                    $no = 1;
                    $listKeluarPendanaan = $mListLaporan->find()->where(['or', ['jenis_laporan' => 'ARUS KAS KELUAR DARI AKTIVITAS PENDANAAN']])->all();
                    foreach($listKeluarPendanaan as $lapKeluarPendanaan){
                        $keluarPendanaan = $lapKeluarPendanaan->akun->saldoArusKasRange($date1,$date2,$cabang_id,$lapKeluarPendanaan->posisi_perhitungan_khusus);
                        $akun = $lapKeluarPendanaan->akun;
                        $t_keluar_pendanaan += $keluarPendanaan;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $akun->id; ?></td>
                    <td><?= $akun->nama_akun; ?></td>
                    <td><?= Yii::$app->formatter->asCurrency($keluarPendanaan); ?></td>
                </tr>
                <?php
                    }
                ?>
                <tr>
                    <td colspan="3">
                        <strong>
                            Total Arus Kas Keluar  Dari Aktivitas Pendanaan 
                        </strong>
                    </td>
                    <td><?= Yii::$app->formatter->asCurrency($t_keluar_pendanaan); ?></td>
                </tr>

                <?php
                    $t_masuk = $t_masuk_operasional + $t_masuk_investasi + $t_masuk_pendanaan;
                    $t_keluar = $t_keluar_operasional + $t_keluar_investasi +  $t_keluar_pendanaan;

                    $naik_turun = $t_masuk - $t_keluar;
                    $saldoAwal = $model->saldoAwal($cabang_id);
                    $total_arus_kas = $saldoAwal + $naik_turun;

                ?>

                <tr>
                    <td colspan="3">
                        <h3>
                            Kenaikan/Penurunan Kas : 
                        </h3>
                    </td>
                    <td>
                        <h3>
                            <?= Yii::$app->formatter->asCurrency($naik_turun); ?>
                        </h3>
                    </td>
                </tr>

                <tr>
                    <td colspan="3">
                        <h3>
                            Saldo Awal Kas : 
                        </h3>
                    </td>
                    <td>
                        <h3>
                            <?= Yii::$app->formatter->asCurrency($saldoAwal); ?>
                        </h3>
                    </td>
                </tr>

                <tr>
                    <td colspan="3">
                        <h3>
                            Saldo Akhir Kas : 
                        </h3>
                    </td>
                    <td>
                        <h3>
                            <?= Yii::$app->formatter->asCurrency($total_arus_kas); ?>
                        </h3>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>

