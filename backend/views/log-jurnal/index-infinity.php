<?php
use ereminmdev\yii2\infinite_scroll\InfiniteScroll;
?>

<div class="pagination">
    <?= InfiniteScroll::widget([
        'pagination' => $dataProvider->getPagination(),
        'clientOptions' => [
            'container' => '.items',
            'item' => '.item',
            'pagination' => '.pagination',
        ],
        'clientExtensions' => [
            InfiniteScroll::EXT_TRIGGER => [
                'offset' => 0,
                'text' => Yii::t('app', 'Load more...'),
                'html' => '<div class="ias-trigger ias-trigger-next"><a class="btn btn-default">{text}</a></div>',
                'textPrev' => Yii::t('app', 'Load previous...'),
                'htmlPrev' => '<div class="ias-trigger ias-trigger-prev"><a class="btn btn-default">{text}</a></div>',
            ],
            InfiniteScroll::EXT_SPINNER => [
                'html' => '<div class="ias-spinner"><i class="fa fa-refresh fa-spin fa-lg"></i></div>',
            ],
            InfiniteScroll::EXT_NONE_LEFT => [
                'html' => 'You reached the end.',
            ],
        ],
        'clientEvents' => [
            'rendered' => new \yii\web\JsExpression('function() { console.log("on rendered"); }'),
        ],
    ]) ?>
</div>
<?php
$this->registerJs("
    $(document).on('click', '#sample_filter', function (event) {
    $('.list-view').load('sample_url', function () {
        //reinitialize plugin after load success
        jQuery.ias().reinitialize();
    });
    event.preventDefault();
})");
