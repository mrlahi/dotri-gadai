<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\number\NumberControl;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model common\models\JadwalShift */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jadwal-shift-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'nama_shift' => [],
            'hitung_lembur' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => ['SETELAH PULANG' => 'SETELAH PULANG', 'SEBELUM DATANG' => 'SEBELUM DATANG'],
                    'options' => [
                        'prompt' => 'Pilih Hitung Lembur'
                    ]
                ]
            ],
            'jam_masuk' => [
                'options' => [
                    'type' => 'time'
                ]
            ],
            'jam_pulang' => [
                'options' => [
                    'type' => 'time'
                ]
            ],
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
