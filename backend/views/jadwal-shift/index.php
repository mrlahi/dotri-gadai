<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\JadwalShiftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jadwal Shift';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a('Tambah Jadwal Shift', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'nama_shift',
                'jam_masuk',
                'jam_pulang',
                [
                    'attribute' => 'hitung_lembur',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ['SETELAH PULANG' => 'SETELAH PULANG', 'SEBELUM DATANG' => 'SEBELUM DATANG'],
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => 'Filter Lembur'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update}',
                    'buttons' => [
                        'update' => function($url,$model){
                            return Html::a('<span class="fa fa-edit"></span> Update', [$url], ['class' => 'btn btn-warning btn-sm']);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
