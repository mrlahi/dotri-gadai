<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\JadwalShift */

$this->title = 'Update Jadwal Shift : ' . $model->nama_shift;
$this->params['breadcrumbs'][] = ['label' => 'Jadwal Shifts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
