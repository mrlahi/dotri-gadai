<?php

use common\models\Profile;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Attendance';
$this->params['breadcrumbs'][] = $this->title;

$csrfToken = Yii::$app->request->csrfToken;
?>

<div class="box box-success">
    <div class="box-body">
        <?php if(Profile::myCabang() == "Outlet Not Set"){
        ?>
        <h1 align="center">Outlet For This Account Not Set Yet.</h1>
        <?php
        }
        else{
            $cabangActive = Profile::myProfile2()->cabangActive;
        ?>
        <h3>Outlet : <?= Profile::myCabang(); ?></h3>
        <strong id="display-distance"></strong>
        <p>Activate the camera to use the attendance.</p>

        <center>
            <video id="video" autoplay muted></video>
        </center>
        
        <button id="capture" class="btn btn-success btn-icon-split btn-block">
            <span class="icon text-white-50"><i class="fas fa-camera"></i></span>
            <span class="text"> Capture & Save</span>
        </button>

        <canvas id="snapshot" style="display: none;"></canvas>

        <?= Html::beginForm(['/check-in-out/record-attendance'], 'post', ['id' => 'attendance-form']) ?>
            <?= Html::hiddenInput('outlet_id', $cabangActive->id_cabang, ['id' => 'outlet-id']) ?>
            <?= Html::hiddenInput('image_data', '', ['id' => 'image-data']) ?>
            <?= Html::hiddenInput('profile_id', '',  ['id' => 'profile-id']) ?>
        <?= Html::endForm() ?>

        <?php } ?>
    </div>
</div>

<?php
$this->registerJsFile('/js/face-api/dist/face-api.js', ['position' => \yii\web\View::POS_HEAD]);

$script = <<<JS
const video = document.getElementById('video');
const snapshot = document.getElementById('snapshot');
const imageDataInput = document.getElementById('image-data');
const profileIdInput = document.getElementById('profile-id');
const outletIdInput = document.getElementById('outlet-id');

// Load models
Promise.all([
    faceapi.nets.tinyFaceDetector.loadFromUri('/js/face-api/weights'),
    faceapi.nets.faceLandmark68Net.loadFromUri('/js/face-api/weights'),
    faceapi.nets.faceRecognitionNet.loadFromUri('/js/face-api/weights'),
]).then(startVideo);

// Start webcam
function startVideo() {
    navigator.mediaDevices.getUserMedia({ video: {} })
        .then(stream => video.srcObject = stream)
        .catch(err => console.error(err));
}

// Capture image and match face
document.getElementById('capture').addEventListener('click', async () => {
    const detection = await faceapi.detectSingleFace(video, new faceapi.TinyFaceDetectorOptions())
        .withFaceLandmarks()
        .withFaceDescriptor();

    if (detection) {
        const descriptor = detection.descriptor;
        
        // Send descriptor to server to match face
        const response = await fetch('/check-in-out/match-face', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-Token': '<?= $csrfToken ?>'
            },
            body: JSON.stringify({ descriptor })
        });

        const result = await response.json();


        if (result.success) {
            // If face matched, save profile_id for attendance
            profileIdInput.value = result.profileId;

            const canvas = document.createElement('canvas');
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            const context = canvas.getContext('2d');
            context.drawImage(video, 0, 0, canvas.width, canvas.height);

            // Konversi gambar ke data URL
            const imageDataUrl = canvas.toDataURL('image/png');

            imageDataInput.value = imageDataUrl;

            // Submit attendance form
            document.getElementById('attendance-form').submit();
            
        } else {
            alert('Wajah tidak terdeteksi atau tidak cocok');
        }
    } else {
        alert('Wajah tidak terdeteksi. Pastikan kamera mengarah ke wajah Anda.');
    }
});


    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
        alert("Geolocation is not supported by this browser.");
    }

    function showPosition(position) {
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;
        const latitude1 = $cabangActive->latitude;
        const longitude1 = $cabangActive->longitude;

        let distance = haversine(latitude1, longitude, latitude, longitude);

        if(distance > 1){
            document.getElementById("capture").disabled = true;
            document.getElementById('display-distance').innerHTML = "Distance with Outlet too Far : " + distance.toFixed(2) + " km.";
            document.getElementById('display-distance').className = "text-danger";
        }
        else{
            document.getElementById('display-distance').innerHTML = "Distance with Outlet : " + distance.toFixed(2)*1000 + " mtr.";
            document.getElementById('display-distance').className = "text-success";
        }

    }

    function haversine(lat1, lon1, lat2, lon2, unit = "km") {
        const toRadian = angle => (Math.PI / 180) * angle;
        const earthRadius = unit === "km" ? 6371 : 3958.8; // Radius bumi: km atau mil

        const deltaLat = toRadian(lat2 - lat1);
        const deltaLon = toRadian(lon2 - lon1);

        const a = Math.sin(deltaLat / 2) ** 2 +
                Math.cos(toRadian(lat1)) * Math.cos(toRadian(lat2)) *
                Math.sin(deltaLon / 2) ** 2;

        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return earthRadius * c; // Jarak dalam unit yang dipilih
    }


    function showError(error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                alert("User denied the request for Geolocation.");
                break;
            case error.POSITION_UNAVAILABLE:
                alert("Location information is unavailable.");
                break;
            case error.TIMEOUT:
                alert("The request to get user location timed out.");
                break;
            case error.UNKNOWN_ERROR:
                alert("An unknown error occurred.");
                break;
        }
    }

JS;

$this->registerJs($script);
?>
