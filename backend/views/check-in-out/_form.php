<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CheckInOut */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="check-in-out-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'absen_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tgl_absen')->textInput() ?>

    <?= $form->field($model, 'jam_absen')->textInput() ?>

    <?= $form->field($model, 'cabang_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
