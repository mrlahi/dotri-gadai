<?php

use common\models\Cabang;
use common\models\Profile;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CheckInOutSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Check In Outs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?php // Html::a('Create Check In Out', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php

            $gridExport = [
                [
                    'attribute' => 'absen_id',
                    'label' => 'ID Absen'
                ],
                [
                    'attribute' => 'absen_id',
                    'label' => 'Pegawai', 
                    'value' => 'pegawai.nama',
                ],
                [
                    'attribute' => 'tgl_absen',    
                ],
                'jam_absen',
                [
                    'attribute' => 'cabang_id',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => Cabang::listCabang(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
            ];

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridExport,
                'showColumnSelector' => false,
                'exportConfig' => [
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_CSV => false,
                    ExportMenu::FORMAT_TEXT => false,
                    ExportMenu::FORMAT_PDF => [
                        'pdfConfig' => [
                            'orientation' => 'L',
                        ],
                    ],
                ],
                'filename' => 'Data-Absensi'
            ]);

        ?>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute' => 'absen_id',
                    'label' => 'ID Absen'
                ],
                [
                    'attribute' => 'absen_id',
                    'label' => 'Pegawai', 
                    'value' => 'pegawai.nama',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => Profile::listPegawaiAbsen(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                [
                    'attribute' => 'tgl_absen',
                    'format' => 'date',
                    'filterType' => GridView::FILTER_DATE_RANGE,
                    'filterWidgetOptions' => [       
                        'attribute' => 'tgl_masuk',
                        'pluginOptions' => [
                            'separator' => ' - ',
                            'format' => 'YYYY-MM-DD',
                            'locale' => [
                                'format' => 'YYYY-MM-DD'
                            ],
                        ],
                    ]
                ],
                'jam_absen',
                [
                    'attribute' => 'cabang_id',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => Cabang::listCabang(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                //'created_at',

                //['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
