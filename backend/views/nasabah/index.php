<?php

use common\models\Cabang;
use common\models\Pekerjaan;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\NasabahExtSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Nasabah';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
    <p>

        <?php

        $gridExport = [
            'no_ktp',
            'nama',
            'alamat:ntext',
            'email:email',
            [
                'attribute' => 'pekerjaan_id',
                'label' => 'Pekerjaan',
                'value' => 'pekerjaan.nama_pekerjaan',
            ],
            'no_hp',
            'no_hp2',
            //'created_by',
            //'created',
            //'akses',
            [
                'attribute' => 'id_cabang',
                'label' => 'Cabang',
                'value' => 'cabanguser.nama_cabang'
            ],
            [
                'attribute' => 'is_blacklist',
                'label' => 'Status Blacklist',
                'value' => 'isBlacklist'
            ],
            [
                'attribute' => 'created',
                'label' => 'Tgl. Input',
                'format' => 'date',
            ],
        ];

        //Tombol Exportnya
        echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridExport,
            'showColumnSelector' => false,
            'exportConfig' => [
                ExportMenu::FORMAT_HTML => false,
                ExportMenu::FORMAT_CSV => false,
                ExportMenu::FORMAT_TEXT => false,
                ExportMenu::FORMAT_PDF => [
                    'pdfConfig' => [
                        'orientation' => 'L',
                    ],
                ],
            ],
            'filename' => 'Data-Nasabah'
        ]);
        ?>

        <?= Html::a('<span class="fa fa-plus-circle"></span> Tambah Nasabah', ['create'], ['class' => 'btn btn-success']) ?>

        <?php echo Html::button('<span class="fa fa-money"></span> Cek Rekomendasi Harga Barang Gudang',['value'=>'/taksiran-harga-pasar/index-cek/','class'=>'btn btn-primary btn-pop']); ?>

        <?php echo Html::button('<span class="fa fa-money"></span> Cek Rekomendasi Harga Emas',['value'=>'/pasaran-harga-emas/index-cek/','class'=>'btn btn-info btn-pop']); ?>
    </p>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function($model,$key,$index,$widget) {
            if ($model->is_blacklist == 1) {
                return ['class' => 'danger'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_user',
            //'is_blacklist',
            'no_ktp',
            [
                'attribute' => 'nama',
                'filter' => Yii::$app->user->can('admin') ? $searchModel->nama : false
            ],
            'alamat:ntext',
            'email:email',
            [
                'attribute' => 'pekerjaan_id',
                'label' => 'Pekerjaan',
                'value' => 'pekerjaan.nama_pekerjaan',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Pekerjaan::listPekerjaan(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ],
            ],
            'no_hp',
            'no_hp2',
            //'created_by',
            //'created',
            //'akses',
            [
                'attribute' => 'id_cabang',
                'label' => 'Cabang',
                'value' => 'cabanguser.nama_cabang',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Cabang::listCabang(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ],
            ],
            [
                'attribute' => 'is_blacklist',
                'label' => 'Status Blacklist',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ['1' => 'BLACKLIST', '0' => 'WHITELIST'],
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ],
                'value' => 'isBlacklist'
            ],
            [
                'attribute' => 'alasan_blacklist',
            ],
            [
                'attribute' => 'created',
                'label' => 'Tgl. Input',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [       
                    'attribute' => 'created',
                    'pluginOptions' => [
                        'separator' => ' - ',
                        'format' => 'YYYY-MM-DD',
                        'locale' => [
                            'format' => 'YYYY-MM-DD'
                        ],
                    ],
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'blacklist' => function ($model){
                        if(Yii::$app->user->can('admin')){
                            return TRUE;
                        }
                        else if(Yii::$app->user->can('defaultFinance')){
                            return TRUE;
                        }
                    },
                    'create-barang' => function($model){
                        if($model->is_blacklist == 0){
                            return TRUE;
                        }
                    }
                ],
                'template' => '{create-barang} {update}<br>{blacklist}',
                'contentOptions' => ['style' => ['white-space' => 'nowrap']],
                'buttons' => [
                    'create-barang' => function($url,$model){
                        $link = '/barang/create?id_nasabah='.$model->id_user;
                        return Html::a('<span class="fa fa-plus"></span> Gadai Baru', [$link], ['class' => 'btn btn-info btn-xs m-2', 'data' => ['confirm' => 'Buat Gadai Baru Untuk Nasabah Ini..??']]);
                    },
                    'update' => function ($url, $model){
                        return Html::a('<span class="fa fa-edit"></span> Update', [$url], ['class' => 'btn btn-success btn-xs m-2']);
                    },
                    'blacklist' => function ($url, $model){
                        $targetUrl = '/nasabah/blacklist?id='.$model->id_user;
                        return Html::button('<span class="fa fa-close"></span> Blacklist/Whitelist',['value'=>Url::to([$targetUrl]),'class'=>'btn btn-danger btn-pop btn-xs m-2']);
                    },
                ]
            ],
        ],
    ]); ?>
    </div>
</div>

<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-lg',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");