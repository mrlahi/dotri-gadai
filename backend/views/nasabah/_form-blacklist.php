<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\NasabahExt */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nasabah-ext-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' =>[
            'is_blacklist' => [
                'label' => 'Status',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => ['1' => 'Blacklist', '0' => 'WhiteList'],
                    'options' => ['placeholder' => 'Pilih Jenis Status']
                ]
            ],
            'alasan_blacklist' => [
                'label'=> 'Alasan Status',
                'type' => Form::INPUT_TEXTAREA,

            ],
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
