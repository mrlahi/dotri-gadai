<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\NasabahExt */

$this->title = 'Blacklist Nasabah : ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Nasabah Ext', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id_user, 'url' => ['view', 'id' => $model->id_user]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <blockquote>
            Anda akan melakukan blacklist Nasabah. Nasabah yang diblacklist TIDAK DIPERBOLEHKAN melakukan transaksi disemua cabang Dotri.
        </blockquote>
    <?= $this->render('_form-blacklist', [
        'model' => $model,
    ]) ?>
    </div>
</div>
