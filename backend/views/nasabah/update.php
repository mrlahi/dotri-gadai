<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\NasabahExt */

$this->title = 'Update Nasabah Ext: ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Nasabah Exts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_user, 'url' => ['view', 'id' => $model->id_user]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
    <?= $this->render('_form-update', [
        'model' => $model,
    ]) ?>
    </div>
</div>
