<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LoginToken */

$this->title = 'Login Token';
$this->params['breadcrumbs'][] = ['label' => 'Login Tokens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box">

    <div class="login-logo">
        <!-- <img src="/images/logo-dotri.png" class="img img-fluid" width="50%"> -->
        <img src="/images/lock.png" class="img img-fluid" width="100%">
    </div>
    <div class="login-box-body">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
    

</div>
