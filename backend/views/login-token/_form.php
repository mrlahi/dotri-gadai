<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LoginToken */
/* @var $form yii\widgets\ActiveForm */
?>

<style>

#key{
    background: transparent;
    border: none;
    caret-color: transparent;
    text-align:center;
}


</style>


<div class="login-token-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'key')->textInput(['type' => 'password', 'id' => 'key', 'class' => 'form-control', 'autocomplete' => 'off', 'onfocus' => 'this.removeAttribute("readonly")'])->label(''); ?>


    <!-- <div class="form-group">
        <?php // Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div> -->

    <?php ActiveForm::end(); ?>

    <div id="ciye" class="col-md-12"></div>

</div>

<script>
    document.getElementById("key").focus();
    document.getElementById("key").value = "";

    let inputStart, inputStop;

    $("#key")[0].onpaste = e => e.preventDefault();
    // handle a key value being entered by either keyboard or scanner
    var lastInput

    let checkValidity = () => {
        if ($("#key").val().length < 10) {
            $("#key").val('');
            $("#ciye").html('<div class="bg-danger text-center">Eits ketahuan mau ngetik langsung.<br>Jangan nakal, udah pake scan kartu aja.!!</div>');
        } else {
            //$("body").append($('<div style="background:green;padding: 5px 12px;margin-bottom:10px;" id="msg">ok</div>'))
        }
        timeout = false
    }

    let timeout = false
    $("#key").keypress(function(e) {
        if (performance.now() - lastInput > 1000) {
            $("#key").val('')
        }
        lastInput = performance.now();
        if (!timeout) {
            timeout = setTimeout(checkValidity, 200)
        }
    });

</script>
