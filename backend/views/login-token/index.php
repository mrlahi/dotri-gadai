<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LoginTokenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Login Token';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute' => 'pegawai_id',
                    'label' => 'Pegawai',
                    'value' => 'pegawai.nama'
                ],
                [
                    'attribute' => 'koordinator_id',
                    'label' => 'Koordinator',
                    'value' => 'koordinator.nama'
                ],
                'cabang_id',
                'token',
                [
                    'attribute' => 'created_at',
                    'label' => 'Tgl. Login',
                    'format' => 'date'
                ],

                //['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>

    


</div>
