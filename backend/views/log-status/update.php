<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\LogStatusExt */

$this->title = 'Update Log Status Ext: ' . $model->id_status;
$this->params['breadcrumbs'][] = ['label' => 'Log Status Exts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_status, 'url' => ['view', 'id' => $model->id_status]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="log-status-ext-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
