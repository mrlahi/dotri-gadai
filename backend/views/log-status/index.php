<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\LogStatusExtSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log Status Exts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
//Table Yang Mau diexport
    $gridExport = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'No Kontrak',
            'value' => 'barang.no_kontrak'
        ],
        [
            'label' => 'Outlet',
            'value' => 'cabang.nama_cabang'
        ],
        [
            'label' => 'Tipe SBG',
            'value' => 'jenisBarang.nama_barang'
        ],
        [
            'label' => 'Tgl SBG',
            'value' => 'barang.tgl_masuk',
            'format' => 'date'
        ],
        [
            'label' => 'Jth Tempo',
            'value' => 'barang.jatuh_tempo',
            'format' => 'date'
        ],
        [
            'label' => 'Nasabah',
            'value' => 'nasabah.nama'
        ],
        [
            'label' => 'Nama Barang',
            'value' => 'barang.nama_barang'
        ],
        [
            'label' => 'Merk',
            'value' => 'barang.merk'
        ],
        [
            'label' => 'Tipe Barang',
            'value' => 'barang.tipe'
        ],
        [
            'label' => 'Kelengkapan',
            'value' => 'barang.kelengkapan'
        ],
        [
            'label' => 'Harga Taksir',
            'value' => 'barang.harga_taksir'
        ],
        [
            'label' => 'Nilai Pinjam',
            'value' => 'barang.nilai_pinjam'
        ],
        [
            'label' => 'Sisa Pinjam',
            'value' => 'sisaPinjam'
        ],
        [
            'label' => 'Tgl Pelunasan',
            'value' => 'tglLunas',
            'format' => 'date'
        ],
        [
            'label' => 'Status',
            'value' => 'barang.status'
        ],
    ];
?>

<?php
//Tombol Exportnya
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridExport,
    'showColumnSelector' => false,
    'exportConfig' => [
        ExportMenu::FORMAT_HTML => false,
        ExportMenu::FORMAT_CSV => false,
        ExportMenu::FORMAT_TEXT => false,
        ExportMenu::FORMAT_PDF => [
            'pdfConfig' => [
                'orientation' => 'L',
            ],
        ],
    ],
    'filename' => 'Data History'
]);
?>

<?php

/* echo \kartik\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridExport
]);
*/
    ?>

<div class="clearfix"></div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'nomor_kontrak',
                'label' => 'No Kontrak',
                'value' => 'barang.no_kontrak',
            ],
            [
                'label' => 'Outlet',
                'value' => 'cabang.nama_cabang'
            ],
            [
                'label' => 'Tipe SBG',
                'value' => 'jenisBarang.nama_barang'
            ],
            [
                'label' => 'Tgl SBG',
                'value' => 'barang.tgl_masuk',
                'format' => 'date'
            ],
            [
                'label' => 'Jth Tempo',
                'value' => 'barang.jatuh_tempo',
                'format' => 'date'
            ],
            [
                'label' => 'Nasabah',
                'value' => 'nasabah.nama'
            ],
            [
                'label' => 'Nama Barang',
                'value' => 'barang.nama_barang'
            ],
            [
                'label' => 'Merk',
                'value' => 'barang.merk'
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Tgl Status'
            ],
            [
                'attribute' => 'status',
                'label' => 'Status',
                'value' => 'status',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ['Aktif' => 'Aktif', 'JatuhTempo' => 'JatuhTempo', 'Lelang' => 'Lelang', 'Lunas'=>'Lunas','Terjual' => 'Terjual'],
                'filterWidgetOptions' => [
                    'options' => ['prompt' => 'Filter Status'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            [
                'attribute' => 'status_barang',
                'label' => 'Status Sekarang',
                'value' => 'barang.status',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ['Aktif' => 'Aktif', 'JatuhTempo' => 'JatuhTempo', 'Lelang' => 'Lelang', 'Lunas'=>'Lunas','Terjual' => 'Terjual'],
                'filterWidgetOptions' => [
                    'options' => ['prompt' => 'Filter Status'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ]
        ],
    ]); ?>

    <?php Pjax::end(); ?>
    </div>
</div>
