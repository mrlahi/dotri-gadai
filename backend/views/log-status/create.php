<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\LogStatusExt */

$this->title = 'Create Log Status Ext';
$this->params['breadcrumbs'][] = ['label' => 'Log Status Exts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-status-ext-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
