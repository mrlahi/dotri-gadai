<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HonorPegawai */

$this->title = 'Update Honor Pegawai: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Honor Pegawais', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="honor-pegawai-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
