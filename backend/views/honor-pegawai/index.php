<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel common\models\HonorPegawaiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Honor : '.$dataPegawai->nama;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?php

                echo Html::a('<span class="fa fa-backward"></span> Kembali', ['index-pegawai'], ['class' => 'btn btn-warning btn-sm'])." ";

                $link = '/honor-pegawai/create?pegawai_id='.$dataPegawai->id_user;
                echo Html::button('<span class="fa fa-plus"></span> Tambah Honor',['value'=> $link,'class'=>'btn btn-primary btn-pop btn-sm']);
            ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute' => 'honor',
                    'format' => 'currency'
                ],
                [
                    'attribute' => 'is_active',
                    'label' => 'Status',
                    'value' => function($model){
                        if($model->is_active){
                            return "AKTIF";
                        }
                        else{
                            return "NONAKTIF";
                        }
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ['0' => 'NONAKTIF', '1' => 'AKTIF'],
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                //'created_at',
                //'created_by',

                //['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>

<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-sm',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");

?>