<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HonorPegawai */

$this->title = 'Honor Baru : '.$dataPegawai->nama;
$this->params['breadcrumbs'][] = ['label' => 'Honor Pegawais', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-body">
        <h3><?= Html::encode($this->title) ?></h3>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
    

</div>
