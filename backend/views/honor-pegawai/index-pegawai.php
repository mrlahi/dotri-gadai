<?php

use yii\helpers\Html;
use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Pegawai';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id_user',
                'nama',
                'alamat:ntext',
                'email:email',
                'no_hp',
                [
                    'attribute' => 'id_cabang',
                    'label' => 'Cabang', 
                    'value' => 'cabanguser.nama_cabang',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => $searchModel->listCabang(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                //'tgl_lhr',
                //'created_by',
                //'created',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{daftar-honor}',
                    'buttons' => [
                        'daftar-honor' => function($url,$model){
                            return Html::a('<span class="fa fa-list"></span> Daftar Honor', [$url], ['class' => 'btn btn-info btn-sm']);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
