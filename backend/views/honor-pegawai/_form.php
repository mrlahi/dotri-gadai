<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $model common\models\HonorPegawai */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="honor-pegawai-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 3,
        'attributes' =>[
            'honor' => [
                'label' => 'Honor Pegawai',
                // 'options' => [
                //     'type' => 'number',
                //     'id' => 'harga_taksir',
                //     //'readonly' => 'readonly'
                // ]
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required', 
                        'id' => 'harga_taksir'
                    ]
                ]
            ],
        ]
        ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
