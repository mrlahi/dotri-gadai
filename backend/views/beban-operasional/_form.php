<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\number\NumberControl;
use common\models\KasBank;

/* @var $this yii\web\View */
/* @var $model common\models\BebanOperasional */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="beban-operasional-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 4,
        'attributes' =>[
            'nama_pengeluaran' => ['label' =>'Nama Pengeluaran', 'options' => ['placeholder' => 'Nama Pengeluaran']],
            'jumlah_pengeluaran' =>[
                'label' => 'Jumlah Pengeluaran',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                ]
            ],
            'type_operasional' =>[
                'label' => 'Tipe Operasional',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => $model->listOperasional(),
                    'options' => ['placeholder' =>'Jenis Operasional']
                ]
            ],
            'kas_bank_id' =>[
                'label' => 'Tipe Pencairan',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => KasBank::listKas(),
                ]
            ],
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
