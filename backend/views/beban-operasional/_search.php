<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BebanOperasionalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="beban-operasional-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_beban') ?>

    <?= $form->field($model, 'nama_pengeluaran') ?>

    <?= $form->field($model, 'jumlah_pengeluaran') ?>

    <?= $form->field($model, 'type_pencairan') ?>

    <?= $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
