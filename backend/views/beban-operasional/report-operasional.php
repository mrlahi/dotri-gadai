<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogFinanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Beban Operasional Hari Ini';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Beban Operasional</h3>
    </div>
    <div class="box-body">
        <table>
            <tr>
                <td><h4>Nama Cabang</h4></td>
                <td>:</td>
                <td>
                    <?php
                        if($id_cabang == 1000000){
                            $namaCabang = "SEMUA CABANG";
                        }
                        else{
                            $namaCabang = $model->myCabang($id_cabang)->nama_cabang;
                        }
                    ?>
                    <h4>&nbsp;&nbsp;<?= $namaCabang; ?></h4>
                </td>
            </tr>
            <tr>
                <td><h4>Tanggal</h4></td>
                <td>:</td>
                <td><h4>&nbsp;&nbsp;<?= date_format(date_create($date1),'D, d M Y')." - ". date_format(date_create($date2),'D, d M Y');?></h4></td>
            </tr>
        </table>
    </div>
</div>



<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Beban Operasional Hari Ini :</h3>
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <td>No</td>
                    <td>Nama Pengeluaran</td>
                    <td>Outlet</td>
                    <td>Jumlah Pengeluaran</td>
                    <td>Jenis Pengeluaran</td>
                    <td>Tgl</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no =1;
                    $t_operasional = 0;
                    $operasional = $model->dailyOperasional($date1,$date2,$id_cabang);
                    foreach ($operasional as $operasionals){
                        $t_operasional += $operasionals->jumlah_pengeluaran;
                ?>
               <tr>
                   <td><?= $no++; ?></td>
                   <td><?= $operasionals->nama_pengeluaran; ?></td>
                   <td><?= $operasionals->cabang->nama_cabang; ?></td>
                   <td>Rp. <?= number_format($operasionals->jumlah_pengeluaran,0,",","."); ?></td>
                   <td><?= $operasionals->jenisBiaya->akun->nama_akun; ?></td>
                   <td><?= date_format(date_create($operasionals->created_at),'D, d M Y') ?></td>
               </tr>
               <?php
                } ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td><b><?= "Rp. ". number_format($t_operasional,'2',',','.'); ?></b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

