<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BebanOperasional */

$this->title = 'Update Beban Operasional: ' . $model->id_beban;
$this->params['breadcrumbs'][] = ['label' => 'Beban Operasionals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_beban, 'url' => ['view', 'id' => $model->id_beban]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-warning">
    <div class="box-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="box-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
</div>
