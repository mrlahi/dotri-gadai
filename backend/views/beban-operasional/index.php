<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BebanOperasionalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Beban Operasional';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a('Tambah Operasional', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_beban',
            'nama_pengeluaran',
            [
                'attribute' => 'jumlah_pengeluaran',
                'label' => 'Jumlah pengeluaran',
                'value' => function ($model){
                        return "Rp. ".number_format($model->jumlah_pengeluaran,0,",",".");
                    },
                'filter' => false
            ],
            [
                'attribute' => 'type_operasional',
                'label' => 'Jenis',
                'value' => 'jenisBiaya.nama_jenis',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->listOperasional(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => 'Jenis Operasional'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
                
            ],
            [
                'attribute' => 'type_pencairan',
                'label' => 'Pengeluaran',
                'value' =>'type_pencairan',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ['Cash' => 'Cash', 'Transfer' => 'Transfer'],
                'filterWidgetOptions' => [
                    'options' => ['prompt' => 'Jenis Pengeluaran'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Tgl',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [       
                    'attribute' => 'tgl_masuk',
                    'pluginOptions' => [
                    'separator' => ' - ',
                    'format' => 'YYYY-MM-DD',
                    'locale' => [
                        'format' => 'YYYY-MM-DD'
                    ],
                ],
                ]
            ],
            [
                'attribute' => 'id_cabang',
                'label' => 'Outlet',
                'value' => 'cabang.nama_cabang',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->listCabang(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            //'created_by',
            //'created_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => function ($model){
                        if($model->created_at == date('Y-m-d')){
                            return TRUE;
                        }
                    },
                    'delete' => function ($model){
                        if($model->created_at == date('Y-m-d')){
                            return TRUE;
                        }
                    }
                ],
                'template' => '{delete}',
                'contentOptions' => ['style' => ['white-space' => 'nowrap']],
                'buttons' => [
                    'update' => function ($url, $model){
                        return Html::a('Update', [$url], ['class' => 'btn btn-primary']);
                    },
                    'delete' => function ($url, $model){
                        return Html::a('Hapus', [$url], ['class' => 'btn btn-danger', 'data' => ['confirm' => 'Yakin ingin menghapus data ini..??'], 'data-method' => 'post']);
                    }
                ]
            ],
        ],
    ]); ?>

</div>
</div>
