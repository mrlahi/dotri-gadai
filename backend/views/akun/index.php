<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AkunSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Akun';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a('Tambah Akun', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'label' => 'No. Akun'
                ],
                'nama_akun',
                [
                    'label' => 'Parent',
                    'value' => 'parentKode.nama_akun'
                ],
                //'created_by',
                //'created_at',
                //'saldo_normal',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update}',
                    'buttons' => [
                        'update' => function($url,$model){
                            return Html::a('<span class="fa fa-edit"></span> Update', [$url], ['class' => 'btn btn-sm btn-warning']);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
