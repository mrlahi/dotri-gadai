<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Akun */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akun-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' =>[
                'id' =>[
                    'label' => 'Kode Akun',
                    'options' => ['type' => 'number']
                ],
                'nama_akun' =>[
                    'options' => ['placeholder' => 'Nama Akun']
                ],
                'parent_kode' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::classname(),
                    'options' =>[
                        'data' => $model->listParent(),
                        'options' => ['placeholder' => 'Pilih Parent Akun']
                    ]
                ],
                'saldo_normal' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::classname(),
                    'options' =>[
                        'data' => [ 'DEBET' => 'DEBET', 'KREDIT' => 'KREDIT', 'DEFAULT' => 'DEFAULT', ],
                        'options' => ['placeholder' => 'Pilih Saldo Normal']
                    ]
                ],
                'status_laporan' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::classname(),
                    'options' =>[
                        'data' => [ 'AKTIVA' => 'AKTIVA', 'PASIVA' => 'PASIVA', 'BIAYA' => 'BIAYA', 'PAJAK' => 'PAJAK', 'BIAYA NON OPERASIONAL' => 'BIAYA NON OPERASIONAL', 'PENDAPATAN' => 'PENDAPATAN', 'PENDAPATAN NON OPERASIONAL' => 'PENDAPATAN NON OPERASIONAL', ' ' => 'DEFAULT'],
                        'options' => ['placeholder' => 'Pilih Status Laporan']
                    ]
                ]
            ]
        ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
