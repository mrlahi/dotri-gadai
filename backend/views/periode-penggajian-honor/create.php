<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PeriodePenggajianHonor */

$this->title = 'Create Periode Penggajian Honor';
$this->params['breadcrumbs'][] = ['label' => 'Periode Penggajian Honors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="periode-penggajian-honor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
