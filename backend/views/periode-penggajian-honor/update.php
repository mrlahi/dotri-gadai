<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PeriodePenggajianHonor */

$this->title = 'Update Periode Penggajian Honor: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Periode Penggajian Honors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="periode-penggajian-honor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
