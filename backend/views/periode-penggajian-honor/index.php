<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PeriodePenggajianHonorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Periode Penggajian '.Yii::$app->formatter->asDate($dataPeriode->start_date).' s/d '.Yii::$app->formatter->asDate($dataPeriode->end_date);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute' => 'nama_pegawai',
                    'value' => 'pegawai.nama'
                ],
                [
                    'label' => 'Gaji Pokok', 
                    'value' => 'honor.honor',
                    'format' => 'currency'
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{rekapitulasi}',
                    'buttons' => [
                        'rekapitulasi' => function($url,$model){
                            $link = '/rekapitulasi-absensi-pegawai?periode_penggajian_honor_id='.$model->id;
                            return Html::a('<span class="fa fa-print"> Rekapituasi', [$link], ['class' => 'btn btn-primary', 'target' => '_blank']);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
