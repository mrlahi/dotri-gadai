<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MerkBarang */

$this->title = 'Update Merk Barang : ' . $model->nama_merek;
$this->params['breadcrumbs'][] = ['label' => 'Merk Barangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-info">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]);
        ?>
    </div>
</div>

