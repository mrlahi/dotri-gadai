<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LogUangKeluar */

$this->title = 'Update Log Uang Keluar: ';
$this->params['breadcrumbs'][] = ['label' => 'Log Uang Keluars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_log_uang_keluar, 'url' => ['view', 'id' => $model->id_log_uang_keluar]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-info">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
    <?= $this->render('_form', [
        'model' => $model,
        'modelProfile' => $modelProfile,
    ]) ?>
    </div>
</div>
