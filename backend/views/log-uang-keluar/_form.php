<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\number\NumberControl;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\LogUangKeluar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-uang-keluar-form">

    <?php $form = ActiveForm::begin([
        'type'=>ActiveForm::TYPE_VERTICAL,
        'options' => [
            'data-pjax' => 'true',
            'id' => 'modal'
            ]
        ]);
        ?>
    <?php
    Pjax::begin(['id' => 'modal']);
    ?>
    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'nama_pengeluaran' => [
                'options' => [
                    'placeholder' => 'Nama Pengeluaran',
                ]
            ],
            'jumlah_pengeluaran' => [
                'label' => 'Jumlah Pengeluaran',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false,
                        'rightAlign' => false
                    ],
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required'
                    ]
                ],
            ],
            'type_pengeluaran' =>[
                'label' => 'Type Pengeluaran',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => ['Cash' => 'Cash', 'Transfer' => 'Transfer']
                ]
            ],
            'id_cabang' =>[
                'label' => 'Cabang',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => $modelProfile->listCabang()
                ]
            ],
            'created_at' =>[
                'label' => 'Tgl Pengeluaran',
                'options' => [
                    'type' => 'date',
                    'value' => date('Y-m-d')
                    ]
            ],
        ]
    ]);?>

    <?php Pjax::end(); ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
