<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LogUangKeluarSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-uang-keluar-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_log_uang_keluar') ?>

    <?= $form->field($model, 'nama_pengeluaran') ?>

    <?= $form->field($model, 'jumlah_pengeluaran') ?>

    <?= $form->field($model, 'type_pengeluaran') ?>

    <?= $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'id_cabang') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
