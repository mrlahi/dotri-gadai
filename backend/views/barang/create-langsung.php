<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Barang */

$this->title = 'Barang Baru';
$this->params['breadcrumbs'][] = ['label' => 'Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?= $this->render('_form-langsung', [
            'model' => $model,
            'modelJenisBarang' => $modelJenisBarang,
            'modelNasabah' => $modelNasabah,
            'id_nasabah' => $id_nasabah,
        ]) ?>
    </div>
</div>
