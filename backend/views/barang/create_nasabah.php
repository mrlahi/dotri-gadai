<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use common\models\Pekerjaan;

/* @var $this yii\web\View */
/* @var $model backend\models\NasabahExt */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nasabah-ext-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'no_ktp' => ['label' =>'Nomor KTP', 'options' => ['placeholder' => 'Nomor KTP']],
            'nama' => ['label' =>'Nama', 'options' => ['placeholder' => 'Nama Lengkap']],
            'email' => ['label' => 'Email','options' => ['placeholder' =>'Email Aktif','type' => 'text']],
            'no_hp' => ['label' => 'No. HP','options' => ['placeholder' =>'Nomor Handphone']],
            'pekerjaan_id' => [
                'label'=> 'Pekerjaan',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => Pekerjaan::listPekerjaan(),
                    'options' => ['placeholder' => 'Pekerjaan Nasabah']
                ],
            ],
        ]
    ]);
    ?>

<?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' =>[
            'alamat' => [
                'label'=> 'Alamat Lengkap',
                'type' => Form::INPUT_TEXTAREA,

            ],
            'domisili' => [
                'label'=> 'Domisili',
                'type' => Form::INPUT_TEXTAREA,

            ],
            
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
