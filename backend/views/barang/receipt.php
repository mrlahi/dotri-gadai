<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Receipt</title>

    <link href="https://fonts.googleapis.com/css2?family=VT323&display=swap" rel="stylesheet">

    <style>
        body{
            font-size:9pt;
            font-family: 'Arial';
        }
    </style>

</head>
<body>
<?php
if($modelLogReceipt->type_receipt == "Gadai Baru"){
?>
    <table align="center">
        <tr align="center">
            <td colspan="2">
                <strong>Dotri Gadai<br>
                <?= $modelLogReceipt->myCabang()->nama_cabang; ?> 
                </strong>
            </td>
        </tr>
        <tr align="center">
            <td>
            NOTA <?= $modelLogReceipt->type_receipt; ?>
            </td>
        </tr>
    </table>
   <table align="center">
    <tr>
        <td>Tanggal</td>
        <td></td>
        <td><?= date_format(date_create($modelLogReceipt->created_at),'d M y H:m'); ?></td>
    </tr>
    <tr>
        <td>No. Kwitansi</td>
        <td>:</td>
        <td><?= $modelLogReceipt->myBarang()->no_kontrak.$modelLogReceipt->id_log_receipt; ?></td>
    </tr>
    <tr>
        <td>No. SBG</td>
        <td>:</td>
        <td><?= $modelLogReceipt->myBarang()->no_kontrak; ?></td>
    </tr>
    <tr>
        <td>Barang</td>
        <td>:</td>
        <td><?= $modelLogReceipt->myBarang()->nama_barang." - ".$modelLogReceipt->myBarang()->merk." - ".$modelLogReceipt->myBarang()->tipe; ?></td>
    </tr>
    <tr>
        <td>SN/IMEI</td>
        <td>:</td>
        <td><?= $modelLogReceipt->myBarang()->serial; ?></td>
    </tr>
    <tr>
        <td>Pinjaman</td>
        <td>:</td>
        <td><?= "Rp. ".number_format($modelLogReceipt->myBarang()->nilai_pinjam,0,",","."); ?></td>
    </tr>
    <tr>
        <td>Biaya Admin</td>
        <td>:</td>
        <td><?= "-Rp. ".number_format($modelLogReceipt->myBarang()->biaya_admin,0,",","."); ?></td>
    </tr>
    <?php
    $bunga = $modelLogReceipt->myBarang()->nilai_pinjam * ($modelLogReceipt->myBarang()->bunga/100);
    $bersih = $modelLogReceipt->myBarang()->nilai_pinjam - $bunga - $modelLogReceipt->myBarang()->biaya_admin - $modelLogReceipt->myBarang()->biaya_simpan;
    ?>
    <tr>
        <td>Bunga <?= "(".$modelLogReceipt->barang->bunga."%)" ?></td>
        <td>:</td>
        <td><?= "-Rp. ".number_format($bunga,0,",","."); ?></td>
    </tr>
    <tr>
        <td>Penerimaan</td>
        <td>:</td>
        <td><?= $modelLogReceipt->type_pembayaran; ?></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2"><hr></td>
    </tr>
    <tr>
        <td>Total</td>
        <td>:</td>
        <td><strong> Rp. <?= number_format($bersih,0,",","."); ?></strong></td>
    </tr>
   </table>

   <p align="center">
    Apabila sampai dengan tanggal jatuh tempo nasabah tidak melunasi atau memperpanjang, maka nasabah memberikan kuasa kepada DOTRI GADAI untuk menjual barang jaminan tersebut.
   <br>
   NOTA TRANSAKSI INI MERUPAKAN SATU KESATUAN YANG TIDAK TERPISAHKAN DARI NO. SBG DIATAS
   <br>
    Terimakasi atas kepercayaan Anda kepada <br>
    DOTRI GADAI
   </p>

   <p align="center">
   <strong>Keamanan barang Anda prioritas kami</strong>
   </p>
    
    <table width="100%" align="center">
        <tr>
            <td width="50%" align="center">Petugas,</td>
            <td width="50%" align="center">Nasabah</td>
        </tr>
        <tr>
            <td><br><br><br></td>
            <td></td>
        </tr>
        <tr>
            <td width="50%" align="center"><?= $modelLogReceipt->myKasir()->nama; ?></td>
            <td width="50%" align="center"><?= $modelLogReceipt->myNasabah()->nama; ?></td>
        </tr>
    </table>
<?php }
else{
    ?>
    <table align="center">
        <tr align="center">
            <td colspan="2">
                <strong>Dotri Gadai<br>
                <?= $modelLogReceipt->myCabang()->nama_cabang; ?> 
                </strong>
            </td>
        </tr>
        <tr align="center">
            <td>
            NOTA <?= $modelLogReceipt->type_receipt; ?>
            </td>
        </tr>
    </table>
   <table align="center">
    <tr>
        <td>Tanggal</td>
        <td></td>
        <td><?= date_format(date_create($modelLogReceipt->created_at),'d M y H:m'); ?></td>
    </tr>
    <tr>
        <td>No. Kwitansi</td>
        <td>:</td>
        <td><?= $modelLogReceipt->myBarang()->no_kontrak.$modelLogReceipt->id_log_receipt; ?></td>
    </tr>
    <tr>
        <td>No. SBG</td>
        <td>:</td>
        <td><?= $modelLogReceipt->myBarang()->no_kontrak; ?></td>
    </tr>
    <tr>
        <td>Barang</td>
        <td>:</td>
        <td><?= $modelLogReceipt->myBarang()->nama_barang." - ".$modelLogReceipt->myBarang()->merk." - ".$modelLogReceipt->myBarang()->tipe; ?></td>
    </tr>
    <tr>
        <td>SN/IMEI</td>
        <td>:</td>
        <td><?= $modelLogReceipt->myBarang()->serial; ?></td>
    </tr>
    <tr>
        <td>Jatuh Tempo</td>
        <td>:</td>
        <td><?= date_format(date_create($modelLogReceipt->myBarang()->jatuh_tempo),'D, d M y'); ?></td>
    </tr>
    <?php
        $jatuh_tempo = $modelLogReceipt->myBarang()->jatuh_tempo;
        $jual = date("Y-m-d", strtotime($jatuh_tempo .$modelLogReceipt->barang->tglJual()) );
    ?>
    <tr>
        <td>Tgl. Jual</td>
        <td>:</td>
        <td><?= date_format(date_create($jual),'D, d M y'); ?></td>
    </tr>
    <tr>

        <?php
            if($modelLogReceipt->type_receipt == "Pelunasan"){
                $label = "Pelunasan";
            }
            else{
                $label = "Angsuran";
            }
        ?>
        <td><?= $label; ?></td>
        <td>:</td>
        <td><?= "Rp. ".number_format($modelLogReceipt->angsuran,0,",","."); ?></td>
    </tr>
    <tr>
        <td>Denda</td>
        <td>:</td>
        <td><?= "-Rp. ".number_format($modelLogReceipt->denda,0,",","."); ?></td>
    </tr>
    
    <tr>
        <td>Bunga <?= "(".$modelLogReceipt->barang->bunga."%)" ?></td>
        <td>:</td>
        <td><?= "-Rp. ".number_format($modelLogReceipt->bunga,0,",","."); ?></td>
    </tr>
    <tr>
        <td>Cashback</td>
        <td>:</td>
        <td><?= "-Rp. ".number_format($modelLogReceipt->cashback,0,",","."); ?></td>
    </tr>
    <tr>
        <td>Pembayaran</td>
        <td>:</td>
        <td><?= $modelLogReceipt->type_pembayaran; ?></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="2"><hr></td>
    </tr>
    <?php 
    $total = $modelLogReceipt->angsuran+$modelLogReceipt->denda+$modelLogReceipt->bunga;
    ?>
    <tr>
        <td>Total</td>
        <td>:</td>
        <td><strong> Rp. <?= number_format($total,0,",","."); ?></strong></td>
    </tr>
   </table>

   <p align="center">
   <strong>Keamanan barang Anda prioritas kami</strong>
   </p>
    
    <table width="100%" align="center">
        <tr>
            <td width="50%" align="center">Petugas,</td>
            <td width="50%" align="center">Nasabah</td>
        </tr>
        <tr>
            <td><br><br><br></td>
            <td></td>
        </tr>
        <tr>
            <td width="50%" align="center"><?= $modelLogReceipt->myKasir()->nama; ?></td>
            <td width="50%" align="center"><?= $modelLogReceipt->myNasabah()->nama;?></td>
        </tr>
    </table>
<?php }
?>

<script>
window.print();
</script>

</body>
</html>