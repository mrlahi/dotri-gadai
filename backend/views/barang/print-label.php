<style>

    body{
        font-family: 'Arial';
    }

    .label{
        width: 100%;
        text-align: center;
        margin-top: 0px;
        padding: 0px;
    }

    h1{
        margin-top: 0;
        margin-bottom: 0;
    }


</style>

<div class="label">
    <u>
        <h2><?= $model->no_kontrak; ?></h2>
    </u>
    
    <h3>
        <?= $model->nasabah->nama; ?><br>
        <?= $model->merk." - ".$model->tipe; ?><br>
        <?= "Rp. ".Yii::$app->formatter->asInteger($model->nilai_pinjam); ?><br>
        <?= Yii::$app->formatter->asDate($model->jatuh_tempo); ?>
    </h3>
</div>

<script>
    window.print();
</script>