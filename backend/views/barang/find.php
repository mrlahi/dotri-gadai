<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\dialog\Dialog;
use yii\helpers\Url;
use yii\bootstrap\Modal;

$this->title = $sbg;
$this->params['breadcrumbs'][] = $this->title;
?>

<style>

@import url('https://fonts.googleapis.com/css?family=Balthazar|Nunito:300');

body {
  background: #eceff1;
}

h1 {
	text-align: center;
	font-family: 'Balthazar', serif;
	letter-spacing: 2px;
}
p {
  margin: 2px 0;
}

.table {
  display: flex;
  justify-content: center;
  flex-flow: row wrap;
}

.card {
  width: 550px;
  max-height: 460px; 
  border: 1px solid #222;
  box-sizing: border-box;
  padding: 10px;
  text-align: center;
  font-family: 'Nunito', sans-serif;
  font-weight: 300;
  margin: 10px;
  box-shadow: 0 0 8px rgba(43,47,62,.1);
  transition: 1s all ease;
}

.property {
  border: 2px solid #222;
  text-align: center;
  text-transform: uppercase;
  padding: 5px;
  font-size: 28px;
  letter-spacing: 1px;
  font-family: 'Balthazar', serif;
  font-weight: 400;
  cursor: pointer;
  outline: none;
  transition: 1s all ease;
}

.yellow { background: #f8f218; }
.lightblue { background: #c1dced; }
.darkblue { background: #317cd5; }

.eyebrow {
  display: block;
  font-size: 14px;
  letter-spacing: 4px;
  margin-bottom: 5px;
}

::-webkit-details-marker {
  display: none;
}

.rent {
  text-transform: uppercase;
  margin-top: 10px;
}

.priceTable {
  width: 70%;
  display: flex;
  flex-wrap: wrap;
  margin: 0 auto;
  overflow: auto;
  height: 150px;
  
}

.priceTable .qty,
.priceTable .price { 
  width: 50%;
}

.priceTable .qty {
  text-transform: capitalize;
  text-align: left;
}

.priceTable .price {
  text-align: right;
}

.hotel {
  margin-top: 5px;
}

.disclaimer {
  font-size: 10px;
  margin: 10px 0;
}

.notice {
    padding: 15px;
    background-color: #fafafa;
    border-left: 6px solid #7f7f84;
    margin-bottom: 10px;
    -webkit-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
    -moz-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
    box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
}
.notice-sm {
    padding: 10px;
    font-size: 80%;
}
.notice-lg {
    padding: 35px;
    font-size: large;
}
.notice-success {
    border-color: #80D651;
}
.notice-success>strong {
    color: #80D651;
}
.notice-info {
    border-color: #45ABCD;
}
.notice-info>strong {
    color: #45ABCD;
}
.notice-warning {
    border-color: #FEAF20;
}
.notice-warning>strong {
    color: #FEAF20;
}
.notice-danger {
    border-color: #d73814;
}
.notice-danger>strong {
    color: #d73814;
}

</style>

<div class="box box-info">
    <div class="box-header">
        <h3>Data SBG : </h3>
    </div>
    <div class="box-body">
    <h1><?= $this->title; ?></h1>
    <hr>
    <?= Html::a('<span class="fa fa-print"></span> Print Invoice', ['invoice', 'id' => $model->myDetail($sbg)->id_barang], ['class' => 'btn btn-info btn-sm', 'target' => '_blank']); ?>
    <?php
      if($model->myDetail($sbg)->status != "Lunas"){//Jika barang sudah lunas, tombol tidak muncul
    ?>
    
      <?php } ?>
    <div class="clearfix"></div>
    <hr>
    <?php
      $id_cabang = $model->myDetail($sbg)->id_cabang;
    ?>
    <h3 align="center">Outlet : <?= $model->myCabang($id_cabang)->nama_cabang; ?></h3>
<div class="table">
  <section>
    <details class="card">
      <summary class="property lightblue">
        <span class="eyebrow">Klik Untuk Membuka</span>
        Data Barang
      </summary>
      <p class="rent"><strong><?= $model->myDetail($sbg)->nama_barang; ?></strong></p>
      <div class="priceTable">
        <div class="qty">Merk</div><div class="price"><?= $model->myDetail($sbg)->merk; ?></div>
        <div class="qty">Tipe</div><div class="price"><?= $model->myDetail($sbg)->tipe; ?></div>
        <div class="qty">Serial</div><div class="price"><?= $model->myDetail($sbg)->serial; ?></div>
        <div class="qty">Jenis SBG</div><div class="price"><?= $model->myDetail($sbg)->jenisBarang->nama_barang; ?></div>
        <div class="qty">Password</div><div class="price"><?= $model->myDetail($sbg)->kata_sandi; ?></div>
        <div class="qty">Tgl Masuk</div><div class="price"><?= date_format(date_create($model->myDetail($sbg)->tgl_masuk),'D, d M y'); ?></div>
        <div class="qty">Jatuh Tempo</div><div class="price"><?= date_format(date_create($model->myDetail($sbg)->jatuh_tempo),'D, d M y'); ?></div>
        <div class="qty">Nilai Pinjam</div><div class="price">Rp. <?= number_format($model->myDetail($sbg)->nilai_pinjam,0,",","."); ?></div>
        <div class="qty">Bunga</div><div class="price"><?= $model->myDetail($sbg)->bunga; ?> %</div>
        <div class="qty">Biaya Admin</div><div class="price">Rp. <?= number_format($model->myDetail($sbg)->biaya_admin,0,",","."); ?></div>
        <div class="qty">Nama Penafsir</div><div class="price"><?= $model->myDetail($sbg)->createdBy->nama; ?></div>
      </div>
    </details>

    <details class="card">
      <summary class="property lightblue">
        <span class="eyebrow">Klik Untuk Membuka</span>
        Nilai Barang
      </summary>
      <p class="rent">Rp. <?= number_format($model->myDetail($sbg)->nilai_pinjam,0,",","."); ?></p>
      <div class="priceTable">
        <div class="qty">Nilai Pinjaman</div><div class="price">Rp. <?= number_format($model->myDetail($sbg)->nilai_pinjam,0,",","."); ?></div>
        <div class="qty">Harga Taksir</div><div class="price">Rp. <?= number_format($model->myDetail($sbg)->harga_taksir,0,",","."); ?></div>
        <div class="qty">Harga Pasar</div><div class="price">Rp. <?= number_format($model->myDetail($sbg)->harga_pasar,0,",","."); ?></div>
        <div class="qty">Bunga</div><div class="price">
        <?php
        $bunga = $model->myDetail($sbg)->nilai_pinjam*$model->myDetail($sbg)->bunga/100;
        $diskon_bunga = $model->myDiskonBunga($sbg);
        $nilai_tagihan = $model->myDetail($sbg)->nilai_pinjam+$bunga-$diskon_bunga;
        ?>
        Rp. <?= number_format($bunga,0,",","."); ?>
        </div>
        <div class="qty">Diskon (5%)</div><div class="price"><u>Rp. <?= number_format($diskon_bunga,0,",","."); ?></u></div>
        <div class="qty">Nilai Tagihan</div><div class="price"><i> Rp. <?= number_format($nilai_tagihan,0,",","."); ?></i></div>
      </div>
    </details>

    <details class="card">
      <summary class="property lightblue">
        <span class="eyebrow">Klik Untuk Membuka</span>
        Data Nasabah
      </summary>
      <?php
        $myNasabah = $model->myNasabah2($model->myDetail($sbg)->id_nasabah);
      ?>
      <p class="rent"><?= $myNasabah->nama; ?></p>
      <div class="priceTable">
        <div class="qty">Nama Nasabah</div><div class="price"><?= $myNasabah->nama; ?></div>
        <div class="qty">No. KTP</div><div class="price"><?= $myNasabah->no_ktp; ?></div>
        <div class="qty">No. HP</div><div class="price"><?= $myNasabah->no_hp; ?></div>
        <div class="qty">Email</div><div class="price"><p><?= $myNasabah->email; ?></p></div>
      </div>
    </details>
  </section>

  <section>
    <details class="card">
      <summary class="property darkblue">
        <span class="eyebrow">Klik Untuk Membuka</span>
        Data Perpanjang
      </summary>
      <p class="rent">Perpanjangan Pinjaman</p>
      <div class="priceTable">
        <?php
            $perpanjangan_ke = 1;
            $dataPerpanjang = $modelPerpanjang->myDetailPerpanjang($model->myDetail($sbg)->id_barang);
            foreach ($dataPerpanjang as $dataPerpanjangs){
        ?>
              <div class="qty">Perpanjang <?= $perpanjangan_ke++ ?></div><div class="price"><?= $dataPerpanjangs-> created_at?></div>
            <?php 
          } ?>
      </div>
    </details>

    <details class="card">
      <summary class="property darkblue">
        <span class="eyebrow">Klik Untuk Membuka</span>
        Histori Barang
      </summary>
      <p class="rent">Pembayaran</p>
      <div class="priceTable">
      <?php
        $dataLogFinance = $modelLogFinance->myRiwayatFinance($model->myDetail($sbg)->id_barang);
        foreach ($dataLogFinance as $dataLogFinances){
      ?>
      <br>
        <div class="qty">
          <?= date_format(date_create($dataLogFinances->created_at),'D, d M y')." - ".$dataLogFinances->status; ?>
        </div>
        <div class="price">
          Rp. <?= number_format($dataLogFinances->jumlah_uang,0,",","."); ?>
        </div>
        <?php } ?>
      </div>
    </details>

    <details class="card">
      <summary class="property darkblue">
        <span class="eyebrow">Klik Untuk Membuka</span>
        Data Log
      </summary>
      <p class="rent">Log Status</p>
      <div class="priceTable">
      <?php
        $dataLog = $model->myDataLog($sbg);
        foreach ($dataLog as $dataLogs){
      ?>
        <div class="qty"><?= date_format(date_create($dataLogs->created_at),'D, d M y'); ?></div><div class="price"><?= $dataLogs->status; ?></div>
        <?php } ?>
      </div>
    </details>
  </section>
</div>
    </div>
</div>

<div class="box box-info">
    <div class="box-header">
          <h3>Rincian Biaya Sebelum Penjualan</h3>
    </div>
    <div class="box-body">
    <blockquote>
      <?php
        if(Yii::$app->user->can('admin')){
          $targetUrl = '/tambahan-jual/create-by-find?id_barang='.$model->myDetail($sbg)->id_barang."&sbg=".$sbg;
          echo Html::button('<span class="fa fa-money"></span> Tambah Biaya',['value'=>Url::to([$targetUrl]),'class'=>'btn btn-info btn-pop']);
        }
      ?>
      <hr>
  <table width="50%" border="0">
    <thead>
        <?php
        $no=1;
        $dataBiaya = $modelTambahan->find()->where(['id_barang' => $model->myDetail($sbg)->id_barang])->all();
        foreach ($dataBiaya as $dataBiayas){
        ?>
        <tr>
            <td><?= $no++ ?></td>
            <td><?= $dataBiayas->desk_tambahan; ?></td>
            <td>:</td>
            <td><?= "Rp. ". number_format($dataBiayas->biaya_tambah,'2',',','.'); ?></td>
            <td>
              <?php
                if(Yii::$app->user->can('admin')){
                  echo Html::a('<span class="fa fa-trash"></span> Hapus', ['/tambahan-jual/delete-by-find', 'id' => $dataBiayas->id_tambah_jual, 'sbg' => $sbg], ['class' => 'btn btn-danger btn-sm', 'data' => ['confirm' => 'Yakin ingin menghapus biaya..??', 'data-method' => 'post']]);
                }
              ?>
            </td>
        </tr>
        <?php } ?>
   </table>
   <hr>
   <h3>
        Total : Rp. <?= number_format($modelTambahan->myTotal($model->myDetail($sbg)->id_barang),'2',',','.'); ?>
   </h3>
   
</blockquote>
    </div>
</div>

  <div class="box box-success">
      <div class="box-header">
            <h3>Detail Spesifikasi</h3>
      </div>
      <div class="box-body">
      <div class="container">
        <div class="notice notice-lg">
          <strong>Nama Barang : </strong> <?= $model->myDetail($sbg)->nama_barang; ?>
          <br>
          <strong>Merk : </strong> <?= $model->myDetail($sbg)->merk; ?>
          <br>
          <strong>Tipe : </strong> <?= $model->myDetail($sbg)->tipe; ?>
        </div>
        <div class="notice notice-success notice-lg">
            <strong>Spesifikasi : </strong> <?= $model->myDetail($sbg)->spek; ?>
        </div>
        <div class="notice notice-danger notice-lg">
            <strong>Kelengkapan : </strong> <?= $model->myDetail($sbg)->kelengkapan; ?>
        </div>
        <div class="notice notice-info notice-lg">
            <strong>Kondisi : </strong> <?= $model->myDetail($sbg)->kondisi; ?>
        </div>
</div>
      </div>
  </div>



<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-lg',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");