<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\dialog\Dialog;
use yii\helpers\Url;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $searchModel common\models\BarangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Barang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'no_kontrak',
                'label' => "No.SBG",
                'format' => 'raw'
            ],
            [
                'attribute' => 'nama_barang',
            ],
            [
                'attribute' => 'merk',
            ],
            [
                'attribute' => 'tipe',
            ],
            [
                'attribute' => 'nilai_pinjam',
                'label' => 'Jumlah Pinjaman',
                'value' => 'JumlahPinjam'
            ],
            [
                'label' => 'SP',
                'value' => 'sisapinjam2',
                'format' => 'currency',
            ],
            //'tipe',
            //'serial',
            //'id_jenis',
            [
                'attribute' => 'nilai_pinjam',
                'label' => 'Jumlah Pinjaman',
                'value' => 'JumlahPinjam',
                'filter' => false
            ],
            [
                'label' => 'Tanggal Input',
                'value' => 'tgl_masuk',
                'format' => 'date',
                'filter' => false
            ],
            //'spek:ntext',
            //'kelengkapan:ntext',
            //'kondisi:ntext',
            //'harga_pasar',
            //'harga_taksir',
            //'nilai_pinjam',
            //'bunga',
            //'biaya_admin',
            //'biaya_simpan',
            //'jangka',
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'hapus' => function ($model){
                        if($model->tgl_masuk == date('Y-m-d')){
                            return TRUE;
                        }
                    }
                ],
                'template' => '{hapus}',
                'contentOptions' => ['style' => ['white-space' => 'nowrap']],
                'buttons' => [
                    'hapus' => function ($url, $model){
                        $targetUrl = '/barang/delete?id='.$model->id_barang;
                        return Html::a('<span class="fa fa-trash"></span> Hapus', [$targetUrl], ['class' => 'btn btn-warning','data' => ['confirm' => 'Yakin ingin menghapus barang..??','method' => 'post',]]);
                    },
                ]
            ],
        ],
    ]); ?>
    </div>
</div>


<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-lg',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");