<?php

use common\models\LokasiSimpan;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\number\NumberControl;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $model common\models\Barang */
/* @var $form yii\widgets\ActiveForm */
?> 

<div class="barang-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' =>[
          
            'lokasi_simpan_id' =>[
                'label' => 'Jenis Barang',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => LokasiSimpan::listLokasi(),
                    'options' => [
                        'placeholder' => 'Pilih Lokasi',
                    ]
                ]
            ],
            
        ]
    ]);
    
    ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>