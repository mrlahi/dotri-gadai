<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\dialog\Dialog;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\export\ExportMenu;
use common\models\JenisBarang;
/* @var $this yii\web\View */
/* @var $searchModel common\models\BarangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Barang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
//Table Yang Mau diexport
    $gridExport = [
        [
            'attribute' => 'nama',
            'label' => 'Nama Nasabah',
            'value' => 'nasabah.nama'
        ],
        [   
            'attribute' => 'no_ktp',
            'label' => 'No. KTP',
            'value' => function($model){
                return $model->nasabah->no_ktp;
            }
        ],
        [
            'label' => 'Pekerjaan',
            'value' => 'nasabah.pekerjaan.nama_pekerjaan'
        ],
        [
            'attribute' => 'no_kontrak',
            'label' => 'No. SBG',
        ],
        [
            'label' => 'No. Imei',
            'value' =>  function($model){
                return  "'".$model->barang->serial;
            }
        ],
        'nama_barang',
        'merk',
        'tipe',
        'spek',
        'kelengkapan',
        'kondisi',
        'kata_sandi',
        //'serial',
        //'id_jenis',
        [
            'attribute' => 'tgl_masuk',
            'label' => 'Tgl Masuk',
        ],
        [
            'attribute' => 'jatuh_tempo',
            'label' => 'Jatuh Tempo',
        ],
        [
            'attribute' => 'tgl_status',
            'label' => 'Tgl Lunas',
        ],
        [
            'label' => 'TJ',
            'value' => 'forjual',
        ],
        [
            'attribute' => 'nilai_pinjam',
            'label' => 'Jumlah Pinjaman',
        ],
        [
            'attribute' => 'bunga',
            'label' => 'Bunga',
            'value' => function($model){
                return $model->bunga ." %";
            }
        ],
        [
            'label' => 'Sisa Pinjam',
            'value' => 'sisapinjam2',
        ],
        [
            'attribute' => 'created_by',
            'label' => 'Pegawai',
            'value' => 'pegawai.nama',
        ],
        [
            'attribute' => 'id_cabang',
            'label' => 'Outlet',
            'value' => 'cabang.nama_cabang',
        ],
        [
            'attribute' => 'id_jenis',
            'label' => 'Jenis',
            'value' => 'jenisBarang.nama_barang',
        ],
    ];
?>

<?php
//Tombol Exportnya
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridExport,
    'showColumnSelector' => false,
    'exportConfig' => [
        ExportMenu::FORMAT_HTML => false,
        ExportMenu::FORMAT_CSV => false,
        ExportMenu::FORMAT_TEXT => false,
        ExportMenu::FORMAT_PDF => [
            'pdfConfig' => [
                'orientation' => 'L',
            ],
        ],
    ],
    'filename' => $judulFile
]);
?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => ['table-responsive, table-hover'],
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'nama',
                'label' => 'Nama Nasabah',
                'value' => 'nasabah.nama'
            ],
            [   
                'attribute' => 'no_ktp',
                'label' => 'No. KTP',
                'value' => 'nasabah.no_ktp'
            ],
            [
                'attribute' => 'no_kontrak',
                'label' => 'No. SBG',
            ],
            'nama_barang',
            //'tipe',
            //'serial',
            //'id_jenis',
            [
                'attribute'=>'tgl_masuk',
                'label' => 'Tgl Masuk'
            ],
            [
                'attribute' => 'jatuh_tempo',
                'label' => 'Jatuh Tempo',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'autoClear' => true,
                        'format' => 'YYYY-MM-DD'
                    ]
                ]
            ],
            [
                'attribute' => 'nilai_pinjam',
                'label' => 'Jumlah Pinjaman',
                'value' => 'JumlahPinjam'
            ],
            [
                'attribute' => 'created_by',
                'label' => 'Pegawai',
                'value' => 'pegawai.nama',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $modelProfile->listPegawai(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            [
                'attribute' => 'id_cabang',
                'label' => 'Outlet',
                'value' => 'cabang.nama_cabang',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->listCabang(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            [
                'attribute' => 'status',
                'label' => 'Status',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ['Aktif' => 'Aktif', 'JatuhTempo' => 'JatuhTempo', 'Lelang' => 'Lelang', 'Lunas'=>'Lunas','Terjual' => 'Terjual'],
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            //'spek:ntext',
            //'kelengkapan:ntext',
            //'kondisi:ntext',
            //'harga_pasar',
            //'harga_taksir',
            //'nilai_pinjam',
            //'bunga',
            //'biaya_admin',
            //'biaya_simpan',
            //'jangka',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{detail}',
                'contentOptions' => ['style' => ['white-space' => 'nowrap']],
                'buttons' => [
                    'detail' => function ($url, $model){
                        $targetUrl = '/barang/find?sbg='.$model->no_kontrak;
                        return '<a href="'.$targetUrl.'" target="_blank"><button class="btn btn-warning"><span class="fa fa-eye"></span> Detail</button></a>';
                    },
                ]
            ],
        ],
    ]); ?>

    
    </div>
</div>



<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-lg',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");