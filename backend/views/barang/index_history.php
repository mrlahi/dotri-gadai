<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\dialog\Dialog;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use common\models\Pekerjaan;
use common\models\JenisBarang;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel common\models\BarangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Barang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">

    
    <?php

    $gridExport = [
        [
            'attribute' => 'nama',
            'label' => 'Nama Nasabah',
            'value' => 'nasabah.nama'
        ],
        [   
            'label' => 'No. KTP',
            'value' => function($model){
                return "'".$model->nasabah->no_ktp;
            }
        ],
        [
            'label' => 'Almt',
            'value' => 'nasabah.alamat'
        ],
        [
            'label' => 'Domisili',
            'value' => 'nasabah.domisili'
        ],
        [
            'attribute' => 'pekerjaan_id',
            'label' => 'Pekerjaan', 
            'value' => 'nasabah.pekerjaan.nama_pekerjaan',
        ],
        [
            'attribute' => 'no_kontrak',
            'label' => 'No. SBG',
        ],
        [
            'attribute' => 'serial',
            'label' => 'No. Imei',
            'value' =>  function($model){
                return  "'".$model->serial;
            }
        ],
        'nama_barang',
        'merk',
        'tipe',
        'spek',
        'kelengkapan',
        'kondisi',
        'kata_sandi',
        [
            'attribute' => 'tgl_masuk',
            'label' => 'Tgl Masuk',
        ],
        [
            'attribute' => 'nilai_pinjam',
            'label' => 'Jumlah Pinjaman',
        ],
        [
            'label' => 'SP',
            'value' => 'sisapinjam2',
            'format' => 'currency',
        ],
        [
            'label' => 'Bunga',
            'value' => function($model){
                return $model->bunga ." %";
            }
        ],
        [
            'attribute' => 'status',
            'label' => 'Status',
            'filter' => false
        ],
        [
            'attribute' => 'tgl_status',
            'label' => 'Tgl Status',
        ],
        [
            'attribute' => 'id_cabang',
            'label' => 'Outlet',
            'value' => 'cabang.nama_cabang',
        ],
        [
            'attribute' => 'created_by',
            'label' => 'Pegawai',
            'value' => 'pegawai.nama',
        ],
        [
            'attribute' => 'id_jenis',
            'label' => 'Jenis',
            'value' => 'jenisBarang.nama_barang',
        ],
    ];
    //Tombol Exportnya
    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridExport,
        'showColumnSelector' => false,
        'exportConfig' => [
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_CSV => false,
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_PDF => [
                'pdfConfig' => [
                    'orientation' => 'L',
                ],
            ],
        ],
        'filename' => 'Data-Barang'
    ]);
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-striped table-hover'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'nama',
                'label' => 'Nama Nasabah',
                'value' => 'nasabah.nama'
            ],
            [   
                'attribute' => 'no_ktp',
                'label' => 'No. KTP',
                'value' => 'nasabah.no_ktp'
            ],
            [
                'label' => 'Almt',
                'value' => 'nasabah.alamat'
            ],
            [
                'label' => 'Domisili',
                'value' => 'nasabah.domisili'
            ],
            // [
            //     'attribute' => 'pekerjaan_id',
            //     'label' => 'Pekerjaan', 
            //     'value' => 'nasabah.pekerjaan.nama_pekerjaan',
            //     'filterType' => GridView::FILTER_SELECT2,
            //     'filter' => Pekerjaan::listPekerjaan(),
            //     'filterWidgetOptions' => [
            //         'options' => ['prompt' => ''],
            //         'pluginOptions' => [
            //             'allowClear' => true,
            //         ]
            //     ]
            // ],
            [
                'attribute' => 'no_kontrak',
                'label' => 'No. SBG',
            ],
            'nama_barang',
            'merk',
            'tipe',
            [
                'attribute' => 'serial',
                'label' => 'SN/IMEI',
            ],
            [
                'attribute' => 'tgl_masuk',
                'label' => 'Tgl Masuk',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [       
                    'attribute' => 'tgl_masuk',
                    'pluginOptions' => [
                    'separator' => ' - ',
                    'format' => 'YYYY-MM-DD',
                    'locale' => [
                        'format' => 'YYYY-MM-DD'
                    ],
                ],
                ]
            ],
            [
                'attribute' => 'nilai_pinjam',
                'label' => 'Jumlah Pinjaman',
                'value' => 'JumlahPinjam',
                'filter' => false
            ],
            [
                'label' => 'SP',
                'value' => 'sisapinjam2',
                'format' => 'currency',
            ],
            [
                'attribute' => 'bunga',
                'label' => 'Bunga',
                'value' => function($model){
                    return $model->bunga ." %";
                }
            ],
            [
                'attribute' => 'status',
                'label' => 'Status',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ['Aktif' => 'Aktif', 'Pasif' => 'Pasif', 'JatuhTempo' => 'JatuhTempo', 'Lunas' => 'Lunas', 'Lelang' => 'Lelang', 'Terjual' => 'Terjual'],
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            [
                'attribute' => 'tgl_status',
                'label' => 'Tgl Status',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [       
                    'attribute' => 'tgl_status',
                    'pluginOptions' => [
                      'separator' => ' - ',
                      'format' => 'YYYY-MM-DD',
                      'locale' => [
                            'format' => 'YYYY-MM-DD'
                        ],
                    ],
                  ]
            ],
            [
                'attribute' => 'id_cabang',
                'label' => 'Outlet',
                'value' => 'cabang.nama_cabang',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->listCabang(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            [
                'attribute' => 'created_by',
                'label' => 'Pegawai',
                'value' => 'pegawai.nama',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $modelProfile->listPegawai(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            [
                'attribute' => 'id_jenis',
                'label' => 'Jenis',
                'value' => 'jenisBarang.nama_barang',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => JenisBarang::listJenisBarang(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            //'spek:ntext',
            //'kelengkapan:ntext',
            //'kondisi:ntext',
            //'harga_pasar',
            //'harga_taksir',
            //'nilai_pinjam',
            //'bunga',
            //'biaya_admin',
            //'biaya_simpan',
            //'jangka',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{find} {note}',
                'contentOptions' => ['style' => ['white-space' => 'nowrap']],
                'buttons' => [
                    'find' => function ($url, $model){
                        $targetUrl = '/barang/find?sbg='.$model->no_kontrak;
                        return '<a href="'.$targetUrl.'" target="_blank"><button class="btn btn-info"><span class="fa fa-search"></span> Detail</button></a>';
                    },
                    'note' => function ($url, $model){
                        $targetUrl = '/note-barang/index?barang_id='.$model->id_barang;
                        return Html::button('<span class="fa fa-edit"></span> Note',['value'=>Url::to([$targetUrl]),'class'=>'btn btn-primary btn-pop']);
                    },
                ]
            ],
        ],
    ]); ?>

    
    </div>
</div>
<?php
if(!empty($_GET['idnya'])){
    ?>
    <script>
    window.open("/barang/invoice?id=<?= $_GET['idnya']; ?>");
    </script>
<?php }

if(!empty($_GET['receipt'])){
    ?>
     <script>
    window.open("/barang/receipt?id_receipt=<?= $_GET['id_receipt'] ?>");
    </script>
    <?php
}
?>


<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-lg',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");