<?php

use common\models\KasBank;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\widgets\Pjax;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\number\NumberControl;
?>

<?php Pjax::begin(); ?>
<div class="box box-primary">
<div class="box-header">
   <h3>
    Konfirmasi Pembayaran
    </h3> 
</div>
<div class="box-body">
<blockquote>
   <table class="table table-responsive table-striped">
        <tr>
            <td>Nilai Pinjaman</td>
            <td>:</td>
            <td><?= "Rp. ".number_format($model->nilai_pinjam,2,",","."); ?></td>
        </tr>
        <?php
                $kali_bunga = floor($modelBayar->myDenda($id)['telat']/30);//kalau telat sudah lebih dari 2 bulan
                if($kali_bunga == 0){
                    $kali_bunga = 1;
                }
                $bunganya = $modelBayar->myBunga($id)['bunga']*$kali_bunga;
            ?>
        <tr>
            <td>Telat (<?= $kali_bunga-1; ?> bulan)</td>
            <td>:</td>
            <td><?= $modelBayar->myDenda($id)['telat']," hari x 5000"; ?></td>
        </tr>
        <tr>
            <td>Denda</td>
            <td>:</td>
            <td><?= "Rp. ". number_format($modelBayar->myDenda($id)['denda'],2,",",".");?></td>
        </tr>

        <tr>
            <td>Biaya Admin</td>
            <td>:</td>
            <td><?= "Rp. ". number_format($model->biaya_admin,2,",",".");?></td>
        </tr>

        <tr>
            <td>Bunga (<?= $model->bunga*$kali_bunga ?>%)</td>
            <td>:</td>
            <td><?= "Rp. ". number_format($bunganya,2,",",".");?></td>
        </tr>

        <tr>
            <td>Kembali Bunga (Cash)</td>
            <td>:</td>
            <td><?= "Rp. ". number_format($modelBayar->myBunga($id)['diskon_bunga'],2,",",".");?></td>
        </tr>
        <tr>
            <td><strong>Total Hutang</strong></td>
            <td><strong>:</strong></td>
            <td>
                <strong>
                <?php
                $sisabayar = $model->nilai_pinjam+$model->biaya_admin+$model->biaya_simpan+$bunganya-$modelBayar->myBunga($id)['diskon_bunga']+$modelBayar->myDenda($id)['denda'];
                echo "Rp. ".number_format($sisabayar,2,",","."); ?>
                </strong>
            </td>
        </tr>
   </table>
   History Cicil :
   <ol>
        <?php
            $cicilan = $modelCicil->myRiwayatCicil($id);
            $sudah_bayar = 0;
            foreach($cicilan as $cicilans){
                $jum_cicil = $cicilans['jumlah_cicil'];
        ?>
            <li><?= "Rp. ".number_format($jum_cicil,2,",","."); ?> - <?= date_format(date_create($cicilans['created_at']),'d M Y'); ?></li>
            <?php 
                $sudah_bayar = $sudah_bayar + $jum_cicil;
            } ?>
   </ol>
   <h3> Sisa Hutang : <?= "Rp. ".number_format($sisa_hutang = $sisabayar - $sudah_bayar,2,",","."); ?></h>

</blockquote>

<?php
if(!Yii::$app->user->can('Penafsir')){

    $maxAngsur = $sisa_hutang - 1000;

?>
<?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

<?= Form::widget([
        'model' => $modelCicil,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'jumlah_cicil' =>[
                'label' => ' ',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false,
                        'min' => 1000,
                        'max' => $maxAngsur
                    ],
                    'options' => [
                        'placeholder' => 'Besar Angsuran',
                        'required'=>'required'
                    ]
                ]
            ]
        ]
]); ?>

<?= $form->field($modelLogFinance, 'kas_bank_id')->dropDownList(KasBank::listKas(),['prompt' => 'Tipe Pembayaran','required' => 'required','name' => 'kas_bank_id'])->label('') ?>

<?= Html::submitButton('<span class="fa fa-check"></span> Bayar', ['class' => 'btn btn-success pull-right']) ?>

<button class="btn btn-warning pull-right" data-dismiss="modal"><span class="fa fa-close"></span> Tidak</button>
<?php ActiveForm::end(); ?>
<?php } ?>
</div>
</div>

<?php Pjax::end(); ?>