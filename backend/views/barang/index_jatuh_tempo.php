<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\dialog\Dialog;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use common\models\JenisBarang;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel common\models\BarangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Barang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
    
    <?php

    $gridExport = [
        [
            'attribute' => 'nama',
            'label' => 'Nama Nasabah',
            'value' => 'nasabah.nama',
        ],
        [   
            'attribute' => 'no_ktp',
            'label' => 'No. KTP',
            'value' => function($model){
                return $model->nasabah->no_ktp;
            }
        ],
        [
            'label' => 'Pekerjaan',
            'value' => 'nasabah.pekerjaan.nama_pekerjaan'
        ],
        [
            'attribute' => 'no_kontrak',
            'label' => 'No. SBG',
        ],
        [
            'attribute' => 'serial',
            'label' => 'No. Imei',
            'value' =>  function($model){
                return  "'".$model->serial;
            }
        ],
        'nama_barang',
        'merk',
        'tipe',
        'spek',
        'kelengkapan',
        'kondisi',
        'kata_sandi',
        //'serial',
        //'id_jenis',
        [
            'attribute' => 'tgl_masuk',
            'label' => 'Tgl Masuk',
        ],
        [
            'attribute' => 'jatuh_tempo',
            'label' => 'Jatuh Tempo',
        ],
        [
            'attribute' => 'tgl_status',
            'label' => 'Tgl Lunas',
        ],
        [
            'label' => 'TJ',
            'value' => 'forjual',
        ],
        [
            'attribute' => 'nilai_pinjam',
            'label' => 'Jumlah Pinjaman',
        ],
        [
            'attribute' => 'bunga',
            'label' => 'Bunga',
            'value' => function($model){
                return $model->bunga ." %";
            }
        ],
        [
            'label' => 'Sisa Pinjam',
            'value' => 'sisapinjam2',
        ],
        [
            'label' => 'Sisa Pinjam',
            'value' => 'sisapinjam2',
        ],
        [
            'label' => 'Harga Jual',
            'value' => 'harga_jual',
        ],
        [
            'attribute' => 'created_by',
            'label' => 'Pegawai',
            'value' => 'pegawai.nama',
        ],
        [
            'attribute' => 'id_cabang',
            'label' => 'Outlet',
            'value' => 'cabang.nama_cabang',
        ],
        [
            'attribute' => 'id_jenis',
            'label' => 'Jenis',
            'value' => 'jenisBarang.nama_barang',
        ],
    ];

        //Tombol Exportnya
        echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridExport,
            'showColumnSelector' => false,
            'exportConfig' => [
                ExportMenu::FORMAT_HTML => false,
                ExportMenu::FORMAT_CSV => false,
                ExportMenu::FORMAT_TEXT => false,
                ExportMenu::FORMAT_PDF => [
                    'pdfConfig' => [
                        'orientation' => 'L',
                    ],
                ],
            ],
            'filename' => 'Data-Barang'
        ]);
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'nama',
                'label' => 'Nama Nasabah',
                'value' => 'nasabah.nama',
                'filter' => Yii::$app->user->can('admin') ? $searchModel->nama : false
            ],
            [   
                'attribute' => 'no_ktp',
                'label' => 'No. KTP',
                'value' => 'nasabah.no_ktp'
            ],
            [
                'attribute' => 'no_kontrak',
                'label' => 'No. SBG',
            ],
            'nama_barang',
            'merk',
            'tipe',
            [
                'attribute' => 'serial',
                'label' => 'SN/IMEI'
            ],
            [
                'attribute' => 'tgl_masuk',
                'label' => 'Tgl Masuk',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [       
                    'attribute' => 'tgl_masuk',
                    'pluginOptions' => [
                    'separator' => ' - ',
                    'format' => 'YYYY-MM-DD',
                    'locale' => [
                        'format' => 'YYYY-MM-DD'
                        ],
                    ],
                ]
            ],
            [
                'attribute' => 'jatuh_tempo',
                'label' => 'Jatuh Tempo',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [       
                    'attribute' => 'tgl_masuk',
                    'pluginOptions' => [
                    'separator' => ' - ',
                    'format' => 'YYYY-MM-DD',
                    'locale' => [
                        'format' => 'YYYY-MM-DD'
                        ],
                    ],
                ]
            ],
            [
                'label' => 'TJ',
                'value' => 'forjual',
                'format' => 'date'
            ],
            [
                'attribute' => 'nilai_pinjam',
                'label' => 'Jumlah Pinjaman',
                'value' => 'JumlahPinjam'
            ],
            [
                'attribute' => 'bunga',
                'label' => 'Bunga',
                'value' => function($model){
                    return $model->bunga ." %";
                }
            ],
            [
                'label' => 'SP',
                'value' => 'sisapinjam2',
                'format' => 'currency',
            ],
            [
                'label' => 'Denda',
                'value' => 'denda'
            ],
            [
                'attribute' => 'created_by',
                'label' => 'Pegawai',
                'value' => 'pegawai.nama',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $modelProfile->listPegawai(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            [
                'attribute' => 'id_cabang',
                'label' => 'Outlet',
                'value' => 'cabang.nama_cabang',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->listCabang(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            [
                'attribute' => 'id_jenis',
                'label' => 'Jenis',
                'value' => 'jenisBarang.nama_barang',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => JenisBarang::listJenisBarang(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            //'spek:ntext',
            //'kelengkapan:ntext',
            //'kondisi:ntext',
            //'harga_pasar',
            //'harga_taksir',
            //'nilai_pinjam',
            //'bunga',
            //'biaya_admin',
            //'biaya_simpan',
            //'jangka',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{lunaskan} {perpanjang} {receipt} {print}',
                'contentOptions' => ['style' => ['white-space' => 'nowrap']],
                'buttons' => [
                    'lunaskan' => function ($url, $model){
                        $targetUrl = '/barang/lunaskan?id='.$model->id_barang;
                        return Html::button('<span class="fa fa-money"></span> Lunaskan',['value'=>Url::to([$targetUrl]),'class'=>'btn btn-info btn-pop btn-sm','data' => ['confirm' => 'Pastikan anda mengisi dengan benar.']]);
                    },
                    'perpanjang' => function ($url, $model){
                        $targetUrl = '/barang/perpanjang?id='.$model->id_barang;
                        return Html::button('<span class="fa fa-plus"></span> Perpanjang',['value'=>Url::to([$targetUrl]),'class'=>'btn btn-warning btn-pop btn-sm']);
                    },
                    'receipt' => function ($url, $model){
                        $targetUrl = '/barang/log-receipt?id='.$model->id_barang;
                        return Html::button('<span class="fa fa-eye"></span> Receipt',['value'=>Url::to([$targetUrl]),'class'=>'btn btn-success btn-pop btn-sm']);
                    },
                    'print' => function ($url, $model){
                        $targetUrl = '/barang/invoice?id='.$model->id_barang;
                        return '<a href="'.$targetUrl.'" target="_blank"><button class="btn btn-warning btn-sm"><span class="fa fa-print"></span> Invoice</button></a>';
                    },
                ]
            ],
        ],
    ]); ?>
    </div>
</div>


<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-lg',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");