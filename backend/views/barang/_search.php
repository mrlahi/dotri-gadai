<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BarangSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="barang-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id_barang') ?>

    <?= $form->field($model, 'no_kontrak') ?>

    <?= $form->field($model, 'id_nasabah') ?>

    <?= $form->field($model, 'nama_barang') ?>

    <?= $form->field($model, 'tipe') ?>

    <?php // echo $form->field($model, 'serial') ?>

    <?php // echo $form->field($model, 'id_jenis') ?>

    <?php // echo $form->field($model, 'tgl_masuk') ?>

    <?php // echo $form->field($model, 'jatuh_tempo') ?>

    <?php // echo $form->field($model, 'spek') ?>

    <?php // echo $form->field($model, 'kelengkapan') ?>

    <?php // echo $form->field($model, 'kondisi') ?>

    <?php // echo $form->field($model, 'harga_pasar') ?>

    <?php // echo $form->field($model, 'harga_taksir') ?>

    <?php // echo $form->field($model, 'nilai_pinjam') ?>

    <?php // echo $form->field($model, 'bunga') ?>

    <?php // echo $form->field($model, 'biaya_admin') ?>

    <?php // echo $form->field($model, 'biaya_simpan') ?>

    <?php // echo $form->field($model, 'jangka') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
