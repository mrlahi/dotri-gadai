<table class="table table-hover table-strip">
    <thead>
    <th>
    <tr>
        <td>No. Kwitansi</td>
        <td>Tanggal</td>
        <td></td>
    </tr>
    </th>
    </thead>
    <tbody>
    <?php
    foreach ($modelLogReceipt as $modelLogReceipts){
    ?>
    <tr>
        <td><?= $modelLogReceipts->id_log_receipt; ?></td>
        <td><?= date_format(date_create($modelLogReceipts->created_at),'D, d M y'); ?></td>
        <td><a href="/barang/receipt?id_receipt=<?= $modelLogReceipts->id_log_receipt; ?>" target="_blank"><button class="btn btn-info"><span class="fa fa-print"></span></button></a></td>
    </tr>
    <?php } ?>
    </tbody>
</table>