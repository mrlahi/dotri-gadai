<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\number\NumberControl;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $model common\models\Barang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="barang-form">

    <?= Html::button('<span class="fa fa-user"></span> Tambah Nasabah',['value'=>'/barang/create-nasabah/','class'=>'btn btn-info btn-pop']); ?>

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 3,
        'attributes' =>[
            'id_cabang' => [
                'label' => 'Cabang Outlet',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => $model->listCabang(),
                    'options' => ['placeholder' => 'Pilih Cabang Outlet']
                ]
            ],
            'created_by' => [
                'label' => 'Kasir',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => $model->listPegawai(),
                    'options' => ['placeholder' => 'Pilih Kasir/Penafsir']
                ]
            ],
            'id_nasabah' =>[
                'label' => 'NAMA/KTP Nasabah',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => $modelNasabah->listNasabah(),
                    'options' => ['placeholder' => 'Ketikkan No. KTP atau Nama Nasabah','value' => $id_nasabah]
                ],
            ],
            'tgl_masuk' =>[
                'options' => [
                    'type' => 'date',
                ]
            ],
            'nama_barang' =>['label' => 'Nama Barang','options' => ['placeholder' =>'Nama Barang']],
            'merk' =>['label' => 'Merk Barang','options' => ['placeholder' =>'Merk Barang']],
            'tipe' => ['label' => 'Tipe Barang','options' => ['placeholder' =>'Tipe/Versi Barang']],
            'serial' => ['label' => 'Serial Number','options' => ['placeholder' =>'Serial Number']],
            'id_jenis' =>[
                'label' => 'Jenis Barang',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => $modelJenisBarang->listJenisBarang(),
                    'options' => ['placeholder' => 'Pilih Jenis Barang']
                ]
            ],
            'spek' => [
                'label' => 'Spesifikasi Barang',
                'type' => FORM::INPUT_TEXTAREA,
                'options' => ['placeholder' =>'Spesifikasi Lengkap...']
            ],
            'kelengkapan' => [
                'label' => 'Kelengkapan Barang',
                'type' => FORM::INPUT_TEXTAREA,
                'options' => ['placeholder' =>'Kelengkapan Barang...']
            ],
            'kondisi' => [
                'label' => 'Kondisi Barang',
                'type' => FORM::INPUT_TEXTAREA,
                'options' => ['placeholder' =>'Kondisi Barang...']
            ],
            'harga_pasar' => [
                'label' => 'Harga Pasaran',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required'
                    ]
                ]
            ],
            'harga_taksir' => [
                'label' => 'Harga Taksiran',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required'
                    ]
                ]
            ],
            'nilai_pinjam' => [
                'label' => 'Nilai Pinjam',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required'
                    ]
                ]
            ],
            'bunga' => ['label' => 'Bunga Pinjaman', 'options' => ['placeholder' => 'Bunga Pinjaman', 'type' => 'number', 'value' => '10','readonly' => 'readonly']],
            'biaya_admin' => [
                'label' =>'Biaya Admin',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                    'readonly' => true,
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required',
                        'value' => '0',
                        'readonly' => 'readonly',
                    ]
                ]
            ],
            'biaya_simpan' => [
                'label' => '',
                'options' => ['value' => '0', 'type' => 'hidden']
            ],
        ]
    ]);
?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-lg',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");