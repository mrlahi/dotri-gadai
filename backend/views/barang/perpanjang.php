<?php
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\number\NumberControl;
use common\models\KasBank;

$myDenda = $modelBayar->myDenda($id)['denda'];
$myBunga = $modelBayar->myBunga($id)['bunga'];
?>
<div class="box box-primary">
<div class="box-header">
   <h3>
    Konfirmasi Pembayaran
    </h3> 
</div>
<div class="box-body">
<blockquote>
    <?php
        if($modelPerpanjang->cekPerpanjangNow($id)){
            echo "<h1>Barang sudah diperpanjang hari ini.</h1>";
        }
        else{
    ?>
   <table class="table table-responsive table-striped">
        <tr>
            <td>Nilai Pinjaman</td>
            <td>:</td>
            <td><?= "Rp. ".number_format($model->nilai_pinjam,2,",","."); ?></td>
        </tr>
        <?php
                $kali_bunga = floor($modelBayar->myDenda($id)['telat']/30);//kalau telat sudah lebih dari 2 bulan
                if($kali_bunga == 0){
                    $kali_bunga = 1;
                }
                $bunganya = $modelBayar->myBunga($id)['bunga']*$kali_bunga;
            ?>
        <tr>
            <td>Telat (<?= $kali_bunga-1; ?> bulan)</td>
            <td>:</td>
            <td><?= $modelBayar->myDenda($id)['telat']," hari x 5000"; ?></td>
        </tr>
        <tr>
            <td>Denda</td>
            <td>:</td>
            <td><?= "Rp. ". number_format($modelBayar->myDenda($id)['denda'],2,",",".");?></td>
        </tr>

        <tr>
            <td>Biaya Admin</td>
            <td>:</td>
            <td><?= "Rp. ". number_format($model->biaya_admin,2,",",".");?></td>
        </tr>

        <tr>
            <td>Bunga (<?= $model->bunga*$kali_bunga ?>%)</td>
            <td>:</td>
            <td><?= "Rp. ". number_format($bunganya,2,",",".");?></td>
        </tr>
        <tr>
            <td><strong>Total Hutang</strong></td>
            <td><strong>:</strong></td>
            <td>
                <strong>
                <?php
                 $sisabayar = $model->nilai_pinjam+$model->biaya_admin+$model->biaya_simpan+$bunganya-$modelBayar->myBunga($id)['diskon_bunga']+$modelBayar->myDenda($id)['denda'];
                 echo "Rp. ".number_format($sisabayar,2,",","."); ?>
                </strong>
            </td>
        </tr>
   </table>
   History Cicil :
   <ol>
        <?php
            $cicilan = $modelCicil->myRiwayatCicil($id);
            $sudah_bayar = 0;
            foreach($cicilan as $cicilans){
                $jum_cicil = $cicilans['jumlah_cicil'];
        ?>
            <li><?= "Rp. ".number_format($jum_cicil,2,",","."); ?> - <?= date_format(date_create($cicilans['created_at']),'d M Y'); ?></li>
            <?php 
                $sudah_bayar = $sudah_bayar + $jum_cicil;
            } ?>
    </ol>

    <h3> Sisa Hutang : <?= "Rp. ".number_format($sisa_hutang = $sisabayar - $sudah_bayar,2,",","."); ?></h>
    <h4>Jatuh Tempo Selanjutnya : <?= date_format(date_create($model->jatuhTempoNew()),'D, d M Y'); ?></h4>

</blockquote>

<?php
    $dendaPerpanjang = $model->bunga/100*($sisa_hutang-$modelBayar->myDenda($id)['denda']);
    $nilaiPerpanjang = $dendaPerpanjang + $modelBayar->myDenda($id)['denda'];
?>
<hr>

<h3>Total Perpanjang : <?= Yii::$app->formatter->asCurrency($nilaiPerpanjang); ?></h3>
<hr>

<?php
if(!Yii::$app->user->can('Penafsir')){
?>
<?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $modelPerpanjang,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'denda' =>[
                'label' => 'denda',
                'options' => [
                    'value' => $modelBayar->myDenda($id)['denda'],
                    'readonly' => 'readonly'
                    ]
            ],
            'bunga' =>[
                'label' => 'bunga',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'readonly' => true,
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false,
                    ],
                    'options' => [
                        'placeholder' => 'Besar Bunga',
                        'value' => $dendaPerpanjang,
                        'required'=>'required',
                    ],
                ]
            ]
        ]
    ]); ?>
    <?= $form->field($modelLogFinance, 'kas_bank_id')->dropDownList(KasBank::listKas(),['prompt' => 'Tipe Pembayaran','required' => 'required','name' => 'kas_bank_id'])->label('') ?>

    <?= Html::submitButton('<span class="fa fa-check"></span> Perpanjang', ['class' => 'btn btn-success pull-right']); ?>
<button class="btn btn-warning pull-right" data-dismiss="modal"><span class="fa fa-close"></span> Tidak</button>
<?php ActiveForm::end(); ?>
<?php } ?>
<?php } ?>
</div>
</div>