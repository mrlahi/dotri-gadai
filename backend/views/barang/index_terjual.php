<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\dialog\Dialog;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use common\models\Barang;
use common\models\JenisBarang;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel common\models\BarangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Barang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">

    <?php

    $gridExport = [
        [
            'attribute' => 'nama',
            'label' => 'Nama Nasabah',
            'value' => 'barang.nasabah.nama'
        ],
        [   
            'attribute' => 'no_ktp',
            'label' => 'No. KTP',
            'value' => function($model){
                return "'".$model->barang->nasabah->no_ktp;
            }
        ],
        [
            'label' => 'Pekerjaan',
            'value' => 'barang.nasabah.pekerjaan.nama_pekerjaan'
        ],
        [
            'attribute' => 'barang.no_kontrak',
            'label' => 'No. SBG',
        ],
        [
            'label' => 'No. Imei',
            'value' =>  function($model){
                return  "'".$model->barang->serial;
            }
        ],
        [
            'label' => 'Nama Barang',
            'value' => 'barang.nama_barang'
        ],
        [
            'label' => 'Merk',
            'value' => 'barang.merk'
        ],
        [
            'label' => 'Tipe',
            'value' => 'barang.tipe'
        ],
        [
            'label' => 'Spek',
            'value' => 'barang.spek'
        ],
        [
            'label' => 'Kelengkapan',
            'value' => 'barang.kelengkapan'
        ],
        [
            'label' => 'Kondisi',
            'value' => 'barang.kondisi'
        ],
        [
            'label' => 'Password',
            'value' => 'barang.kata_sandi'
        ],
        [
            'value' => 'barang.tgl_masuk',
            'label' => 'Tgl Masuk',
        ],
        [
            'value' => 'barang.jatuh_tempo',
            'label' => 'Jatuh Tempo',
        ],
        [
            'label' => 'Jumlah Pinjaman',
            'value' => function($model){
                return $model->barang->nilai_pinjam;
            }
        ],
        [
            'attribute' => 'bunga',
            'label' => 'Bunga',
            'value' => function($model){
                return $model->barang->bunga ." %";
            }
        ],
        [
            'label' => 'Sisa Pinjam',
            'value' => 'barang.sisapinjam2',
        ],
        [
            'label' => 'Harga Jual',
            'value' => 'harga_jual',
        ],
        [
            'label' => 'Tanggal Jual',
            'value' => 'created_at',
        ],
        [
            'attribute' => 'created_by',
            'label' => 'Pegawai',
            'value' => 'barang.pegawai.nama',
        ],
        [
            'attribute' => 'id_cabang',
            'label' => 'Outlet',
            'value' => 'barang.cabang.nama_cabang',
        ],
        [
            'attribute' => 'id_jenis',
            'label' => 'Jenis',
            'value' => 'barang.jenisBarang.nama_barang',
        ],
    ];

    //Tombol Exportnya
    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridExport,
        'showColumnSelector' => false,
        'exportConfig' => [
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_CSV => false,
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_PDF => [
                'pdfConfig' => [
                    'orientation' => 'L',
                ],
            ],
        ],
        'filename' => 'Data-Barang'
    ]);
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Nama Nasabah',
                'value' => 'barang.nasabah.nama'
            ],
            [   
                'label' => 'No. KTP',
                'value' => 'barang.nasabah.no_ktp'
            ],
            [
                'attribute' => 'no_kontrak',
                'value' => 'barang.no_kontrak',
                'label' => 'No. SBG',
            ],
            [
                'attribute' => 'nama_barang',
                'value' => 'barang.nama_barang',
            ],
            [
                'attribute' => 'merk',
                'value' => 'barang.merk',
            ],
            [
                'attribute' => 'tipe',
                'value' => 'barang.tipe',
            ],
            [
                'attribute' => 'serial',
                'label' => 'SN/IMEI',
                'value' => 'barang.serial',
            ],
            [
                'attribute' => 'tgl_masuk',
                'value' => 'barang.tgl_masuk',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [       
                    'attribute' => 'tgl_masuk',
                    'pluginOptions' => [
                    'separator' => ' - ',
                    'format' => 'YYYY-MM-DD',
                    'locale' => [
                            'format' => 'YYYY-MM-DD'
                        ],
                    ],
                ]
            ],
            /*
            [
                'attribute' => 'jatuh_tempo',
                'label' => 'Jatuh Tempo',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'autoClear' => true,
                        'format' => 'yyyy-m-d'
                    ]
                ]
            ],
            */
            [
                'label' => 'Jumlah Pinjaman',
                'filter' => false,
                'value' => function($model){
                    return "Rp. ".number_format($model->barang->nilai_pinjam,'0',',','.');
                }
            ],
            [
                'label' => 'SP',
                'value' => 'barang.sisapinjam2',
                'format' => 'decimal',
            ],
            [
                'label' => 'Bunga',
                'value' => 'barang.bunga'
            ],
            [
                'label' => 'Harga Jual',
                'value' => function($model){
                    return "Rp. ".number_format($model->harga_jual,'0',',','.');
                }
            ],
            [
                'attribute' => 'jatuh_tempo',
                'value' => 'barang.jatuh_tempo',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'autoClear' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]
            ],
            [
                'attribute' => 'tgl_jual',
                'label' => 'Tgl Jual',
                'format' => 'date',
                'value' => 'created_at',
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [       
                    'attribute' => 'tgl_masuk',
                    'pluginOptions' => [
                    'separator' => ' - ',
                    'format' => 'YYYY-MM-DD',
                    'locale' => [
                            'format' => 'YYYY-MM-DD'
                        ],
                    ],
                ]
            ],
            [
                'label' => 'Status',
                'value' => 'status',
            ],
            [
                'attribute' => 'created_by',
                'label' => 'Pegawai',
                'value' => 'barang.pegawai.nama',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $modelProfile->listPegawai(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            [
                'attribute' => 'id_cabang',
                'label' => 'Outlet',
                'value' => 'barang.cabang.nama_cabang',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Barang::listCabang(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            [
                'attribute' => 'id_jenis',
                'label' => 'Jenis',
                'value' => 'barang.jenisBarang.nama_barang',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => JenisBarang::listJenisBarang(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            [
                'attribute' => 'is_jurnal',
                'label' => 'Status Jurnal',
                'value' => function($model){
                    if($model->is_jurnal){
                        return "SUDAH";
                    }
                    else{
                        return "BELUM";
                    }
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ['0' => 'BELUM', '1' => 'SUDAH'],
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{detail} {tambah-biaya} {receipt} {print} {jurnal}',
                'visibleButtons' => [
                    'tambah-biaya' => Yii::$app->user->can('admin'),
                    'jurnal' => function($model){
                        if(!$model->is_jurnal){
                            return TRUE;
                        }
                    }
                ],
                'contentOptions' => ['style' => ['white-space' => 'nowrap']],
                'buttons' => [
                    'detail' => function ($url, $model){
                        $targetUrl = '/barang/find?sbg='.$model->barang->no_kontrak;
                        return '<a href="'.$targetUrl.'" target="_blank"><button class="btn btn-warning"><span class="fa fa-eye"></span> Detail</button></a>';
                    },
                    'tambah-biaya' => function ($url, $model){               
                        $targetUrl = '/tambahan-jual/create-langsung?id_barang='.$model->id_barang;
                        return Html::button('<span class="fa fa-money"></span> Tambah Biaya',['value'=>Url::to([$targetUrl]),'class'=>'btn btn-info btn-pop']);
                    },
                    'receipt' => function ($url, $model){
                        $targetUrl = '/barang/log-receipt?id='.$model->id_barang;
                        return Html::button('<span class="fa fa-eye"></span> Receipt',['value'=>Url::to([$targetUrl]),'class'=>'btn btn-success btn-pop']);
                    },
                    'print' => function ($url, $model){
                        $targetUrl = '/barang/invoice?id='.$model->id_barang;
                        return '<a href="'.$targetUrl.'" target="_blank"><button class="btn btn-warning"><span class="fa fa-print"></span> Invoice</button></a>';
                    },
                    'jurnal' => function($url, $model){
                        $targetUrl = '/log-jurnal-penjualan/index?barang_id='.$model->id_barang;
                        return '<a href="'.$targetUrl.'"><button class="btn btn-info"><span class="fa fa-list"></span> Jurnal</button></a>';
                    }
                ]
            ],
        ],
    ]); ?>
    </div>
</div>


<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-lg',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");