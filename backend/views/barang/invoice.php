<?php
use barcode\barcode\BarcodeGenerator as BarcodeGenerator;

/* @var $this yii\web\View */
/* @var $model common\models\Barang */

$this->title = $model->no_kontrak;
$this->params['breadcrumbs'][] = ['label' => 'Barangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= $this->title; ?></title>
    <link href="https://fonts.googleapis.com/css2?family=Raleway+Dots&display=swap" rel="stylesheet">

    <style>
        
        body{
            font-family: 'Arial';
            font-weight: bold;
            font-size:10.5pt;
        }

        .utama{
            
            /*background-image: url("/images/invoice.jpg");*/
            background-repeat: no-repeat;
            background-size: cover;
            height:138mm;
            width:210mm;
        }

        .row{
            display:block;
            width:100%;
        }

        .box-logo{
            width: 40mm;
            height:40mm;
            padding:1px;
            float:left;
        }

        .box-aturan{
            width:103mm;
            height:81mm;
            padding:1px;
            float:right;
        }

        .box-ttd{
            width:105mm;
            height:30mm;
            float:left;
            padding:1px;

        }

        .box-1{
            width:65mm;
            height : 25mm;
            float:left;
            padding:1px;
        }

        .box-2{
            width: 100mm;
            height : 25mm;
            float:left;
            padding:1px;
            padding-left:3mm;
        }

        .box-3{
            width:65mm;
            height:15mm;
            float:left;
            padding:1px;
        }

        .box-4{
            width: 103mm;
            height: 15mm;
            float:left;
            padding:1px;
        }

        .box-5{
            width: 105mm;
            height: 25mm;
            float:left;
            padding:1px;
        }

        .box-6{
            width: 95mm;
            height:25mm;
            float:left;
            padding-left:5mm;
            
        }

        .box-7{
            width: 105mm;
            height:15mm;
            float:left;
            padding:1px;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
</head>

<body>
<br><br>
    <?php
    $optionsArray = array(
        'elementId'=> 'showBarcode', /* div or canvas id*/
        'value'=> $model->no_kontrak, /* value for EAN 13 be careful to set right values for each barcode type */
        'type'=>'code128',/*supported types  ean8, ean13, upc, std25, int25, code11, code39, code93, code128, codabar, msi, datamatrix*/
        'settings' => [
            'barWidth' => 2,
            'barHeight' => 50,
        ]
        );
        echo BarcodeGenerator::widget($optionsArray);
    ?>
    <div class="utama">
        <div class="row">
            <div class="box-logo">
            </div>
            <div class="box-1">
            <br>
                    Cabang/Outlet : <?= $model->myCabang($model->id_cabang)->nama_cabang; ?>
                    <br>
                    <?= $model->myCabang($model->id_cabang)->alamat; ?>
                    <br>
                    Tgl Izin : 21 Desember 2020
                    <br>
                    No izin: KEP-169/NB.I/2020
                    <br>
                    No. Telp/HP : <?= $model->myCabang($model->id_cabang)->telp; ?>
            </div>
            <div class="box-2">
            <br>
                <table cellpadding="2">
                    <tr>
                        <td>Harga Pasaran Setempat</td>
                        <td>:</td>
                        <td><?= number_format($model->harga_pasar,0,",","."); ?></td>
                        <td rowspan="3" border="1" align="center">
                            &nbsp;&nbsp;
                        </td>
                        <td rowspan="3" border="1" align="center">
                            <h2>
                                Bunga <br>
                                <?= $model->bunga." %" ?>
                            </h2>
                        </td>
                    </tr>
                    <tr>
                        <td>Harga Taksiran</td>
                        <td>:</td>
                        <td><?= number_format($model->harga_taksir,0,",","."); ?></td>
                    </tr>
                    <tr>
                        <td>Nilai Pinjaman</td>
                        <td>:</td>
                        <td><?= number_format($model->nilai_pinjam,0,",","."); ?></td>
                    </tr>
                </table>
            </div>
       </div>
       
       <div class="row">
            <div class="box-3">
            <br>
                    NO SBG : <?= $model->no_kontrak; ?>
                    <br>
                    NO FAK : <?= $model->no_fak; ?>
            </div>
            <div class="box-4">
                <table>
                    <tr>
                        <td>Tgl. Transaksi</td>
                        <td>:</td>
                        <td><?= date_format(date_create($model->tgl_masuk), 'd M Y'); ?></td>
                    </tr>
                    <tr>
                        <td>Tgl. Jatuh Tempo</td>
                        <td>:</td>
                        <td><?= date_format(date_create($model->jatuh_tempo), 'd M Y'); ?></td>
                    </tr>
                    <tr>
                        <td>Tgl. Jual</td>
                        <td>:</td>
                        <td><?= date_format(date_create($tgl_lelang), 'd M Y'); ?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="box-5">
            <br>
                <strong>
                    <?= "&nbsp;&nbsp;&nbsp;&nbsp;".$model->myNasabah()->no_ktp; ?><br>
                    <?= "&nbsp;&nbsp;&nbsp;&nbsp;".$model->myNasabah()->nama; ?> (<?= $model->myNasabah()->pekerjaan->nama_pekerjaan; ?>)<br>
                    <?= "&nbsp;&nbsp;&nbsp;&nbsp;".$model->myNasabah()->no_hp; ?> / <?= $model->myNasabah()->no_hp2; ?><br>
                    <?= "&nbsp;&nbsp;&nbsp;&nbsp;".$model->myNasabah()->alamat; ?><br>
                    <?= "&nbsp;&nbsp;&nbsp;&nbsp;Domisili : ".$model->myNasabah()->domisili; ?>
                </strong>
            </div>
            <div class="box-aturan">
            </div>
       </div>
       <div class="row">
            <div class="box-6">
            <br>
                <?= $model->nama_barang." - ".$model->merk." ".$model->tipe." - ".$model->spek." - ".$model->serial." - ".$model->kelengkapan." - ".$model->kondisi; ?>
                <br>
                Password : <?= $model->kata_sandi; ?>
            </div>
       </div>
      
       <div class="row">
            <div class="box-ttd"></div>
       </div>
    </div>
</body>
</html>

<script>
window.print();
</script>
