<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="box box-primary">
<div class="box-header">
   <h3>
    Detail Kontrak  : <?= $model->no_kontrak; ?>
    </h3> 
</div>
<div class="box-body">
<blockquote>
   <table class="table table-responsive table-striped table-hover">
        <tr>
            <td>Nama Nasabah</td>
            <td>:</td>
            <td><?= $model->myNasabah()->nama; ?></td>
        </tr>
        <tr>
            <td>Nama Barang</td>
            <td>:</td>
            <td><?= $model->nama_barang; ?></td>
        </tr>
        <tr>
            <td>Nilai Pinjaman</td>
            <td>:</td>
            <td><?= "Rp. ".number_format($model->nilai_pinjam,2,",","."); ?></td>
        </tr>

        <tr>
            <td>Status Barang</td>
            <td>:</td>
            <td><?= $model->status; ?></td>
        </tr>
        
        <tr>
            <td><strong>Harga Jual</strong></td>
            <td><strong>:</strong></td>
            <td>
                <strong>
                <?= "Rp. ".number_format($model->myTerjual()['harga_jual'],2,",","."); ?>
                </strong>
            </td>
        </tr>

        <tr>
            <td><strong>Dana Kembali</strong></td>
            <td><strong>:</strong></td>
            <td>
                <strong>
                <?= "Rp. ".number_format($model->myTerjual()['sisa'],2,",","."); ?>
                </strong>
            </td>
        </tr>
   </table>
</blockquote>

</div>
</div>