<?php
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\number\NumberControl;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use common\models\KasBank;

$this->title = 'Jual Barang';
?>
<div class="box box-primary">
<div class="box-header">
   <h3>
    Konfirmasi Pembayaran
    </h3> 
</div>
<div class="box-body">
<blockquote>
   <table class="table table-responsive table-striped">
        <tr>
            <td>Nilai Pinjaman</td>
            <td>:</td>
            <td><?= "Rp. ".number_format($model->nilai_pinjam,2,",","."); ?></td>
        </tr>
        <tr>
            <td>Sisa Pinjam</td>
            <td>:</td>
            <td><?= "Rp. ". number_format($model->getSisapinjam2(),'2',',','.'); ?></td>
        </tr>
        <tr>
            <td>Harga Taksir</td>
            <td>:</td>
            <td><?= "Rp. ".number_format($model->harga_taksir,2,",","."); ?></td>
        </tr>
        <tr>
            <td>Harga Pasar</td>
            <td>:</td>
            <td><?= "Rp. ".number_format($model->harga_pasar,2,",","."); ?></td>
        </tr>
        <tr>
            <td>Nama Barang</td>
            <td>:</td>
            <td><?= $model->nama_barang ?></td></td>
        </tr>
        <tr>
            <td>Merk</td>
            <td>:</td>
            <td><?= $model->merk; ?></td>
        </tr>
        <tr>
            <td>Tipe</td>
            <td>:</td>
            <td><?= $model->tipe; ?></td>
        </tr>
        <tr>
            <td>Spesifikasi</td>
            <td>:</td>
            <td><?= $model->spek; ?></td>
        </tr>
        <tr>
            <td>Kondisi</td>
            <td>:</td>
            <td><?= $model->kondisi; ?></td>
        </tr>
        <tr>
            <td>Kelengkapan</td>
            <td>:</td>
            <td><?= $model->kelengkapan; ?></td>
        </tr>
   </table>
    <hr>
    <?php
    $targetUrl = '/tambahan-jual/create?id_barang='.$id;
    echo Html::button('<span class="fa fa-money"></span> Tambah Biaya',['value'=>Url::to([$targetUrl]),'class'=>'btn btn-info btn-pop']);
    ?>
   <table class="table table-striped table-responsive">
    <thead>
        <tr>
            <th>No.</th>
            <th>Biaya Tambahan</th>
            <th>Jumlah Uang</th>
            <th>Hapus</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no=1;
        $dataBiaya = $modelTambahan->find()->where(['id_barang' => $id])->all();
        foreach ($dataBiaya as $dataBiayas){
        ?>
        <tr>
            <td><?= $no++ ?></td>
            <td><?= $dataBiayas->desk_tambahan; ?></td>
            <td><?= "Rp. ". number_format($dataBiayas->biaya_tambah,'2',',','.'); ?></td>
            <td>
                <?php
                    $targetUrl = '/tambahan-jual/delete?id='.$dataBiayas->id_tambah_jual."&id_barang=".$dataBiayas->id_barang;
                    echo Html::a('<span class="fa fa-trash"></span> Hapus', [$targetUrl], ['class' => 'btn btn-warning','data' => ['confirm' => 'Yakin ingin menghapus barang..??','method' => 'post',]]);
                ?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
   </table>
   <h3>
        Total : Rp. <?= number_format($modelTambahan->myTotal($id),'2',',','.'); ?>
   </h3>
   
</blockquote>
<?php
if(!Yii::$app->user->can('Penafsir')){
?>
<?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

<?= Form::widget([
        'model' => $modelJual,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'harga_jual' =>[
                'label' => 'Harga Jual',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false,
                        'required'=> true,
                    ],
                ]
            ],
        ]
]); ?>

<?= $form->field($modelLogFinance, 'kas_bank_id')->dropDownList(KasBank::listKasJual(),['prompt' => 'Tipe Pembayaran','required' => 'required','name' => 'kas_bank_id'])->label('') ?>

<?= Html::submitButton('<span class="fa fa-check"></span> Terjual', ['class' => 'btn btn-success pull-right']) ?>
<button class="btn btn-warning pull-right" data-dismiss="modal"><span class="fa fa-close"></span> Tidak</button>
<?php ActiveForm::end(); ?>
<?php } ?>
</div>
</div>

<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-lg',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");