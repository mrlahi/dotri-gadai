<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\dialog\Dialog;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use common\models\JenisBarang;
use common\models\LokasiSimpan;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel common\models\BarangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Barang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">

    <?php

    $gridExport = [
        [
            'attribute' => 'nama',
            'label' => 'Nama Nasabah',
            'value' => 'nasabah.nama'
        ],
        [   
            'attribute' => 'no_ktp',
            'label' => 'No. KTP',
            'value' => function($model){
                return $model->nasabah->no_ktp;
            }
        ],
        [
            'label' => 'Pekerjaan',
            'value' => 'nasabah.pekerjaan.nama_pekerjaan'
        ],
        [
            'attribute' => 'no_kontrak',
            'label' => 'No. SBG',
        ],
        [
            'attribute' => 'serial',
            'label' => 'No. Imei',
            'value' =>  function($model){
                return  "'".$model->serial;
            }
        ],
        'nama_barang',
        'merk',
        'tipe',
        'spek',
        'kelengkapan',
        'kondisi',
        'kata_sandi',
        //'serial',
        //'id_jenis',
        [
            'attribute' => 'tgl_masuk',
            'label' => 'Tgl Masuk',
        ],
        [
            'attribute' => 'jatuh_tempo',
            'label' => 'Jatuh Tempo',
        ],
        [
            'attribute' => 'tgl_status',
            'label' => 'Tgl Lunas',
        ],
        [
            'label' => 'TJ',
            'value' => 'forjual',
        ],
        [
            'attribute' => 'nilai_pinjam',
            'label' => 'Jumlah Pinjaman',
        ],
        [
            'attribute' => 'bunga',
            'label' => 'Bunga',
            'value' => function($model){
                return $model->bunga ." %";
            }
        ],
        [
            'label' => 'Sisa Pinjam',
            'value' => 'sisapinjam2',
        ],
        [
            'label' => 'Harga Jual',
            'value' => 'harga_jual',
        ],
        [
            'attribute' => 'created_by',
            'label' => 'Pegawai',
            'value' => 'pegawai.nama',
        ],
        [
            'attribute' => 'id_cabang',
            'label' => 'Outlet',
            'value' => 'cabang.nama_cabang',
        ],
        [
            'attribute' => 'id_jenis',
            'label' => 'Jenis',
            'value' => 'jenisBarang.nama_barang',
        ],
    ];

    //Tombol Exportnya
    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridExport,
        'showColumnSelector' => false,
        'exportConfig' => [
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_CSV => false,
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_PDF => [
                'pdfConfig' => [
                    'orientation' => 'L',
                ],
            ],
        ],
        'filename' => 'Data-Barang'
    ]);
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'nama',
                'label' => 'Nama Nasabah',
                'value' => 'nasabah.nama',
                'filter' => Yii::$app->user->can('admin') ? $searchModel->nama : false
            ],
            [   
                'attribute' => 'no_ktp',
                'label' => 'No. KTP',
                'value' => 'nasabah.no_ktp'
            ],
            [
                'attribute' => 'no_kontrak',
                'label' => 'No. SBG',
            ],
            'nama_barang',
            'merk',
            'tipe',
            [
                'attribute' => 'serial',
                'label' => 'SN/IMEI'
            ],
            [
                'attribute' => 'tgl_masuk',
                'label' => 'Tgl Masuk',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [       
                    'attribute' => 'tgl_masuk',
                    'pluginOptions' => [
                      'separator' => ' - ',
                      'format' => 'YYYY-MM-DD',
                      'locale' => [
                            'format' => 'YYYY-MM-DD'
                        ],
                    ],
                  ]
            ],
            [
                'attribute' => 'jatuh_tempo',
                'label' => 'Jatuh Tempo',
                'filterType' => GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => [       
                    'attribute' => 'tgl_masuk',
                    'pluginOptions' => [
                      'separator' => ' - ',
                      'format' => 'YYYY-MM-DD',
                      'locale' => [
                            'format' => 'YYYY-MM-DD'
                        ],
                    ],
                  ]
            ],
            [
                'label' => 'TJ',
                'value' => 'forjual',
                'format' => 'date'
            ],
            [
                'attribute' => 'nilai_pinjam',
                'label' => 'Jumlah Pinjaman',
                'value' => 'JumlahPinjam',
                'filter' => false
            ],
            [
                'label' => 'SP',
                'value' => 'sisapinjam2',
                'format' => 'currency',
            ],
            [
                'attribute' => 'bunga',
                'label' => 'Bunga',
                'value' => function($model){
                    return $model->bunga ." %";
                }
            ],
            [
                'label' => 'Denda',
                'value' => 'denda'
            ],
            [
                'attribute' => 'lokasi_simpan_id',
                'label' => 'Lokasi', 
                'value' => function($model){
                    if(empty($model->lokasi_simpan_id)){
                        $link = '/barang/update-lokasi?id='.$model->id_barang;
                        return Html::button('<span class="fa fa-upload"></span> Belum Ditentukan',['value' => Url::to([$link]), 'class'=>'btn-pop']);
                    }
                    else{
                        $link = '/barang/update-lokasi?id='.$model->id_barang;
                        return Html::button('<span class="fa fa-upload"></span> '.$model->lokasi->nama_lokasi,['value' => Url::to([$link]), 'class'=>'btn-pop']);
                    }
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => LokasiSimpan::listLokasi(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ],
                'format' => 'raw'
            ],
            [
                'attribute' => 'id_cabang',
                'label' => 'Outlet',
                'value' => 'cabang.nama_cabang',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->listCabang(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            [
                'attribute' => 'id_jenis',
                'label' => 'Jenis',
                'value' => 'jenisBarang.nama_barang',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => JenisBarang::listJenisBarang(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            //'spek:ntext',
            //'kelengkapan:ntext',
            //'kondisi:ntext',
            //'harga_pasar',
            //'harga_taksir',
            //'nilai_pinjam',
            //'bunga',
            //'biaya_admin',
            //'biaya_simpan',
            //'jangka',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{lunaskan} {lunaskan-pasif} {jual} {bayar} {perpanjang} {note} {pindah-belum-terjual} {pindah-lelang}',
                'contentOptions' => ['style' => ['white-space' => 'nowrap']],
                'visibleButtons' => [
                    'jual' => function($model){
                        if(Yii::$app->user->can('defaultFinance') OR Yii::$app->user->can('admin')){
                            return TRUE;
                        }
                    },
                    'pindah-belum-terjual' => function($model){
                        if($model->status == "Lelang" and (Yii::$app->user->can('admin') or Yii::$app->user->can('defaultFinance'))){
                            return TRUE;
                        }
                    },
                    'pindah-lelang' => function($model){
                        if($model->status == "Belum Terjual"){
                            return TRUE;
                        }
                    }
                ],
                'buttons' => [
                    'jual' => function ($url, $model){
                        $targetUrl = '/barang/jual-lelang?id='.$model->id_barang;
                        return Html::a('<span class="fa fa-gavel"></span> Jual', [$targetUrl], ['class' => 'btn btn-success btn-xs','data' => ['confirm' => 'Yakin ingin menjual barang..??']]);
                    },
                    'lunaskan' => function ($url, $model){
                        $targetUrl = '/barang/lunaskan?id='.$model->id_barang;
                        return Html::button('<span class="fa fa-money"></span> Gadai Ulang',['value'=>Url::to([$targetUrl]),'class'=>'btn btn-info btn-pop btn-xs']);
                    },
                    'lunaskan-pasif' => function ($url, $model){
                        $targetUrl = '/barang/lunaskan-pasif?id='.$model->id_barang;
                        return Html::button('<span class="fa fa-money"></span> Tebus Putus',['value'=>Url::to([$targetUrl]),'class'=>'btn btn-primary btn-pop btn-xs']);
                    },
                    // 'bayar' => function ($url, $model){
                    //     $targetUrl = '/barang/bayar?id='.$model->id_barang;
                    //     return Html::button('<span class="fa fa-money"></span> Bayar',['value'=>Url::to([$targetUrl]),'class'=>'btn btn-info btn-pop']);
                    // },
                    // 'perpanjang' => function ($url, $model){
                    //     $targetUrl = '/barang/perpanjang?id='.$model->id_barang;
                    //     return Html::button('<span class="fa fa-plus"></span> Perpanjang',['value'=>Url::to([$targetUrl]),'class'=>'btn btn-warning btn-pop']);
                    // },

                    'note' => function ($url, $model){
                        $targetUrl = '/note-barang/index?barang_id='.$model->id_barang;
                        return Html::button('<span class="fa fa-edit"></span> Note',['value'=>Url::to([$targetUrl]),'class'=>'btn btn-primary btn-pop btn-xs']);
                    },
                    'pindah-belum-terjual' => function($url, $model){
                        return Html::a('<span class="fa fa-undo"></span> Belum Terjual', [$url], ['class' => 'btn btn-danger btn-xs','data' => ['confirm' => 'Pindahkan ke Lelang Belum Terjual..??']]);
                    },
                    'pindah-lelang' => function($url, $model){
                        return Html::a('<span class="fa fa-undo"></span> Lelang', [$url], ['class' => 'btn btn-danger btn-xs','data' => ['confirm' => 'Pindahkan ke Lelang..??']]);
                    }
                ]
            ],
        ],
    ]); ?>
    </div>
</div>


<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-lg',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");