<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\number\NumberControl;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $model common\models\Barang */
/* @var $form yii\widgets\ActiveForm */
?> 

<div class="barang-form">

    <p>
        <?= Html::button('<span class="fa fa-user"></span> Tambah Nasabah',['value'=>'/barang/create-nasabah/','class'=>'btn btn-info btn-pop']); ?>

        <?php //" ".Html::button('<span class="fa fa-money"></span> Cek Rekomendasi Harga',['value'=>'/taksiran-grade/index-taksiran/','class'=>'btn btn-primary btn-pop']); ?>

        <?php //echo Html::button('<span class="fa fa-money"></span> Cek Rekomendasi Harga Barang Gudang',['value'=>'/taksiran-harga-pasar/index-cek/','class'=>'btn btn-primary btn-pop']); ?>

        <?php //echo Html::button('<span class="fa fa-money"></span> Cek Rekomendasi Harga Emas',['value'=>'/pasaran-harga-emas/index-cek/','class'=>'btn btn-success btn-pop']); ?>
    </p>

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 3,
        'attributes' =>[
            // 'id_nasabah' =>[
            //     'label' => 'NAMA/KTP Nasabah',
            //     'type' => Form::INPUT_WIDGET,
            //     'widgetClass' => Select2::classname(),
            //     'options' =>[
            //         'data' => $modelNasabah->listNasabah(),
            //         'options' => ['placeholder' => 'Ketikkan No. KTP atau Nama Nasabah','value' => $id_nasabah]
            //     ],
            // ],
            'no_fak' => [
                'options' => [
                    'placeholder' => 'Isi dengan Nomor FAK'
                ]
            ],
            'nama_barang' =>['label' => 'Nama Barang','options' => ['placeholder' =>'Nama Barang']],
            'merk' =>['label' => 'Merk Barang','options' => ['placeholder' =>'Merk Barang']],
            'tipe' => ['label' => 'Tipe Barang','options' => ['placeholder' =>'Tipe/Versi Barang']],
            'serial' => ['label' => 'Serial Number/IMEI','options' => ['placeholder' =>'Serial Number/IMEI']],
            'id_jenis' =>[
                'label' => 'Jenis Barang',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => $modelJenisBarang->listJenisBarang(),
                    'options' => [
                        'placeholder' => 'Pilih Jenis Barang',
                        'id' => 'jenis-barang',
                        'onchange' => '
                            $("#biaya_admin").val("0");
                            if($(this).val() == "4"){
                                $("#harga_taksir").prop("readonly",false);
                            }else{
                                //$("#harga_taksir").prop("readonly",true);
                                //$("#harga_taksir").val("");
                            }
                            
                        '
                    ]
                ]
            ],
            'spek' => [
                'label' => 'Spesifikasi Barang',
                'type' => FORM::INPUT_TEXTAREA,
                'options' => ['placeholder' =>'Spesifikasi Lengkap...']
            ],
            'kelengkapan' => [
                'label' => 'Kelengkapan Barang',
                'type' => FORM::INPUT_TEXTAREA,
                'options' => ['placeholder' =>'Kelengkapan Barang...']
            ],
            'kondisi' => [
                'label' => 'Kondisi Barang',
                'type' => FORM::INPUT_TEXTAREA,
                'options' => ['placeholder' =>'Kondisi Barang...']
            ],
            'kata_sandi' => [
                'options' => [
                    'placehorlder' => 'Kata Sandi Perangkat'
                ]
            ],
        ]
    ]);
    
    ?>

<?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 3,
        'attributes' =>[
            'harga_pasar' => [
                'label' => 'Harga Pasaran',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required'
                    ]
                ]
            ],
            'harga_taksir' => [
                'label' => 'Harga Taksiran',
                // 'options' => [
                //     'type' => 'number',
                //     'id' => 'harga_taksir',
                //     //'readonly' => 'readonly'
                // ]
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required', 
                        'id' => 'harga_taksir'
                    ]
                ]
            ],
            'nilai_pinjam' => [
                'label' => 'Nilai Pinjam',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required'
                    ]
                ]
            ],
            'jangka' => [
                'label' => 'Jangka Waktu',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => ['15' => '15 Hari', '30' => '30 Hari'],
                    'options' => ['placeholder' => 'Pilih Jangka Waktu Gadai', 'id' => 'jangka', 'onchange' => '
                    if($(this).val() == "30"){
                        if($("#jenis-barang").val() == "4"){
                            $("#bunga").val("2.4");
                            $("#biaya_admin").val("20000");
                        }
                        else{
                            $("#bunga").val("10");
                            $("#biaya_admin").val("0");
                        }
                    }
                    else {
                        if($("#jenis-barang").val() == "4"){
                            $("#bunga").val("1.2");
                            $("#biaya_admin").val("20000");
                        }
                        else{
                            $("#bunga").val("5");
                            $("#biaya_admin").val("0");
                        }
                    }
                    '],
                ]
            ],
            'bunga' => ['label' => 'Bunga Pinjaman', 'options' => ['placeholder' => 'Bunga Pinjaman', 'type' => 'number','readonly' => 'readonly', 'id' => 'bunga']],
            'biaya_admin' => [
                'label' =>'Biaya Admin',
                'options' => [
                    'type' => 'number',
                    'id' => 'biaya_admin',
                    'readonly' => 'readonly'
                ]
                // 'type' => Form::INPUT_WIDGET,
                // 'widgetClass' => NumberControl::classname(),
                // 'options' => [
                //     'maskedInputOptions' =>[
                //         'prefix' => 'Rp.  ',
                //         'groupSeparator' => '.',
                //         'radixPoint' => ',',
                //         'allowMinus' => false,
                //     ],
                //     'readonly' => true,
                //     'options' => [
                //         'placeholder' => '',
                //         'required'=>'required',
                //         'value' => '0',
                //         'id' => 'biaya_admin'
                //     ]
                //]
            ],
            'biaya_simpan' => [
                'label' => '',
                'options' => ['value' => '0', 'type' => 'hidden']
            ],
        ]
    ]);
?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    function getHarga(harga){
        document.getElementById('harga_taksir').value = harga;
    }
</script>

<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-lg',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");

?>