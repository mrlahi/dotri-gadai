<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PeriodePenggajian */

$this->title = 'Update Periode Penggajian: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Periode Penggajians', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="periode-penggajian-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
