<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model common\models\PeriodePenggajian */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="periode-penggajian-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'tahun' => [
                'options' => [
                    'type' => 'number',
                    'min' => '2023'
                ],
            ],
            'bulan' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => [
                        '1' => 'JAN',
                        '2' => 'FEB',
                        '3' => 'MAR',
                        '4' => 'APR',
                        '5' => 'MEI',
                        '6' => 'JUN',
                        '7' => 'JUL',
                        '8' => 'AGT',
                        '9' => 'SEP',
                        '10' => '0KT',
                        '11' => 'NOV',
                        '12' => 'DES'
                    ]
                ]
            ],
            'start_date' => [
                'options' => [
                    'type' => 'date'
                ]
            ],
            'end_date' => [
                'options' => [
                    'type' => 'date'
                ]
            ]
        ]
    ]);
    ?>

    <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
