<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PeriodePenggajianSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Periode Penggajian';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a('<span class="fa fa-plus"></span> Tambah Periode Penggajian', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'tahun',
                'bulan',
                [
                    'attribute' => 'start_date',
                    'format' => 'date'
                ],
                [
                    'attribute' => 'end_date',
                    'format' => 'date'
                ],
                'deskripsi:ntext',
                //'created_at',
                //'created_by',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{generate-rekap} {rekap-penggajian}',
                    'contentOptions' => ['style' => ['white-space' => 'nowrap']],
                    'buttons' => [
                        'generate-rekap' => function($url,$model){
                            return Html::a('<span class="fa fa-refresh"></span> Generate Rekap', [$url], ['class' => 'btn btn-success btn-sm m-3', 'data' => ['confirm' => 'Yakin ingin membuat rekapitulasi untuk periode ini..??']]);
                        },
                        'rekap-penggajian' => function($url,$model){
                            $link = '/periode-penggajian-honor/index?periode_id='.$model->id;
                            return Html::a('<span class="fa fa-list"></span> Rekapitulasi', [$link], ['class' => 'btn btn-primary btn-sm m-3']);
                        },
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
