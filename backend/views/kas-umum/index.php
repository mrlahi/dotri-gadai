<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\dialog\Dialog;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel common\models\KasUmumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kas Umums';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a('Generate Kas', ['generate-kas'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_kas_umum',
            [
                'attribute' => 'jumlah_uang_masuk',
                'value' => function($model) {
                    return "Rp. ".number_format($model->jumlah_uang_masuk, 0,",",".");
                },
                'filter' => false
            ],
            [
                'attribute' => 'jumlah_uang_keluar',
                'value' => function($model) {
                    return "Rp. ".number_format($model->jumlah_uang_keluar, 0,",",".");
                },
                'filter' => false
            ],
            [
                'label' => 'Kas',
                'value' => function($model) {
                    $kas = $model->jumlah_uang_masuk - $model->jumlah_uang_keluar;
                    return "Rp. ".number_format($kas, 0,",",".");
                }
            ],
            [
                'attribute' => 'id_cabang',
                'label' => 'Outlet',
                'value' => 'cabang.nama_cabang',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->listCabang(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Tanggal',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'autoClear' => true,
                        'format' => 'yyyy-mm-d'
                    ]
                ]
            ],
            //'created_by',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>
