<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\KasUmum */

$this->title = 'Create Kas Umum';
$this->params['breadcrumbs'][] = ['label' => 'Kas Umums', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kas-umum-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
