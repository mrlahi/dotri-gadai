<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\KasUmum */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kas-umum-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jumlah_uang_masuk')->textInput() ?>

    <?= $form->field($model, 'jumlah_uang_keluar')->textInput() ?>

    <?= $form->field($model, 'id_cabang')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
