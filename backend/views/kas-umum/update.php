<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\KasUmum */

$this->title = 'Update Kas Umum: ' . $model->id_kas_umum;
$this->params['breadcrumbs'][] = ['label' => 'Kas Umums', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_kas_umum, 'url' => ['view', 'id' => $model->id_kas_umum]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kas-umum-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
