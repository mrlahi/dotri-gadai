<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\models\Akun;

/* @var $this yii\web\View */
/* @var $searchModel common\models\JenisBiayaOperasionalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jenis Biaya Operasional';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a('<span class="fa fa-plus"></span> Tambah Jenis Biaya Operasional', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'nama_jenis',
                [
                    'attribute' => 'akun_id',
                    'value' => 'akun.nama_akun',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => Akun::listAkun(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                // 'created_at',
                // 'created_by',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update}',
                    'buttons' => [
                        'update' => function($url,$model){
                            return Html::a('<span class="fa fa-edit"></span> Update', [$url], ['class' => 'btn btn-warning btn-sm']);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
