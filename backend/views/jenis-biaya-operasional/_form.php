<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use common\models\Akun;

/* @var $this yii\web\View */
/* @var $model common\models\JenisBiayaOperasional */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jenis-biaya-operasional-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'nama_jenis' => [
                'label' => 'Nama Jenis Biaya'
            ],
            'akun_id' => [
                'label' => 'Akun Debet',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => Akun::listAkun(),
                    'options' => ['placeholder' =>'Jenis Operasional']
                ]
            ]
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
