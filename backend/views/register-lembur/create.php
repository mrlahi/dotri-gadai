<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RegisterLembur */

$this->title = 'Register Lembur';
$this->params['breadcrumbs'][] = ['label' => 'Register Lembur', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?php
            if(Yii::$app->user->can('admin')){
                echo $this->render('_form-admin', [
                    'model' => $model,
                ]);
            }
            else{
                echo $this->render('_form', [
                    'model' => $model,
                ]);
            }
        ?>
    </div>
</div>
