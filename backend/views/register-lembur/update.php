<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RegisterLembur */

$this->title = 'Update Register Lembur : ' . $model->pegawai->nama;
$this->params['breadcrumbs'][] = ['label' => 'Register Lemburs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?php
            if(Yii::$app->user->can('admin')){
                echo $this->render('_form-admin', [
                    'model' => $model,
                ]);
            }
            else{
                echo $this->render('_form', [
                    'model' => $model,
                ]);
            }
        ?>
    </div>
</div>
