<?php

use common\models\Profile;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $model common\models\RegisterLembur */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="register-lembur-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'tgl_lembur' => [
                'options' => [
                    'type' => 'date'
                ]
            ],
            'jam_lembur' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'suffix' => ' jam',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required'
                    ]
                ]
            ]
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
