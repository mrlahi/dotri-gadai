<?php

use common\models\Profile;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\RegisterLemburSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Register Lembur';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a('Register Lembur', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute' => 'pegawai_id',
                    'label' => 'Pegawai', 
                    'value' => 'pegawai.nama',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => Profile::listPegawai(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                [
                    'attribute' => 'tgl_lembur',
                    'format' => 'date',
                    'filterType' => GridView::FILTER_DATE,
                    'filterWidgetOptions' => [
                        'pluginOptions' => [
                            'autoClear' => true,
                            'format' => 'yyyy-mm-d'
                        ]
                    ]
                ],
                'jam_lembur',
                [
                    'attribute' => 'status_lembur',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ['DIAJUKAN' => 'DIAJUKAN', 'DISETUJUI' => 'DISETUJUI', 'DITOLAK' => 'DITOLAK'],
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                //'created_at',
                //'created_by',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{setujui} {tolak} {update}',
                    'visibleButtons' => [
                        'setujui' => function($model){
                            if(Yii::$app->user->can('admin') AND ($model->status_lembur == 'DIAJUKAN' OR $model->status_lembur == 'DITOLAK')){
                                return TRUE;
                            }
                        },
                        'tolak' => function($model){
                            if(Yii::$app->user->can('admin') AND ($model->status_lembur == 'DIAJUKAN' OR $model->status_lembur == 'DISETUJUI')){
                                return TRUE;
                            }
                        },
                        'update' => function($model){
                            if($model->status_lembur == 'DIAJUKAN' AND (Yii::$app->user->can('admin') OR $model->created_by == Yii::$app->user->id)){
                                return TRUE;
                            }
                        }
                    ],
                    'contentOptions' => ['style' => ['white-space' => 'nowrap']],
                    'buttons' => [
                        'setujui' => function($url,$model){
                            return Html::a('<span class="fa fa-check"></span> Setujui', [$url], ['class' => 'btn btn-success btn-sm', 'data' => ['confirm' => 'Setujui Lembur Ini..??']]);
                        },
                        'tolak' => function($url,$model){
                            return Html::a('<span class="fa fa-close"></span> Tolak', [$url], ['class' => 'btn btn-danger btn-sm', 'data' => ['confirm' => 'Tolak Lembur Ini..??']]);
                        },
                        'update' => function($url,$model){
                            return Html::a('<span class="fa fa-edit"></span> Update', [$url], ['class' => 'btn btn-warning btn-sm']);
                        }
                    ]
                ],
            ],
        ]); ?>

    </div>
</div>
