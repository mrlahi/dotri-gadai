<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LogNotifikasi */

$this->title = 'Create Log Notifikasi';
$this->params['breadcrumbs'][] = ['label' => 'Log Notifikasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-notifikasi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
