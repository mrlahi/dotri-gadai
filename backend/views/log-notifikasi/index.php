<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\models\Cabang;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogNotifikasiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log Notifikasi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?php //Html::a('Create Log Notifikasi', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute' => 'no_sbg',
                    'value' => function($model){
                        return '<a href="/barang/find?sbg='.$model->barang->no_kontrak.'" target="_blank">'.$model->barang->no_kontrak.'</a>';
                    },
                    'format' => 'html'
                ],
                [
                    'label' => 'Status',
                    'value' => 'barang.status'
                ],
                [
                    'label' => 'Nama Nasabah',
                    'value' => 'barang.nasabah.nama'
                ],
                [
                    'attribute' => 'cabang_id',
                    'label' => 'Cabang',
                    'value' => 'barang.cabang.nama_cabang',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => Cabang::listCabang(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                [
                    'attribute' => 'status_wa',
                    'label' => 'Status WA',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ['ONPROCESS' => 'ONPROCESS', 'PENDING' => 'PENDING', 'SENT' => 'SENT', 'READ' => 'READ', 'CANCEL' => 'CANCEL', 'DELIVERED' => 'DELIVERED', 'REJECT' => 'REJECT'],
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                //'status_sms',
                //'created_at',
                [
                    'attribute' => 'jenis_notifikasi',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ['JATUH TEMPO' => 'JATUH TEMPO', 'PASIF' => 'PASIF', 'LELANG' => 'LELANG'],
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                // 'device_id',
                'message_id',
                'message:ntext',
                'no_hp_nasabah',
                [
                    'attribute' => 'created_at',
                    'label' => 'Tgl. Kirim',
                    'format' => 'date',
                    'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'autoClear' => true,
                        'format' => 'yyyy-mm-d'
                    ]
                ]
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{kirim-ulang}',
                    'buttons' => [
                        'kirim-ulang' => function($url,$model){
                            return Html::a('<span class="fa fa-recycle"></span> Kirim Ulang', [$url], ['class' => 'btn btn-info', 'data' => ['confirm' => 'Kirim Ulang Notifikasi..??']]);
                        }
                    ]
                ],
                
            ],
        ]); ?>
    </div>
</div>
