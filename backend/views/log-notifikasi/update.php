<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LogNotifikasi */

$this->title = 'Update Log Notifikasi: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Log Notifikasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="log-notifikasi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
