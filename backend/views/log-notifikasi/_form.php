<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LogNotifikasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-notifikasi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'barang_id')->textInput() ?>

    <?= $form->field($model, 'status_wa')->dropDownList([ 'TERKIRIM' => 'TERKIRIM', 'TERTUNDA' => 'TERTUNDA', 'TIDAK TERKIRIM' => 'TIDAK TERKIRIM', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'status_sms')->dropDownList([ 'TERKIRIM' => 'TERKIRIM', 'TERTUNDA' => 'TERTUNDA', 'TIDAK TERKIRIM' => 'TIDAK TERKIRIM', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'jenis_notifikasi')->dropDownList([ 'JATUH TEMPO' => 'JATUH TEMPO', 'LELANG' => 'LELANG', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'device_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'message_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
