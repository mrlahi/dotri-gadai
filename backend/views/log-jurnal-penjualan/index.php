<?php

use common\models\Akun;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\editable\Editable;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogJurnalPenjualanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log Jurnal';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">

        <p>
            <?php
                $targetUrl = '/log-jurnal-penjualan/create?barang_id='.$dataJual->id_barang;
                echo Html::button('<span class="fa fa-plus"></span> Tambah Jurnal',['value' => Url::to([$targetUrl]),'class'=>'btn btn-info btn-pop btn-sm']);
            ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                // 'id',
                // 'barang_id',
                [
                    'attribute' => 'akun_id',
                    'label' => 'Akun',
                    'value' => 'akun.nama_akun',
                    'class' => 'kartik\grid\EditableColumn',
                    'editableOptions' =>[
                        'asPopover' => true,
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => Akun::listAkun(),
                        'size'=>'md',
                        'displayValueConfig' => Akun::listAkun(),
                        //'options' => ['class'=>'form-control']
                    ],
                ],
                [
                    'attribute' => 'jumlah_uang',
                    'class' => 'kartik\grid\EditableColumn',
                    'editableOptions' =>[
                        'asPopover' => true,
                        'inputType' => Editable::INPUT_TEXT,
                        'size'=>'md',
                        //'options' => ['class'=>'form-control']
                    ],
                    'format' => 'currency'
                ],
                [
                    'attribute' => 'posisi_keuangan',
                    'class' => 'kartik\grid\EditableColumn',
                    'editableOptions' =>[
                        'asPopover' => true,
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => ['DEBET' => 'DEBET', 'KREDIT' => 'KREDIT'],
                        'size'=>'md',
                        //'options' => ['class'=>'form-control']
                    ],
                ],
                [
                    'attribute' => 'deskripsi',
                    'class' => 'kartik\grid\EditableColumn',
                    'editableOptions' =>[
                        'asPopover' => true,
                        'inputType' => Editable::INPUT_TEXTAREA,
                        'size'=>'md',
                        //'options' => ['class'=>'form-control']
                    ],
                ],
                //'created_at',
                //'created_by',

                //['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

        <?= Html::a('<span class="fa fa-check"></span> Jurnal', ['/log-jurnal-penjualan/jurnal', 'barang_id' => $dataJual->id_barang], ['class' => 'btn btn-success', 'data' => ['confirm' => 'Yakin inging menjurnal transaksi ini..??'], 'data-method' => 'post']) ?>

    </div>
</div>

<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-lg',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");