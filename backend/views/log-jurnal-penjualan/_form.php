<?php

use common\models\Akun;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $model common\models\LogJurnalPenjualan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-jurnal-penjualan-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 3,
        'attributes' =>[
            'akun_id' => [
                'label' => 'Nama/Kode Akun',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => Akun::listAkun(),
                    'options' => [
                        'placeholder' => 'Pilih Akun'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'dropdownParent' => '#modalPopUp'
                        ]
                ],
                
            ],
            'jumlah_uang' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required'
                    ]
                ]
            ],
            'posisi_keuangan' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => ['DEBET' => 'DEBET', 'KREDIT' => 'KREDIT'],
                    'options' => ['placeholder' => 'Pilih Posisi']
                ]
            ]
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
