<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TaksiranHarga */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="taksiran-harga-form">

    <?php $form = ActiveForm::begin(); ?>

    <blockquote>
        <strong>
            Setelah dibuat, harga taksiran sebelumnya akan di-nonaktifkan dan diganti dengan data saat ini. Lanjutkan..??
        </strong>
    </blockquote>

    <?= $form->field($model, 'tgl_aktif')->textInput(['type' => 'date', 'readonly' => 'readonly']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
