<?php
    $this->title = "Device Status";
?>
<?php
    foreach($dataDevice as $device){
        $pureToken = explode('.',$device->token);

?>
<div class="box box-info">
    <div class="box-header">
        <h3><?= $device->nama_device; ?></h3>
    </div>
    <div class="box-body">
        <?php
            $status = $device->status;
        ?>
        <h3>STATUS DEVICE : <?= strtoupper($status->status); ?></h3>
        <hr>
        <?php
            if($status->status == "disconnected"){
        ?>
        <object type="text/html" data="https://deu.wablas.com/api/device/scan?token=<?= $pureToken[0]; ?>" width="100%" height="600px" style="overflow:auto;border:5px ridge blue">
        </object>
        <?php } ?>
    </div>
</div>

<?php } ?>