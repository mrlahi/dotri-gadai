<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\NoteBarangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Note Barang ' . $dataBarang->no_kontrak;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?=
            $this->render('create', [
                'model' => $model,
            ]);
        ?>
    </div>
</div>

<div class="box box-primary">
    <div class="box-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'barang_id',
            'note:ntext',
            [
                'label' => 'Tgl. Input',
                'attribute' => 'created_at',
                'format' => 'date'
            ],
            [
                'label' => 'Nama Pegawai',
                'attribute' => 'created_by',
                'value' => 'createdBy.nama'
            ],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>
