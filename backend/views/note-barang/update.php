<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\NoteBarang */

$this->title = 'Update Note Barang: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Note Barangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="note-barang-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
