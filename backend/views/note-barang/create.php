<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\NoteBarang */

$this->title = 'Tambah Note Barang';
// $this->params['breadcrumbs'][] = ['label' => 'Note Barangs', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="note-barang-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
