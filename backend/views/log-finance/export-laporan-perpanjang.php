<?php
header('Content-type: application/vnd-ms-excel');
header('Content-Disposition: attachment; filename=laporan-perpanjang.xls');
?>
<table class="table table-responsive table-bordered">
    <thead>
        <tr>
                <th rowspan="2">No</th>
                <th rowspan="2">Tgl.</th>
                <th rowspan="2">SBG</th>
                <th rowspan="2">Nasabah</th>
                <th rowspan="2">KTP</th>
                <th rowspan="2">Type</th>
                <th colspan="5">Perpanjang</th>
                <th rowspan="2">Outlet</th>
                <th rowspan="2">Jenis Pembayaran</th>
            </tr>
        <tr>
            <td>Bunga 1.2%</td>
            <td>Bunga 2.4%</td>
            <td>Bunga 5%</td>
            <td>Bunga 10%</td>
            <td>Denda</td>
        </tr>
    </thead>
    <tbody>
        <?php
            $no = 1;
            $tb1_2 = 0;
            $tb2_4 = 0;
            $tb5 = 0;
            $tb10 = 0;
            $tDenda = 0;
            foreach($dataPerpanjang as $perpanjang){
                $b1_2 = 0;
                $b2_4 = 0;
                $b5 = 0;
                $b10 = 0;
                $barang = $perpanjang->barang;
                $denda = $perpanjang->dendaPerpanjang();

                $tDenda += $denda;

                if($barang->bunga == 1.2){
                    $b1_2 = $perpanjang->nilaiBungaPerpanjang();
                    $tb1_2 += $b1_2;
                }

                if($barang->bunga == 2.4){
                    $b2_4 = $perpanjang->nilaiBungaPerpanjang();
                    $tb2_4 += $b2_4;
                }

                if($barang->bunga == 5){
                    $b5 = $perpanjang->nilaiBungaPerpanjang();
                    $tb5 += $b5;
                }

                if($barang->bunga == 10){
                    $b10 = $perpanjang->nilaiBungaPerpanjang();
                    $tb10 += $b10;
                }
        ?>
        <tr>
            <td><?= $no++; ?></td>
            <td><?= Yii::$app->formatter->asDate($perpanjang->created_at); ?></td>
            <td><?= $barang->no_kontrak; ?></td>
            <td><?= $barang->nasabah->nama; ?></td>
            <td><?= $barang->nasabah->no_ktp; ?></td>
            <td><?= $barang->merk." - ".$barang->tipe; ?></td>
            <td><?= Yii::$app->formatter->asInteger($b1_2); ?></td>
            <td><?= Yii::$app->formatter->asInteger($b2_4); ?></td>
            <td><?= Yii::$app->formatter->asInteger($b5); ?></td>
            <td><?= Yii::$app->formatter->asInteger($b10); ?></td>
            <td><?= Yii::$app->formatter->asInteger($denda); ?></td>
            <td><?= $barang->cabang->nama_cabang; ?></td>
            <td><?= $perpanjang->type_pembayaran; ?></td>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="5">
                <strong>
                    Grand Total
                </strong>    
            </td>
            <td><?= $tb1_2; ?></td>
            <td><?= $tb2_4; ?></td>
            <td><?= $tb5; ?></td>
            <td><?= $tb10; ?></td>
            <td><?= $tDenda; ?></td>
            <td></td>
        </tr>
    </tbody>
</table>