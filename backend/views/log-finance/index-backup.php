<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogFinanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Lengkap Hari Ini';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Harian</h3>
    </div>
    <div class="box-body">
        <table>
            <tr>
                <td><h4>Nama Cabang</h4></td>
                <td>:</td>
                <td><h4>&nbsp;&nbsp;<?= $model->myCabang($id_cabang)->nama_cabang; ?></h4></td>
            </tr>
            <tr>
                <td><h4>Tanggal</h4></td>
                <td>:</td>
                <td><h4>&nbsp;&nbsp;<?= date_format(date_create($date1),'D, d M Y')." - ". date_format(date_create($date2),'D, d M Y');?></h4></td>
            </tr>
        </table>
    </div>
</div>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Gadai :</h3>
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <td rowspan="2">No</td>
                    <td rowspan="2">No. Faktur</td>
                    <td rowspan="2">Type</td>
                    <td colspan="5" align="center">Gadai</td>
                    <td rowspan="2">Jenis Pembayaran</td>
                </tr>
                <tr>
                    <td>Pinjaman</td>
                    <td>Admin</td>
                    <td>Bunga (5%)</td>
                    <td>Bunga (10%)</td>
                    <td>Selisih</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no =1;
                    $gadai = $model->dailyGadai($date1,$date2,$id_cabang);
                    $t_pinjaman = 0;
                    $t_admin = 0;
                    $t_bunga_pinjam = 0;
                    $t_selisih = 0;
                    $t_bunga_pinjam5 = 0;
                    $t_bunga_pinjam10 = 0;
                    foreach ($gadai as $gadais){
                        $id_barang = $gadais->id_barang;
                        $pinjaman = $model->dailyEach($id_barang,'Pinjaman',$date1,$date2,$id_cabang);

                        $admin = $model->dailyEach($id_barang,'Admin',$date1,$date2,$id_cabang);
                        $bunga = $model->dailyEach($id_barang,'Bunga',$date1,$date2,$id_cabang);
                        $selisih = $pinjaman - ($bunga + $admin);

                        $t_pinjaman +=$pinjaman;
                        $t_admin += $admin;
                        $t_bunga_pinjam += $bunga;
                        $t_selisih += $selisih;

                        if($gadais->barang->jangka == 30){
                            $bunga5 = "";
                            $bunga10 = "Rp. ". number_format($bunga,'0',',','.');
                            $t_bunga_pinjam10 += $bunga;
                        }
                        else{
                            $bunga5 = "Rp. ". number_format($bunga,'0',',','.');
                            $bunga10 = "";
                            $t_bunga_pinjam5 += $bunga;
                        }
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $model->myBarang($id_barang)->no_kontrak; ?> </td>
                    <td><?= $model->myBarang($id_barang)->merk." - ".$model->myBarang($id_barang)->tipe; ?></td>
                    <td><?= "Rp. ". number_format($pinjaman,'0',',','.'); ?></td>
                    <td><?= "Rp. ". number_format($admin,'0',',','.'); ?></td>
                    <td><?= $bunga5; ?></td>
                    <td><?= $bunga10; ?></td>
                    <td><?= "Rp. ". number_format($selisih,'0',',','.'); ?></td>
                    <td><?= $gadais->type_pembayaran ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="3" align="center"><b>Total :</b></td>
                    <td><b><?= "Rp. ". number_format($t_pinjaman,'0',',','.'); ?></b></td>
                    <td><b><?= "Rp. ". number_format($t_admin,'0',',','.'); ?></b></td>
                    <td><b><?= "Rp. ". number_format($t_bunga_pinjam5,'0',',','.'); ?></b></td>
                    <td><b><?= "Rp. ". number_format($t_bunga_pinjam10,'0',',','.'); ?></b></td>
                    <td><b><?= "Rp. ". number_format($t_selisih,'0',',','.'); ?></b></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Perpanjang :</h3>
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover">
            <thead>
                <tr align="center">
                    <td rowspan="2">No</td>
                    <td rowspan="2">No. Faktur</td>
                    <td rowspan="2">Type</td>
                    <td colspan="3">Perpanjang</td>
                    <td rowspan="2">Jenis Pembayaran</td>
                </tr>
                <tr align="center">
                    <td>Bunga (5%)</td>
                    <td>Bunga (10%)</td>
                    <td>Denda</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no =1;
                    $t_bunga_panjang5 = 0;
                    $t_bunga_panjang10 = 0;
                    $t_bunga_panjang = 0;
                    $t_denda_panjang = 0;
                    $perpanjang = $model->dailyPerpanjang($date1,$date2,$id_cabang);
                    foreach ($perpanjang as $perpanjangs){
                        $id_barang = $perpanjangs->id_barang;
                        $bunga = $model->dailyEach($id_barang,'BungaPerpanjang',$date1,$date2,$id_cabang);
                        $denda = $model->dailyEach($id_barang,'DendaPerpanjang',$date1,$date2,$id_cabang);

                        $t_bunga_panjang += $bunga;
                        $t_denda_panjang += $denda;

                        if($perpanjangs->barang->jangka == 30){
                            $bunga5 = "";
                            $bunga10 = "Rp. ". number_format($bunga,'0',',','.');
                            $t_bunga_panjang10 += $bunga;
                        }
                        else{
                            $bunga5 = "Rp. ". number_format($bunga,'0',',','.');
                            $bunga10 = "";
                            $t_bunga_panjang5 += $bunga;
                        }
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $model->myBarang($id_barang)->no_kontrak; ?> </td>
                    <td><?= $model->myBarang($id_barang)->merk." - ".$model->myBarang($id_barang)->tipe; ?></td>
                    <td><?= $bunga5 ?></td>
                    <td><?= $bunga10; ?></td>
                    <td><?= "Rp. ". number_format($denda,'0',',','.'); ?></td>
                    <td><?= $perpanjangs->type_pembayaran; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="3" align="center"><b>Total :</b></td>
                    <td><b><?= "Rp. ". number_format($t_bunga_panjang5,'0',',','.'); ?></b></td>
                    <td><b><?= "Rp. ". number_format($t_bunga_panjang10,'0',',','.'); ?></b></td>
                    <td><b><?= "Rp. ". number_format($t_denda_panjang,'0',',','.'); ?></b></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Pelunasan :</h3>
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <td rowspan="2">No</td>
                    <td rowspan="2">No. Faktur</td>
                    <td rowspan="2">Type</td>
                    <td colspan="4" align="center">Menebus</td>
                    <td rowspan="2">Jenis Pembayaran</td>
                </tr>
                <tr align="center">
                    <td>Pembayaran</td>
                    <td>Denda</td>
                    <td>Tebus 5%</td>
                    <td>P. Pokok</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no =1;
                    $t_pembayaran = 0;
                    $t_denda_lunas = 0;
                    $t_tebus5persen = 0;
                    $t_pokok = 0;
                    $menebus = $model->dailyMenebus($date1,$date2,$id_cabang);
                    foreach ($menebus as $menebuss){
                        $id_barang = $menebuss->id_barang;
                        $pembayaran = $model->dailyEach($id_barang,'Pelunasan',$date1,$date2,$id_cabang);
                        $denda = $model->dailyEach($id_barang,'DendaPelunasan',$date1,$date2,$id_cabang);
                        $tebus5persen = $model->dailyEach($id_barang,'KembaliBunga5Persen',$date1,$date2,$id_cabang);
                        $pokok = $pembayaran + $tebus5persen;

                        $t_pembayaran += $pembayaran;
                        $t_denda_lunas += $denda;
                        $t_tebus5persen += $tebus5persen;
                        $t_pokok += $pokok;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $model->myBarang($id_barang)->no_kontrak; ?> </td>
                    <td><?= $model->myBarang($id_barang)->merk." - ".$model->myBarang($id_barang)->tipe; ?></td>
                    <td><?= "Rp. ". number_format($pembayaran,'0',',','.'); ?></td>
                    <td><?= "Rp. ". number_format($denda,'0',',','.'); ?></td>
                    <td><?= "Rp. ". number_format($tebus5persen,'0',',','.'); ?></td>
                    <td><?= "Rp. ". number_format($pokok,'0',',','.'); ?></td>
                    <td><?= $menebuss->type_pembayaran; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="3" align="center"><b>Total :</b></td>
                    <td><b><?= "Rp. ". number_format($t_pembayaran,'0',',','.'); ?></b></td>
                    <td><b><?= "Rp. ". number_format($t_denda_lunas,'0',',','.'); ?></b></td>
                    <td><b><?= "Rp. ". number_format($t_tebus5persen,'0',',','.'); ?></b></td>
                    <td><b><?= "Rp. ". number_format($t_pokok,'0',',','.'); ?></b></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Angsuran Hari Ini :</h3>
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <td>No</td>
                    <td>No. Faktur</td>
                    <td>Type</td>
                    <td>Angsuran</td>
                    <td>Jenis Pembayaran</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no =1;
                    $t_angsuran = 0;
                    $angsuran = $model->dailyAngsuran($date1,$date2,$id_cabang);
                    foreach ($angsuran as $angsurans){
                        $id_barang = $angsurans->id_barang;
                        $angsuran = $angsurans->jumlah_uang;

                        $t_angsuran += $angsuran;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $model->myBarang($id_barang)->no_kontrak; ?> </td>
                    <td><?= $model->myBarang($id_barang)->merk." - ".$model->myBarang($id_barang)->tipe; ?></td>
                    <td><?= "Rp. ". number_format($angsuran,'0',',','.'); ?></td>
                    <td><?= $angsurans->type_pembayaran; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="3" align="center"><b>Total :</b></td>
                    <td><b><?= "Rp. ". number_format($t_angsuran,'0',',','.'); ?></b></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Terjual Hari Ini :</h3>
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <td>No</td>
                    <td>Tgl. Terjual</td>
                    <td>No. Faktur</td>
                    <td>Type</td>
                    <td>Pinjaman</td>
                    <td>Harga Jual</td>
                    <td>Keterangan</td>
                    <td>Jenis Pembayaran</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no =1;
                    $t_lelang = 0;
                    $lelang = $model->dailyLelang($date1,$date2,$id_cabang);
                    foreach ($lelang as $lelangs){
                        $id_barang = $lelangs->id_barang;
                        $lelang = $model->dailyEach($id_barang,'Lelang',$date1,$date2,$id_cabang);

                        $t_lelang += $lelang;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= date_format(date_create($lelangs->created_at),'D, d M y'); ?></td>
                    <td><?= $model->myBarang($id_barang)->no_kontrak; ?> </td>
                    <td><?= $model->myBarang($id_barang)->merk." - ".$model->myBarang($id_barang)->tipe; ?></td>
                    <td><?= $model->myBarang($id_barang)->nilai_pinjam; ?> </td>
                    <td><?= "Rp. ". number_format($lelang,'0',',','.'); ?></td>
                    <td><?= $model->myStatusLelang($id_barang); ?></td>
                    <td><?= $lelangs->type_pembayaran; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="3" align="center"><b>Total :</b></td>
                    <td><b><?= "Rp. ". number_format($t_lelang,'0',',','.'); ?></b></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="box box-info">
    <div class="box-header">
        <h3>Rekapitulasi Hari Ini</h3>
    </div>
    <div class="box-body">
        <table width="50%">
            <tr>
                <td>Biaya Admin</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($t_admin,'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Bunga Pinjaman 5%</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($t_bunga_pinjam5,'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Bunga Pinjaman 10%</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($t_bunga_pinjam10,'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Bunga Perpanjang 5%</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($t_bunga_panjang5,'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Bunga Perpanjang 10%</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($t_bunga_panjang10,'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Denda (Perpanjang + Tebus)</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($t_denda_panjang+$t_denda_lunas,'0',',','.'); ?></b></td>
            </tr>
            <?php
                $pendapatan_operasional = $t_admin+$t_bunga_pinjam+$t_bunga_panjang+$t_denda_panjang+$t_denda_lunas;
            ?>
            <tr>
                <td>Pendapatan Operasional</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($pendapatan_operasional,'0',',','.'); ?></b></td>
            </tr>
        </table>
        
        <hr>

        <table width="50%">
            <tr>
                <td>Pelunasan</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($t_pokok,'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Angsuran</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($t_angsuran,'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Penambahan Modal</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($model->dailyModal($date1,$date2,$id_cabang),'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Pendapatan Jual</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($t_lelang,'0',',','.'); ?></b></td>
            </tr>
            <?php
                $saldo = $t_pokok + $t_angsuran + $model->dailyModal($date1,$date2,$id_cabang)+$t_lelang;
            ?>
            <tr>
                <td>Saldo</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($saldo,'0',',','.'); ?></b></td>
            </tr>
        </table>

        <hr>

        <table width="50%">
            <tr>
                <td>Pinjaman</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($t_pinjaman,'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Beban Operasional</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($model->dailyOperasional($date1,$date2,$id_cabang),'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Cashback</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($model->dailyKembaliBunga($date1,$date2,$id_cabang),'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Uang Keluar</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($model->dailyPengeluaran($date1,$date2,$id_cabang),'0',',','.'); ?></b></td>
            </tr>
            <?php
                $pengeluaran = $t_pinjaman+$model->dailyOperasional($date1,$date2,$id_cabang)+$model->dailyKembaliBunga($date1,$date2,$id_cabang)+$model->dailyPengeluaran($date1,$date2,$id_cabang);
            ?>
            <tr>
                <td>Pengeluaran:</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($pengeluaran,'0',',','.'); ?></b></td>
            </tr>
        </table>

        <hr>
        <h3>Via Transfer</h3>
        <table width="50%">
        <tr>
                <td>Angsuran</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($model->dailyViaTransfer($date1,$date2,$id_cabang,'Angsuran'),'0',',','.'); ?></b></td>
            </tr>        
            <tr>
                <td>Bunga Perpanjang</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($model->dailyViaTransfer($date1,$date2,$id_cabang,'BungaPerpanjang'),'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Denda Perpanjang</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($model->dailyViaTransfer($date1,$date2,$id_cabang,'DendaPerpanjang'),'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Tebus Pinjaman</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($model->dailyViaTransfer($date1,$date2,$id_cabang,'Pelunasan'),'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Tebus Denda</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($model->dailyViaTransfer($date1,$date2,$id_cabang,'DendaPelunasan'),'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Pendapatan Jual</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($model->dailyViaTransfer($date1,$date2,$id_cabang,'Lelang'),'0',',','.'); ?></b></td>
            </tr>
            <?php
            $via_transfer = $model->dailyViaTransfer($date1,$date2,$id_cabang,'Angsuran') + $model->dailyViaTransfer($date1,$date2,$id_cabang,'BungaPerpanjang') + $model->dailyViaTransfer($date1,$date2,$id_cabang,'DendaPerpanjang') + $model->dailyViaTransfer($date1,$date2,$id_cabang,'Pelunasan') + $model->dailyViaTransfer($date1,$date2,$id_cabang,'DendaPelunasan') + $model->dailyViaTransfer($date1,$date2,$id_cabang,'Lelang')
            ?>
            <tr>
                <td>Via Transfer</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($via_transfer,'0',',','.'); ?></b></td>
            </tr>
        </table>

        <hr>

        <?php
            $sisa_saldo = $pendapatan_operasional + $saldo - $pengeluaran - $via_transfer;
            $laba_bersih = $pendapatan_operasional-$model->dailyOperasional($date1,$date2,$id_cabang)-$model->dailyKembaliBunga($date1,$date2,$id_cabang);
        ?>

        <h3>Kas Ditangan : <?= "Rp. ". number_format($sisa_saldo,'2',',','.'); ?></h3>
        <h3>Laba Bersih : <?= "Rp. ". number_format($laba_bersih,'2',',','.'); ?></h3>
    </div>
</div>

