<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\widgets\reportpinjaman\ReportPinjamanWidgets;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogFinanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Lengkap Hari Ini';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Harian</h3>
    </div>
    <div class="box-body">
        <table>
            <tr>
                <td><h4>Nama Cabang</h4></td>
                <td>:</td>
                <td><h4>&nbsp;&nbsp;<?= $model->myCabang($id_cabang)->nama_cabang; ?></h4></td>
            </tr>
            <tr>
                <td><h4>Tanggal</h4></td>
                <td>:</td>
                <td><h4>&nbsp;&nbsp;<?= date_format(date_create($date1),'D, d M Y')." - ". date_format(date_create($date2),'D, d M Y');?></h4></td>
            </tr>
        </table>

        <a href="/log-finance/export-report-excel?date1=<?= $date1 ?>&date2=<?= $date2 ?>&id_cabang=<?= $id_cabang ?>" target="_blank" class="btn btn-success">
            <span class="fa fa-table"></span> Export Excel
        </a>

    </div>
</div>

<?=  ReportPinjamanWidgets::widget(['date1' => $date1, 'date2' => $date2, 'id_cabang' => $id_cabang]); ?>