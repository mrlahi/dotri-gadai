<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\TambahanJual;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogFinanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Lengkap Penjualan Hari Ini';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Penjualan Harian</h3>
    </div>
    <div class="box-body">
        <table>
            <tr>
                <td><h4>Nama Cabang</h4></td>
                <td>:</td>
                <td><h4>&nbsp;&nbsp;<?= $model->myCabang($id_cabang)->nama_cabang; ?></h4></td>
            </tr>
            <tr>
                <td><h4>Tanggal</h4></td>
                <td>:</td>
                <td><h4>&nbsp;&nbsp;<?= date_format(date_create($date1),'D, d M Y')." - ". date_format(date_create($date2),'D, d M Y');?></h4></td>
            </tr>
        </table>
    </div>
</div>

<div class="box box-success">
    <div class="box-header">
        
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <td>No</td>
                    <td>Tgl. Terjual</td>
                    <td>No. Faktur</td>
                    <td>Type</td>
                    <td>Sisa Pinjam</td>
                    <td>Harga Jual</td>
                    <td>Keterangan</td>
                    <td>Jenis Pembayaran</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no =1;
                    $t_lelang = 0;
                    $t_pinjaman = 0;
                    $lelang = $model->dailyLelang($date1,$date2,$id_cabang);
                    foreach ($lelang as $lelangs){
                        $id_barang = $lelangs->id_barang;
                        $lelang = $model->dailyEach($id_barang,'Lelang',$date1,$date2,$id_cabang);
                        $t_lelang += $lelang;
                        $t_pinjaman += $lelangs->barang->sisaPinjam2;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= date_format(date_create($lelangs->created_at),'D, d M y'); ?></td>
                    <td><?= $model->myBarang($id_barang)->no_kontrak; ?> </td>
                    <td><?= $model->myBarang($id_barang)->merk." - ".$model->myBarang($id_barang)->tipe; ?></td>
                    <td><?= "Rp. ".number_format($lelangs->barang->sisaPinjam2, '0',',','.'); ?> </td>
                    <td><?= "Rp. ". number_format($lelang,'0',',','.'); ?></td>
                    <td><?= $model->myStatusLelang($id_barang); ?></td>
                    <td><?= $lelangs->type_pembayaran; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="3" align="center"><b>Total :</b></td>
                    <td><b><?= "Rp. ". number_format($t_lelang,'0',',','.'); ?></b></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Biaya Penjualan</hh3>
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>No. Faktur</th>
                    <th>Nama Biaya</th>
                    <th>Jumlah Biaya</th>
                </tr>
            </thead>
            <?php
                $lelang2 = $model->dailyLelang($date1,$date2,$id_cabang);
                $no = 1;
                $t_biaya = 0;

                $biaya_penjualan = 0;
                $biaya_service = 0;
                $denda = 0;
                $bunga = 0;
                
                foreach ($lelang2 as $lelangs2){
                    $mTambahan = new TambahanJual();
            ?>
            <tbody>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $lelangs2->barang->no_kontrak; ?></td>
                    <td><?= $mTambahan->listTambah($lelangs2->id_barang)['list']; ?></td>
                    <td>
                        <strong>
                            <?= "Rp. ".number_format($mTambahan->listTambah($lelangs2->id_barang)['total'],'0',',','.'); ?>
                        </strong>
                    </td>
                </tr>
            </tbody>
            <?php 
                $t_biaya += $mTambahan->listTambah($lelangs2->id_barang)['total'];

                $biaya_penjualan += $mTambahan->listTambah($lelangs2->id_barang)['biaya_penjualan'];
                $biaya_service += $mTambahan->listTambah($lelangs2->id_barang)['biaya_service'];
                $denda += $mTambahan->listTambah($lelangs2->id_barang)['denda'];
                $bunga += $mTambahan->listTambah($lelangs2->id_barang)['bunga'];
            } ?>
            <tr>
                <td colspan="3">Total Biaya Jual</td>
                <td>
                    <strong>
                        <?= "Rp.". number_format($t_biaya,'0',',','.');?>
                    </strong>
                </td>
            </tr>
        </table>
    </div>
</div>

<div class="box box-info">
    <div class="box-header">
        <h3>Rekapitulasi Penjualan</h3>
    </div>
    <div class="box-body">
        <table width="50%">
            <tr>
                <td>Total Semua</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($t_lelang,'0',',','.'); ?></b></td>
            </tr>
            <?php
                $jualTrasfer = $model->dailyViaTransfer($date1,$date2,$id_cabang,'Lelang');
                $jualCash = $t_lelang - $jualTrasfer;
            ?>
            <tr>
                <td>Via Transfer</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($jualTrasfer,'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Via Cash</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($jualCash,'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Biaya penjualan</td>
                <td>:</td>
                <td>
                    <strong>
                        <?= "Rp.". number_format($t_biaya,'0',',','.');?>
                    </strong>
                </td>
            </tr>
        </table>

        <h4>Total Penjualan : <?= "Rp. ". number_format($t_lelang,'2',',','.'); ?></h4>
        <h4>Total Pinjaman : <?= "Rp. ". number_format($t_pinjaman,'2',',','.'); ?></h4>

        <hr>

        <h3>
            <?php
                $keuntungan = $t_lelang - $t_biaya - $t_pinjaman;
            ?>
            Keuntungan Penjualan : Rp. <?= number_format($keuntungan,'2',',','.'); ?>
        </h3>
    </div>
</div>

<div class="box box-warning">
    <div class="box-header">
        <h3>Rekapitulasi Biaya Tambahan</h3>
    </div>
    <div class="box-body">
        <table width="50%">
            <tr>
                <td>Biaya Service</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($biaya_service,'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Biaya Penjualan</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($biaya_penjualan,'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Denda</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($denda,'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Bunga</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($bunga,'0',',','.'); ?></b></td>
            </tr>
            
        </table>

        <hr>

        <h3>
            Total Biaya Tambahan : Rp. <?= number_format($t_biaya,'2',',','.'); ?>
        </h3>
    </div>
</div>