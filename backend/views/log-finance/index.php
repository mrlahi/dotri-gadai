<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogFinanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Lengkap Hari Ini';
$this->params['breadcrumbs'][] = $this->title;

$cabang = $model->myCabang($id_cabang);

set_time_limit(10000);

?>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Harian</h3>
    </div>
    <div class="box-body">
        <table>
            <tr>
                <td><h4>Nama Cabang</h4></td>
                <td>:</td>
                <td><h4>&nbsp;&nbsp;<?= $cabang->nama_cabang; ?></h4></td>
            </tr>
            <tr>
                <td><h4>Tanggal</h4></td>
                <td>:</td>
                <td><h4>&nbsp;&nbsp;<?= Yii::$app->formatter->asDate($date1)." - ". Yii::$app->formatter->asDate($date2);?></h4></td>
            </tr>
        </table>
    </div>
</div>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Gadai :</h3>
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <td rowspan="2">No</td>
                    <td rowspan="2">No. Faktur</td>
                    <td rowspan="2">Nama Nasabah</td>
                    <td rowspan="2">KTP</td>
                    <td rowspan="2">Type</td>
                    <td rowspan="2">Nama Barang</td>
                    <td colspan="6" align="center">Gadai</td>
                    <td rowspan="2">Selisih</td>
                    <td rowspan="2">Jenis Pembayaran</td>
                </tr>
                <tr>
                    <td>Pinjaman</td>
                    <td>Admin</td>
                    <td>Bunga (1.2%)</td>
                    <td>Bunga (2.4%)</td>
                    <td>Bunga (5%)</td>
                    <td>Bunga (10%)</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no =1;
                    $gadai = $model->dailyGadai($date1,$date2,$id_cabang);
                    $t_pinjaman = 0;
                    $t_admin = 0;
                    $t_bunga_pinjam = 0;
                    $t_selisih = 0;
                    $t_bunga_pinjam1_2 = 0;
                    $t_bunga_pinjam2_4 = 0;
                    $t_bunga_pinjam5 = 0;
                    $t_bunga_pinjam10 = 0;
                    foreach ($gadai as $gadais){
                        $id_barang = $gadais->id_barang;
                        //$pinjaman = $model->dailyEach($id_barang,'Pinjaman',$date1,$date2,$id_cabang);
                        $pinjaman = $gadais->barang->nilai_pinjam;

                        $admin = $model->dailyEach($id_barang,'Admin',$date1,$date2,$id_cabang);
                        $bunga = $model->dailyEach($id_barang,'Bunga',$date1,$date2,$id_cabang);
                        $selisih = $pinjaman - ($bunga + $admin);

                        $t_pinjaman +=$pinjaman;
                        $t_admin += $admin;
                        $t_bunga_pinjam += $bunga;
                        $t_selisih += $selisih;

                        if($gadais->barang->bunga == 1.2){
                            $bunga1_2 = "Rp. ". Yii::$app->formatter->asInteger($bunga);
                            $t_bunga_pinjam1_2 += $bunga;
                            $bunga2_4 = "";
                            $bunga5 = "";
                            $bunga10 = "";
                        }
                        else if($gadais->barang->bunga == 2.4){
                            $bunga1_2 = "";
                            $bunga2_4 = "Rp. ". Yii::$app->formatter->asInteger($bunga);
                            $t_bunga_pinjam2_4 += $bunga;
                            $bunga5 = "";
                            $bunga10 = "";
                        }
                        else if($gadais->barang->bunga == 10){
                            $bunga1_2 = "";
                            $bunga2_4 = "";
                            $bunga5 = "";
                            $bunga10 = "Rp. ". Yii::$app->formatter->asInteger($bunga);
                            $t_bunga_pinjam10 += $bunga;
                        }
                        else{
                            $bunga1_2 = "";
                            $bunga2_4 = "";
                            $bunga5 = "Rp. ". Yii::$app->formatter->asInteger($bunga);
                            $bunga10 = "";
                            $t_bunga_pinjam5 += $bunga;
                        }
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $gadais->barang->no_kontrak; ?></td>
                    <td>
                        <?= $gadais->barang->nasabah->nama; ?>
                        
                    </td>
                    <td>
                    <?= $gadais->barang->nasabah->no_ktp; ?>
                    </td>
                    <td><?= $gadais->barang->merk." - ".$gadais->barang->tipe; ?></td>
                    <td><?= $gadais->barang->nama_barang; ?></td>
                    <td><?= "Rp. ". Yii::$app->formatter->asInteger($pinjaman); ?></td>
                    <td><?= "Rp. ". Yii::$app->formatter->asInteger($admin); ?></td>
                    <td><?= $bunga1_2; ?></td>
                    <td><?= $bunga2_4; ?></td>
                    <td><?= $bunga5; ?></td>
                    <td><?= $bunga10; ?></td>
                    <td><?= "Rp. ". Yii::$app->formatter->asInteger($selisih); ?></td>
                    <td><?= $gadais->type_pembayaran ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="6" align="center"><b>Total :</b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_pinjaman); ?></b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_admin); ?></b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_pinjam1_2); ?></b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_pinjam2_4); ?></b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_pinjam5); ?></b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_pinjam10); ?></b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_selisih); ?></b></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Perpanjang :</h3>
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover">
            <thead>
                <tr align="center">
                    <td rowspan="2">No</td>
                    <td rowspan="2">No. Faktur</td>
                    <td rowspan="2">Nama Nasabah</td>
                    <td rowspan="2">KTP</td>
                    <td rowspan="2">Type</td>
                    <td rowspan="2">Nama Barang</td>
                    <td colspan="5">Perpanjang</td>
                    <td rowspan="2">Jenis Pembayaran</td>
                </tr>
                <tr align="center">
                    <td>Bunga (1.2%)</td>
                    <td>Bunga (2.4%)</td>
                    <td>Bunga (5%)</td>
                    <td>Bunga (10%)</td>
                    <td>Denda</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no =1;
                    $t_bunga_panjang1_2 = 0;
                    $t_bunga_panjang2_4 = 0;
                    $t_bunga_panjang5 = 0;
                    $t_bunga_panjang10 = 0;
                    $t_bunga_panjang = 0;
                    $t_denda_panjang = 0;
                    $perpanjang = $model->dailyPerpanjang($date1,$date2,$id_cabang);
                    foreach ($perpanjang as $perpanjangs){
                        $id_barang = $perpanjangs->id_barang;
                        $bunga = $model->dailyEach($id_barang,'BungaPerpanjang',$date1,$date2,$id_cabang);
                        $denda = $model->dailyEach($id_barang,'DendaPerpanjang',$date1,$date2,$id_cabang);

                        $t_bunga_panjang += $bunga;
                        $t_denda_panjang += $denda;

                        if($perpanjangs->barang->bunga == 1.2){
                            $bunga1_2 = "Rp. ". Yii::$app->formatter->asInteger($bunga);
                            $t_bunga_panjang1_2 += $bunga;
                            $bunga2_4 = "";
                            $bunga5 = "";
                            $bunga10 = "";
                        }
                        else if($perpanjangs->barang->bunga == 2.4){
                            $bunga1_2 = "";
                            $bunga2_4 = "Rp. ". Yii::$app->formatter->asInteger($bunga);
                            $t_bunga_panjang2_4 += $bunga;
                            $bunga5 = "";
                            $bunga10 = "";
                        }
                        else if($perpanjangs->barang->bunga == 10){
                            $bunga1_2 = "";
                            $bunga2_4 = "";
                            $bunga5 = "";
                            $t_bunga_panjang10 += $bunga;
                            $bunga10 = "Rp. ". Yii::$app->formatter->asInteger($bunga);
                        }
                        else{
                            $bunga1_2 = "";
                            $bunga2_4 = "";
                            $bunga5 = "Rp. ". Yii::$app->formatter->asInteger($bunga);
                            $bunga10 = "";
                            $t_bunga_panjang5 += $bunga;
                        }
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $perpanjangs->barang->no_kontrak; ?> </td>
                    <td>
                        <?= $perpanjangs->barang->nasabah->nama; ?>
                    </td>
                    <td>
                        <?= $perpanjangs->barang->nasabah->no_ktp; ?>
                    </td>
                    <td><?= $perpanjangs->barang->merk." - ".$perpanjangs->barang->tipe; ?></td>
                    <td><?= $perpanjangs->barang->nama_barang; ?></td>
                    <td><?= $bunga1_2 ?></td>
                    <td><?= $bunga2_4; ?></td>
                    <td><?= $bunga5 ?></td>
                    <td><?= $bunga10; ?></td>
                    <td><?= "Rp. ". Yii::$app->formatter->asInteger($denda); ?></td>
                    <td><?= $perpanjangs->type_pembayaran; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="6" align="center"><b>Total :</b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_panjang1_2); ?></b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_panjang2_4); ?></b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_panjang5); ?></b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_panjang10); ?></b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_denda_panjang); ?></b></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Pelunasan :</h3>
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <td rowspan="2">No</td>
                    <td rowspan="2">No. Faktur</td>
                    <td rowspan="2">Nama Nasabah</td>
                    <td rowspan="2">KTP</td>
                    <td rowspan="2">Type</td>
                    <td rowspan="2">Nama Barang</td>
                    <td colspan="5" align="center">Menebus</td>
                    <td rowspan="2">P. Pokok</td>
                    <td rowspan="2">Jenis Pembayaran</td>
                </tr>
                <tr align="center">
                    <td>Pembayaran</td>
                    <td>Bunga Pelunasan</td>
                    <td>Denda</td>
                    <td>Cashback 1.2%</td>
                    <td>Cashback 5%</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no =1;
                    $t_pembayaran = 0;
                    $t_denda_lunas = 0;
                    $t_tebus5persen = 0;
                    $t_tebus1_2persen = 0;
                    $t_pokok = 0;
                    $t_bunga_pelunasan = 0;
                    $menebus = $model->dailyMenebus($date1,$date2,$id_cabang);
                    foreach ($menebus as $menebuss){
                        $id_barang = $menebuss->id_barang;
                        $pembayaran = $model->dailyEach($id_barang,'Pelunasan',$date1,$date2,$id_cabang);
                        $bungaPelunasan = $model->dailyEach($id_barang,'BungaPelunasanPasif',$date1,$date2,$id_cabang);
                        $denda = $model->dailyEach($id_barang,'DendaPelunasan',$date1,$date2,$id_cabang);
                        $tebus1_2persen = $model->dailyEach($id_barang,'KembaliBunga1.2Persen',$date1,$date2,$id_cabang);
                        $tebus5persen = $model->dailyEach($id_barang,'KembaliBunga5Persen',$date1,$date2,$id_cabang);
                        $pokok = $pembayaran + $tebus5persen + $tebus1_2persen;
                        

                        $t_pembayaran += $pembayaran;
                        $t_denda_lunas += $denda;
                        $t_tebus5persen += $tebus5persen;
                        $t_tebus1_2persen += $tebus1_2persen;
                        $t_pokok += $pokok;
                        $t_bunga_pelunasan += $bungaPelunasan;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $menebuss->barang->no_kontrak; ?> </td>
                    <td>
                        <?= $menebuss->barang->nasabah->nama; ?>
                    </td>
                    <td>
                        <?= $menebuss->barang->nasabah->no_ktp; ?>
                    </td>
                    <td><?= $menebuss->barang->merk." - ".$menebuss->barang->tipe; ?></td>
                    <td><?= $menebuss->barang->nama_barang; ?></td>
                    <td><?= "Rp. ". Yii::$app->formatter->asInteger($pembayaran); ?></td>
                    <td><?= "Rp. ". Yii::$app->formatter->asInteger($bungaPelunasan); ?></td>
                    <td><?= "Rp. ". Yii::$app->formatter->asInteger($denda); ?></td>
                    <td><?= "Rp. ". Yii::$app->formatter->asInteger($tebus1_2persen); ?></td>
                    <td><?= "Rp. ". Yii::$app->formatter->asInteger($tebus5persen); ?></td>
                    <td><?= "Rp. ". Yii::$app->formatter->asInteger($pokok); ?></td>
                    <td><?= $menebuss->type_pembayaran; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="6" align="center"><b>Total :</b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_pembayaran); ?></b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_pelunasan); ?></b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_denda_lunas); ?></b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_tebus1_2persen); ?></b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_tebus5persen); ?></b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_pokok); ?></b></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Angsuran Hari Ini :</h3>
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <td>No</td>
                    <td>No. Faktur</td>
                    <td rowspan="2">Nama Nasabah</td>
                    <td rowspan="2">KTP</td>
                    <td>Type</td>
                    <td>Nama Barang</td>
                    <td>Angsuran</td>
                    <td>Jenis Pembayaran</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no =1;
                    $t_angsuran = 0;
                    $angsuran = $model->dailyAngsuran($date1,$date2,$id_cabang);
                    foreach ($angsuran as $angsurans){
                        $id_barang = $angsurans->id_barang;
                        $angsuran = $angsurans->jumlah_uang;

                        $t_angsuran += $angsuran;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $angsurans->barang->no_kontrak; ?> </td>
                    <td>
                        <?= $angsurans->barang->nasabah->nama; ?>
                    </td>
                    <td>
                        <?= $angsurans->barang->nasabah->no_ktp; ?>
                    </td>
                    <td><?= $angsurans->barang->merk." - ".$angsurans->barang->tipe; ?></td>
                    <td><?= $angsurans->barang->nama_barang; ?></td>
                    <td><?= "Rp. ". Yii::$app->formatter->asInteger($angsuran); ?></td>
                    <td><?= $angsurans->type_pembayaran; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="6" align="center"><b>Total :</b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_angsuran); ?></b></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Terjual Hari Ini :</h3>
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <td>No</td>
                    <td>Tgl. Terjual</td>
                    <td>No. Faktur</td>
                    <td>Type</td>
                    <td>Nama Barang</td>
                    <td>Pinjaman</td>
                    <td>Harga Jual</td>
                    <td>Keterangan</td>
                    <td>Jenis Pembayaran</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no =1;
                    $t_lelang = 0;
                    $lelang = $model->dailyLelang($date1,$date2,$id_cabang);
                    foreach ($lelang as $lelangs){
                        $id_barang = $lelangs->id_barang;
                        $lelang = $model->dailyEach($id_barang,'Lelang',$date1,$date2,$id_cabang);

                        $t_lelang += $lelang;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= Yii::$app->formatter->asDate($lelangs->created_at); ?></td>
                    <td><?= $lelangs->barang->no_kontrak; ?> </td>
                    <td><?= $lelangs->barang->merk." - ".$lelangs->barang->tipe; ?></td>
                    <td><?= $lelangs->barang->nama_barang; ?></td>
                    <td><?= $lelangs->barang->nilai_pinjam; ?> </td>
                    <td><?= "Rp. ". Yii::$app->formatter->asInteger($lelang); ?></td>
                    <td><?= $model->myStatusLelang($id_barang); ?></td>
                    <td><?= $lelangs->type_pembayaran; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="4" align="center"><b>Total :</b></td>
                    <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_lelang); ?></b></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="box box-success">
    <div class="box-header">
        <h3>Beban Operasional</h3>
    </div>
    <div class="box-body">
        <?php
            $dataOperasional = $mOperasional->find()->where(['between', 'created_at', $date1, $date2])->andWhere(['id_cabang' => $id_cabang])->all();
        ?>
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Pengeluaran</th>
                    <th>Jumlah</th>
                    <th>Jenis</th>
                    <th>Pengeluaran</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                    $t_operasional = 0;
                    foreach($dataOperasional as $operasional){
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $operasional->nama_pengeluaran; ?></td>
                    <td><?= "Rp. ". Yii::$app->formatter->asInteger($operasional->jumlah_pengeluaran); ?></td>
                    <td><?= $operasional->jenisBiaya->nama_jenis; ?></td>
                    <td><?= $operasional->type_pencairan; ?></td>
                </tr>
                <?php
                        $t_operasional += $operasional->jumlah_pengeluaran;
                    }
                ?>
                <tr>
                    <td>Total</td>
                    <td>:</td>
                    <td>
                        <strong>
                            <?= "Rp. ". Yii::$app->formatter->asInteger($t_operasional); ?>
                        </strong>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="box box-info">
    <div class="box-header">
        <h3>Rekapitulasi Hari Ini</h3>
    </div>
    <div class="box-body">
        <strong>PENDAPATAN</strong>
        <table width="50%">
            <tr>
                <td>Pendapatan Administrasi</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_admin); ?></b></td>
            </tr>
            <tr>
                <td>Bunga Gadai 1.2%</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_pinjam1_2); ?></b></td>
            </tr>
            <tr>
                <td>Bunga Gadai 2.4%</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_pinjam2_4); ?></b></td>
            </tr>
            <tr>
                <td>Bunga Gadai 5%</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_pinjam5); ?></b></td>
            </tr>
            <tr>
                <td>Bunga Gadai 10%</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_pinjam10); ?></b></td>
            </tr>
            <tr>
                <td>Bunga Perpanjang 1.2%</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_panjang1_2); ?></b></td>
            </tr>
            <tr>
                <td>Bunga Perpanjang 2.4%</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_panjang2_4); ?></b></td>
            </tr>
            <tr>
                <td>Bunga Perpanjang 5%</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_panjang5); ?></b></td>
            </tr>
            <tr>
                <td>Bunga Perpanjang 10%</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_panjang10); ?></b></td>
            </tr>
            <tr>
                <td>Denda Perpanjang</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_denda_panjang); ?></b></td>
            </tr>
            <tr>
                <td>Denda Pelunasan</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_denda_lunas); ?></b></td>
            </tr>
            <tr>
                <td>Bunga Pelunasan</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_bunga_pelunasan); ?></b></td>
            </tr>
            <?php
                $pendapatan_operasional = $t_admin+$t_bunga_pinjam+$t_bunga_panjang+$t_denda_panjang+$t_denda_lunas+$t_bunga_pelunasan;
            ?>
            <tr>
                <td>&nbsp; &nbsp; &nbsp; Pendapatan Kotor</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($pendapatan_operasional); ?></b></td>
            </tr>
        </table>
        
        <hr>
        <strong>PENERIMAAN KAS</strong>
        <table width="50%">
            <tr>
                <td>Pelunasan</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_pokok); ?></b></td>
            </tr>
            <tr>
                <td>Angsuran</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_angsuran); ?></b></td>
            </tr>
            <tr>
                <td>Penambahan Kas</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($model->dailyModal($date1,$date2,$id_cabang)); ?></b></td>
            </tr>
            <tr>
                <td>Pendapatan Jual</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_lelang); ?></b></td>
            </tr>
            <?php
                $saldo = $t_pokok + $t_angsuran + $model->dailyModal($date1,$date2,$id_cabang)+$t_lelang;
            ?>
            <tr>
                <td>&nbsp; &nbsp; &nbsp; Total Penerimaan Kas</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($saldo); ?></b></td>
            </tr>
        </table>

        <hr>
        <strong>PENGELUARAN KAS</strong>
        <table width="50%">
            <tr>
                <td>Pinjaman</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($t_pinjaman); ?></b></td>
            </tr>
            <tr>
                <td>Beban Operasional</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($model->dailyOperasional($date1,$date2,$id_cabang)); ?></b></td>
            </tr>
            <tr>
                <td>Cashback</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($model->dailyKembaliBunga($date1,$date2,$id_cabang)); ?></b></td>
            </tr>
            <tr>
                <td>Mutasi Kas</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($model->dailyPengeluaran($date1,$date2,$id_cabang)); ?></b></td>
            </tr>
            <?php
            $via_transfer = $model->dailyViaTransfer($date1,$date2,$id_cabang,'Angsuran') + $model->dailyViaTransfer($date1,$date2,$id_cabang,'BungaPerpanjang') + $model->dailyViaTransfer($date1,$date2,$id_cabang,'DendaPerpanjang') + $model->dailyViaTransfer($date1,$date2,$id_cabang,'Pelunasan') + $model->dailyViaTransfer($date1,$date2,$id_cabang,'DendaPelunasan') + $model->dailyViaTransfer($date1,$date2,$id_cabang,'Lelang') +
            $model->dailyViaTransfer($date1,$date2,$id_cabang,'BungaPelunasanPasif');
            ?>
            <tr>
                <td>Transaksi Via Transfer</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($via_transfer); ?></b></td>
            </tr>
            <?php
                $pengeluaran = $t_pinjaman+$model->dailyOperasional($date1,$date2,$id_cabang)+$model->dailyKembaliBunga($date1,$date2,$id_cabang)+$model->dailyPengeluaran($date1,$date2,$id_cabang)+$via_transfer;
            ?>
            <tr>
                <td>&nbsp; &nbsp; &nbsp; Total Pengeluaran Kas :</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($pengeluaran); ?></b></td>
            </tr>
        </table>

        <hr>
        <h3>Rincian Via Transfer</h3>
        <table width="50%">
        <tr>
                <td>Angsuran</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($model->dailyViaTransfer($date1,$date2,$id_cabang,'Angsuran')); ?></b></td>
            </tr>        
            <tr>
                <td>Bunga Perpanjang</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($model->dailyViaTransfer($date1,$date2,$id_cabang,'BungaPerpanjang')); ?></b></td>
            </tr>
            <tr>
                <td>Denda Perpanjang</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($model->dailyViaTransfer($date1,$date2,$id_cabang,'DendaPerpanjang')); ?></b></td>
            </tr>
            <tr>
                <td>Tebus Pinjaman</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($model->dailyViaTransfer($date1,$date2,$id_cabang,'Pelunasan')); ?></b></td>
            </tr>
            <tr>
                <td>Tebus Denda</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($model->dailyViaTransfer($date1,$date2,$id_cabang,'DendaPelunasan')); ?></b></td>
            </tr>
            <tr>
                <td>Bunga Pelunasan</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($model->dailyViaTransfer($date1,$date2,$id_cabang,'BungaPelunasanPasif')); ?></b></td>
            </tr>
            <tr>
                <td>Pendapatan Jual</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($model->dailyViaTransfer($date1,$date2,$id_cabang,'Lelang')); ?></b></td>
            </tr>
            <?php
            $via_transfer = $model->dailyViaTransfer($date1,$date2,$id_cabang,'Angsuran') + $model->dailyViaTransfer($date1,$date2,$id_cabang,'BungaPerpanjang') + $model->dailyViaTransfer($date1,$date2,$id_cabang,'DendaPerpanjang') + $model->dailyViaTransfer($date1,$date2,$id_cabang,'Pelunasan') + $model->dailyViaTransfer($date1,$date2,$id_cabang,'DendaPelunasan') + $model->dailyViaTransfer($date1,$date2,$id_cabang,'Lelang') +
            $model->dailyViaTransfer($date1,$date2,$id_cabang,'BungaPelunasanPasif')
            ?>
            <tr>
                <td>Via Transfer</td>
                <td>:</td>
                <td><b><?= "Rp. ". Yii::$app->formatter->asInteger($via_transfer); ?></b></td>
            </tr>
        </table>

        <hr>

        <?php
            $sisa_saldo = $pendapatan_operasional + $saldo - $pengeluaran;
            $laba_bersih = $pendapatan_operasional-$model->dailyOperasional($date1,$date2,$id_cabang)-$model->dailyKembaliBunga($date1,$date2,$id_cabang);
        ?>

        <h3>Kas : <?= "Rp. ". Yii::$app->formatter->asInteger($sisa_saldo); ?></h3>
        <h3>Pendapatan Bersih : <?= "Rp. ". Yii::$app->formatter->asInteger($laba_bersih); ?></h3>
    </div>
</div>

