<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model backend\models\BarangExt */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Filter Laporan Penjualan';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'created_at' =>[
                'label' => 'Tentukan Range Tanggal',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => DateRangePicker::classname(),
                'filterWidgetOptions' => [       
                    'attribute' => 'only_date',
                    'presetDropdown' => true,
                    'convertFormat' => false,
                ],
                'options' => [
                    'options' => ['required' => true, 'autocomplete'=>'off']]
            ],
            'id_cabang' =>[
                'label' => 'Cabang',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => $model->listCabang()
                ]
            ],
        ]
    ]);
    
    ?>

    <div class="form-group">
        <?= Html::submitButton('<span class="glyphicon glyphicon-search"></span> Filter', ['class' => 'btn btn-info']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
</div>
