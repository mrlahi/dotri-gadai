<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogFinanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Terjual Hari Ini';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Terjual</h3>
    </div>
    <div class="box-body">
        <table>
            <tr>
                <td><h4>Nama Cabang</h4></td>
                <td>:</td>
                <td><h4>&nbsp;&nbsp;<?= $model->myCabang($id_cabang)->nama_cabang; ?></h4></td>
            </tr>
            <tr>
                <td><h4>Tanggal</h4></td>
                <td>:</td>
                <td><h4>&nbsp;&nbsp;<?= date_format(date_create($date1),'D, d M Y')." - ". date_format(date_create($date2),'D, d M Y');?></h4></td>
            </tr>
        </table>
    </div>
</div>



<div class="box box-success">
    <div class="box-header">
        
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <td>No</td>
                    <td>Tgl. Terjual</td>
                    <td>No. Faktur</td>
                    <td>Type</td>
                    <td>Pinjaman</td>
                    <td>Harga Jual</td>
                    <!-- <td>Keterangan</td> -->
                </tr>
            </thead>
            <tbody>
                <?php
                    $no =1;
                    $t_lelang = 0;
                    $t_pinjam = 0;
                    $lelang = $model->dailyLelang($date1,$date2,$id_cabang);
                    foreach ($lelang as $lelangs){
                        $id_barang = $lelangs->id_barang;
                        $lelang = $model->dailyEach($id_barang,'Lelang',$date1,$date2,$id_cabang);

                        $t_lelang += $lelang;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= date_format(date_create($lelangs->created_at),'D, d M y'); ?></td>
                    <td><?= $model->myBarang($id_barang)->no_kontrak; ?> </td>
                    <td><?= $model->myBarang($id_barang)->merk." - ".$model->myBarang($id_barang)->tipe; ?></td>
                    <td><?= "Rp. ".number_format($model->myBarang($id_barang)->getSisapinjam2(), 0,",","."); ?> </td>
                    <td><?= "Rp. ". number_format($lelang,'0',',','.'); ?></td>
                    <!-- <td><?php //$model->myStatusLelang($id_barang); ?></td> -->
                </tr>
                <?php 
                $t_pinjam += $model->myBarang($id_barang)->getSisapinjam2();
                } ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b><?= "Rp. ". number_format($t_pinjam,'2',',','.'); ?></b></td>
                    <td><b><?= "Rp. ". number_format($t_lelang,'2',',','.'); ?></b></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        
        <?php
            $selisih = $t_lelang - $t_pinjam;

            if($selisih < 0){
                echo "<h3> Rugi : Rp. ".number_format($selisih,'2',',','.')."</h3>";
            }
            else{
                echo "<h3> Laba : Rp. ".number_format($selisih,'2',',','.')."</h3>";
            }
        ?>
    </div>
</div>

