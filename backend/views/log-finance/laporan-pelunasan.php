<?php

use yii\helpers\Html;

$this->title = "Laporan Pelunasan";
?>
<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Pelunasan Tanggal : <?= $date1 ?> sd <?= $date2 ?></h3>
    </div>
    <div class="box-body table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Tgl.</th>
                    <th rowspan="2">Cabang</th>
                    <th rowspan="2">SBG</th>
                    <th rowspan="2">Nasabah</th>
                    <th rowspan="2">KTP</th>
                    <th rowspan="2">Type</th>
                    <th rowspan="2">Bunga(%)</th>
                    <th colspan="6">Menebus</th>
                    <th rowspan="2">Outlet</th>
                    <th rowspan="2">Jenis Pembayaran</th>

                </tr>
                <tr>
                    <td>Pembayaran</td>
                    <td>Bunga Pelunasan</td>
                    <td>Denda</td>
                    <td>Cashback 1.2%</td>
                    <td>Cashback 5%</td>
                    <td>P.Pokok</td>
               </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                    $tDenda = 0;
                    $tC1_2 = 0;
                    $tC5 = 0;
                    $tPembayaran = 0;
                    $tBungaPelunasan = 0;
                    foreach($dataPelunasan as $pelunasan){
                        $barang = $pelunasan->barang;
                        $denda = $pelunasan->dendaPelunasan();
                        $bungaPelunasan = $pelunasan->bungaPelunasan();
                        $tDenda += $denda;
                        $tPembayaran += $pelunasan->jumlah_uang;
                        $tBungaPelunasan += $bungaPelunasan;
                        $c1_2 = 0;
                        $c5 = 0;

                        if($barang->bunga == 2.4){
                            $c1_2 = $pelunasan->nilaiCashback1_2();
                            $tC1_2 += $c1_2;
                        }

                        if($barang->bunga == 2.4){
                            $c5 = $pelunasan->nilaiCashback5();
                            $tC5 += $c5;
                        }
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= Yii::$app->formatter->asDate($pelunasan->created_at); ?></td>
                    <td><?= $barang->cabang->nama_cabang; ?></td>
                    <td><?= $barang->no_kontrak; ?></td>
                    <td><?= $barang->nasabah->nama; ?></td>
                    <td><?= $barang->nasabah->no_ktp; ?></td>
                    <td><?= $barang->merk." - ".$barang->tipe; ?></td>
                    <td><?= $barang->bunga; ?></td>
                    <td><?= Yii::$app->formatter->asInteger($pelunasan->jumlah_uang); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($bungaPelunasan); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($denda); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($c1_2); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($c5); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($barang->nilai_pinjam); ?></td>
                    <td><?= $barang->cabang->nama_cabang; ?></td>
                    <td><?= $pelunasan->type_pembayaran; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="6">
                        <strong>
                            Grand Total
                        </strong>    
                    </td>
                    <td><?= Yii::$app->formatter->asInteger($tPembayaran); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($tBungaPelunasan); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($tDenda); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($tC1_2); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($tC5); ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <br>
        <?= Html::a('<span class="fa fa-table"></span> Export', ['export-laporan-pelunasan', 'cabang_id' => $cabang_id, 'date1' => $date1, 'date2' => $date2], ['class' => 'btn btn-primary', 'target' => '_blank']); ?>
    </div>
</div>