<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LogFinance */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-finance-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jumlah_uang')->textInput() ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'id_cabang')->textInput() ?>

    <?= $form->field($model, 'id_barang')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
