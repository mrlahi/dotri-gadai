<?php
header('Content-type: application/vnd-ms-excel');
header('Content-Disposition: attachment; filename=laporan-pelunasan.xls');
?>
<table class="table table-responsive table-bordered">
    <thead>
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">Tgl.</th>
            <th rowspan="2">Cabang</th>
            <th rowspan="2">SBG</th>
            <th rowspan="2">Nasabah</th>
            <th rowspan="2">KTP</th>
            <th rowspan="2">Type</th>
            <th rowspan="2">Bunga</th>
            <th colspan="6">Menebus</th>
            <th rowspan="2">Outlet</th>
            <th rowspan="2">Jenis Pembayaran</th>
        </tr>
        <tr>
            <td>Pembayaran</td>
            <td>Denda</td>
            <td>Cashback 1.2%</td>
            <td>Cashback 5%</td>
            <td>P.Pokok</td>
        </tr>
    </thead>
    <tbody>
        <?php
            $no = 1;
            $tDenda = 0;
            $tC1_2 = 0;
            $tC5 = 0;
            $tPembayaran = 0;
            foreach($dataPelunasan as $pelunasan){
                $barang = $pelunasan->barang;
                $denda = $pelunasan->dendaPelunasan();
                $tDenda += $denda;
                $tPembayaran += $pelunasan->jumlah_uang;
                $c1_2 = 0;
                $c5 = 0;

                if($barang->bunga == 2.4){
                    $c1_2 = $pelunasan->nilaiCashback1_2();
                    $tC1_2 += $c1_2;
                }

                if($barang->bunga == 2.4){
                    $c5 = $pelunasan->nilaiCashback5();
                    $tC5 += $c5;
                }
        ?>
        <tr>
            <td><?= $no++; ?></td>
            <td><?= Yii::$app->formatter->asDate($pelunasan->created_at); ?></td>
            <td><?= $barang->cabang->nama_cabang; ?></td>
            <td><?= $barang->no_kontrak; ?></td>
            <td><?= $barang->nasabah->nama; ?></td>
            <td><?= $barang->nasabah->no_ktp; ?></td>
            <td><?= $barang->merk." - ".$barang->tipe; ?></td>
            <td><?= $barang->bunga; ?></td>
            <td><?= $pelunasan->jumlah_uang; ?></td>
            <td><?= $denda; ?></td>
            <td><?= $c1_2; ?></td>
            <td><?= $c5; ?></td>
            <td><?= $barang->nilai_pinjam; ?></td>
            <td><?= $barang->cabang->nama_cabang; ?></td>
            <td><?= $pelunasan->type_pembayaran; ?></td>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="6">
                <strong>
                    Grand Total
                </strong>    
            </td>
            <td><?= $tPembayaran; ?></td>
            <td><?= $tDenda; ?></td>
            <td><?= $tC1_2; ?></td>
            <td><?= $tC5; ?></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>
