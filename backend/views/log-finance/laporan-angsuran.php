<?php

use yii\helpers\Html;

$this->title = "Laporan Angsuran";
?>
<div class="box box-success">
    <div class="box-header">
        <div class="box-header">
            <h3>Laporan Pelunasan Tanggal : <?= $date1 ?> sd <?= $date2 ?></h3>
        </div>
    </div>
    <div class="box-body table-responsive">
    <table class="table table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tgl.</th>
                    <th>Cabang</th>
                    <th>SBG</th>
                    <th>Nasabah</th>
                    <th>KTP</th>
                    <th>Type</th>
                    <th>Angsuran</th>
                    <th>Outlet</th>
                    <th>Jenis Pembayaran</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                    $tAngsuran = 0;
                    foreach($dataAngsuran as $angsuran){
                        $barang = $angsuran->barang;
                        $jumAngsuran = $angsuran->jumlah_uang;
                        $tAngsuran += $jumAngsuran;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= Yii::$app->formatter->asDate($angsuran->created_at); ?></td>
                    <td><?= $barang->cabang->nama_cabang; ?></td>
                    <td><?= $barang->no_kontrak; ?></td>
                    <td><?= $barang->nasabah->nama; ?></td>
                    <td><?= $barang->nasabah->no_ktp; ?></td>
                    <td><?= $barang->merk." - ".$barang->tipe; ?></td>
                    <td><?= Yii::$app->formatter->asInteger($jumAngsuran); ?></td>
                    <td><?= $barang->cabang->nama_cabang; ?></td>
                    <td><?= $angsuran->type_pembayaran; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="5">
                        <strong>
                            Grand Total
                        </strong>    
                    </td>
                    <td><?= Yii::$app->formatter->asInteger($tAngsuran); ?></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <br>
        <?= Html::a('<span class="fa fa-table"></span> Export', ['export-laporan-angsuran', 'cabang_id' => $cabang_id, 'date1' => $date1, 'date2' => $date2], ['class' => 'btn btn-primary', 'target' => '_blank']); ?>
    </div>
</div>