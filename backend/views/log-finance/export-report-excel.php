<?php
use backend\widgets\reportpinjaman\ReportPinjamanWidgets;
header('Content-type: application/vnd-ms-excel');
header('Content-Disposition: attachment; filename=report.xls');
?>

<table>
    <tr>
        <td><h4>Nama Cabang</h4></td>
        <td>:</td>
        <td><h4>&nbsp;&nbsp;<?= $model->myCabang($id_cabang)->nama_cabang; ?></h4></td>
    </tr>
    <tr>
        <td><h4>Tanggal</h4></td>
        <td>:</td>
        <td><h4>&nbsp;&nbsp;<?= date_format(date_create($date1),'D, d M Y')." - ". date_format(date_create($date2),'D, d M Y');?></h4></td>
    </tr>
</table>

<?=  ReportPinjamanWidgets::widget(['date1' => $date1, 'date2' => $date2, 'id_cabang' => $id_cabang]); ?>