<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LogFinance */

$this->title = 'Update Log Finance: ' . $model->id_log_finance;
$this->params['breadcrumbs'][] = ['label' => 'Log Finances', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_log_finance, 'url' => ['view', 'id' => $model->id_log_finance]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="log-finance-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
