<?php

use yii\helpers\Html;

$this->title = "Laporan Perpanjangan";
?>
<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Perpanjang Tanggal : <?= $date1 ?> sd <?= $date2 ?></h3>
    </div>
    <div class="box-body table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Tgl.</th>
                    <th rowspan="2">Cabang</th>
                    <th rowspan="2">SBG</th>
                    <th rowspan="2">Nasabah</th>
                    <th rowspan="2">KTP</th>
                    <th rowspan="2">Type</th>
                    <th colspan="5">Perpanjang</th>
                    <th rowspan="2">Outlet</th>
                    <th rowspan="2">Jenis Pembayaran</th>
                </tr>
                <tr>
                    <td>Bunga 1.2%</td>
                    <td>Bunga 2.4%</td>
                    <td>Bunga 5%</td>
                    <td>Bunga 10%</td>
                    <td>Denda</td>
               </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                    $tb1_2 = 0;
                    $tb2_4 = 0;
                    $tb5 = 0;
                    $tb10 = 0;
                    $tDenda = 0;
                    foreach($dataPerpanjang as $perpanjang){
                        $b1_2 = 0;
                        $b2_4 = 0;
                        $b5 = 0;
                        $b10 = 0;
                        $barang = $perpanjang->barang;
                        $denda = $perpanjang->dendaPerpanjang();

                        $tDenda += $denda;

                        if($barang->bunga == 1.2){
                            $b1_2 = $perpanjang->bungaPerpanjang();
                            $tb1_2 += $b1_2;
                        }

                        if($barang->bunga == 2.4){
                            $b2_4 = $perpanjang->bungaPerpanjang();
                            $tb2_4 += $b2_4;
                        }

                        if($barang->bunga == 5){
                            $b5 = $perpanjang->bungaPerpanjang();
                            $tb5 += $b5;
                        }

                        if($barang->bunga == 10){
                            $b10 = $perpanjang->bungaPerpanjang();
                            $tb10 += $b10;
                        }
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= Yii::$app->formatter->asDate($perpanjang->created_at); ?></td>
                    <td><?= $barang->cabang->nama_cabang; ?></td>
                    <td><?= $barang->no_kontrak; ?></td>
                    <td><?= $barang->nasabah->nama; ?></td>
                    <td><?= $barang->nasabah->no_ktp; ?></td>
                    <td><?= $barang->merk." - ".$barang->tipe; ?></td>
                    <td><?= Yii::$app->formatter->asInteger($b1_2); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($b2_4); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($b5); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($b10); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($denda); ?></td>
                    <td><?= $barang->cabang->nama_cabang; ?></td>
                    <td><?= $perpanjang->type_pembayaran; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="5">
                        <strong>
                            Grand Total
                        </strong>    
                    </td>
                    <td><?= Yii::$app->formatter->asInteger($tb1_2); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($tb2_4); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($tb5); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($tb10); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($tDenda); ?></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <br>
        <?= Html::a('<span class="fa fa-table"></span> Export', ['export-laporan-perpanjang', 'cabang_id' => $cabang_id, 'date1' => $date1, 'date2' => $date2], ['class' => 'btn btn-primary', 'target' => '_blank']); ?>
    </div>
</div>