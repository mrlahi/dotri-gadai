<?php
header('Content-type: application/vnd-ms-excel');
header('Content-Disposition: attachment; filename=laporan-angsuran.xls');
?>
<table class="table table-responsive table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th>Tgl.</th>
            <th>SBG</th>
            <th>Nasabah</th>
            <th>KTP</th>
            <th>Type</th>
            <th>Angsuran</th>
            <th>Outlet</th>
            <th>Jenis Pembayaran</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $no = 1;
            $tAngsuran = 0;
            foreach($dataAngsuran as $angsuran){
                $barang = $angsuran->barang;
                $jumAngsuran = $angsuran->jumlah_uang;
                $tAngsuran += $jumAngsuran;
        ?>
        <tr>
            <td><?= $no++; ?></td>
            <td><?= Yii::$app->formatter->asDate($angsuran->created_at); ?></td>
            <td><?= $barang->no_kontrak; ?></td>
            <td><?= $barang->nasabah->nama; ?></td>
            <td><?= $barang->nasabah->no_ktp; ?></td>
            <td><?= $barang->merk." - ".$barang->tipe; ?></td>
            <td><?= Yii::$app->formatter->asInteger($jumAngsuran); ?></td>
            <td><?= $barang->cabang->nama_cabang; ?></td>
            <td><?= $angsuran->type_pembayaran; ?></td>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="5">
                <strong>
                    Grand Total
                </strong>    
            </td>
            <td><?= $tAngsuran; ?></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>