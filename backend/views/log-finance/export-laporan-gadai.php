<?php
header('Content-type: application/vnd-ms-excel');
header('Content-Disposition: attachment; filename=laporan-gadai.xls');
?>

<table>
    <thead>
        <tr align="center">
            <th rowspan="2">No.</th>
            <th rowspan="2">Tgl.</th>
            <th rowspan="2">Cabang</th>
            <th rowspan="2">SBG</th>
            <th rowspan="2">Nasabah</th>
            <th rowspan="2">KTP</th>
            <th rowspan="2">Type</th>
            <th colspan="6">Gadai</th>
        </tr>
        <tr>
            <td>Pinjaman</td>
            <td>Bunga 1.2%</td>
            <td>Bunga 2.4%</td>
            <td>Bunga 5%</td>
            <td>Bunga 10%</td>
            <td>Selisih</td>
    </tr>
    </thead>
    <tbody>
        <?php

                                                use yii\helpers\Html;

            $no = 1;
            $tb1_2 = 0;
            $tb2_4 = 0;
            $tb5 = 0;
            $tb10 = 0;
            $tPinjam = 0;
            $tSelisih = 0;
            foreach($dataGadai as $gadai){
                $b1_2 = 0;
                $b2_4 = 0;
                $b5 = 0;
                $b10 = 0;

                $barang = $gadai->barang;
                $nilaiPinjam = $barang->nilai_pinjam;
                $selisih = $nilaiPinjam - $barang->nilaiBunga();
                if($barang->bunga == 1.2){
                    $b1_2 = $barang->nilaiBunga();
                    $tb1_2 += $b1_2;
                }

                if($barang->bunga == 2.4){
                    $b2_4 = $barang->nilaiBunga();
                    $tb2_4 += $b2_4;
                }

                if($barang->bunga == 5){
                    $b5 = $barang->nilaiBunga();
                    $tb5 += $b5;
                }

                if($barang->bunga == 10){
                    $b10 = $barang->nilaiBunga();
                    $tb10 += $b10;
                }

                $tPinjam += $nilaiPinjam;
                $tSelisih += $selisih;
                
        ?>
        <tr>
            <td><?= $no++; ?></td>
            <td><?= Yii::$app->formatter->asDate($gadai->created_at); ?></td>
            <td><?= $barang->cabang->nama_cabang; ?></td>
            <td><?= $barang->no_kontrak; ?></td>
            <td><?= $barang->nasabah->nama; ?></td>
            <td><?= $barang->nasabah->no_ktp; ?></td>
            <td><?= $barang->merk." - ".$barang->tipe; ?></td>
            <td><?= $nilaiPinjam; ?></td>
            <td><?= $b1_2; ?></td>
            <td><?= $b2_4; ?></td>
            <td><?= $b5; ?></td>
            <td><?= $b10; ?></td>
            <td>
                <?= $selisih; ?>
            </td>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="5">
                <strong>
                    Grand Total
                </strong>    
            </td>
            <td><?= $tPinjam; ?></td>
            <td><?= $tb1_2; ?></td>
            <td><?= $tb2_4; ?></td>
            <td><?= $tb5; ?></td>
            <td><?= $tb10; ?></td>
            <td><?= $tSelisih; ?></td>
        </tr>
    </tbody>
</table>