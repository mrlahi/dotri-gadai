<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogFinanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Lengkap Hari Ini';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Harian</h3>
    </div>
    <div class="box-body">
        <table>
            <tr>
                <td><h4>Nama Cabang</h4></td>
                <td>:</td>
                <td><h4>&nbsp;&nbsp;<?= $model->myCabang($id_cabang)->nama_cabang; ?></h4></td>
            </tr>
            <tr>
                <td><h4>Tanggal</h4></td>
                <td>:</td>
                <td><h4>&nbsp;&nbsp;<?= date_format(date_create($date1),'D, d M Y')." - ". date_format(date_create($date2),'D, d M Y');?></h4></td>
            </tr>
        </table>
    </div>
</div>

<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Terjual:</h3>
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <td>No</td>
                    <td>Tgl. Terjual</td>
                    <td>No. Faktur</td>
                    <td>Type</td>
                    <td>Pinjaman</td>
                    <td>Harga Jual</td>
                    <td>Keterangan</td>
                    <td>Jenis Pembayaran</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no =1;
                    $t_lelang = 0;
                    $lelang = $model->dailyLelang($date1,$date2,$id_cabang);
                    foreach ($lelang as $lelangs){
                        $id_barang = $lelangs->id_barang;
                        $lelang = $model->dailyEach($id_barang,'Lelang',$date1,$date2,$id_cabang);

                        $t_lelang += $lelang;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= date_format(date_create($lelangs->created_at),'D, d M y'); ?></td>
                    <td><?= $model->myBarang($id_barang)->no_kontrak; ?> </td>
                    <td><?= $model->myBarang($id_barang)->merk." - ".$model->myBarang($id_barang)->tipe; ?></td>
                    <td><?= $model->myBarang($id_barang)->nilai_pinjam; ?> </td>
                    <td><?= "Rp. ". number_format($lelang,'0',',','.'); ?></td>
                    <td><?= $model->myStatusLelang($id_barang); ?></td>
                    <td><?= $lelangs->type_pembayaran; ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="3" align="center"><b>Total :</b></td>
                    <td><b><?= "Rp. ". number_format($t_lelang,'0',',','.'); ?></b></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="box box-success">
    <div class="box-header">
        <h3>Penambahan :</h3>
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <td>No.</td>
                    <td>Nama Penambahan</td>
                    <td>Jumlah Penambahan</td>
                </tr>
            </thead>
            <tbody>
                <?php
                        $no = 1;
                        $t_modal = 0;
                        foreach($dataModal as $modal){ 
                    ?>

                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $modal->keterangan; ?></td>
                        <td><?= "Rp. ".number_format($modal->jumlah_masuk,'0',',','.'); ?></td>
                    </tr>

                    <?php
                        $t_modal += $modal->jumlah_masuk;
                    }
                ?>
                <tr>
                    <td colspan="2">Total : </td>
                    <td>
                        <strong>
                            <?= "Rp. ".number_format($t_modal,'0',',','.'); ?>
                        </strong>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="box box-success">
    <div class="box-header">
        <h3>Pengeluaran:</h3>
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <td>No.</td>
                    <td>Nama Pengeluaran</td>
                    <td>Jumlah Pengeluaran</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                    $t_pengeluaran = 0;
                    foreach($dataUangKeluar as $uangKeluar){
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $uangKeluar->nama_pengeluaran; ?></td>
                    <td><?= "Rp. ".number_format($uangKeluar->jumlah_pengeluaran,'0',',','.'); ?></td>
                </tr>
                <?php
                    $t_pengeluaran += $uangKeluar->jumlah_pengeluaran;
                }
                ?>
                <tr>
                    <td colspan="2">Total</td>
                    <td>
                        <strong>
                            <?= "Rp. ".number_format($t_pengeluaran,'0',',','.'); ?>
                        </strong>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="box box-info">
    <div class="box-header">
        <h3>Rekapitulasi</h3>
    </div>
    <div class="box-body">
        <hr>
        <table width="50%">
            <tr>
                <td>Penambahan</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($t_modal,'0',',','.'); ?></b></td>
            </tr>
            <tr>
                <td>Penjualan</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($t_lelang,'0',',','.'); ?></b></td>
            </tr>
            <?php
                $saldo = $t_modal+$t_lelang;
            ?>
            <tr>
                <td>Saldo</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($saldo,'0',',','.'); ?></b></td>
            </tr>
        </table>

        <hr>

        <table width="50%">
            <tr>
                <td>Pengeluaran:</td>
                <td>:</td>
                <td><b><?= "Rp. ". number_format($t_pengeluaran,'0',',','.'); ?></b></td>
            </tr>
        </table>

        <?php
            $sisa_saldo = $saldo - $t_pengeluaran;
        ?>

        <h3>Kas Ditangan : <?= "Rp. ". number_format($sisa_saldo,'2',',','.'); ?></h3>
    </div>
</div>

