<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cabang */

$this->title = 'Update Cabang : ' . $model->nama_cabang;
$this->params['breadcrumbs'][] = ['label' => 'Cabangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_cabang, 'url' => ['view', 'id' => $model->id_cabang]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
