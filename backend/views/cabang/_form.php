<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Cabang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cabang-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'nama_cabang' => ['label' => 'Nama Cabang', 'options' =>['placeholder'=>'Nama Cabang']],
            'telp' => ['label' => 'Telp. Cabang', 'options' =>['placeholder'=>'Telepon Cabang']]
        ]
    ]);
    ?>

    <?= $form->field($model, 'alamat')->textarea() ?>

    <?= Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' =>[
                'latitude' => [
                    'type' => Form::INPUT_TEXTAREA,
                ],
                'longitude' => [
                    'type' => Form::INPUT_TEXTAREA,
                ]
            ]
        ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
        alert("Geolocation is not supported by this browser.");
    }

    function showPosition(position) {
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;

        document.getElementById('cabang-latitude').value = latitude;
        document.getElementById('cabang-longitude').value = longitude;
        
    }

    function showError(error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                alert("User denied the request for Geolocation.");
                break;
            case error.POSITION_UNAVAILABLE:
                alert("Location information is unavailable.");
                break;
            case error.TIMEOUT:
                alert("The request to get user location timed out.");
                break;
            case error.UNKNOWN_ERROR:
                alert("An unknown error occurred.");
                break;
        }
    }

</script>