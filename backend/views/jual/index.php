<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\JualSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Juals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jual-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Jual', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_jual',
            'id_barang',
            'id_nasabah',
            'nilai_pinjam',
            'harga_pasar',
            //'harga_jual',
            //'status',
            //'created_at',
            //'created_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
