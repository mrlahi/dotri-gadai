<?php

use yii\helpers\Html;

$this->title = "Data Penjualan Barang";

?>
<div class="box box-success">
    <div class="box-header">
        <h3 align="center">
            Data Barang Terjual
        </h3>
        <hr>
    </div>
    <div class="box-body">
        <table class="table table-hover table-striped table-responsive">
            <thead>
                <tr>
                    <th rowspan="2">No.</th>
                    <th rowspan="2">SBG</th>
                    <th rowspan="2">Nama Nasabah</th>
                    <th rowspan="2">KTP</th>
                    <th rowspan="2">Type</th>
                    <th rowspan="2">Nama Barang</th>
                    <th rowspan="2">Sisa Pinjam</th>
                    <th colspan="5">Rincian Penjualan</th>
                    <th rowspan="2">Total Harga</th>
                </tr>
                <tr>
                    <th>Kerugian</th>
                    <th>Bunga</th>
                    <th>Denda</th>
                    <th>Laba</th>
                    <th>Kelebihan</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                    $tSisaPinjam = 0;
                    $tKerugian = 0;
                    $tBunga = 0;
                    $tDenda = 0;
                    $tLaba = 0;
                    $tKelebihan = 0;
                    $tTotalHarga = 0 ;
                    foreach($dataJual as $jual){
                        $barang = $jual->barang;
                        $sisaPinjam = $barang->sisapinjam2;
                        $tSisaPinjam += $sisaPinjam;

                        if(!$barang->getJurnalJuals()){
                            $kerugian = 0;
                            $bunga = 0;
                            $denda = 0;
                            $laba = 0;
                            $kelebihan = 0;
                        }
                        else{
                            $kerugian = $barang->getJurnalJuals()->andWhere(['akun_id' => 5015])->sum('jumlah_uang');
                            $bunga = $barang->getJurnalJuals()->andWhere(['akun_id' => 4017])->sum('jumlah_uang');
                            $denda = $barang->getJurnalJuals()->andWhere(['akun_id' => 4018])->sum('jumlah_uang');
                            $laba = $barang->getJurnalJuals()->andWhere(['akun_id' => 4019])->sum('jumlah_uang');
                            $kelebihan = $barang->getJurnalJuals()->andWhere(['akun_id' => 2109])->sum('jumlah_uang');

                            $totalHarga = $sisaPinjam - $kerugian + $bunga + $denda + $laba + $kelebihan;
                        }

                        $tKerugian += $kerugian;
                        $tBunga += $bunga;
                        $tDenda += $denda;
                        $tLaba += $laba;
                        $tKelebihan += $kelebihan;
                        $tTotalHarga += $totalHarga;


                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $barang->no_kontrak; ?></td>
                    <td><?= $barang->nasabah->nama; ?></td>
                    <td><?= $barang->nasabah->no_ktp; ?></td>
                    <td><?= $barang->merk." - ".$barang->tipe; ?></td>
                    <td><?= $barang->nama_barang; ?></td>
                    <td><?= Yii::$app->formatter->asInteger($sisaPinjam); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($kerugian); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($bunga); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($denda); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($laba); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($kelebihan); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($totalHarga); ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="6">TOTAL : </td>
                    <td><?= Yii::$app->formatter->asInteger($tSisaPinjam); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($tKerugian); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($tBunga); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($tDenda); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($tLaba); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($tKelebihan); ?></td>
                    <td><?= Yii::$app->formatter->asInteger($tTotalHarga); ?></td>
                </tr>
            </tbody>
        </table>

        <?= Html::a('<span class="fa fa-table"></span> Export ',['print-data-jual', 'date1' => $date1, 'date2' => $date2], ['class' => 'btn btn-info btn-block', 'target' => '_blank']); ?>

    </div>
</div>