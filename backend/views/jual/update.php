<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Jual */

$this->title = 'Update Jual: ' . $model->id_jual;
$this->params['breadcrumbs'][] = ['label' => 'Juals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_jual, 'url' => ['view', 'id' => $model->id_jual]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jual-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
