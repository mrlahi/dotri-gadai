<?php
header('Content-type: application/vnd-ms-excel');
header('Content-Disposition: attachment; filename=FormatJurnal.xls');
?>
<table width="90%" align="center" border="1">
            <thead>
                <tr>
                    <th>Tgl</th>
                    <th>No. Transaksi</th>
                    <th>Transaksi</th>
                    <th>Cabang</th>
                    <th>No. Akun</th>
                    <th>Nama Akun</th>
                    <th>No. SBG</th>
                    <th>Debet</th>
                    <th>Kredit</th>
                    <th>Keterangan</th>
                </tr>
                <?php
                    $no = 1;
                    foreach($dataTransaksi as $transaksi){
                        $dataJurnal = $transaksi->getLogJurnals()->all();
                        $jumJurnal = $transaksi->getLogJurnals()->count();
                        $line = 1;
                        foreach($dataJurnal as $jurnal){
                            if($line == 1){
                ?>
                    <tr>
                        <td rowspan="<?= $jumJurnal; ?>">
                            <?= $jurnal->created_at; ?>
                        </td>
                        <td rowspan="<?= $jumJurnal; ?>"><?= $no; ?></td>
                        <td rowspan="<?= $jumJurnal; ?>"><?= $transaksi->jenis_transaksi; ?></td>
                        <td>
                        <?php
                            if(isset($jurnal->cabang->nama_cabang)){
                                echo $jurnal->cabang->nama_cabang;
                            }
                            else{
                                echo "NON CABANG";
                            }
                            ?>
                        </td>
                        <td><?= $jurnal->akun_kode; ?></td>
                        <td><?= $jurnal->akunKode->nama_akun; ?></td>
                        <td><?= $jurnal->noKontrak(); ?></td>
                        <td><?= $jurnal->debet; ?></td>
                        <td><?= $jurnal->kredit; ?></td>
                        <td><?= $jurnal->keterangan; ?></td>
                    </tr>
                    <?php
                        $line++;
                            }
                        else{
                        ?>
                    <tr>
                        <td>
                        <?php
                            if(isset($jurnal->cabang->nama_cabang)){
                                echo $jurnal->cabang->nama_cabang;
                            }
                            else{
                                echo "NON CABANG";
                            }
                            ?>
                        </td>
                        <td><?= $jurnal->akun_kode; ?></td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;<?= $jurnal->akunKode->nama_akun; ?></td>
                        <td><?= $jurnal->noKontrak(); ?></td>
                        <td><?= $jurnal->debet; ?></td>
                        <td><?= $jurnal->kredit; ?></td>
                        <td><?= $jurnal->keterangan; ?></td>
                    </tr>
                        <?php }
                        }
                        $no++;
                    }
                ?>
            </thead>
        </table>