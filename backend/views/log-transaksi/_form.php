<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use common\models\Cabang;

/* @var $this yii\web\View */
/* @var $model common\models\LogTransaksi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-transaksi-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 4,
        'attributes' =>[
            'cabang_id' => [
                'label' => 'Nama Cabang',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => Cabang::listCabang(),
                    'options' => ['placeholder' =>'Jenis Operasional']
                ]
            ]
        ]
    ]);
    ?>

    <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
