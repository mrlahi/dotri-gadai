<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogTransaksiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transaksi Mutasi Kas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a('<span class="fa fa-plus"></span> Tambah Mutasi', ['/log-jurnal/mutasi-kas'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id_log_transaksi',
                'jenis_transaksi',
                'deskripsi:ntext',
                [
                    'attribute' => 'cabang_id',
                    'value' => 'cabang.nama_cabang'
                ],
                [
                    'attribute' => 'created_at',
                    'label' => 'Tgl. Input',
                    'format' => 'date'
                ],
                [
                    'label' => 'Jumlah Mutasi',
                    'value' => function($model){
                        return $model->getLogJurnals()->sum('kredit');
                    },
                    'format' => 'currency'
                ],
                //'created_by',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete-mutasi}',
                    'buttons' => [
                        'delete-mutasi' => function($url, $model){
                            return Html::a('<span class="fa fa-trash"></span> Hapus', [$url], ['class' => 'btn btn-sm btn-danger', 'data' => ['confirm' => 'Yakin Ingin Menghapus Data..??', 'method' => 'post']]);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
