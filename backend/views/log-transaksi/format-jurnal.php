<?php
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\daterange\DateRangePicker;

$this->title = "Jurnal Transaksi";
?>

<div class="box box-warning">
    <div class="box-header">
        <h3>Per Tanggal : <?= Yii::$app->formatter->asDate($start). " sd ".Yii::$app->formatter->asDate($end); ?></h3>
    </div>
    <div class="box-body">
    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'created_at' =>[
                'label' => 'Tentukan Range Tanggal',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => DateRangePicker::classname(),
                'filterWidgetOptions' => [       
                    // 'attribute' => 'only_date',
                    'presetDropdown' => true,
                    'convertFormat' => false,
                ],
                'options' => [
                    'options' => ['required' => true]]
            ],
        ]
        ]);?>

    <div class="form-group">
        <?= Html::submitButton('Filter', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
        </div>
</div>

<div class="box box-info">
    <div class="box-header">
        <h3>Jurnal Transaksi</h3>
    </div>
    <div class="box-body">
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Tgl</th>
                    <th>No. Transaksi</th>
                    <th>Transaksi</th>
                    <th>Cabang</th>
                    <th>No. Akun</th>
                    <th>Nama Akun</th>
                    <th>No. SBG</th>
                    <th>Debet</th>
                    <th>Kredit</th>
                </tr>
                <?php
                    $no = 1;
                    foreach($dataTransaksi as $transaksi){
                        $dataJurnal = $transaksi->getLogJurnals()->all();
                        $jumJurnal = $transaksi->getLogJurnals()->count();
                        $line = 1;
                        foreach($dataJurnal as $jurnal){
                            if($jurnal->debet < 0){
                                $debet = $jurnal->debet*-1;
                            }
                            else{
                                $debet = $jurnal->debet;
                            }
                            if($line == 1){
                ?>
                    <tr>
                        <td rowspan="<?= $jumJurnal; ?>">
                            <?= Yii::$app->formatter->asDate($jurnal->created_at); ?>
                        </td>
                        <td rowspan="<?= $jumJurnal; ?>"><?= $no; ?></td>
                        <td rowspan="<?= $jumJurnal; ?>">
                            <?= $transaksi->jenis_transaksi; ?><br>
                            <?php
                                if($transaksi->jenis_transaksi == "INVENTORY"){
                                    echo $transaksi->deskripsi;
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                            if(isset($jurnal->cabang->nama_cabang)){
                                echo $jurnal->cabang->nama_cabang;
                            }
                            else{
                                echo "NON CABANG";
                            }
                            ?>
                        </td>
                        <td><?= $jurnal->akun_kode; ?></td>
                        <td><?= $jurnal->akunKode->nama_akun; ?></td>
                        <td><?= $jurnal->noKontrak(); ?></td>
                        <td><?= Yii::$app->formatter->asCurrency($debet); ?></td>
                        <td><?= Yii::$app->formatter->asCurrency($jurnal->kredit); ?></td>
                    </tr>
                    <?php
                        $line++;
                            }
                        else{
                        ?>
                    <tr>
                        <td>
                        <?php
                            if(isset($jurnal->cabang->nama_cabang)){
                                echo $jurnal->cabang->nama_cabang;
                            }
                            else{
                                echo "NON CABANG";
                            }
                            ?>
                        </td>
                        </td>
                        <td><?= $jurnal->akun_kode; ?></td>
                        <td>
                            <?php
                                if($debet == 0){
                                    echo "&nbsp;&nbsp;&nbsp;&nbsp;";
                                }
                            ?>
                            <?= $jurnal->akunKode->nama_akun; ?>
                        </td>
                        <td><?= $jurnal->noKontrak(); ?></td>
                        <td><?= Yii::$app->formatter->asCurrency($debet); ?></td>
                        <td><?= Yii::$app->formatter->asCurrency($jurnal->kredit); ?></td>
                    </tr>
                        <?php }
                        }
                        $no++;
                    }
                ?>
            </thead>
        </table>

        <hr>
        <?= Html::a('<span class="fa fa-table"></span> Export', ['export-jurnal', 'start' => $start, 'end' => $end], ['class' => 'btn btn-info pull-right']) ?>

    </div>
</div>