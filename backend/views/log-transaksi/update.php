<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LogTransaksi */

$this->title = 'Update Log Transaksi: ' . $model->id_log_transaksi;
$this->params['breadcrumbs'][] = ['label' => 'Log Transaksis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_log_transaksi, 'url' => ['view', 'id' => $model->id_log_transaksi]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-info">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
