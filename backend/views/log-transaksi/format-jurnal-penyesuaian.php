<?php
    use yii\helpers\Html;
    use yii\bootstrap\Modal;
    $this->title = "Jurnal Transaksi";
?>

<div class="box box-info">
    <div class="box-header">
        <h3>Jurnal Transaksi</h3>
    </div>
    <div class="box-body">
        <p>
            <?php
                echo Html::a('<span class="fa fa-backward"></span> Kembali', ['index-penyesuaian'], ['class' => 'btn btn-warning'])
            ?>

            <?php
                $link = '/log-jurnal/create-penyesuaian?log_transaksi_id='.$dataTransaksi->id_log_transaksi.'&cabang_id='.$dataTransaksi->cabang_id;
                echo Html::button('<span class="fa fa-plus"></span> Tambah Jurnal',['value'=> $link,'class'=>'btn btn-info btn-pop']);
            ?>
        </p>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Tgl</th>
                    <th>No. Transaksi</th>
                    <th>Transaksi</th>
                    <th>Cabang</th>
                    <th>No. Akun</th>
                    <th>Nama Akun</th>
                    <th>No. SBG</th>
                    <th>Debet</th>
                    <th>Kredit</th>
                    <th>Tgl. Reg</th>
                    <th>Keterangan</th>
                    <th>Hapus</th>
                </tr>
                <?php
                    $no = 1;
                        $dataJurnal = $dataTransaksi->getLogJurnals()->all();
                        $jumJurnal = $dataTransaksi->getLogJurnals()->count();
                        $line = 1;
                        foreach($dataJurnal as $jurnal){
                            if($jurnal->debet < 0){
                                $debet = $jurnal->debet*-1;
                            }
                            else{
                                $debet = $jurnal->debet;
                            }
                            if($line == 1){
                ?>
                    <tr>
                        <td rowspan="<?= $jumJurnal; ?>">
                            <?= Yii::$app->formatter->asDate($jurnal->logTransaksi->created_at); ?>
                        </td>
                        <td rowspan="<?= $jumJurnal; ?>"><?= $no; ?></td>
                        <td rowspan="<?= $jumJurnal; ?>"><?= $dataTransaksi->jenis_transaksi; ?></td>
                        <td><?php
                            if(isset($jurnal->cabang->nama_cabang)){
                                echo $jurnal->cabang->nama_cabang;
                            }
                            else{
                                echo "NON CABANG";
                            }
                            ?>
                        </td>
                        <td><?= $jurnal->akun_kode; ?></td>
                        <td><?= $jurnal->akunKode->nama_akun; ?></td>
                        <td><?= $jurnal->noKontrak(); ?></td>
                        <td><?= Yii::$app->formatter->asCurrency($debet); ?></td>
                        <td><?= Yii::$app->formatter->asCurrency($jurnal->kredit); ?></td>
                        <td><?= Yii::$app->formatter->asDate($jurnal->created_at); ?></td>
                        <td><?= $jurnal->keterangan; ?></td>
                        <td>
                            <?= Html::a('<span class="fa fa-trash"></span>',['delete-jurnal-penyesuaian', 'id' => $jurnal->id,'transaksi_id' => $jurnal->log_transaksi_id],['class' => 'btn btn-danger btn-sm', 'data' => ['confirm' => 'Yakin ingin menghapus jurnal..??'], 'data-method' => 'post']); ?>
                        </td>
                    </tr>
                    <?php
                        $line++;
                            }
                        else{
                        ?>
                    <tr>
                        <td>
                        <?php
                            if(isset($jurnal->cabang->nama_cabang)){
                                echo $jurnal->cabang->nama_cabang;
                            }
                            else{
                                echo "NON CABANG";
                            }
                            ?>
                        </td>
                        <td><?= $jurnal->akun_kode; ?></td>
                        <td>
                            <?php
                                if($debet == 0){
                                    echo "&nbsp;&nbsp;&nbsp;&nbsp;";
                                }
                            ?>
                            <?= $jurnal->akunKode->nama_akun; ?>
                        </td>
                        <td><?= $jurnal->noKontrak(); ?></td>
                        <td><?= Yii::$app->formatter->asCurrency($debet); ?></td>
                        <td><?= Yii::$app->formatter->asCurrency($jurnal->kredit); ?></td>
                        <td><?= Yii::$app->formatter->asDate($jurnal->created_at); ?></td>
                        <td><?= $jurnal->keterangan; ?></td>
                        <td>
                            <?= Html::a('<span class="fa fa-trash"></span>',['delete-jurnal-penyesuaian', 'id' => $jurnal->id,'transaksi_id' => $jurnal->log_transaksi_id],['class' => 'btn btn-danger btn-sm', 'data' => ['confirm' => 'Yakin ingin menghapus jurnal..??'], 'data-method' => 'post']); ?>
                        </td>
                    </tr>
                        <?php }
                        }
                        $no++;
                ?>
            </thead>
        </table>

        <hr>
        <?= Html::a('<span class="fa fa-table"></span> Export', ['export-jurnal', 'date' => $date], ['class' => 'btn btn-info pull-right']) ?>

    </div>
</div>

<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-lg',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");