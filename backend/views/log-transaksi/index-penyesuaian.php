<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogTransaksiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transaksi Penyesuaian';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a('<span class="fa fa-plus"></span> Tambah Transaksi', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id_log_transaksi',
                'jenis_transaksi',
                'deskripsi:ntext',
                [
                    'attribute' => 'cabang_id',
                    'value' => 'cabang.nama_cabang'
                ],
                [
                    'attribute' => 'created_at',
                    'label' => 'Tgl. Input',
                    'format' => 'date'
                ],
                [
                    'label' => 'Tgl. Jurnal',
                    'value' => 'tglJurnal',
                    'format' => 'date'
                ],
                //'created_by',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {jurnal-penyesuaian} {delete-penyesuaian}',
                    'buttons' => [
                        'update' => function($url, $model){
                            return Html::a('<span class="fa fa-edit"></span> Ubah', [$url], ['class' => 'btn btn-sm btn-warning']);
                        },
                        'jurnal-penyesuaian' => function($url, $model){
                            return Html::a('<span class="fa fa-list"></span> Jurnal', [$url], ['class' => 'btn btn-sm btn-info']);
                        },
                        'delete-penyesuaian' => function($url, $model){
                            return Html::a('<span class="fa fa-trash"></span> Hapus', [$url], ['class' => 'btn btn-sm btn-danger', 'data' => ['confirm' => 'Takin Ingin Menghapus Data..??', 'method' => 'post']]);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
