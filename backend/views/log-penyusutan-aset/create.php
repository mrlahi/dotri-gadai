<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LogPenyusutanAset */

$this->title = 'Create Log Penyusutan Aset';
$this->params['breadcrumbs'][] = ['label' => 'Log Penyusutan Asets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-penyusutan-aset-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
