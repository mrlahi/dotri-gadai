<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogPenyusutanAsetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log Penyusutan : '.$dataInventaris->nama_barang."(".$dataInventaris->no_inventaris.")";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">

        <p>
            <?= Html::a('<span class="fa fa-backward"></span> Kembali', ['/inventory/'], ['class' => 'btn btn-warning btn-sm']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                //'inventaris_id',
                [
                    'attribute' => 'nilai_penyusutan',
                    'format' => 'currency'
                ],
                [
                    'attribute' => 'tgl_penyusutan',
                    'format' => 'date'
                ],
                [
                    'label' => 'Status Penyusutan',
                    'value' => function($model){
                        if($model->is_penyusutan){
                            return "Sudah";
                        }
                        else{
                            return "Belum";
                        }
                    }
                ],
                //'created_at',
                //'created_by',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{susutkan}',
                    'visibleButtons' => [
                        'susutkan' => function($model){
                            if(!$model->is_penyusutan and $model->tgl_penyusutan < date('Y-m-d')){
                                return TRUE;
                            }
                        }
                    ],
                    'buttons' => [
                        'susutkan' => function($url, $model){
                            return Html::a('<span class="fa fa-download"></span> Susutkan', [$url],['class' => 'btn btn-info btn-sm', 'data' => ['confirm' => 'Susutkan Barang ini..??']]);
                        }
                    ]
                ],
            ],
        ]); ?>

        <h3>
            Nilai Sisa : <?= Yii::$app->formatter->asCurrency($dataInventaris->nilaiSisa()); ?>
        </h3>

    </div>
</div>
