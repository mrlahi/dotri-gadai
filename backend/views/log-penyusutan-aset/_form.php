<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LogPenyusutanAset */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-penyusutan-aset-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'inventaris_id')->textInput() ?>

    <?= $form->field($model, 'nilai_penyusutan')->textInput() ?>

    <?= $form->field($model, 'tgl_penyusutan')->textInput() ?>

    <?= $form->field($model, 'is_penyusutan')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
