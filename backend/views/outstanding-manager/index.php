<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OutstandingManagerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Outstanding Manager';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
    <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a('Outstanding Baru', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_outstanding_manager',
            [
                'attribute' => 'tgl_start',
                'label' => 'Mulai',
                'format' => 'date'
            ],
            [
                'attribute' => 'tgl_end',
                'label' => 'Selesai',
                'format' => 'date'
            ],
            [
                'attribute' => 'status_generator',
                'label' => 'Status',
                'value' => 'status',
                'format' => 'raw'
            ],
            //'created_by',
            //'created_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{check}{update}{exl}',
                'buttons' => [
                    'check' => function ($url,$model){
                        if($model->status_generator == "EXPORTING" OR $model->status_generator == "SUCCESS"){
                            $link = "/log-status-temp/index?outstanding_id=".$model->id_outstanding_manager."&start=".$model->tgl_start."&end=".$model->tgl_end;
                            return Html::a('<span class="fa fa-check"></span> Checkout', [$link], ['class' => 'btn btn-success']);
                        }
                    },
                    'update' => function ($url,$model){
                        if($model->status_generator == "ON PROCESS"){
                            return Html::a('<span class="fa fa-edit"></span> Update', [$url], ['class' => 'btn btn-warning']);
                        }
                    },
                    'exl' => function ($url,$model){
                        if($model->status_generator == "SUCCESS"){
                            $link = "/outstanding/".$model->id_outstanding_manager."-".$model->tgl_start."-".$model->tgl_end.".xls";
                            return Html::a('<span class="fa fa-download"></span> Export', [$link], ['class' => 'btn btn-info']);
                        }
                    },
                ]
            ],
        ],
    ]); ?>
</div>
</div>
