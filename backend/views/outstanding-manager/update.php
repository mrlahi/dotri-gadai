<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OutstandingManager */

$this->title = 'Ubah Outstanding : ' . $model->id_outstanding_manager;
$this->params['breadcrumbs'][] = ['label' => 'Outstanding Managers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_outstanding_manager, 'url' => ['view', 'id' => $model->id_outstanding_manager]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
