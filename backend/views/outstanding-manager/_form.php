<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model common\models\OutstandingManager */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="outstanding-manager-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'tgl_start' =>[
                'label' => 'Tentukan Range Tanggal',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => DateRangePicker::classname(),
                'filterWidgetOptions' => [       
                    'presetDropdown' => true,
                    'convertFormat' => false,
                ],
                'options' => [
                    'options' => ['required' => true]]
            ],
        ]
    ]);?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
