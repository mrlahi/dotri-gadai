<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\KasBank */

$this->title = 'Update Kas Bank: ' . $model->nama_kas_bank;
$this->params['breadcrumbs'][] = ['label' => 'Kas Banks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_kas_bank, 'url' => ['view', 'id' => $model->id_kas_bank]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-info">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body"> 
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
