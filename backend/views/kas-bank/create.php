<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\KasBank */

$this->title = 'Kas Bank Baru';
$this->params['breadcrumbs'][] = ['label' => 'Kas Bank', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body"> 
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
