<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\KasBankSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kas & Bank';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a('Tambah Kas Bank', ['create'], ['class' => 'btn btn-success bnt-sm']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id_kas_bank',
                'akun_kode',
                'nama_kas_bank',
                // 'created_at',
                // 'created_by',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update}',
                    'buttons' => [
                        'update' => function($url,$model){
                            return Html::a('<span class="fa fa-edit"></span> Update', [$url], ['class' => 'btn btn-sm btn-warning']);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
