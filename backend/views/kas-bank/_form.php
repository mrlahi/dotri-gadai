<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use common\models\Akun;

/* @var $this yii\web\View */
/* @var $model common\models\KasBank */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kas-bank-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' =>[
                'akun_kode' => [
                    'label' => 'Akun Jurnal',
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::classname(),
                    'options' =>[
                        'data' => Akun::listAkun(),
                        'options' => ['placeholder' => 'Pilih Akun Jurnal']
                    ]
                ],
                'nama_kas_bank' => [
                    'options' => [
                        'placeholder' => 'Nama Kas/Bank'
                    ]
                ]
            ]
    ]);?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
