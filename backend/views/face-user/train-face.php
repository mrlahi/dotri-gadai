<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Train Face Recognition';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-success">
    <div class="box-body">
        <h3>Train Face For : <?= $dataProfile->nama; ?></h3>
        <p>Activate camera then click "Capture & Save" to save face into the system.</p>

        <center>
            <video id="video" autoplay muted></video>
        </center>
        
        <button id="capture" class="btn btn-success btn-icon-split btn-block">
            <span class="icon text-white-50"><i class="fas fa-camera"></i></span>
            <span class="text"> Capture & Save</span>
        </button>
        <canvas id="snapshot" style="display: none;"></canvas>

        <?= Html::beginForm(['face-user/train-face?id='.$id], 'post', ['id' => 'train-face-form']) ?>
            <?= Html::hiddenInput('image_data', '', ['id' => 'image-data']) ?>
            <?= Html::hiddenInput('image_url', '', ['id' => 'image-url']) ?>
        <?= Html::endForm() ?>
    </div>
</div>

<?php
$this->registerJsFile('/js/face-api/dist/face-api.js', ['position' => \yii\web\View::POS_HEAD]);


$script = <<<JS
// Tunggu face-api.js selesai dimuat
document.addEventListener('DOMContentLoaded', function() {
    // Load models setelah face-api.js selesai dimuat
    Promise.all([
        faceapi.nets.tinyFaceDetector.loadFromUri('/js/face-api/weights'),
        faceapi.nets.faceLandmark68Net.loadFromUri('/js/face-api/weights'),
        faceapi.nets.faceRecognitionNet.loadFromUri('/js/face-api/weights'),
    ]).then(startVideo);

    const video = document.getElementById('video');
    const snapshot = document.getElementById('snapshot');
    const imageDataInput = document.getElementById('image-data');
    const imageUrl = document.getElementById('image-url');

    // Start webcam
    function startVideo() {
        navigator.mediaDevices.getUserMedia({ video: {} })
            .then(stream => video.srcObject = stream)
            .catch(err => console.error(err));
    }

    // Capture image and save face descriptor
    document.getElementById('capture').addEventListener('click', async () => {
        const detection = await faceapi.detectSingleFace(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceDescriptor();

        if (detection) {
            const descriptor = detection.descriptor;
            imageDataInput.value = JSON.stringify(descriptor);

            const canvas = document.createElement('canvas');
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            const context = canvas.getContext('2d');
            context.drawImage(video, 0, 0, canvas.width, canvas.height);

            // Konversi gambar ke data URL
            const imageDataUrl = canvas.toDataURL('image/png');

            imageUrl.value = imageDataUrl;

            document.getElementById('train-face-form').submit();
        } else {
            alert('Wajah tidak terdeteksi. Pastikan kamera mengarah ke wajah Anda.');
        }
    });
});
JS;

$this->registerJs($script, \yii\web\View::POS_END);
?>
