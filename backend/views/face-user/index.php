<?php

use common\models\FaceUser;
use common\models\Profile;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var common\models\FaceUserSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Face Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-body">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'nama',
                'no_hp',
                'email',
                [
                    'label' => 'Face Record',
                    'value' => 'cekFaceRecorded'
                ],
                [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, Profile $model, $key, $index, $column) {
                        return Url::toRoute([$action, 'id' => $model->id_user]);
                    },
                    'template' => '{train-face} {detail-user}',
                    'buttons' => [
                        'train-face' => function($url,$model){
                            return Html::a('<span class="fa fa-camera"></span> Record Face', [$url], ['class' => 'btn btn-info btn-sm']);
                        },
                        'detail-user' => function($url,$model){
                            return Html::a('<span class="fa fa-eye"></span> Detail User', [$url], ['class' => 'btn btn-primary btn-sm']);
                        }
                    ],
                    'visibleButtons' => [
                        'train-face' => function($model){
                            return $model->getCekFaceRecorded() <= 5;
                        }
                    ]
                ],
            ],
        ]); ?>


    </div>
</div>
