<?php

$this->title = $dataProfile->nama;

?>
<div class="row">
    <div class="col-lg-4">
        <div class="box box-success">
            <div class="box-body">
                <table class="table table-responsive table-triped">
                    <tr>
                        <td>Name</td>
                        <th><?= $dataProfile->nama; ?></th>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <th><?= $dataProfile->email; ?></th>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <th><?= $dataProfile->no_hp; ?></th>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <th><?= $dataProfile->alamat; ?></th>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="box box-success shadow h-100 py-2">
            <div class="box-body">
                <div class="row">
                    <?php foreach($dataFace as $face): ?>
                        <?php
                            $dirPath = Yii::getAlias('@backend/web/uploads/user_face_photos/');
                            $compressedFilePath = $dirPath.$face->id."_compressed.png";
                            if(file_exists($compressedFilePath)){
                                $filePath = "/uploads/user_face_photos/".$face->id."_compressed.png";
                            }
                            else{
                                $filePath = "/uploads/user_face_photos/".$face->id.".png";
                            }
                        ?>
                        <div class="col-lg-4">
                            <img src="<?= $filePath; ?>" alt="" class="img img-fluid img-thumbnail">
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>