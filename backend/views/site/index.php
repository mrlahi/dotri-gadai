<?php
use backend\widgets\summaryUang\SummaryUangWidgets;
use backend\widgets\summaryFinance\SummaryFinanceWidgets;
use backend\widgets\summaryBarang\SummaryBarangWidgets;
use backend\widgets\summaryCabang\SummaryCabangWidgets;

$this->title = 'DOTRI Gadai';
?>

<?php
  if($modelProfile->myProfile()->akses != "admin" AND $modelProfile->myProfile()->akses != "defaultFinance"){
    $id_cabang = Yii::$app->session['id_cabang'];
  }
  else{
    if(empty($_GET['id_cabang'])){
      $id_cabang = Yii::$app->session['id_cabang'];
    }
    else{
      $id_cabang = $_GET['id_cabang'];
    }
  }
?>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <?php if($modelProfile->myProfile()->akses == "admin" OR $modelProfile->myProfile()->akses == "defaultFinance"){
      ?>
      <!--Panel Admin -->
      <div class="row">

      <?php
        $dataCabang = $modelCabang->find()->where(['type_cabang' => 'CABANG BIASA'])->all();
        $total = 0;
        foreach($dataCabang as $dataCabs){
          $id_cabang_sum = $dataCabs->id_cabang;
          $jumlah = $modelBarang->outstandingUangAll($id_cabang_sum);
          $total += $jumlah;

          echo SummaryUangWidgets::widget(['id_cabang' => $id_cabang_sum]);
        }
      ?>

        <div class="col-lg-12 col-xs-6">
          <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?= "Rp. ".number_format($total,2,",","."); ?></h3>

                  <p>Total Outstanding</p>
                </div>
                <div class="icon">
                  <i class="fa fa-check"></i>
                </div>
              <a href="#" class="small-box-footer">Semua Status</a>
            </div>
        </div>
        <!-- ./col -->
      </div>
        <?= SummaryFinanceWidgets::widget(); ?>
      <?php
        }
      else{
      
        echo SummaryCabangWidgets::widget(['id_cabang' => $id_cabang]);

      }

      echo SummaryBarangWidgets::widget();

      ?>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">
              <li class="active"><a href="#now30" data-toggle="tab">Besok (10%)</a></li>
              <li>
                <a href="#all30" data-toggle="tab">Semua Tagihan (10%)</a>
              </li>
              <li><a href="#now15" data-toggle="tab">Besok (5%)</a></li>
              <li>
                <a href="#all15" data-toggle="tab">Semua Tagihan (5%)</a>
              </li>
              <li class="pull-left header"><i class="fa fa-inbox"></i> Tagihan</li>
            </ul>
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
              <div class="tab-pane active" id="now30" style="height: 300px; overflow:auto;">
                <table class="table table-bordered table-hover">
                  <thead>
                      <th>No</th>
                      <th>No. Sbg</th>
                      <th>HP</th>
                      <th>Status</th>
                      <th>Detail</th>
                  </thead>
                  <?php
                    $no =1;
                    $tagihNow = $modelBarang->listTagihNow($id_cabang,30);
                    foreach ($tagihNow as $tagihNows){
                      $id_nasabah = $tagihNows->id_nasabah;
                  ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td>
                      <?= $tagihNows->no_kontrak; ?>
                      <br>
                      <?= $modelCabang->myCabang($tagihNows->id_cabang)->nama_cabang; ?>
                    </td>
                    <td><?= $modelBarang->myNasabah2($id_nasabah)->nama; ?><br>
                    <span class="fa fa-phone"></span> <?= $modelBarang->myNasabah2($id_nasabah)->no_hp." ".$modelBarang->myNasabah2($id_nasabah)->no_hp2; ?></td>
                    <td>
                      <?php
                        echo $tagihNows->status."<br>".Yii::$app->formatter->asDate($tagihNows->jatuh_tempo)."";
                      ?>
                    </td>
                    <td><a href="/barang/find?sbg=<?= $tagihNows->no_kontrak ?>" class="btn btn-warning"><span class="fa fa-pencil"></span> Tagih</a></td>
                  </tr>
                    <?php } ?>
                </table>
              </div>
              <div class="tab-pane" id="all30" style="height: 300px; overflow:auto;">
                <table class="table table-bordered table-hover">
                  <thead>
                      <th>No</th>
                      <th>No. Sbg</th>
                      <th>HP</th>
                      <th>Status</th>
                      <th>Detail</th>
                  </thead>
                  <?php
                    $no =1;
                    $tagihAll = $modelBarang->listTagihAll($id_cabang,30);
                    foreach ($tagihAll as $tagihAlls){
                      $id_nasabah = $tagihAlls->id_nasabah;
                  ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td>
                      <?= $tagihAlls->no_kontrak; ?>
                      <br>
                      <?= $modelCabang->myCabang($tagihAlls->id_cabang)->nama_cabang; ?>
                    </td>
                    <td><?= $modelBarang->myNasabah2($id_nasabah)->nama; ?><br>
                    <span class="fa fa-phone"></span> <?= $modelBarang->myNasabah2($id_nasabah)->no_hp." ".$modelBarang->myNasabah2($id_nasabah)->no_hp2; ?></td>
                    <td>
                    <?php
                        $now = date_create(date('Y-m-d'));
                        $jatuh_tempo = date_create($tagihAlls->jatuh_tempo);
                        $interval = date_diff($now,$jatuh_tempo);
                        $interval = $interval->format("%a");
                      ?>
                      <?= $tagihAlls->status."<br>".
                          $interval. " Hari"; ?>
                    </td>
                    <td><a href="/barang/find?sbg=<?= $tagihAlls->no_kontrak ?>" class="btn btn-warning"><span class="fa fa-pencil"></span> Tagih</a></td>
                  </tr>
                    <?php } ?>
                </table>
              </div>

              <div class="tab-pane" id="now15" style="height: 300px; overflow:auto;">
                <table class="table table-bordered table-hover">
                  <thead>
                      <th>No</th>
                      <th>No. Sbg</th>
                      <th>HP</th>
                      <th>Status</th>
                      <th>Detail</th>
                  </thead>
                  <?php
                    $no =1;
                    $tagihNow = $modelBarang->listTagihNow($id_cabang,15);
                    foreach ($tagihNow as $tagihNows){
                      $id_nasabah = $tagihNows->id_nasabah;
                  ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td>
                      <?= $tagihNows->no_kontrak; ?>
                      <br>
                      <?= $modelCabang->myCabang($tagihNows->id_cabang)->nama_cabang; ?>
                    </td>
                    <td><?= $modelBarang->myNasabah2($id_nasabah)->nama; ?><br>
                    <span class="fa fa-phone"></span> <?= $modelBarang->myNasabah2($id_nasabah)->no_hp." ".$modelBarang->myNasabah2($id_nasabah)->no_hp2; ?></td>
                    <td><?= $tagihNows->status; ?></td>
                    <td><a href="/barang/find?sbg=<?= $tagihNows->no_kontrak ?>" class="btn btn-warning"><span class="fa fa-pencil"></span> Tagih</a></td>
                  </tr>
                    <?php } ?>
                </table>
              </div>
              <div class="tab-pane" id="all15" style="height: 300px; overflow:auto;">
                <table class="table table-bordered table-hover">
                  <thead>
                      <th>No</th>
                      <th>No. Sbg</th>
                      <th>HP</th>
                      <th>Status</th>
                      <th>Detail</th>
                  </thead>
                  <?php
                    $no =1;
                    $tagihAll = $modelBarang->listTagihAll($id_cabang,15);
                    foreach ($tagihAll as $tagihAlls){
                      $id_nasabah = $tagihAlls->id_nasabah;
                  ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td>
                      <?= $tagihAlls->no_kontrak; ?>
                      <br>
                      <?= $modelCabang->myCabang($tagihAlls->id_cabang)->nama_cabang; ?>
                    </td>
                    <td><?= $modelBarang->myNasabah2($id_nasabah)->nama; ?><br>
                    <span class="fa fa-phone"></span> <?= $modelBarang->myNasabah2($id_nasabah)->no_hp." ".$modelBarang->myNasabah2($id_nasabah)->no_hp2; ?></td>
                    <td>
                    <?php
                        $now = date_create(date('Y-m-d'));
                        $jatuh_tempo = date_create($tagihAlls->jatuh_tempo);
                        $interval = date_diff($now,$jatuh_tempo);
                        $interval = $interval->format("%a");
                      ?>
                      <?= $tagihAlls->status."<br>".
                          $interval. " Hari"; ?>
                    </td>
                    <td><a href="/barang/find?sbg=<?= $tagihAlls->no_kontrak ?>" class="btn btn-warning"><span class="fa fa-pencil"></span> Tagih</a></td>
                  </tr>
                    <?php } ?>
                </table>
              </div>
            </div>
          </div>
          <!-- /.nav-tabs-custom -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">

          <!-- Map box -->
          <div class="box box-info">
            <div class="box-header">
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse"
                        data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                  <i class="fa fa-minus"></i></button>
              </div>
              <!-- /. tools -->

              <i class="fa fa-money"></i>

              <h3 class="box-title">
                Kas Hari Ini (Cash)
              </h3>
            </div>
            <div class="box-body">
              <div id="" style="height: 250px; width: 100%;">
              <h1>Rp. <?= number_format($modelLogFinance->kasNow($id_cabang),2,",","."); ?></h1>
              <hr>
              <?php
              if($modelProfile->myProfile()->akses == "admin" OR $modelProfile->myProfile()->akses == "defaultFinance"){
              ?>
                <form action="" method="get">
                  <div class="form-group row">
                  <div class="col-sm-8">
                    <select name="id_cabang" id="" class="form-control">
                      <?php
                        $dataCabang = $modelCabang->find()->where(['type_cabang' => 'CABANG BIASA'])->all();
                        foreach ($dataCabang as $dataCabangs){
                      ?>
                      <option value="<?= $dataCabangs->id_cabang; ?>" <?php if($id_cabang == $dataCabangs->id_cabang){echo 'selected';} ?>><?= $dataCabangs->nama_cabang; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-sm-4">
                    <input type="submit" value="Cari" class="btn btn-primary">
                  </div>
                  </div>
                </form>
                <?php } ?>
                <table align="center" width="100%">
                      <tr>
                        <td> Modal Masuk</td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->modalNow($id_cabang),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Pelunasan</td>
                        <td>:</td>
                        <?php
                          $pelunasan = $pelunasan = $modelLogFinance->kembaliBungaNow($id_cabang,"KembaliBunga5Persen") + $modelLogFinance->kembaliBungaNow($id_cabang,"KembaliBunga1.2Persen") + $modelLogFinance->pelunasanNow($id_cabang);
                        ?>
                        <td>Rp. <?= number_format($pelunasan,2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Angsuran</td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->pembayaranNow($id_cabang),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Bunga Pinjaman 1.2%</td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->biayaBungaNow2($id_cabang,1.2),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Bunga Pinjaman 2.4%</td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->biayaBungaNow2($id_cabang,2.4),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Bunga Pinjaman 5%</td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->biayaBungaNow2($id_cabang,5),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Bunga Pinjaman 10%</td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->biayaBungaNow2($id_cabang,10),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Bunga Perpanjangan 1.2%
                        </td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->bungaPerpanjangNow2($id_cabang,1.2),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Bunga Perpanjangan 2.4%
                        </td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->bungaPerpanjangNow2($id_cabang,2.4),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Bunga Perpanjangan 5%
                        </td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->bungaPerpanjangNow2($id_cabang,5),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Bunga Perpanjangan 10%
                        </td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->bungaPerpanjangNow2($id_cabang,10),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Denda Perpanjangan</td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->dendaPerpanjangNow($id_cabang),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Denda Pelunasan</td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->dendaPelunasanNow($id_cabang),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Bunga Pelunasan</td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->bungaPelunasanNow($id_cabang),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Pendapatan Administrasi</td>
                        <td>:</td>
                        <td> Rp. <?= number_format($modelLogFinance->biayaAdminNow($id_cabang),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Beban Operasional</td>
                        <td>:</td>
                        <td> Rp. <?= number_format($modelLogFinance->bebanOperasionalNow($id_cabang),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Pinjaman</td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->pinjamanNow($id_cabang),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Cashback (1.2%)</td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->kembaliBungaNow($id_cabang,'KembaliBunga1.2Persen'),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Cashback (5%)</td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->kembaliBungaNow($id_cabang,'KembaliBunga5Persen'),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Jual</td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->lelangNow($id_cabang),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Uang Keluar</td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->pengeluaranNow($id_cabang),2,",","."); ?></td>
                      </tr>
                      <tr>
                        <td>Via Transfer</td>
                        <td>:</td>
                        <td>Rp. <?= number_format($modelLogFinance->viaTransferNow($id_cabang),2,",","."); ?></td>
                      </tr>
                </table>

              </p>
              </div>
            </div>
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->

          

        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
