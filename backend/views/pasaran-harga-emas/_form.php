<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $model common\models\PasaranHargaEmas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pasaran-harga-emas-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'nilai_karat' => [
                'label' => 'Nilai Karat',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'suffix' => ' karat',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required'
                    ]
                ]
            ],
            'harga' => [
                'label' => 'Harga Pasaran',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required'
                    ]
                ]
            ],
            'deskripsi' => [
                'type' => Form::INPUT_TEXTAREA,

            ],
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
