<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PasaranHargaEmas */

$this->title = 'Pasaran Harga Emas: ' . $model->nilai_karat. ' K';
$this->params['breadcrumbs'][] = ['label' => 'Pasaran Harga Emas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
