<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PasaranHargaEmasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pasaran Harga Emas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'nilai_karat',
                'value' => function($model){
                    return $model->nilai_karat." K";
                }
            ],
            [
                'attribute' => 'harga',
                'format' => 'currency'
            ],
            [
                'attribute' => 'deskripsi'
            ],
            // 'created_at',
            // 'created_by',

            // [
            //     'class' => 'yii\grid\ActionColumn',
            //     'template' => '{update}',
            //     'buttons' => [
            //         'update' => function($url,$model){
            //             return Html::a('<span class="fa fa-edit"></span> Update', [$url], ['class' => 'btn btn-warning']);
            //         }
            //     ]
            // ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>
</div>
