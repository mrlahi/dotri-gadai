<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PasaranHargaEmasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pasaran Harga Emas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a('Tambahan Pasaran Harga', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'nilai_karat',
                'value' => function($model){
                    return $model->nilai_karat." K";
                }
            ],
            [
                'attribute' => 'harga',
                'format' => 'currency'
            ],
            [
                'attribute' => 'is_active',
                'label' => 'Status',
                'value' => function($model){
                    if($model->is_active){
                        return 'AKTIF';
                    }
                    else{
                        return 'NON AKTIF';
                    }
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ['0' => 'NON AKTIF', '1' => 'AKTIF'],
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Tgl. Input',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'autoClear' => true,
                        'format' => 'yyyy-mm-d'
                    ]
                ]
            ],
            [
                'attribute' => 'deskripsi'
            ],
            // 'created_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{aktifkan} {non-aktifkan}',
                'visibleButtons' => [
                    'aktifkan' => function($model){
                        if($model->is_active == 0){
                            return TRUE;
                        }
                    },
                    'non-aktifkan' => function($model){
                        if($model->is_active == 1){
                            return TRUE;
                        }
                    }
                ],
                'buttons' => [
                    'update' => function($url,$model){
                        return Html::a('<span class="fa fa-edit"></span> Update', [$url], ['class' => 'btn btn-warning btn-sm']);
                    },
                    'aktifkan' => function($url,$model){
                        return Html::a('<span class="fa fa-check"></span> Aktifkan', [$url], ['class' => 'btn btn-success btn-sm']);
                    },
                    'non-aktifkan' => function($url,$model){
                        return Html::a('<span class="fa fa-close"></span> Non Aktifkan', [$url], ['class' => 'btn btn-danger btn-sm']);
                    }
                ]
            ],
        ],
    ]); ?>
    </div>
</div>
