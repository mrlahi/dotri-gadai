<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HariKerjaPegawai */

$this->title = 'Update Hari Kerja Pegawai: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Hari Kerja Pegawais', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hari-kerja-pegawai-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
