<?php

use common\models\HariKerjaPegawai;
use common\models\JadwalShift;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\editable\Editable;

/* @var $this yii\web\View */
/* @var $searchModel common\models\HariKerjaPegawaiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hari Kerja Pegawai : '.$dataPegawai->nama;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a('Tambah Hari Kerja', ['create', 'pegawai_id' => $dataPegawai->id_user], ['class' => 'btn btn-success']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax' => true,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                //'pegawai_id',
                [
                    'attribute' => 'range_tgl',
                    'value' => 'hariKerja.tgl_kerja',
                    'format' => 'date',
                    'filterType' => GridView::FILTER_DATE_RANGE,
                    'filterWidgetOptions' => [       
                        'attribute' => 'tgl_masuk',
                        'pluginOptions' => [
                            'separator' => ' - ',
                            'format' => 'YYYY-MM-DD',
                            'locale' => [
                                'format' => 'YYYY-MM-DD'
                            ],
                        ],
                    ]
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'shift_id',
                    'label' => 'Shift',
                    'value' => function($model){
                        $shift = $model->shift;
                        return $shift->nama_shift.' ('.$shift->jam_masuk." - ".$shift->jam_pulang.')';
                    },
                    'editableOptions' =>[
                        'asPopover' => true,
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => JadwalShift::listShift(),
                        'size'=>'md',
                        'displayValueConfig' => JadwalShift::listShift(),
                        //'options' => ['class'=>'form-control']
                    ],
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => JadwalShift::listShift(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'status_absensi',
                    'editableOptions' =>[
                        'asPopover' => false,
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => HariKerjaPegawai::listStatus(),
                        'size'=>'md',
                        'displayValueConfig' => HariKerjaPegawai::listStatus(),
                        //'options' => ['class'=>'form-control']
                    ],
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => HariKerjaPegawai::listStatus(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                [
                    'attribute' => 'deskripsi',
                    'class' => 'kartik\grid\EditableColumn',
                    'editableOptions' =>[
                        'asPopover' => false,
                        'inputType' => Editable::INPUT_TEXTAREA,
                        'size'=>'md',
                    ],
                ],
                //'created_at',
                //'created_by',

                //['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
