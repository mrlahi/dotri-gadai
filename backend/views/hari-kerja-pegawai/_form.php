<?php

use common\models\JadwalShift;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\HariKerjaPegawai */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hari-kerja-pegawai-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'range_tgl_kerja' =>[
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => DateRangePicker::classname(),
            ],
            'shift_id' => [
                'label' => 'Shift',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => JadwalShift::listShift()
                ]
            ]
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
