<?php

use common\models\Akun;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ListLaporanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List Laporan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a('Tambah List Laporan', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute' => 'jenis_laporan',
                    'label' => 'Jenis Laporan',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => [
                        'BIAYA' => 'BIAYA',
                        'BIAYA NON OPERASIONAL' => 'BIAYA NON OPERASIONAL',
                        'PASIVA' => 'PASIVA',
                        'AKTIVA' => 'AKTIVA',
                        'PENDAPATAN' => 'PENDAPATAN',
                        'PENDAPATAN NON OPERASIONAL' => 'PENDAPATAN NON OPERASIONAL',
                        'ARUS KAS MASUK DARI AKTIVITAS OPERASIONAL' => 'ARUS KAS MASUK DARI AKTIVITAS OPERASIONAL',
                        'ARUS KAS KELUAR DARI AKTIVITAS OPERASIONAL' => 'ARUS KAS KELUAR DARI AKTIVITAS OPERASIONAL',
                        'ARUS KAS MASUK DARI AKTIVITAS INVESTASI' => 'ARUS KAS MASUK DARI AKTIVITAS INVESTASI',
                        'ARUS KAS KELUAR DARI AKTIVITAS INVESTASI' => 'ARUS KAS KELUAR DARI AKTIVITAS INVESTASI',
                        'ARUS KAS MASUK DARI AKTIVITAS PENDANAAN' => 'ARUS KAS MASUK DARI AKTIVITAS PENDANAAN',
                        'ARUS KAS KELUAR DARI AKTIVITAS PENDANAAN' => 'ARUS KAS KELUAR DARI AKTIVITAS PENDANAAN'],
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                [
                    'attribute' => 'akun_id',
                    'label' => 'Nama Akun',
                    'value' => 'akun.nama_akun',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => Akun::listAkun(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                [
                    'attribute' => 'posisi_perhitungan_khusus',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => [
                        '-' => '-',
                        'DEBET' => 'DEBET',
                        'KREDIT' => 'KREDIT'
                        ],
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                [
                    'attribute' => 'bentuk_laporan',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => [
                        'POSITIF' => 'POSITIF',
                        'NEGATIF' => 'NEGATIF'
                        ],
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                // 'created_at',
                // 'created_by',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}'
                ],
            ],
        ]); ?>
    </div>
</div>
