<?php

use common\models\Akun;
use common\models\Cabang;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $model common\models\ListLaporan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="list-laporan-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' =>[
                'jenis_laporan' =>[
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::classname(),
                    'options' =>[
                        'data' => [
                            'BIAYA' => 'BIAYA',
                            'BIAYA NON OPERASIONAL' => 'BIAYA NON OPERASIONAL',
                            'PASIVA' => 'PASIVA',
                            'AKTIVA' => 'AKTIVA',
                            'PENDAPATAN' => 'PENDAPATAN',
                            'PENDAPATAN NON OPERASIONAL' => 'PENDAPATAN NON OPERASIONAL',
                            'ARUS KAS MASUK DARI AKTIVITAS OPERASIONAL' => 'ARUS KAS MASUK DARI AKTIVITAS OPERASIONAL',
                            'ARUS KAS KELUAR DARI AKTIVITAS OPERASIONAL' => 'ARUS KAS KELUAR DARI AKTIVITAS OPERASIONAL',
                            'ARUS KAS MASUK DARI AKTIVITAS INVESTASI' => 'ARUS KAS MASUK DARI AKTIVITAS INVESTASI',
                            'ARUS KAS KELUAR DARI AKTIVITAS INVESTASI' => 'ARUS KAS KELUAR DARI AKTIVITAS INVESTASI',
                            'ARUS KAS MASUK DARI AKTIVITAS PENDANAAN' => 'ARUS KAS MASUK DARI AKTIVITAS PENDANAAN',
                            'ARUS KAS KELUAR DARI AKTIVITAS PENDANAAN' => 'ARUS KAS KELUAR DARI AKTIVITAS PENDANAAN'],
                        'options' => ['placeholder' => 'Pilih Akun']
                    ]
                ],
                'akun_id' =>[
                    'label' => 'Kode Akun',
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::classname(),
                    'options' =>[
                        'data' => Akun::listAkun(),
                        'options' => ['placeholder' => 'Pilih Akun']
                    ]
                ],
                'posisi_perhitungan_khusus' =>[
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::classname(),
                    'options' =>[
                        'data' => [
                            '-' => '-',
                            'DEBET' => 'DEBET',
                            'KREDIT' => 'KREDIT',
                        ],
                        'options' => ['placeholder' => 'Pilih Posisi']
                    ]
                ],
                'bentuk_laporan' =>[
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::classname(),
                    'options' =>[
                        'data' => [
                            'POSITIF' => 'POSITIF',
                            'NEGATIF' => 'NEGATIF',
                        ],
                        'options' => ['placeholder' => 'Pilih Bentuk Laporan']
                    ]
                ],
            ]
        ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
