<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel common\models\LogStatusTempSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Outstanding ('.date_format(date_create($start),'d/m/y')." - ".date_format(date_create($end),'d/m/y').")";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
    
    <?= Html::a('<span class="fa fa-backward"></span> Kembali', ['/outstanding-manager/'], ['class' => 'btn btn-success pull-right']) ?>
   

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
// //Table Yang Mau diexport
//     $gridExport = [
//         ['class' => 'yii\grid\SerialColumn'],
//         [
//             'label' => 'No Kontrak',
//             'value' => 'barang.no_kontrak'
//         ],
//         [
//             'label' => 'Outlet',
//             'value' => 'cabang.nama_cabang'
//         ],
//         [
//             'label' => 'Tipe SBG',
//             'value' => 'jenisBarang.nama_barang'
//         ],
//         [
//             'label' => 'Tgl SBG',
//             'value' => 'barang.tgl_masuk',
//             'format' => 'date'
//         ],
//         [
//             'label' => 'Jth Tempo',
//             'value' => 'barang.jatuh_tempo',
//             'format' => 'date'
//         ],
//         [
//             'label' => 'Nasabah',
//             'value' => 'nasabah.nama'
//         ],
//         [
//             'label' => 'Nama Barang',
//             'value' => 'barang.nama_barang'
//         ],
//         [
//             'label' => 'Merk',
//             'value' => 'barang.merk'
//         ],
//         [
//             'label' => 'Tipe Barang',
//             'value' => 'barang.tipe'
//         ],
//         [
//             'label' => 'Kelengkapan',
//             'value' => 'barang.kelengkapan'
//         ],
//         [
//             'label' => 'Harga Taksir',
//             'value' => 'barang.harga_taksir'
//         ],
//         [
//             'label' => 'Nilai Pinjam',
//             'value' => 'barang.nilai_pinjam'
//         ],
//         [
//             'label' => 'Sisa Pinjam',
//             'value' => function ($model) {
//                 $id_barang = $model->id_barang;
//                 $start = $_GET['start'];
//                 $end = $_GET['end'];
//                 $sisaPinjam = $model->sisaPinjam($id_barang,$start,$end);
//                 return $sisaPinjam;
//             },
//         ],
//         [
//             'label' => 'Tgl Pelunasan',
//             'value' => 'tglLunas',
//             'format' => 'date'
//         ],
//         [
//             'label' => 'Status',
//             'value' => 'barang.status'
//         ],
//         [
//             'label' => ' ',
//             'value' => function(){ return "OUTSTANDING"; },
//             'format' => 'raw'
//         ]
//     ];
?>

<?php
//Tombol Exportnya
// echo ExportMenu::widget([
//     'dataProvider' => $dataProvider,
//     'columns' => $gridExport,
//     'showColumnSelector' => false,
//     'exportConfig' => [
//         ExportMenu::FORMAT_HTML => false,
//         ExportMenu::FORMAT_CSV => false,
//         ExportMenu::FORMAT_TEXT => false,
//         ExportMenu::FORMAT_PDF => [
//             'pdfConfig' => [
//                 'orientation' => 'L',
//             ],
//         ],
//     ],
//     'filename' => $judulFile
// ]);
?>

<?php

/* echo \kartik\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridExport
]);
*/
    ?>

<div class="clearfix"></div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id_barang'
            ],
            [
                'label' => 'No Kontrak',
                'value' => 'barang.no_kontrak',
            ],
            [
                'label' => 'Outlet',
                'value' => 'cabang.nama_cabang'
            ],
            [
                'label' => 'Tipe SBG',
                'value' => 'jenisBarang.nama_barang'
            ],
            [
                'label' => 'Tgl SBG',
                'value' => 'barang.tgl_masuk',
                'format' => 'date'
            ],
            [
                'label' => 'Jth Tempo',
                'value' => 'barang.jatuh_tempo',
                'format' => 'date'
            ],
            [
                'label' => 'Nasabah',
                'value' => 'nasabah.nama'
            ],
            [
                'label' => 'Nama Barang',
                'value' => 'barang.nama_barang'
            ],
            [
                'label' => 'Merk',
                'value' => 'barang.merk'
            ],
            [
                'label' => 'Status',
                'value' => 'barang.status'
            ],
            [
                'label' => 'Tgl Pelunasan',
                'value' => 'tglLunas',
                'format' => 'date'
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>
    </div>
</div>
