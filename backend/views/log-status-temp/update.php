<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LogStatusTemp */

$this->title = 'Update Log Status Temp: ' . $model->id_status_temp;
$this->params['breadcrumbs'][] = ['label' => 'Log Status Temps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_status_temp, 'url' => ['view', 'id' => $model->id_status_temp]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="log-status-temp-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
