<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LogStatusTemp */

$this->title = 'Create Log Status Temp';
$this->params['breadcrumbs'][] = ['label' => 'Log Status Temps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-status-temp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
