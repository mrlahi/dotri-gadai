<?php
use yii\helpers\Html;
use backend\assets\DotriAsset;
use common\models\AksesKhusus;
use common\models\LoginToken;
use yii\web\Cookie;

/* @var $this \yii\web\View */
/* @var $content string */


if (Yii::$app->controller->action->id === 'login') { 
/**
 * Do not use this code in your template. Remove it. 
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    DotriAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');

    $cekAksesKhusus = AksesKhusus::find()->where(['profile_id' => Yii::$app->user->id])->one();

    if(empty($cekAksesKhusus)){
        $dataLoginToken = LoginToken::find()->where(['created_at' => date('Y-m-d'), 'pegawai_id' => Yii::$app->user->id])->one();
        if(!Yii::$app->user->can('admin')){
            if(empty($dataLoginToken)){
                return  Yii::$app->response->redirect('/login-token/confirm-login/');
            }
            else{
                $token = Yii::$app->request->cookies->getValue('token');
                if($dataLoginToken->token != $token){
                    return  Yii::$app->response->redirect('/login-token/confirm-login/');
                }
            }
        }
    }
    
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper">
        
        <?= $this->render(
            'header.php',
            ['directoryAsset' => $directoryAsset]
        ) ?>

        <?= $this->render(
            'left.php',
            ['directoryAsset' => $directoryAsset]
        )
        ?>

        <?= $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>

    </div>

    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
