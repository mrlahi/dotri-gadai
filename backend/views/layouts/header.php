<?php

use common\models\Barang;
use common\models\Profile;
use yii\helpers\Html;
use kartik\alert\AlertBlock;
use kartik\growl\Growl;

/* @var $this \yii\web\View */
/* @var $content string */

if(Yii::$app->user->can('admin')){
    $dataJatuhTempo = Barang::find()->where(['jatuh_tempo' => date('Y-m-d')]);
}
else{
    $dataJatuhTempo = Barang::find()->where(['jatuh_tempo' => date('Y-m-d'), 'id_cabang' => Profile::myProfile2()->id_cabang]);
}

?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">DGJ</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-flag-o"></i>
                        <span class="label label-success"><?= $dataJatuhTempo->count(); ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">Jatuh Tempo <?= $dataJatuhTempo->count(); ?> SBG</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <?php
                                    $dataBarang = $dataJatuhTempo->all();
                                    foreach($dataBarang as $barang){
                                ?>
                                <li><!-- start message -->
                                    <a href="#">
                                        <div class="pull-left">
                                            <span class="fa fa-laptop img-circle"></span>
                                        </div>
                                        <h4>
                                            <?= $barang->no_kontrak; ?>
                                            <small><i class="fa fa-clock-o"></i> <?= $barang->jenisBarang->nama_barang; ?></small>
                                        </h4>
                                        <p><?= $barang->nasabah->nama; ?></p>
                                    </a>
                                </li>
                                <!-- end message -->
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">See All Messages</a></li>
                    </ul>
                </li>

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/images/logo-dotri.png" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= Yii::$app->session['nama']; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/images/logo-dotri.png" class="img-circle"
                                 alt="User Image"/>

                            <p>
                                <?= Yii::$app->session['nama']; ?>
                                <small><?= Yii::$app->session['nama_cabang']; ?></small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
    <?php foreach (Yii::$app->session->getAllFlashes() as $message):; ?>
    <?php
    echo Growl::widget([
        'type' => (!empty($message['type'])) ? $message['type'] : 'danger',
        'title' => (!empty($message['title'])) ? Html::encode($message['title']) : 'Title Not Set!',
        'icon' => (!empty($message['icon'])) ? $message['icon'] : 'fa fa-info',
        'body' => (!empty($message['message'])) ? Html::encode($message['message']) : 'Message Not Set!',
        'showSeparator' => true,
        'delay' => 1, //This delay is how long before the message shows
        'pluginOptions' => [
        'delay' => (!empty($message['duration'])) ? $message['duration'] : 3000, //This delay is how long the message shows for
        'placement' => [
            'from' => (!empty($message['positonY'])) ? $message['positonY'] : 'top',
            'align' => (!empty($message['positonX'])) ? $message['positonX'] : 'right',
        ]
        ]
    ]);

    if(!empty($message['justLogin'])){
    ?>
    <script>
        // Mendeteksi tipe perangkat (desktop atau mobile)
        var deviceType = /Mobi/.test(navigator.userAgent) ? "Mobile" : "Desktop";

        console.log(navigator.userAgent);

        // Mendeteksi tipe peramban
        var browserType = navigator.userAgent.match(/Firefox|Chrome|Safari|Edge|IE/i)[0];

        var OSVersion = "Unknown OS Version";
        var userAgent = navigator.userAgent;

        var OSName = "Unknown OS";
        if (navigator.appVersion.indexOf("Win") != -1) OSName = "Windows";
        if (navigator.appVersion.indexOf("Mac") != -1) OSName = "MacOS";
        if (navigator.appVersion.indexOf("Linux") != -1) OSName = "Linux";
        if (navigator.appVersion.indexOf("Android") != -1) OSName = "Android";
        if (navigator.appVersion.indexOf("iOS") != -1) OSName = "iOS";

        if (userAgent.indexOf("Windows NT 10.0") != -1) OSVersion = "Windows 10";
        if (userAgent.indexOf("Windows NT 6.2") != -1) OSVersion = "Windows 8";
        if (userAgent.indexOf("Windows NT 6.1") != -1) OSVersion = "Windows 7";
        if (userAgent.indexOf("Windows NT 6.0") != -1) OSVersion = "Windows Vista";
        if (userAgent.indexOf("Windows NT 5.1") != -1) OSVersion = "Windows XP";
        if (userAgent.indexOf("Mac OS X") != -1) OSVersion = userAgent.match(/Mac OS X (10[\.\_\d]+)/)[1];
        if (userAgent.indexOf("Android") != -1) OSVersion = userAgent.match(/Android\s([^\s]*)/)[1];
        if (userAgent.indexOf("iOS") != -1) OSVersion = userAgent.match(/OS\s([^;]*)/)[1];



        // Mendeteksi lokasi menggunakan API Geolocation (harus diizinkan oleh pengguna)
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var latitude = position.coords.latitude;
                var longitude = position.coords.longitude;

                // Kirim data ke server menggunakan Ajax
                var systemInfo = {
                    user_agent: navigator.userAgent,
                    screen_width: window.screen.width,
                    screen_height: window.screen.height,
                    deviceType: deviceType,
                    browserType: browserType,
                    latitude: latitude,
                    longitude: longitude,
                    OSName : OSName,
                    OSVersion : OSVersion
                };

                //console.log(latitude);

                $.ajax({
                    type: "POST",
                    url: "/log-login/generate/", // Ganti dengan URL skrip PHP Anda
                    data: systemInfo,
                    success: function(response) {
                        console.log(response); // Tampilkan pesan respons dari server (opsional)
                    }
                });
            });
        }
        else{
            var systemInfo = {
                    user_agent: navigator.userAgent,
                    screen_width: window.screen.width,
                    screen_height: window.screen.height,
                    deviceType: deviceType,
                    browserType: browserType,
                    latitude: null,
                    longitude: null,
                    OSName : OSName,
                    OSVersion : OSVersion
                };

            $.ajax({
                type: "POST",
                url: "/log-login/generate/", // Ganti dengan URL skrip PHP Anda
                data: systemInfo,
                success: function(response) {
                    console.log(response); // Tampilkan pesan respons dari server (opsional)
                    console.log("tidak ada");
                }
        });
        }
    </script>
    <?php
    }

    ?>
<?php endforeach; ?>
</header>
