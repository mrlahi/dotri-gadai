<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use mdm\admin\components\MenuHelper;
use mdm\admin\components\Helper;

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/images/logo-dotri.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->session['nama']; ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="/barang/find" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="sbg" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?php
       if (Yii::$app->user->can('admin')) 
       {
               // 'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
               $menuItems= [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Dashboard',
                        'icon' => 'home',//dashboard..kenapa beberapa icon ga tampil ya?
                        'url' => ['/site/index']
                    ],
                    [
                        'label' => 'Master Data',
                        'icon' => 'laptop',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Cabang', 'icon' => 'building', 'url' => ['/cabang']],
                            ['label' => 'Pegawai', 'icon' => 'user', 'url' => ['/pegawai']],
                            ['label' => 'Nasabah', 'icon' => 'users', 'url' => ['/nasabah']],
                            ['label' => 'Pekerjaan', 'icon' => 'wrench', 'url' => ['/pekerjaan']],
                            ['label' => 'Lokasi Simpan', 'icon' => 'map', 'url' => ['/lokasi-simpan']]
                        ]
                    ],
                    [
                        'label' => 'Data Barang',
                        'icon' => 'address-book',
                        'url' => '#',
                        'items' => [
                            ['label' =>'Tambah Barang Lama','icon' => 'recycle','url' => ['/barang/create-langsung']],
                            ['label' =>'Barang Aktif','icon' => 'check','url' => ['/barang/index']],
                            ['label' => 'Barang Lunas', 'icon' => 'window-maximize','url' => ['/barang/lunas']],
                            ['label' => 'Barang Lunas Hari Ini', 'icon' => 'window-maximize','url' => ['/log-status/lunas-now']],
                            ['label' => 'Barang Jatuh Tempo', 'icon' => 'window-close','url' => ['/barang/jatuh-tempo']],
                            ['label' => 'Barang Pasif', 'icon' => 'window-minimize','url' => ['/barang/pasif']],
                            ['label' => 'Barang Lelang', 'icon' => 'flag','url' => ['/barang/lelang']],
                            ['label' => 'Barang Belum Terjual', 'icon' => 'flag','url' => ['/barang/belum-terjual']],
                            ['label' => 'Barang Terjual', 'icon' => 'gavel','url' => ['/barang/terjual']],
                        ]
                    ],
                    ['label' => 'Data Modal','icon' =>'money','url' => ['/log-modal']],
                    [
                        'label' => 'Report',
                        'icon' => 'bar-chart',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Data Outstanding', 'icon' => 'list', 'url' => ['/outstanding-manager/index']],
                            ['label' => 'Semua Data Barang', 'icon' => 'list', 'url' => ['/log-status']],
                            ['label' => 'Keuangan Harian', 'icon' => 'list', 'url' => ['/log-finance/filter']],
                            ['label' => 'Rekap Penjualan', 'icon' => 'list', 'url' => ['/log-finance/filter-penjualan']],
                            ['label' => 'Data Penjualan', 'icon' => 'list', 'url' => ['/jual/filter-data-jual']],
                            ['label' => 'Terjual', 'icon' => 'list', 'url' => ['/log-finance/filter-lelang']],
                            ['label' => 'Beban Operasional', 'icon' => 'list', 'url' => ['/beban-operasional/filter-operasional']],
                            ['label' =>'Seluruh Barang','icon' => 'list','url' => ['/barang/all']],
                        ]
                    ],
                    [
                        'label' => 'Report Baru',
                        'icon' => 'bar-chart',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Laporan Gadai', 'icon' => 'list', 'url' => ['/log-finance/laporan-gadai']],
                            ['label' => 'Laporan Perpanjang', 'icon' => 'list', 'url' => ['/log-finance/laporan-perpanjang']],
                            ['label' => 'Laporan Pelunasan', 'icon' => 'list', 'url' => ['/log-finance/laporan-pelunasan']],
                            ['label' => 'Laporan Angsuran', 'icon' => 'list', 'url' => ['/log-finance/laporan-angsuran']],
                        ]
                    ],

                    [
                        'label' => 'History',
                        'icon' => 'flag',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'History By Gadai',
                                'icon' => 'flag',
                                'url' => ['/barang/history']
                            ],
                            [
                                'label' => 'History By Nasabah',
                                'icon' => 'flag',
                                'url' => ['/barang/history-by-nasabah']
                            ],
                        ]
                    ],
                    
                    [
                        'label' => 'Beban Operasional',
                        'icon' => 'calculator',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Jenis Biaya', 'icon' => 'calculator', 'url' => ['/jenis-biaya-operasional/index']],
                            ['label' => 'Pengeluaran Operasional', 'icon' => 'calculator', 'url' => ['/beban-operasional/index']],
                        ]
                    ],
                    ['label' => 'Hapus Barang','icon' =>'trash','url' => ['/barang/hapus']],
                    ['label' => 'Kas Umum','icon' =>'money','url' => ['/kas-umum/index']],
                    ['label' => 'Pengeluaran','icon' =>'upload','url' => ['/log-uang-keluar/index']],
                    ['label' => 'Taksiran Harga Pinjaman','icon' =>'money','url' => ['/taksiran-harga-pasar/index']],
                    [
                        'label' => 'Config',
                        'icon' => 'key',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Level Akses', 'icon' => 'arrow-up', 'url' => ['/admin/role/index'],],
                            ['label' => 'Route', 'icon' => 'random', 'url' => ['/admin/route'],],
                            ['label' => 'Assignment', 'icon' => 'th-large', 'url' => ['/admin/assignment'],],
                            ['label' => 'Role', 'icon' => 'th-large', 'url' => ['/admin/role'],],
                        ],
                    ],
                    [
                        'label' => 'Jurnal',
                        'icon' => 'bookmark',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Akun', 'icon' => 'list', 'url' => ['/akun/index'],],
                            //['label' => 'Jurnal', 'icon' => 'pencil', 'url' => ['/log-jurnal/index'],],
                            ['label' => 'List Laporan', 'icon' => 'list', 'url' => ['/list-laporan/index'],],
                            ['label' => 'Jurnal Per Transaksi', 'icon' => 'pencil', 'url' => ['/log-transaksi/jurnal'],],
                            ['label' => 'Jurnal Penyesuaian', 'icon' => 'pencil', 'url' => ['/log-transaksi/index-penyesuaian'],],
                            //['label' => 'Buku Besar', 'icon' => 'book', 'url' => ['/log-jurnal/filter-buku-besar'],],
                            ['label' => 'Buku Besar', 'icon' => 'book', 'url' => ['/log-jurnal/filter-buku-besar-lama'],],
                            ['label' => 'Neraca Saldo', 'icon' => 'recycle', 'url' => ['/log-jurnal/filter-neraca-saldo'],],
                            ['label' => 'Laba Rugi', 'icon' => 'money', 'url' => ['/log-jurnal/filter-laba-rugi'],],
                            ['label' => 'Posisi Keuangan', 'icon' => 'money', 'url' => ['/log-jurnal/filter-posisi-keuangan'],],
                            ['label' => 'Arus Kas', 'icon' => 'money', 'url' => ['/log-jurnal/filter-arus-kas'],],
                            ['label' => 'Mutasi Kas', 'icon' => 'money', 'url' => ['/log-transaksi/index-mutasi'],],
                        ],
                    ],
                    [
                        'label' => 'Notifikasi',
                        'icon' => 'phone',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Device', 'icon' => 'list', 'url' => ['/device/index'],],
                            ['label' => 'Device Status', 'icon' => 'list', 'url' => ['/device/status'],],
                            ['label' => 'Notifikasi', 'icon' => 'list', 'url' => ['/log-notifikasi/index'],],
                        ],
                    ],
                    [
                        'label' => 'Saldo Awal',
                        'icon' => 'flag',
                        'url' => ['/log-jurnal/saldo-awal/']
                    ],
                    ['label' => 'Inventori','icon' => 'box','url' => ['/inventory']],
                    [
                        'label' => 'Taksiran Harga',
                        'icon' => 'money',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Kategori', 'icon' => 'list', 'url' => ['/kategori-barang/index'],],
                            ['label' => 'Merk Barang', 'icon' => 'list', 'url' => ['/merk-barang/index'],],
                            ['label' => 'Type Barang', 'icon' => 'list', 'url' => ['/type-barang/index'],],
                            ['label' => 'Grade Barang', 'icon' => 'list', 'url' => ['/grade-barang/index'],],
                        ],
                    ],
                    [
                        'label' => 'Administrator',
                        'icon' => 'users',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Log Login','icon' => 'map','url' => ['/log-login']],
                            ['label' => 'Koordinator Kunci','icon' => 'lock','url' => ['/koordinator-kunci']],
                            ['label' => 'Akses Khusus','icon' => 'lock','url' => ['/akses-khusus']],
                        ],
                    ],
                    ['label' => 'Pasaran Harga Emas','icon' => 'money','url' => ['/pasaran-harga-emas/']],
                    [
                        'label' => 'Absensi & Penggajian',
                        'icon' => 'calendar',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Honor Pegawai', 'icon' => 'list', 'url' => ['/honor-pegawai/index-pegawai'],],
                            ['label' => 'Check In Out', 'icon' => 'list', 'url' => ['/check-in-out/index'],],
                            ['label' => 'Jadwal Shift', 'icon' => 'list', 'url' => ['/jadwal-shift/index'],],
                            ['label' => 'Hari Kerja', 'icon' => 'list', 'url' => ['/hari-kerja/index'],],
                            ['label' => 'Hari Kerja Pegawai', 'icon' => 'list', 'url' => ['/hari-kerja-pegawai/index-pegawai'],],
                            ['label' => 'Periode Penggajian', 'icon' => 'list', 'url' => ['/periode-penggajian/index'],],
                        ],
                    ],
                    [
                        'label' => 'Register Lembur',
                        'icon' => 'flag',
                        'url' => ['/register-lembur/index']
                    ],
                ];      
            $menuItems = Helper::filter($menuItems);
            echo dmstr\widgets\Menu::widget([
                'options' => ['class' => 'sidebar-menu tree','data-widget'=> 'tree'],
                'items' => $menuItems,
            ]); 
       } else {

            // 'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
            $menuItems= [
                ['label' => 'Menu', 'options' => ['class' => 'header']],
                [
                    'label' => 'Dashboard',
                    'icon' => 'home',
                    'url' => ['/site/index']
                ],
                [
                    'label' => 'Nasabah',
                    'icon' => 'users',
                    'url' => ['/nasabah/']
                ],
                [
                    'label' => 'Tambah Barang',
                    'icon' => 'cart-plus',
                    'url' => ['/barang/create']
                ],
                [
                    'label' => 'Data Barang',
                    'icon' => 'address-book',
                    'url' => '#',
                    'items' => [
                        ['label' =>'Tambah Barang Lama','icon' => 'check','url' => ['/barang/create-langsung']],
                        ['label' =>'Barang Aktif','icon' => 'check','url' => ['/barang/index']],
                        ['label' => 'Barang Lunas', 'icon' => 'window-maximize','url' => ['/barang/lunas']],
                        ['label' => 'Barang Lunas Hari Ini', 'icon' => 'window-maximize','url' => ['/log-status/lunas-now']],
                        ['label' => 'Barang Jatuh Tempo', 'icon' => 'window-close','url' => ['/barang/jatuh-tempo']],
                        ['label' => 'Barang Pasif', 'icon' => 'window-minimize','url' => ['/barang/pasif']],
                        ['label' => 'Barang Lelang', 'icon' => 'flag','url' => ['/barang/lelang']],
                        ['label' => 'Barang Belum Terjual', 'icon' => 'flag','url' => ['/barang/belum-terjual']],
                        ['label' => 'Barang Terjual', 'icon' => 'gavel','url' => ['/barang/terjual']],
                    ]
                ],
                ['label' => 'Data Modal','icon' =>'money','url' => ['/log-modal/']],
                ['label' => 'Pengeluaran','icon' =>'upload','url' => ['/log-uang-keluar/']],
                [
                    'label' => 'Report',
                    'icon' => 'bar-chart',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Data Outstanding', 'icon' => 'list', 'url' => ['/outstanding-manager/index']],
                        ['label' => 'Semua Data Barang', 'icon' => 'list', 'url' => ['/log-status/']],
                        ['label' => 'Keuangan', 'icon' => 'list', 'url' => ['/log-finance/filter']],
                        ['label' => 'Rekap Penjualan', 'icon' => 'list', 'url' => ['/log-finance/filter-penjualan']],
                        ['label' => 'Data Penjualan', 'icon' => 'list', 'url' => ['/jual/filter-data-jual']],
                        ['label' => 'Terjual', 'icon' => 'list', 'url' => ['/log-finance/filter-lelang']],
                        ['label' => 'Beban Operasional', 'icon' => 'list', 'url' => ['/beban-operasional/filter-operasional']],
                        ['label' =>'Seluruh Barang','icon' => 'list','url' => ['/barang/all']],
                    ]
                ],
                [
                    'label' => 'Report Baru',
                    'icon' => 'bar-chart',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Laporan Gadai', 'icon' => 'list', 'url' => ['/log-finance/laporan-gadai']],
                        ['label' => 'Laporan Perpanjang', 'icon' => 'list', 'url' => ['/log-finance/laporan-perpanjang']],
                        ['label' => 'Laporan Pelunasan', 'icon' => 'list', 'url' => ['/log-finance/laporan-pelunasan']],
                        ['label' => 'Laporan Angsuran', 'icon' => 'list', 'url' => ['/log-finance/laporan-angsuran']],
                    ]
                ],
                [
                    'label' => 'History',
                    'icon' => 'flag',
                    'url' => '#',
                    'items' => [
                        [
                            'label' => 'History By Gadai',
                            'icon' => 'flag',
                            'url' => ['/barang/history']
                        ],
                        [
                            'label' => 'History By Nasabah',
                            'icon' => 'flag',
                            'url' => ['/barang/history-by-nasabah']
                        ],
                    ]
                ],
                [
                    'label' => 'Beban Operasional',
                    'icon' => 'calculator',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Jenis Biaya', 'icon' => 'calculator', 'url' => ['/jenis-biaya-operasional/index']],
                        ['label' => 'Pengeluaran Operasional', 'icon' => 'calculator', 'url' => ['/beban-operasional/index']],
                    ]
                ],
                ['label' => 'Hapus Barang','icon' =>'trash','url' => ['/barang/hapus']],
                ['label' => 'Taksiran Harga Pinjaman','icon' =>'money','url' => ['/taksiran-harga-pasar/index']],
                [
                    'label' => 'Taksiran Harga',
                    'icon' => 'money',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Kategori', 'icon' => 'list', 'url' => ['/kategori-barang/index'],],
                        ['label' => 'Merk Barang', 'icon' => 'list', 'url' => ['/merk-barang/index'],],
                        ['label' => 'Type Barang', 'icon' => 'list', 'url' => ['/type-barang/index'],],
                        ['label' => 'Grade Barang', 'icon' => 'list', 'url' => ['/grade-barang/index'],],
                    ],
                ],
                [
                    'label' => 'Jurnal',
                    'icon' => 'bookmark',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Akun', 'icon' => 'list', 'url' => ['/akun/index'],],
                        //['label' => 'Jurnal', 'icon' => 'pencil', 'url' => ['/log-jurnal/index'],],
                        ['label' => 'List Laporan', 'icon' => 'list', 'url' => ['/list-laporan/index'],],
                        ['label' => 'Jurnal Per Transaksi', 'icon' => 'pencil', 'url' => ['/log-transaksi/jurnal'],],
                        ['label' => 'Jurnal Penyesuaian', 'icon' => 'pencil', 'url' => ['/log-transaksi/index-penyesuaian'],],
                        //['label' => 'Buku Besar', 'icon' => 'book', 'url' => ['/log-jurnal/filter-buku-besar'],],
                        ['label' => 'Buku Besar', 'icon' => 'book', 'url' => ['/log-jurnal/filter-buku-besar-lama'],],
                        ['label' => 'Neraca Saldo', 'icon' => 'recycle', 'url' => ['/log-jurnal/filter-neraca-saldo'],],
                        ['label' => 'Laba Rugi', 'icon' => 'money', 'url' => ['/log-jurnal/filter-laba-rugi'],],
                        ['label' => 'Posisi Keuangan', 'icon' => 'money', 'url' => ['/log-jurnal/filter-posisi-keuangan'],],
                        ['label' => 'Arus Kas', 'icon' => 'money', 'url' => ['/log-jurnal/filter-arus-kas'],],
                        ['label' => 'Mutasi Kas', 'icon' => 'money', 'url' => ['/log-transaksi/index-mutasi'],],
                    ],
                ],
                ['label' => 'Inventori','icon' => 'box','url' => ['/inventory/']],
                [
                    'label' => 'Notifikasi',
                    'icon' => 'phone',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Device', 'icon' => 'list', 'url' => ['/device/index'],],
                        ['label' => 'Device Status', 'icon' => 'list', 'url' => ['/device/status'],],
                        ['label' => 'Notifikasi', 'icon' => 'list', 'url' => ['/log-notifikasi/index'],],
                    ],
                ],
                ['label' => 'Pasaran Harga Emas','icon' => 'money','url' => ['/pasaran-harga-emas/']],
                [
                    'label' => 'Absensi',
                    'icon' => 'o-clock',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Jadwal Shift', 'icon' => 'list', 'url' => ['/jadwal-shift/index'],],
                        ['label' => 'Hari Kerja', 'icon' => 'list', 'url' => ['/hari-kerja/status'],],
                        ['label' => 'Notifikasi', 'icon' => 'list', 'url' => ['/log-notifikasi/index'],],
                    ],
                ],
                [
                    'label' => 'Register Lembur',
                    'icon' => 'flag',
                    'url' => ['/register-lembur/index']
                ],
            ];      
        $menuItems = Helper::filter($menuItems);
        echo dmstr\widgets\Menu::widget([
            'options' => ['class' => 'sidebar-menu tree','data-widget'=> 'tree'],
            'items' => $menuItems,
        ]); 

    }
    ?>

    </section>

</aside>
