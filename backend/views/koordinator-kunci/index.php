<?php

use common\models\Cabang;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\KoordinatorKunciSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Koordinator';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a('Tambah Koordinator', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute' => 'koordinator_id',
                    'label' => 'Nama Koordinator',
                    'value' => 'koordinator.nama'
                ],
                'username',
                'key_code',
                [
                    'attribute' => 'created_at',
                    'label' => 'Tgl. Input',
                    'format' => 'date'
                ],
                [
                    'attribute' => 'cabang_id',
                    'label' => 'Outlet',
                    'value' => 'koordinator.cabanguser.nama_cabang',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => Cabang::listCabang(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                //'created_by',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update}'
                ],
            ],
        ]); ?>
    </div>

    


</div>
