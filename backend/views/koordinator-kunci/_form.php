<?php

use common\models\Profile;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\KoordinatorKunci */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="koordinator-kunci-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' =>[
            'koordinator_id' => [
                'label' => 'Koordinator',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => Profile::listPegawai(),
                ],
            ],
            'key_code' => []
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
