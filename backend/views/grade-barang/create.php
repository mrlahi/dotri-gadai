<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\GradeBarang */

$this->title = 'Grade Barang';
$this->params['breadcrumbs'][] = ['label' => 'Grade Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
