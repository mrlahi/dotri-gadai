<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\GradeBarang */

$this->title = 'Update Grade Barang : ' . $model->nama_grade;
$this->params['breadcrumbs'][] = ['label' => 'Grade Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
