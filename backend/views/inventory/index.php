<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel common\models\InventorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inventaris';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>

        <?php

            $gridExport = [
                'no_inventaris',
                'nama_barang',
                'merk_barang',
                'type_barang',
                'spesifikasi',
                [
                    'attribute' => 'harga_perolehan',
                    'format' => 'currency'
                ],
                [
                    'attribute' => 'tgl_perolehan',
                    'format' => 'date',
                ],
                [
                    'attribute' => 'persen_penyusutan',
                    'value' => function($model){
                        return $model->persen_penyusutan. " %";
                    }
                ],
                [
                    'attribute' => 'umur_aset',
                    'value' => function($model){
                        return $model->umur_aset. " %";
                    }
                ],
            ];

            //Tombol Exportnya
            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridExport,
                'showColumnSelector' => false,
                'exportConfig' => [
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_CSV => false,
                    ExportMenu::FORMAT_TEXT => false,
                    ExportMenu::FORMAT_PDF => [
                        'pdfConfig' => [
                            'orientation' => 'L',
                        ],
                    ],
                ],
                'filename' => 'Data-Inventaris'
            ]);
        ?>

            <?= Html::a('Tambah Inventory', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'no_inventaris',
                'nama_barang',
                'merk_barang',
                'type_barang',
                'spesifikasi',
                [
                    'attribute' => 'harga_perolehan',
                    'format' => 'currency'
                ],
                [
                    'attribute' => 'tgl_perolehan',
                    'format' => 'date',
                    'filterType' => GridView::FILTER_DATE_RANGE,
                    'filterWidgetOptions' => [       
                        'attribute' => 'tgl_masuk',
                        'pluginOptions' => [
                            'separator' => ' - ',
                            'format' => 'YYYY-MM-DD',
                            'locale' => [
                                'format' => 'YYYY-MM-DD'
                            ],
                        ],
                    ]
                ],
                [
                    'attribute' => 'persen_penyusutan',
                    'value' => function($model){
                        return $model->persen_penyusutan. " %";
                    }
                ],
                [
                    'attribute' => 'umur_aset',
                    'value' => function($model){
                        return $model->umur_aset. " %";
                    }
                ],
                [
                    'attribute' => 'deskripsi'
                ],
                [
                    'label' => 'Nilai Sisa',
                    'value' => function($model){
                        return $model->nilaiSisa();
                    },
                    'format' => 'currency'
                ],
                //'created_at',
                //'created_by',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{log-penyusutan}',
                    'buttons' => [
                        'update' => function($url,$model){
                            return Html::a('<span class="fa fa-edit"></span> Update', [$url], ['class' => 'btn btn-warning btn-sm']);
                        },
                        'log-penyusutan' => function($url,$model){
                            $link = '/log-penyusutan-aset/index?inventaris_id='.$model->id;
                            return Html::a('<span class="fa fa-list"></span> Penyusutan', [$link], ['class' => 'btn btn-info btn-sm']);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>

    


</div>
