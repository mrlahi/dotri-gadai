<?php

use common\models\Akun;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $model common\models\Inventory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inventory-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 3,
        'attributes' =>[
            // 'no_inventaris' => [
            //     'options' => [
            //         'readonly' => 'readonly'
            //     ]
            // ],
            'nama_barang' => [],
            'merk_barang' => [],
            'type_barang' => [],
            'spesifikasi' => [
                'type' => Form::INPUT_TEXTAREA,

            ],
            'harga_perolehan' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required'
                    ]
                ]
            ],
            'tgl_perolehan' => [
                'options' => ['type' => 'date']
            ],
            'persen_penyusutan' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'suffix' => ' %',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required'
                    ]
                ]
            ],
            'umur_aset' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'suffix' => ' tahun',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required'
                    ]
                ]
            ],
            'akun_kode' => [
                'label' => 'Asal Pengadaan',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => Akun::listAkunAktiva(),
                    'options' => ['placeholder' => 'Asal Akun Pengadaan']
                ],
            ],
            'jenis_inventaris' => [
                'label' => 'Jenis Inventaris',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::classname(),
                'options' =>[
                    'data' => ['KANTOR' => 'KANTOR', 'KENDARAAN' => 'KENDARAAN'],
                    'options' => ['placeholder' => 'Asal Akun Pengadaan']
                ],
            ],
            'deskripsi' => [
                'type' => Form::INPUT_TEXTAREA,

            ],
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
