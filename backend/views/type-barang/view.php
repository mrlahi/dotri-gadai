<?php

use kartik\editable\Editable;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\TypeBarang */

$this->title = $model->merkBarang->nama_merek." - ".$model->nama_type;
$this->params['breadcrumbs'][] = ['label' => 'Type Barangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="box box-primary">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                //'id',
                [
                    'label' => 'Merk',
                    'value' => function($model){
                        return $model->merkBarang->nama_merek;
                    }
                ],
                'nama_type',
                'deskripsi:ntext',
                // 'created_at',
                // 'created_by',
                // 'merk_barang_id',
            ],
        ]) ?>
    </div>
</div>

<div class="box box-info">
    <div class="box-header">
        <h3>Taksiran Harga Terbaru</h3>
    </div>
    <div class="box-body">
        <?php
            $targetUrl = '/taksiran-harga/create?id='.$model->id;
            echo Html::button('<span class="fa fa-plus"></span> Taksiran Baru',['value' => Url::to([$targetUrl]),'class'=>'btn btn-danger btn-pop']);
        ?>

    <?= GridView::widget([
        'dataProvider' => $dataProviderGrade,
        'filterModel' => $searchModelGrade,
        'pjax' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'grade_id',
                'label' => 'Grade', 
                'value' => 'grade.nama_grade'
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'lengkap',
                'format'=>'integer',
                'editableOptions' =>[
                    'asPopover' => false,
                    'inputType' => Editable::INPUT_TEXT,
                    'size'=>'md',
                    'options' => ['class'=>'form-control', 'rows'=>5, 'type' => 'number']
                ]
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'kotak',
                'format'=>'integer',
                'editableOptions' =>[
                    'asPopover' => false,
                    'inputType' => Editable::INPUT_TEXT,
                    'size'=>'md',
                    'options' => ['class'=>'form-control', 'rows'=>5, 'type' => 'number']
                ]
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'charger',
                'format'=>'integer',
                'editableOptions' =>[
                    'asPopover' => false,
                    'inputType' => Editable::INPUT_TEXT,
                    'size'=>'md',
                    'options' => ['class'=>'form-control', 'rows'=>5, 'type' => 'number']
                ]
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'batangan',
                'format'=>'integer',
                'editableOptions' =>[
                    'asPopover' => false,
                    'inputType' => Editable::INPUT_TEXT,
                    'size'=>'md',
                    'options' => ['class'=>'form-control', 'rows'=>5, 'type' => 'number']
                ]
            ],
            
            //'created_at',
            //'created_by',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    </div>
</div>

<div class="box box-warning">
    <div class="box-header">
        <h3>Taksiran Harga Sebelumnya</h3>
    </div>
    <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProviderGrade2,
        'filterModel' => $searchModelGrade2,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'tgl_aktif',
                'value' => 'taksiran.tgl_aktif',
                'format' => 'date'
            ],
            [
                'attribute' => 'grade_id',
                'label' => 'Grade', 
                'value' => 'grade.nama_grade'
            ],
            [
                'attribute' => 'lengkap',
                'format' => 'integer'
            ],
            [
                'attribute' => 'kotak',
                'format' => 'integer'
            ],
            [
                'attribute' => 'charger',
                'format' => 'integer'
            ],
            [
                'attribute' => 'batangan',
                'format' => 'integer'
            ],
            //'created_at',
            //'created_by',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>


<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-lg',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");