<?php

use common\models\KategoriBarang;
use common\models\MerkBarang;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\TypeBarang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="type-barang-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

    <?= Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 3,
            'attributes' =>[
                'merk_barang_id' => [
                    'label' => 'Merk Barang',
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::classname(),
                    'options' =>[
                        'data' => MerkBarang::listMerk(),
                        'options' => [
                            'placeholder' => 'Pilih Merk',
                        ]
                    ]
                ],
                'kategori_barang_id' => [
                    'label' => 'Kategori Barang',
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::classname(),
                    'options' =>[
                        'data' => KategoriBarang::listKategori(),
                        'options' => [
                            'placeholder' => 'Pilih Kategori',
                        ]
                    ]
                ],
                'nama_type' => []
            ]
        ]);
    ?>

    <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
