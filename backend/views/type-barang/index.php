<?php

use common\models\KategoriBarang;
use common\models\MerkBarang;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TypeBarangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Type Barang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a('Tambah Type Barang', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute' => 'kategori_barang_id',
                    'label' => 'Kategori',
                    'value' => 'kategoriBarang.nama_kategori',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => KategoriBarang::listKategori(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                [
                    'attribute' => 'merk_barang_id', 
                    'label' => 'Merk Barang', 
                    'value' => 'merkBarang.nama_merek',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => MerkBarang::listMerk(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                'nama_type',
                'deskripsi:ntext',
                // 'created_at',
                // 'created_by',
                //'merk_barang_id',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {view}',
                    'buttons' => [
                        'update' => function($url,$model){
                            return Html::a('<span class="fa fa-edit"></span> Update', [$url], ['class' => 'btn btn-warning btn-sm']);
                        },
                        'view' => function($url,$model){
                            return Html::a('<span class="fa fa-eye"></span> Taksiran', [$url], ['class' => 'btn btn-primary btn-sm']);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
