<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AksesKhusus */

$this->title = 'Update Akses Khusus: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Akses Khususes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="akses-khusus-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
