<?php

use common\models\GradeBarang;
use common\models\MerkBarang;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TaksiranGradeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Taksiran Grade';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                //'taksiran_id',
                //'grade_id',
                [
                    'attribute' => 'merk',
                    'value' => 'taksiran.merk.nama_merek',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => MerkBarang::listMerk(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                [
                    'attribute' => 'type',
                    'value' => 'taksiran.typeBarang.nama_type'
                ],
                [
                    'attribute' => 'grade_id',
                    'value' => 'grade.nama_grade',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => GradeBarang::listGrade(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]
                ],
                [
                    'attribute' => 'lengkap', 
                    'format' => 'currency'
                ],
                [
                    'attribute' => 'batangan', 
                    'format' => 'currency'
                ],
                [
                    'attribute' => 'kotak', 
                    'format' => 'currency'
                ],
                [
                    'attribute' => 'charger', 
                    'format' => 'currency'
                ],
                //'created_at',
                //'created_by',

                //['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
