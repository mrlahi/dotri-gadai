<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TaksiranGrade */

$this->title = 'Create Taksiran Grade';
$this->params['breadcrumbs'][] = ['label' => 'Taksiran Grades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taksiran-grade-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
