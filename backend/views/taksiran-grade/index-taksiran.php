<?php
use common\models\GradeBarang;
use common\models\MerkBarang;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\TaksiranGradeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Taksiran Grade';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?php Pjax::begin([
            'id' => 'filterID',
            'enablePushState' => false,
            'enableReplaceState' => false,
        ]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax' =>true,
            'id' => 'filterID',
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'id',
                //'taksiran_id',
                //'grade_id',
                [
                    'attribute' => 'merk',
                    'value' => 'taksiran.merk.nama_merek',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => MerkBarang::listMerk(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'dropdownParent' => '#modalPopUp'
                        ]
                    ]
                ],
                [
                    'attribute' => 'type',
                    'value' => 'taksiran.typeBarang.nama_type'
                ],
                [
                    'attribute' => 'desk_barang',
                    'label' => 'deskripsi',
                    'value' => 'taksiran.typeBarang.deskripsi'
                ],
                [
                    'attribute' => 'grade_id',
                    'label' => 'Grade',
                    'value' => 'grade.nama_grade',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => GradeBarang::listGrade(),
                    'filterWidgetOptions' => [
                        'options' => ['prompt' => ''],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'dropdownParent' => '#modalPopUp'
                        ]
                    ]
                ],
                [
                    'label' => 'Desk',
                    'value' => 'grade.deskripsi'
                ],
                [
                    'attribute' => 'lengkap',
                    'format' => 'raw', 
                    'filter' => false,
                    'value' => function($model){
                        $harga = "Rp. ".Yii::$app->formatter->asInteger($model->lengkap);
                        return Html::button('<span class="fa fa-money"></span> '.$harga,['class'=>'btn btn-primary btn-xs', 'onclick' => 'getHarga('.$model->lengkap.')', 'data-dismiss' => 'modal']);
                    }
                ],
                [
                    'attribute' => 'batangan', 
                    'format' => 'raw', 
                    'filter' => false,
                    'value' => function($model){
                        $harga = "Rp. ".Yii::$app->formatter->asInteger($model->batangan);
                        return Html::button('<span class="fa fa-money"></span> '.$harga,['class'=>'btn btn-primary btn-xs', 'onclick' => 'getHarga('.$model->batangan.')', 'data-dismiss' => 'modal']);
                    }
                ],
                [
                    'attribute' => 'kotak', 
                    'format' => 'raw', 
                    'filter' => false,
                    'value' => function($model){
                        $harga = "Rp. ".Yii::$app->formatter->asInteger($model->kotak);
                        return Html::button('<span class="fa fa-money"></span> '.$harga,['class'=>'btn btn-primary btn-xs', 'onclick' => 'getHarga('.$model->kotak.')', 'data-dismiss' => 'modal']);
                    }
                ],
                [
                    'attribute' => 'charger', 
                    'format' => 'raw', 
                    'filter' => false,
                    'value' => function($model){
                        $harga = "Rp. ".Yii::$app->formatter->asInteger($model->charger);
                        return Html::button('<span class="fa fa-money"></span> '.$harga,['class'=>'btn btn-primary btn-xs', 'onclick' => 'getHarga('.$model->charger.')', 'data-dismiss' => 'modal']);
                    }
                ],
                //'created_at',
                //'created_by',
                //['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
            <?php Pjax::end(); ?>
    </div>
</div>