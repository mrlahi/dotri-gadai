<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TaksiranGradeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="taksiran-grade-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'taksiran_id') ?>

    <?= $form->field($model, 'grade_id') ?>

    <?= $form->field($model, 'lengkap') ?>

    <?= $form->field($model, 'batangan') ?>

    <?php // echo $form->field($model, 'kotak') ?>

    <?php // echo $form->field($model, 'charger') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
