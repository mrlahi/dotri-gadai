<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TaksiranGrade */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="taksiran-grade-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'taksiran_id')->textInput() ?>

    <?= $form->field($model, 'grade_id')->textInput() ?>

    <?= $form->field($model, 'lengkap')->textInput() ?>

    <?= $form->field($model, 'batangan')->textInput() ?>

    <?= $form->field($model, 'kotak')->textInput() ?>

    <?= $form->field($model, 'charger')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
