<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\dialog\Dialog;
use yii\helpers\Url;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $searchModel common\models\LogModalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Modal Hari Ini';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a('Tambah Modal', ['#'], ['class' => 'btn btn-success btn-pop', 'value' => 'create']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'jumlah_masuk',
                'label' => 'Jumlah Modal',
                'value' => 'jumlah',
                'filter' => false
            ],
            [
                'attribute'=>'created_at',
                'format'=>'date',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'autoClear' => true,
                        'format' => 'yyyy-mm-d'
                    ]
                ]
            ],
            [
                'attribute'=>'id_cabang',
                'label' => 'Cabang',
                'value' => 'cabang.nama_cabang',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $modelProfile->listCabang(),
                'filterWidgetOptions' => [
                    'options' => ['prompt' => 'Filter Cabang'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]
            ],
            'keterangan:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => function($model){
                        if(Yii::$app->user->can('admin')){
                            return TRUE;
                        }

                        if($model->created_at == date('Y-m-d')){
                            return TRUE;
                        }
                    },
                    'delete' => function($model){
                        if(Yii::$app->user->can('admin')){
                            return TRUE;
                        }

                        if($model->created_at == date('Y-m-d')){
                            return TRUE;
                        }
                    },
                ],
                'template' =>'{update} {delete}',
                'buttons' => [
                    'update' => function ($url,$model){
                        //$targetUrl = 'delete?id='.$model->id_log_modal;
                        return Html::a('<span class="fa fa-edit"></span> Update',[$url],['class'=>'btn btn-info']);
                    },
                    'delete' => function ($url,$model){
                        $targetUrl = 'delete?id='.$model->id_log_modal;
                        return Html::a('<span class="fa fa-trash"></span> Hapus',[$targetUrl],['class'=>'btn btn-warning','data' => ['confirm' => 'Yakin ingin menghapus modal..??','method' => 'post',]]);
                    }
                ]
            ],
        ],
    ]); ?>
    </div>
</div>

<?php
Modal::begin([
	'id'=>'modalPopUp',
	'size'=>'modal-lg',
    'class'=>'modal fade',
    'closeButton' => [
        'id'=>'close-button',
        'class'=>'close',
        'data-dismiss' =>'modal',
 ],
]);
echo "<div id='modalContent'></div>";
Modal::end();
   
$this->registerJs("
    $(function(){
  $('.btn-pop').on('click', function() {
      $('#modalPopUp').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));

    });
    });
");