<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\number\NumberControl;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\LogModal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-modal-form">

<?php $form = ActiveForm::begin([
    'type'=>ActiveForm::TYPE_VERTICAL,
    'options' => [
        'data-pjax' => 'true',
        'id' => 'modal'
        ]
    ]);
    ?>
<?php
Pjax::begin(['id' => 'modal']);
?>
<?= Form::widget([
    'model' => $model,
    'form' => $form,
    'columns' => 2,
    'attributes' =>[
        'jumlah_masuk' => [
            'label' => 'Jumlah Modal Masuk',
            'type' => Form::INPUT_WIDGET,
            'widgetClass' => NumberControl::classname(),
            'options' => [
                'maskedInputOptions' =>[
                    'prefix' => 'Rp.  ',
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'allowMinus' => false,
                    'rightAlign' => false
                ],
                'options' => [
                    'placeholder' => '',
                    'required'=>'required'
                ]
            ],
        ],
        'id_cabang' =>[
            'label' => 'Cabang',
            'type' => Form::INPUT_WIDGET,
            'widgetClass' => Select2::classname(),
            'options' =>[
                'data' => $modelProfile->listCabang()
            ]
        ]
        ]
    ]);
?>
    <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>
    
    <?php Pjax::end(); ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
