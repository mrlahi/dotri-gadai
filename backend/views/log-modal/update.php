<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LogModal */

$this->title = 'Ubah Modal: ';
$this->params['breadcrumbs'][] = ['label' => 'Log Modals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_log_modal, 'url' => ['view', 'id' => $model->id_log_modal]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-warning">
    <div class="box-header">
    <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
    <?= $this->render('_form-update', [
        'model' => $model,
        'modelProfile' => $modelProfile
    ]) ?>
    </div>
</div>
