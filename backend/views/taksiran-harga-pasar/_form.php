<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $model common\models\TaksiranHargaPasar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="taksiran-harga-pasar-form">

<?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); ?>

<?= Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 3,
        'attributes' =>[
            'nama_barang' => [],
            'merk_barang' => [],
            'type_barang' => [],
            'spesifikasi' => [],
            'kondisi' => [],
            'kelengkapan' =>[],
            'taksiran_harga' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::classname(),
                'options' => [
                    'maskedInputOptions' =>[
                        'prefix' => 'Rp.  ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false
                    ],
                    'options' => [
                        'placeholder' => '',
                        'required'=>'required'
                    ]
                ]
            ],
        ]
    ]);
?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
