<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TaksiranHargaPasarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Taksiran Harga Pinjaman';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a('Tambah Taksiran', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id_taksiran',
                'nama_barang',
                'merk_barang',
                'type_barang',
                'spesifikasi',
                'kondisi',
                'kelengkapan',
                [
                    'attribute' => 'taksiran_harga',
                    'format' => 'currency',
                    'filter' => false
                ],
                [
                    'attribute' => 'created_at',
                    'label' => 'Tgl. Input',
                    'format' => 'date',
                    'filter' => false
                ],
                //'created_at',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                ],
            ],
        ]); ?>
    </div>
</div>
