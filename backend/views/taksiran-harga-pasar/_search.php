<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TaksiranHargaPasarSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="taksiran-harga-pasar-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_taksiran') ?>

    <?= $form->field($model, 'merk_barang') ?>

    <?= $form->field($model, 'type_barang') ?>

    <?= $form->field($model, 'taksiran_harga') ?>

    <?= $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
