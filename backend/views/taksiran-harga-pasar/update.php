<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TaksiranHargaPasar */

$this->title = 'Update Taksiran Harga Pasar: ' . $model->merk_barang ." - ".$model->type_barang;
$this->params['breadcrumbs'][] = ['label' => 'Taksiran Harga Pasar', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_taksiran, 'url' => ['view', 'id' => $model->id_taksiran]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-warning">  
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>