<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TaksiranHargaPasarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Taksiran Harga Pinjaman';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
    <div class="box-header">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php Pjax::begin([
            'id' => 'filterID',
            'enablePushState' => false,
            'enableReplaceState' => false,
        ]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax' =>true,
            'id' => 'filterID',
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id_taksiran',
                'nama_barang',
                'merk_barang',
                'type_barang',
                'spesifikasi',
                'kondisi',
                'kelengkapan',
                [
                    'attribute' => 'taksiran_harga',
                    'format' => 'currency',
                    'filter' => false
                ],
                [
                    'attribute' => 'created_at',
                    'label' => 'Tgl. Input',
                    'format' => 'date',
                    'filter' => false
                ],
                //'created_at',
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
