<?php
namespace backend\models;

use common\models\Barang;
use common\models\LogStatus;
use common\models\Cicil;
use common\models\Bayar;

class BarangExt extends Barang
{

    public function getOutstanding(){
        $id_barang = $this->id_barang;
        $dataLogStatus = LogStatus::find()->where(['id_barang' => $id_barang])->orderBy(['id_status' => SORT_DESC])->one();

        if($dataLogStatus->status == "Aktif" || $dataLogStatus->status == "JatuhTempo" || $dataLogStatus->status == "Lelang"){
            $outstanding = "OUTSTANDING";
        }
        else{
            $outstanding = "";
        }

        return $outstanding;
    }    

    public function getSisapinjam(){
        $id_barang = $this->id_barang;
        $dataLogStatus = LogStatus::find()->where(['id_barang' => $id_barang])->orderBy(['id_status' => SORT_DESC])->one();

        if($dataLogStatus->status == "Aktif" || $dataLogStatus->status == "JatuhTempo" || $dataLogStatus->status == "Lelang"){
            $sisabayar = $this->nilai_pinjam;
        }
        else{
            $sisabayar = 0;
        }

        return $sisabayar;
    }    

    public function getTgllunas(){
        $id_barang = $this->id_barang;
        $dataLogStatus = LogStatus::find()->where(['id_barang' => $id_barang])->orderBy(['id_status' => SORT_DESC])->one();

        if($dataLogStatus->status == "Lunas"){
            $tgl_lunas = $dataLogStatus->created_at;
        }
        else{
            $tgl_lunas = "";
        }
        return $tgl_lunas;
    }
}

