<?php
namespace backend\models;

use common\models\Profile;

class NasabahExt extends Profile
{
    public function getIsBlacklist(){
        if($this->is_blacklist){
            return "BLACKLIST";
        }
        else{
            return "WHITELIST";
        }
    }
}

