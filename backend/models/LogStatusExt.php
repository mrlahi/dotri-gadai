<?php
namespace backend\models;

use common\models\LogStatus;

class LogStatusExt extends LogStatus
{

    public function getSisapinjam(){
        $id_barang = $this->myBarang()->id_barang;
        $dataLogStatus = LogStatus::find()->where(['id_barang' => $id_barang])->orderBy(['id_status' => SORT_DESC])->one();

        if($dataLogStatus->status == "Aktif" || $dataLogStatus->status == "JatuhTempo" || $dataLogStatus->status == "Lelang"){
            $sisabayar = $this->myBarang()->nilai_pinjam;
        }
        else{
            $sisabayar = 0;
        }

        return $sisabayar;
    }
    
    public function getTgllunas(){
        $id_barang = $this->myBarang()->id_barang;
        $dataLogStatus = LogStatus::find()->where(['id_barang' => $id_barang])->orderBy(['id_status' => SORT_DESC])->one();

        if($dataLogStatus->status == "Lunas"){
            $tgl_lunas = $dataLogStatus->created_at;
        }
        else{
            $tgl_lunas = "";
        }
        return $tgl_lunas;
    }
    
}

