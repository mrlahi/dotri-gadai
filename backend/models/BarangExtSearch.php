<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\BarangExt;

/**
 * BarangExtSearch represents the model behind the search form of `backend\models\BarangExt`.
 */
class BarangExtSearch extends BarangExt
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_barang', 'id_nasabah', 'id_jenis', 'harga_pasar', 'harga_taksir', 'nilai_pinjam', 'bunga', 'biaya_admin', 'biaya_simpan', 'jangka', 'created_by'], 'integer'],
            [['no_kontrak', 'nama_barang', 'merk', 'tipe', 'serial', 'tgl_masuk', 'jatuh_tempo', 'spek', 'kelengkapan', 'kondisi', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BarangExt::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_barang' => $this->id_barang,
            'id_nasabah' => $this->id_nasabah,
            'id_jenis' => $this->id_jenis,
            'tgl_masuk' => $this->tgl_masuk,
            'jatuh_tempo' => $this->jatuh_tempo,
            'harga_pasar' => $this->harga_pasar,
            'harga_taksir' => $this->harga_taksir,
            'nilai_pinjam' => $this->nilai_pinjam,
            'bunga' => $this->bunga,
            'biaya_admin' => $this->biaya_admin,
            'biaya_simpan' => $this->biaya_simpan,
            'jangka' => $this->jangka,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'no_kontrak', $this->no_kontrak])
            ->andFilterWhere(['like', 'nama_barang', $this->nama_barang])
            ->andFilterWhere(['like', 'merk', $this->merk])
            ->andFilterWhere(['like', 'tipe', $this->tipe])
            ->andFilterWhere(['like', 'serial', $this->serial])
            ->andFilterWhere(['like', 'spek', $this->spek])
            ->andFilterWhere(['like', 'kelengkapan', $this->kelengkapan])
            ->andFilterWhere(['like', 'kondisi', $this->kondisi])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
