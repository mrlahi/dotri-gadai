<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\NasabahExt;

/**
 * NasabahExtSearch represents the model behind the search form of `backend\models\NasabahExt`.
 */
class NasabahExtSearch extends NasabahExt
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'created_by', 'id_cabang', 'pekerjaan_id'], 'integer'],
            [['no_ktp', 'nama', 'alamat', 'email', 'no_hp', 'no_hp2', 'created', 'akses', 'is_blacklist', 'alasan_blacklist'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NasabahExt::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_user' => $this->id_user,
            'created_by' => $this->created_by,
            'id_cabang' => $this->id_cabang,
            'is_blacklist' => $this->is_blacklist,
            'pekerjaan_id' => $this->pekerjaan_id
        ]);

        $query->andFilterWhere(['like', 'no_ktp', $this->no_ktp])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'no_hp', $this->no_hp])
            ->andFilterWhere(['like', 'no_hp2', $this->no_hp2])
            ->andFilterWhere(['like', 'akses', $this->akses])
            ->andFilterWhere(['like', 'alasan_blacklist', $this->alasan_blacklist]);

            if(isset($this->created) && $this->created !=''){ //you dont need the if function if yourse sure you have a not null date
                $date_explode=explode(" - ",$this->created);
                $date1 = trim($date_explode[0]);
                $date2 = trim($date_explode[1]);
                $query->andFilterWhere(['between','created',$date1,$date2]);
            }

        return $dataProvider;
    }
}
