<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\LogStatusExt;

/**
 * LogStatusExtSearch represents the model behind the search form of `backend\models\LogStatusExt`.
 */
class LogStatusExtSearch extends LogStatusExt
{
    /**
     * {@inheritdoc}
     */

    public $status_barang;
    public $nomor_kontrak; 

    public function rules()
    {
        return [
            [['id_status', 'id_barang', 'created_by'], 'integer'],
            [['status', 'created_at', 'status_barang', 'nomor_kontrak'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogStatusExt::find();
        $query->joinWith(['barang']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['status_barang'] = [
            'asc' => ['barang.status' => SORT_ASC],
            'desc' => ['barang.status' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['nomor_kontrak'] = [
            'asc' => ['barang.no_kontrak' => SORT_ASC],
            'desc' => ['barang.no_kontrak' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_status' => $this->id_status,
            'id_barang' => $this->id_barang,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'status' => $this->status,
            'barang.status' => $this->status_barang,
            'barang.no_kontrak' => $this->nomor_kontrak
        ]);

        return $dataProvider;
    }
}
