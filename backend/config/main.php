<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
            'timeout' => 3600,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            //'admin/*',
            'site/login',
            'barang/cek-jatuh-tempo',//supaya bisa diaksesn curl cronjob tanpa login
            'barang/cek-pasif',//supaya bisa diaksesn curl cronjob tanpa login
            'barang/cek-lelang',//supaya bisa diaksesn curl cronjob tanpa login
            'kas-umum/generate-kas',//supaya bisa diaksesn curl cronjob tanpa login
            'log-modal/generate-modal',//
            'site/captcha',
            'site/request-password-reset',
            'site/logout',
            'site/reset-password',
            'debug/default/index',
            'debug/default/view',
            'debug/default/toolbar',
            'webhook-wa/index',
            'login-token/confirm-login',
            'api-absensi/*',
            'site/*',
            'api/*'
        ]
    ],
    'params' => $params,
];
