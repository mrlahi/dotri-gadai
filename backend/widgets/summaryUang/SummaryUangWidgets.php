<?php
namespace backend\widgets\summaryuang;

use yii\base\Widget;
use common\models\Barang;
use common\models\Cabang;

class SummaryUangWidgets extends Widget
{

    public $id_cabang;
    public function init(){
        parent::init();
    }
    
    public function run() {
        
        parent::run();

        $modelBarang = new Barang();
        $modelCabang = new Cabang();
        $dataCabang = $modelCabang->findOne($this->id_cabang);
        
        return $this->render('summaryuang', [
            'id_cabang' => $this->id_cabang,
            'modelBarang' => $modelBarang,
            'dataCabang' => $dataCabang
        ]);
        
        
    }
}

