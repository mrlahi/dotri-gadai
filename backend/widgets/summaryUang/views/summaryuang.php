<div class="col-lg-2 col-md-2">
    <!-- small box -->
    <div class="small-box bg-green">
    <div class="inner">
        <?php $uang_aktif = $modelBarang->outstandingUang($id_cabang,'Aktif'); ?>
        <h4><?= "Rp.".number_format($uang_aktif,0,",","."); ?></h4>

        <p>Barang Aktif</p>
    </div>
    <div class="icon">
        <i class="fa fa-money"></i>
    </div>
    <a href="#" class="small-box-footer"><?= $dataCabang->nama_cabang ?></a>
    </div>
</div>
<!-- ./col -->
<div class="col-lg-2 col-md-2">
    <!-- small box -->
    <div class="small-box bg-yellow">
    <div class="inner">
        <?php $uang_jatuh_tempo = $uang_jatuh_tempo = $modelBarang->outstandingUang($id_cabang,'JatuhTempo'); ?>
        <h4><?= "Rp. ".number_format($uang_jatuh_tempo,0,",",".") ?></h4>

        <p>Jatuh Tempo</p>
    </div>
    <div class="icon">
        <i class="fa fa-money"></i>
    </div>
    <a href="#" class="small-box-footer"><?= $dataCabang->nama_cabang ?></a>
    </div>
</div>
<!-- ./col -->
<div class="col-lg-2 col-md-2">
    <!-- small box -->
    <div class="small-box bg-info">
    <div class="inner">
        <?php $uang_pasif = $modelBarang->outstandingUang($id_cabang,'Pasif'); ?>
        <h4><?= "Rp. ".number_format($uang_pasif,0,",","."); ?></h4>

        <p>Pasif</p>
    </div>
    <div class="icon">
        <i class="fa fa-money"></i>
    </div>
    <a href="#" class="small-box-footer"><?= $dataCabang->nama_cabang ?></a>
    </div>
</div>
<!-- ./col -->
<div class="col-lg-2 col-md-2">
    <!-- small box -->
    <div class="small-box bg-red">
    <div class="inner">
        <?php $uang_lelang = $modelBarang->outstandingUang($id_cabang,'Lelang'); ?>
        <h4><?= "Rp. ".number_format($uang_lelang,0,",","."); ?></h4>

        <p>Lelang</p>
    </div>
    <div class="icon">
        <i class="fa fa-money"></i>
    </div>
    <a href="#" class="small-box-footer"><?= $dataCabang->nama_cabang ?></a>
    </div>
</div>
<!-- ./col -->
<div class="col-lg-2 col-md-2">
    <!-- small box -->
    <div class="small-box bg-orange">
    <div class="inner">
        <?php $uang_belum_terjual = $modelBarang->outstandingUang($id_cabang,'Belum Terjual'); ?>
        <h4><?= "Rp. ".number_format($uang_belum_terjual,0,",","."); ?></h4>

        <p>Belum Terjual</p>
    </div>
    <div class="icon">
        <i class="fa fa-money"></i>
    </div>
    <a href="#" class="small-box-footer"><?= $dataCabang->nama_cabang ?></a>
    </div>
</div>
<!-- ./col -->
<div class="col-lg-2 col-md-2">
    <!-- small box -->
    <div class="small-box bg-aqua">
    <div class="inner">
        <?php $total = $uang_aktif+$uang_jatuh_tempo+$uang_pasif+$uang_lelang+$uang_belum_terjual; ?>
        <h4>Rp. <?= number_format($total,0,",","."); ?></h4>

        <p>Outstanding Uang</p>
    </div>
    <div class="icon">
        <i class="fa fa-money"></i>
    </div>
    <a href="#" class="small-box-footer"><?= $dataCabang->nama_cabang ?></a>
    </div>
</div>

<?php return $total; ?>