<?php
namespace backend\widgets\summarybarang;

use yii\base\Widget;
use common\models\Barang;
use common\models\Cabang;

class SummaryBarangWidgets extends Widget
{

    public function init(){
        parent::init();
    }
    
    public function run() {
        
        parent::run();

        $modelBarang = new Barang();
        $modelCabang = new Cabang();
        $dataCabang = $modelCabang->find()->where(['type_cabang' => 'CABANG BIASA'])->all();
        
        return $this->render('summarybarang', [
            'modelBarang' => $modelBarang,
            'dataCabang' => $dataCabang,
        ]);
        
        
    }
}

