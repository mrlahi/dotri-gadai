<div class="row">
    <div class="col-lg-4">
        <div class="box box-success">
        <div class="box-header">
            <h3>Gudang : <?= $modelBarang->jumBarang('JatuhTempo')+$modelBarang->jumBarang('Aktif')+$modelBarang->jumBarang('Lelang')+$modelBarang->jumBarang('Pasif')+$modelBarang->jumBarang('Belum Terjual'); ?></h3>
        </div>
        <div class="box-body">
            <table width="100%">
            <?php
                foreach ($dataCabang as $dataCabs){
                    $id_cabang = $dataCabs->id_cabang;
                    $nama_cabang = $dataCabs->nama_cabang;
            ?>
            <tr>
                <td><h4><?= $nama_cabang; ?></h4></td>
                <td>:</td>
                <td><h4><?= $modelBarang->jumBarangPercabang('Aktif',$id_cabang) + $modelBarang->jumBarangPercabang('JatuhTempo',$id_cabang) + $modelBarang->jumBarangPercabang('Lelang',$id_cabang) + $modelBarang->jumBarangPercabang('Pasif',$id_cabang)+ $modelBarang->jumBarangPercabang('Belum Terjual',$id_cabang); ?></h4></td>
            </tr>
            <?php } ?>
            </table>
        </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="box box-success">
        <div class="box-header">
            <h3>Aktif : <?= $modelBarang->jumBarang('Aktif'); ?></h3>
        </div>
        <div class="box-body">
            <table width="100%">
            <?php
                foreach ($dataCabang as $dataCabs){
                    $id_cabang = $dataCabs->id_cabang;
                    $nama_cabang = $dataCabs->nama_cabang;
            ?>
            <tr>
                <td><h4><?= $nama_cabang ?></h4></td>
                <td>:</td>
                <td><h4><?= $modelBarang->jumBarangPercabang('Aktif',$id_cabang); ?></h4></td>
            </tr>
            <?php } ?>
            </table>
        </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="box box-success">
        <div class="box-header">
            <h3>Jth. Tempo : <?= $modelBarang->jumBarang('JatuhTempo'); ?></h3>
        </div>
        <div class="box-body">
            <table width="100%">
            <?php
                foreach ($dataCabang as $dataCabs){
                    $id_cabang = $dataCabs->id_cabang;
                    $nama_cabang = $dataCabs->nama_cabang;
            ?>
            <tr>
                <td><h4><?= $nama_cabang; ?></h4></td>
                <td>:</td>
                <td><h4><?= $modelBarang->jumBarangPercabang('JatuhTempo',$id_cabang); ?></h4></td>
            </tr>
            <?php } ?>
            </table>
        </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="box box-success">
        <div class="box-header">
            <h3>Pasif : <?= $modelBarang->jumBarang('Pasif'); ?></h3>
        </div>
        <div class="box-body">
            <table width="100%">
            <?php
                foreach ($dataCabang as $dataCabs){
                    $id_cabang = $dataCabs->id_cabang;
                    $nama_cabang = $dataCabs->nama_cabang;
            ?>
            <tr>
                <td><h4><?= $nama_cabang; ?></h4></td>
                <td>:</td>
                <td><h4><?= $modelBarang->jumBarangPercabang('Pasif',$id_cabang); ?></h4></td>
            </tr>
            <?php } ?>
            </table>
        </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="box box-success">
        <div class="box-header">
            <h3>Lunas : <?= $modelBarang->jumBarang('Lunas'); ?></h3>
        </div>
        <div class="box-body">
            <table width="100%">
            <?php
                foreach ($dataCabang as $dataCabs){
                    $id_cabang = $dataCabs->id_cabang;
                    $nama_cabang = $dataCabs->nama_cabang;
            ?>
            <tr>
                <td><h4><?= $nama_cabang; ?></h4></td>
                <td>:</td>
                <td><h4><?= $modelBarang->jumBarangPercabang('Lunas',$id_cabang); ?></h4></td>
            </tr>
            <?php } ?>
            </table>
        </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="box box-warning">
        <div class="box-header">
            <h3>Lelang : <?= $modelBarang->jumBarang('Lelang'); ?></h3>
        </div>
        <div class="box-body">
            <table width="100%">
            <?php
                foreach ($dataCabang as $dataCabs){
                    $id_cabang = $dataCabs->id_cabang;
                    $nama_cabang = $dataCabs->nama_cabang;
            ?>
            <tr>
                <td><h4><?= $nama_cabang; ?></h4></td>
                <td>:</td>
                <td><h4><?= $modelBarang->jumBarangPercabang('Lelang',$id_cabang); ?></h4></td>
            </tr>
            <?php } ?>
            </table>
        </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="box box-danger">
        <div class="box-header">
            <h3>Belum Terjual : <?= $modelBarang->jumBarang('Belum Terjual'); ?></h3>
        </div>
        <div class="box-body">
            <table width="100%">
            <?php
                foreach ($dataCabang as $dataCabs){
                    $id_cabang = $dataCabs->id_cabang;
                    $nama_cabang = $dataCabs->nama_cabang;
            ?>
            <tr>
                <td><h4><?= $nama_cabang; ?></h4></td>
                <td>:</td>
                <td><h4><?= $modelBarang->jumBarangPercabang('Belum Terjual',$id_cabang); ?></h4></td>
            </tr>
            <?php } ?>
            </table>
        </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="box box-danger">
        <div class="box-header">
            <h3>Terjual : <?= $modelBarang->jumBarang('Terjual'); ?></h3>
        </div>
        <div class="box-body">
            <table width="100%">
            <?php
                foreach ($dataCabang as $dataCabs){
                    $id_cabang = $dataCabs->id_cabang;
                    $nama_cabang = $dataCabs->nama_cabang;
            ?>
            <tr>
                <td><h4><?= $nama_cabang; ?></h4></td>
                <td>:</td>
                <td><h4><?= $modelBarang->jumBarangPercabang('Terjual',$id_cabang); ?></h4></td>
            </tr>
            <?php } ?>
            </table>
        </div>
        </div>
    </div>
</div>