<?php
namespace backend\widgets\reportpinjaman;

use yii\base\Widget;
use common\models\Barang;

class ReportPinjamanWidgets extends Widget
{

    public $date1;
    public $date2;
    public $id_cabang;
    public function init(){
        parent::init();
    }
    
    public function run() {
        
        parent::run();

        $dataGadai = Barang::find()->where(['between', 'tgl_masuk', $this->date1, $this->date2])->andWhere(['id_cabang' => $this->id_cabang])->all();
        
        return $this->render('reportgadai', [
            'dataGadai' => $dataGadai,
            'date1' => $this->date1,
            'date2' => $this->date2,
            'id_cabang' => $this->id_cabang
        ]);
        
        
    }
}

