<div class="box box-success">
    <div class="box-header">
        <h3>Laporan Gadai :</h3>
    </div>
    <div class="box-body">
        <table class="table table-responsive table-hover" border="1">
            <thead>
                <tr>
                    <td rowspan="2">No</td>
                    <td rowspan="2">No. Faktur</td>
                    <td rowspan="2">Nama Nasabah</td>
                    <td rowspan="2">Type</td>
                    <td colspan="7" align="center">Gadai</td>
                </tr>
                <tr>
                    <td>Pinjaman</td>
                    <td>Admin</td>
                    <td>Bunga (1.2%)</td>
                    <td>Bunga (2.4%)</td>
                    <td>Bunga (5%)</td>
                    <td>Bunga (10%)</td>
                    <td>Selisih</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no =1;
                    $t_pinjaman = 0;
                    $t_admin = 0;
                    $t_bunga_pinjam = 0;
                    $t_selisih = 0;
                    $t_bunga_pinjam1_2 = 0;
                    $t_bunga_pinjam2_4 = 0;
                    $t_bunga_pinjam5 = 0;
                    $t_bunga_pinjam10 = 0;
                    foreach ($dataGadai as $gadai){
                        $bunga1_2 = $gadai->logBunga(1.2);
                        $bunga2_4 = $gadai->logBunga(2.4);
                        $bunga5 = $gadai->logBunga(5);
                        $bunga10 = $gadai->logBunga(10);
                        $selisih = 0;
                           
                        $t_bunga_pinjam1_2 += $bunga1_2;
                        $t_bunga_pinjam2_4 += $bunga2_4;
                        $t_bunga_pinjam5 += $bunga5;
                        $t_bunga_pinjam10 += $bunga10;
                        $t_selisih += $selisih;
                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $gadai->no_kontrak; ?></td>
                    <td>
                        <?= $gadai->nasabah->nama; ?>
                        <?= "(".$gadai->nasabah->no_ktp.")"; ?>
                    </td>
                    <td><?= $gadai->merk." - ".$gadai->tipe; ?></td>
                    <td><?= number_format($gadai->nilai_pinjam,'0',',','.'); ?></td>
                    <td><?= number_format($gadai->biaya_admin,'0',',','.'); ?></td>
                    <td><?= $bunga1_2; ?></td>
                    <td><?= $bunga2_4; ?></td>
                    <td><?= $bunga5; ?></td>
                    <td><?= $bunga10; ?></td>
                    <td><?= number_format($selisih,'0',',','.'); ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="4" align="center"><b>Total :</b></td>
                    <td><b><?= "Rp. ". number_format($t_pinjaman,'0',',','.'); ?></b></td>
                    <td><b><?= "Rp. ". number_format($t_admin,'0',',','.'); ?></b></td>
                    <td><b><?= "Rp. ". number_format($t_bunga_pinjam1_2,'0',',','.'); ?></b></td>
                    <td><b><?= "Rp. ". number_format($t_bunga_pinjam2_4,'0',',','.'); ?></b></td>
                    <td><b><?= "Rp. ". number_format($t_bunga_pinjam5,'0',',','.'); ?></b></td>
                    <td><b><?= "Rp. ". number_format($t_bunga_pinjam10,'0',',','.'); ?></b></td>
                    <td><b><?= "Rp. ". number_format($t_selisih,'0',',','.'); ?></b></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>