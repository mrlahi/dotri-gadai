<?php

$date = date('Y-m-d');

?>
<div class="row">
  <?php
    foreach ($dataCabang as $dataCabs){
      $id_cabang = $dataCabs->id_cabang;
      $nama_cabang = $dataCabs->nama_cabang;
  ?>
  <div class="col-lg-4">
    <div class="box box-primary">
      <div class="box-header">
        <h3><?= $nama_cabang; ?></h3>
      </div>
      <div class="box-body">
        <table width="100%">
          <tr>
            <td><h4>Gadai</h4></td>
            <td>:</td>
            <td><h4><?= $modelLogFinance->countFinance($date,'Pinjaman',$id_cabang) ?></h4></td>
          </tr>
          <tr>
            <td><h4>Tebus</h4></td>
            <td>:</td>
            <td><h4><?= $modelLogFinance->countFinance($date,'Pelunasan',$id_cabang) ?></h4></td>
          </tr>
          <tr>
            <td><h4>Cashback 1.2 %</h4></td>
            <td>:</td>
            <td><h4><?= $modelLogFinance->countFinance($date,'KembaliBunga1.2Persen',$id_cabang) ?></h4></td>
          </tr>
          <tr>
            <td><h4>Cashback 5 %</h4></td>
            <td>:</td>
            <td><h4><?= $modelLogFinance->countFinance($date,'KembaliBunga5Persen',$id_cabang) ?></h4></td>
          </tr>
          <tr>
            <td><h4>Perpanjang</h4></td>
            <td>:</td>
            <td><h4><?= $modelLogFinance->countFinance($date,'BungaPerpanjang',$id_cabang) ?></h4></td>
          </tr>
          <tr>
            <td><h4>Angsuran</h4></td>
            <td>:</td>
            <td><h4><?= $modelLogFinance->countFinance($date,'Angsuran',$id_cabang) ?></h4></td>
          </tr>
          <tr>
            <td><h4>Via Transfer</h4></td>
            <td>:</td>
            <td><h4><?= $modelLogFinance->countTransfer($date,$id_cabang) ?></h4></td>
          </tr>
        </table>
      </div>
    </div>
  </div>

  <?php } ?>
</div>