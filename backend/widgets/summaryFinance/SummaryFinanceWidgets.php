<?php
namespace backend\widgets\summaryfinance;

use yii\base\Widget;
use common\models\LogFinance;
use common\models\Cabang;

class SummaryFinanceWidgets extends Widget
{

    public function init(){
        parent::init();
    }
    
    public function run() {
        
        parent::run();

        $modelLogFinance = new LogFinance();
        $modelCabang = new Cabang();
        $dataCabang = $modelCabang->find()->where(['type_cabang' => 'CABANG BIASA'])->all();
        
        return $this->render('summaryfinance', [
            'modelLogFinance' => $modelLogFinance,
            'dataCabang' => $dataCabang,
        ]);
        
        
    }
}

