<?php
namespace backend\widgets\summarycabang;

use yii\base\Widget;
use common\models\LogFinance;
use common\models\Barang;

class SummaryCabangWidgets extends Widget
{

    public $id_cabang;
    public function init(){
        parent::init();
    }
    
    public function run() {
        
        parent::run();

        $modelLogFinance = new LogFinance();
        $modelBarang = new Barang();
        
        return $this->render('summarycabang', [
            'modelLogFinance' => $modelLogFinance,
            'modelBarang' => $modelBarang,
            'id_cabang' => $this->id_cabang
        ]);
        
        
    }
}

