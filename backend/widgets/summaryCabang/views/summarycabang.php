<?php
$date = date('Y-m-d');
?>

<div class="col-lg-12 row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?= $modelLogFinance->countFinance($date,'Pinjaman',$id_cabang) ?></h3>

                  <p>
                    Gadai 1.2% : <?= $modelLogFinance->countFinance2($date,'Pinjaman',$id_cabang,1.2)?>  |
                    Gadai 2.4% : <?= $modelLogFinance->countFinance2($date,'Pinjaman',$id_cabang,2.4)?> <br> 
                    Gadai 5% : <?= $modelLogFinance->countFinance2($date,'Pinjaman',$id_cabang,5) ?> |
                    Gadai 10% : <?= $modelLogFinance->countFinance2($date,'Pinjaman',$id_cabang,10) ?>
                  </p>
                </div>
                <div class="icon">
                  <i class="fa fa-check"></i>
                </div>
              <a href="#" class="small-box-footer">Gadai Hari Ini <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?= $modelLogFinance->countFinance($date,'Pelunasan',$id_cabang) ?></h3>

              <p>Tebus</p>
            </div>
            <div class="icon">
              <i class="fa fa-shopping-bag"></i>
            </div>
            <a href="#" class="small-box-footer">Tebus Hari Ini <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <?php
                $cb1_2 = $modelLogFinance->countFinance($date,'KembaliBunga1.2Persen',$id_cabang);
                $cb5 = $modelLogFinance->countFinance($date,'KembaliBunga5Persen',$id_cabang);
                $cb = $cb1_2 + $cb5;
              ?>
              <h3><?= $cb; ?></h3>

              <p>
                Cashback 5 % : <?= $cb5 ?> <br>
                Cashback 1.2 % : <?= $cb1_2 ?> 
              </p>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
            <a href="#" class="small-box-footer">Cashback <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
</div>
<div class="col-lg-12 row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?= $modelLogFinance->countFinance($date,'BungaPerpanjang',$id_cabang) ?></h3>

              <p>
                Perpanjang 1.2% : <?= $modelLogFinance->countFinance2($date,'BungaPerpanjang',$id_cabang,1.2) ?> | Perpanjang 2.4% : <?= $modelLogFinance->countFinance2($date,'BungaPerpanjang',$id_cabang,2.4) ?>
                <br>
                Perpanjang 5% : <?= $modelLogFinance->countFinance2($date,'BungaPerpanjang',$id_cabang,5) ?> | Perpanjang 10% : <?= $modelLogFinance->countFinance2($date,'BungaPerpanjang',$id_cabang,10) ?>
              </p>
            </div>
            <div class="icon">
              <i class="fa fa-flag"></i>
            </div>
            <a href="#" class="small-box-footer">Perpanjang Hari ini <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?= $modelLogFinance->countFinance($date,'Angsuran',$id_cabang) ?></h3>

              <p>Angsuran</p>
            </div>
            <div class="icon">
              <i class="fa fa-gavel"></i>
            </div>
            <a href="#" class="small-box-footer">Angsuran Hari Ini <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?= $modelLogFinance->countTransfer($date,$id_cabang) ?></h3>

              <p>Via Transfer</p>
            </div>
            <div class="icon">
              <i class="fa fa-gavel"></i>
            </div>
            <a href="#" class="small-box-footer">Transfer Hari Ini <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
</div>