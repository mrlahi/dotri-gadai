<?php

namespace backend\controllers;

use common\models\Device;
use common\models\LogNotifikasi;
use common\models\Barang;
class WebhookWaController extends \yii\web\Controller
{

    public function beforeAction($action) 
     { 
         $this->enableCsrfValidation = false; 
         return parent::beforeAction($action); 
     }

    public function actionIndex()
    {

        header("Content-Type: text/plain");
       
        $content = json_decode(file_get_contents('php://input'), true);
        if(isset($content['message'])){
            $message = $content['message'];
            $tMessage = explode(" ",$message);
            $phone = $content['phone'];
            $device_id = $content['deviceId'];

            $dataDevice = Device::findOne($device_id);
            $token = $dataDevice->token;
            $no_device = $dataDevice->no_device;
            
            if($tMessage[0] == 'STATUS') {

                $sbg = $tMessage[1];
                $dataBarang = Barang::find()->where(['no_kontrak' => $sbg])->one();

                if($dataBarang){
                    $curl = curl_init();
                    //$token = "tF9h1Mc5ybbvjn6KZYhtJziXHrTdT6Sma2Nij2I5baYdGBXrLFzU1FX0A2rEZsI1";
                    $payload = [
                        "data" => [
                            [
                                'phone' => $phone,
                                'message'=> [
                                    'title' => [
                                        'type' => 'text',
                                        'content' => 'STATUS SBG',
                                    ],
                                    'buttons' => [
                                        'call' => [
                                            'display' => 'Info Lebih Lanjut',
                                            'phone' => $no_device,
                                        ],
                                    ],
                                    'content' => 'NO. SBG : '.$dataBarang->no_kontrak."\n"."STATUS BARANG : ".$dataBarang->status."\n"."TGL. JATUH TEMPO : ".$dataBarang->jatuh_tempo."\n".$dataBarang->nama_barang." ".$dataBarang->merk." ".$dataBarang->tipe,
                                    'footer' => 'PT. DOTRI GADAI INDONESIA',
                                ],
                            ]
                        ]
                    ];
                    curl_setopt($curl, CURLOPT_HTTPHEADER,
                        array(
                            "Authorization: $token",
                            "Content-Type: application/json"
                        )
                    );
                    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($payload) );
                    curl_setopt($curl, CURLOPT_URL,  "https://deu.wablas.com/api/v2/send-template");
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

                    $result = curl_exec($curl);
                    curl_close($curl);
                    //echo "<pre>";
                    //print_r($result);
                }
            } else {
                echo null;
        }
        }
    }

    public function actionTrack(){
        /**
        * all data POST sent from  https://deu.wablas.com
        * you must create URL what can receive POST data
        * we will sent data like this:
        * id = message ID - string
        * phone = whatsapp number of customer
        * status = status of message - string
        * note = information - string
        * deviceId = device ID - string
        */

        $content = json_decode(file_get_contents('php://input'), true);

        $id = $content['id'];
        $status = $content['status'];
        $phone = $content['phone'];
        $note = $content['note'];
        $sender = $content['sender'];
        $deviceId = $content['deviceId'];
    }

}
