<?php

namespace backend\controllers;

use Yii;
use common\models\LogModal;
use common\models\LogModalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Profile;
use common\models\LogFinance;
use common\models\KasUmum;

/**
 * LogModalController implements the CRUD actions for LogModal model.
 */
class LogModalController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LogModal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogModalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $modelProfile = new Profile();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelProfile' => $modelProfile
        ]);
    }

    /**
     * Displays a single LogModal model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LogModal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LogModal();
        $modelProfile = new Profile();
        $modelLogFinance = new LogFinance();

        $model->created_at = date('Y-m-d');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            //Pencairan pinjaman masuk ke table log finance
            $modelLogFinance->jumlah_uang = $model->jumlah_masuk;
            $modelLogFinance->status = "Modal";
            $modelLogFinance->created_at = date('Y-m-d');
            $modelLogFinance->created_by = Yii::$app->user->identity->id;
            $modelLogFinance->id_cabang = $model->id_cabang;
            $modelLogFinance->id_barang = $model->id_log_modal;
            $modelLogFinance->type_pembayaran = "Cash";
            $modelLogFinance->save(false);

            return $this->redirect(['index']);
        }

        return $this->renderAjax('create', [
            'model' => $model,
            'modelProfile' => $modelProfile
        ]);
    }

    /**
     * Updates an existing LogModal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelProfile = new Profile();
        $mLogFinance = new LogFinance();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $mLogFinance->updateAll(['jumlah_uang' => $model->jumlah_masuk, 'created_at' => $model->created_at],['status' => 'Modal', 'id_barang' => $id]);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'modelProfile' => $modelProfile
        ]);
    }

    /**
     * Deletes an existing LogModal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $modelLogFinance = new LogFinance();
        $modelLogFinance->deleteAll(['id_barang' => $id]);
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    public function actionGenerateModal(){
        $model = new LogModal();
        $modelKasUmum = new KasUmum();

        $date = date('Y-m-d');
        $semalam = date( 'Y-m-d', strtotime( $date . ' -1 day' ) );

        $dataKasUmum = $modelKasUmum->find()->where(['created_at' => $semalam])->all();

        foreach ($dataKasUmum as $dataKasUmums){
            $model = new LogModal();
            $modelLogFinance = new LogFinance();
            $kas = $dataKasUmums->jumlah_uang_masuk - $dataKasUmums->jumlah_uang_keluar;

            $model->jumlah_masuk = $kas;
            $model->id_cabang = $dataKasUmums->id_cabang;
            $model->created_at = $dataKasUmums->created_at = date('Y-m-d');
            $model->keterangan = "Kas Dari tanggal ".$semalam;
            $model->save(false);

            echo $kas."-";


            //Penambahan Modal masuk ke table log finance
            $modelLogFinance->jumlah_uang = $kas;
            $modelLogFinance->status = "Modal";
            $modelLogFinance->created_at = date('Y-m-d');
            $modelLogFinance->created_by = '1';
            $modelLogFinance->id_cabang = $dataKasUmums->id_cabang;
            $modelLogFinance->id_barang = $model->id_log_modal;
            $modelLogFinance->type_pembayaran = "Cash";
            $modelLogFinance->save(false);
            
            echo $kas."<br>";
        }
    }

    public function actionGenerateModalDate($date){
        $model = new LogModal();
        $modelKasUmum = new KasUmum();

        $semalam = date('Y-m-d', strtotime( $date . ' -1 day' ) );

        $dataKasUmum = $modelKasUmum->find()->where(['created_at' => $semalam])->all();

        foreach ($dataKasUmum as $dataKasUmums){
            $model = new LogModal();
            $modelLogFinance = new LogFinance();
            $kas = $dataKasUmums->jumlah_uang_masuk - $dataKasUmums->jumlah_uang_keluar;
            $model->jumlah_masuk = $kas;
            $model->id_cabang = $dataKasUmums->id_cabang;
            $model->created_at = $dataKasUmums->created_at = $date;
            $model->keterangan = "Kas Dari tanggal ".$semalam;
            $model->save(false);

            echo $model->id_log_modal."<br>";

            //Penambahan Modal masuk ke table log finance
            $modelLogFinance->jumlah_uang = $kas;
            $modelLogFinance->status = "Modal";
            $modelLogFinance->created_at = $date;
            $modelLogFinance->created_by = '1';
            $modelLogFinance->id_cabang = $dataKasUmums->id_cabang;
            $modelLogFinance->id_barang = $model->id_log_modal;
            $modelLogFinance->type_pembayaran = "Cash";
            $modelLogFinance->save(false);
            
        }
    }

    /**
     * Finds the LogModal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogModal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogModal::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
