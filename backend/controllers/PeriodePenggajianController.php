<?php

namespace backend\controllers;

use common\models\HonorPegawai;
use Yii;
use common\models\PeriodePenggajian;
use common\models\PeriodePenggajianHonor;
use common\models\PeriodePenggajianSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PeriodePenggajianController implements the CRUD actions for PeriodePenggajian model.
 */
class PeriodePenggajianController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PeriodePenggajian models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PeriodePenggajianSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PeriodePenggajian model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PeriodePenggajian model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PeriodePenggajian();

        $model->created_at = date('Y-m-d');
        $model->created_by = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionGenerateRekap($id){
        PeriodePenggajianHonor::deleteAll(['periode_id' => $id]);
        $dataHonor = HonorPegawai::find()->where(['is_active' => 1])->all();
        foreach($dataHonor as $honor){
            $mPPH = new PeriodePenggajianHonor();
            $mPPH->pegawai_id = $honor->pegawai_id;
            $mPPH->honor_id = $honor->id;
            $mPPH->periode_id = $id;
            $mPPH->created_at = date('Y-m-d');
            $mPPH->created_by = Yii::$app->user->id;
            $mPPH->save();
        }

        return $this->redirect(['index']);

    }

    /**
     * Updates an existing PeriodePenggajian model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PeriodePenggajian model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PeriodePenggajian model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PeriodePenggajian the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PeriodePenggajian::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
