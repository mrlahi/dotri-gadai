<?php

namespace backend\controllers;

use common\models\Barang;
use common\models\Jual;
use common\models\LogJurnal;
use Yii;
use common\models\LogJurnalPenjualan;
use common\models\LogJurnalPenjualanSearch;
use common\models\LogTransaksi;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * LogJurnalPenjualanController implements the CRUD actions for LogJurnalPenjualan model.
 */
class LogJurnalPenjualanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'jurnal' => ['POST']
                ],
            ],
        ];
    }

    /**
     * Lists all LogJurnalPenjualan models.
     * @return mixed
     */
    public function actionIndex($barang_id)
    {
        $searchModel = new LogJurnalPenjualanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['barang_id' => $barang_id]);

        $dataJual = Jual::find()->where(['id_barang' => $barang_id])->one();

        if(Yii::$app->request->post('hasEditable')){
            $logId = Yii::$app->request->post('editableKey');
            $log = LogJurnalPenjualan::findOne($logId);

            $post = [];
            $out = Json::encode(['output' => '', 'message' => '']);

            $posted = current($_POST['LogJurnalPenjualan']);
            $post['LogJurnalPenjualan'] = $posted;

            if($log->load($post)){
                $log->save();
                $output = '';
                $out = Json::encode(['output' => $output, 'message' => '']);
            }
            return $out;
            //return;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataJual' => $dataJual
        ]);
    }

    /**
     * Displays a single LogJurnalPenjualan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LogJurnalPenjualan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($barang_id)
    {
        $model = new LogJurnalPenjualan();

        $model->barang_id = $barang_id;
        $model->created_at = date('Y-m-d');
        $model->created_by = Yii::$app->user->id;
        $model->deskripsi = "-";
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'barang_id' => $barang_id]);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LogJurnalPenjualan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionJurnal($barang_id){
        $dataLog = LogJurnalPenjualan::find()->where(['barang_id' => $barang_id])->orderBy(['posisi_keuangan' => SORT_ASC])->all();
        
        $no = 1;
        foreach($dataLog as $log){
            if($no == 1){
                $mTransaksi = new LogTransaksi();
                $mTransaksi->inTransaksi("PENJUALAN","PENJUALAN BARANG", $log->created_at);
                $transaksi_id = $mTransaksi->id_log_transaksi;
            }
            $mJurnal = new LogJurnal();

            if($log->posisi_keuangan == "DEBET"){
                if($log->jumlah_uang > 0){
                    $mJurnal->inLogJurnalJual($transaksi_id,$log->akun_id,$log->jumlah_uang,0,"BARANG",$barang_id,$log->created_at ,$log->deskripsi);
                }
                else{
                    LogJurnalPenjualan::deleteAll(['id' => $log->id]);
                }
            }
            else{
                if($log->jumlah_uang > 0){
                    $mJurnal->inLogJurnalJual($transaksi_id,$log->akun_id,0,$log->jumlah_uang,"BARANG",$barang_id,$log->created_at, $log->deskripsi);
                }
                else{
                    LogJurnalPenjualan::deleteAll(['id' => $log->id]);
                }
            }
            $no++;
        }

        Jual::updateAll(['is_jurnal' => 1], ['id_barang' => $barang_id]);

        return $this->redirect(['/barang/terjual']);

    }

    /**
     * Deletes an existing LogJurnalPenjualan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LogJurnalPenjualan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogJurnalPenjualan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogJurnalPenjualan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
