<?php

namespace backend\controllers;

use common\models\TaksiranGrade;
use common\models\TaksiranGradeSearch;
use common\models\TaksiranHarga;
use common\models\TaksiranHargaSearch;
use Yii;
use common\models\TypeBarang;
use common\models\TypeBarangSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * TypeBarangController implements the CRUD actions for TypeBarang model.
 */
class TypeBarangController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TypeBarang models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TypeBarangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TypeBarang model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $taksiranAktif = TaksiranHarga::find()->where(['type_barang_id' => $id, 'is_active' => true])->one();

        if($taksiranAktif){
            $taksiran_id = $taksiranAktif->id;
        }
        else{
            $taksiran_id = 0;
        }

        $searchModelGrade = new TaksiranGradeSearch();
        $dataProviderGrade = $searchModelGrade->search(Yii::$app->request->queryParams);
        $dataProviderGrade->query->andFilterWhere(['taksiran_id' => $taksiran_id]);

        $searchModelGrade2 = new TaksiranGradeSearch();
        $dataProviderGrade2 = $searchModelGrade2->search(Yii::$app->request->queryParams);
        $dataProviderGrade2->query->andFilterWhere(['type_barang_id' => $id]);

        if(Yii::$app->request->post('hasEditable')){
            $gradeId = Yii::$app->request->post('editableKey');
            $grade = TaksiranGrade::findOne($gradeId);

            $post = [];
            $out = Json::encode(['output' => '', 'message' => '']);

            $posted = current($_POST['TaksiranGrade']);
            $post['TaksiranGrade'] = $posted;

            if($grade->load($post)){
                $grade->save();
                $output = '';
                $out = Json::encode(['output' => $output, 'message' => '']);
            }
            return $out;
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModelGrade' => $searchModelGrade,
            'dataProviderGrade' => $dataProviderGrade,
            'searchModelGrade2' => $searchModelGrade2,
            'dataProviderGrade2' => $dataProviderGrade2,
        ]);
    }

    /**
     * Creates a new TypeBarang model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TypeBarang();

        $model->created_by = Yii::$app->user->id;
        $model->created_at = date('Y-m-d');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TypeBarang model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionType() {
        $mType = new TypeBarang();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $merk_id = $parents[0];
                $out = $mType->listType($merk_id); 
                return ['output' => $out, 'selected' => ''];
            }
        }
    }

    /**
     * Deletes an existing TypeBarang model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TypeBarang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TypeBarang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TypeBarang::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
