<?php

namespace backend\controllers;

use common\models\Device;
use common\models\LogNotifikasi;
use common\models\Barang;
use common\models\CheckInOut;
use yii\filters\VerbFilter;

class ApiAbsensiController extends \yii\web\Controller
{

    public function beforeAction($action) 
     { 
         $this->enableCsrfValidation = false; 
         return parent::beforeAction($action); 
     }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex($absen_id,$check_date, $checkclock, $cabang_id)
    {
        // $this->clear($absen_id,$check_date,$checkclock,$cabang_id);
        // $model = new CheckInOut();
        // $model->absen_id = $absen_id;
        // $model->tgl_absen = $check_date;
        // $model->jam_absen = $checkclock;
        // $model->cabang_id = $cabang_id;
        // $model->created_at = date('Y-m-d');
        // $model->save(false);
        
    }

    public function clear($absen_id,$tgl_absen,$jam_absen,$cabang_id){
        //CheckInOut::deleteAll(['absen_id' => $absen_id, 'tgl_absen' => $tgl_absen, 'jam_absen' => $jam_absen, 'cabang_id' => $cabang_id]);
    }

}
