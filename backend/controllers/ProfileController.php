<?php

namespace backend\controllers;

use Yii;
use common\models\Profile;
use common\models\ProfileSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;
use common\models\User;
use common\models\AuthAssignment;

/**
 * ProfileController implements the CRUD actions for Profile model.
 */
class ProfileController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Profile models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Profile model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Profile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($stat)
    {
        $model = new Profile();
        $modelUser = new User();
        $modelAuthAssignment = new AuthAssignment();

        if($stat=="nasabah"){
            $role = "defaultNasabah";
        }
        else{
            $role = "defaultStaff";
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $randomPass = Yii::$app->getSecurity()->generateRandomString(8);
            $modelUser->username = $model->email;
            $modelUser->password_hash = Yii::$app->getSecurity()->generatePasswordHash($randomPass);
            $modelUser->password_reset_token = Yii::$app->getSecurity()->generatePasswordHash($randomPass);
            $modelUser->auth_key = Yii::$app->getSecurity()->generatePasswordHash($randomPass);
            $modelUser->id = $model->id_user;
            $modelUser->email = $model->email;
            $modelUser->save(false);

            $modelAuthAssignment->item_name = $role;
            $modelAuthAssignment->user_id = $model->id_user;
            $modelAuthAssignment->created_at =  new Expression('NOW()');
            $modelAuthAssignment->save(false);

            Yii::$app->mailer->view->params['firstname'] = $model->nama;
            Yii::$app->mailer->view->params['randomPass'] = $randomPass;

            Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'kirimDetailLogin-html', 'text' => 'kirimDetailLogin-text'],
                    ['user' => $modelUser])
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo($modelUser->email)
                ->setSubject('Detail Login Aplikasi ' . Yii::$app->name)
                ->send();

            return $this->redirect(['view', 'id' => $model->id_user]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Profile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_user]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Profile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Profile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Profile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Profile::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
