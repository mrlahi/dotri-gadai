<?php

namespace backend\controllers;

use Yii;
use common\models\TambahanJual;
use common\models\TambahanJualSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TambahanJualController implements the CRUD actions for TambahanJual model.
 */
class TambahanJualController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TambahanJual models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TambahanJualSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TambahanJual model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TambahanJual model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_barang)
    {
        $model = new TambahanJual();

        $model->id_barang = $id_barang;
        $model->created_at = date('Y-m-d');
        $model->created_by = Yii::$app->user->identity->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/barang/jual-pasif/', 'id' => $model->id_barang]);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    public function actionCreateLangsung($id_barang)
    {
        $model = new TambahanJual();

        $model->id_barang = $id_barang;
        $model->created_at = date('Y-m-d');
        $model->created_by = Yii::$app->user->identity->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/barang/terjual']);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    public function actionCreateByFind($id_barang,$sbg)
    {
        $model = new TambahanJual();

        $model->id_barang = $id_barang;
        $model->created_at = date('Y-m-d');
        $model->created_by = Yii::$app->user->identity->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/barang/find', 'sbg' => $sbg]);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TambahanJual model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_tambah_jual]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TambahanJual model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id,$id_barang)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/barang/jual-pasif/', 'id' => $id_barang]);
    }

    public function actionDeleteByFind($id,$sbg)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['/barang/find', 'sbg' => $sbg]);
    }

    /**
     * Finds the TambahanJual model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TambahanJual the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TambahanJual::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
