<?php

namespace backend\controllers;

use Yii;
use common\models\HonorPegawai;
use common\models\HonorPegawaiSearch;
use common\models\Profile;
use common\models\ProfileSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * HonorPegawaiController implements the CRUD actions for HonorPegawai model.
 */
class HonorPegawaiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all HonorPegawai models.
     * @return mixed
     */
    public function actionIndexPegawai()
    {
        $searchModel = new ProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['<>','akses','defaultNasabah']);

        return $this->render('index-pegawai', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDaftarHonor($id)
    {
        $searchModel = new HonorPegawaiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['pegawai_id' => $id]);

        $dataPegawai = Profile::findOne($id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataPegawai' => $dataPegawai
        ]);
    }

    /**
     * Displays a single HonorPegawai model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new HonorPegawai model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($pegawai_id)
    {
        $model = new HonorPegawai();
        $dataPegawai = Profile::findOne($pegawai_id);

        $model->pegawai_id = $pegawai_id; 
        $model->created_at = date('Y-m-d');
        $model->created_by = Yii::$app->user->id;
        $model->is_active = 1;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            HonorPegawai::updateAll(['is_active' => 0], ['pegawai_id' => $pegawai_id]);
            HonorPegawai::updateAll(['is_active' => 1], ['id' => $model->id]);
            return $this->redirect(['daftar-honor', 'id' => $model->pegawai_id]);
        }

        return $this->renderAjax('create', [
            'model' => $model,
            'dataPegawai' => $dataPegawai
        ]);
    }

    /**
     * Updates an existing HonorPegawai model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing HonorPegawai model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HonorPegawai model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HonorPegawai the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HonorPegawai::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
