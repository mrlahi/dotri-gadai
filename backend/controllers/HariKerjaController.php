<?php

namespace backend\controllers;

use Yii;
use common\models\HariKerja;
use common\models\HariKerjaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * HariKerjaController implements the CRUD actions for HariKerja model.
 */
class HariKerjaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all HariKerja models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HariKerjaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(Yii::$app->request->post('hasEditable')){
            $hariId = Yii::$app->request->post('editableKey');
            $hari = HariKerja::findOne($hariId);

            $post = [];
            $out = Json::encode(['output' => '', 'message' => '']);

            $posted = current($_POST['HariKerja']);
            $post['HariKerja'] = $posted;

            if($hari->load($post)){
                $hari->save();
                $output = '';
                $out = Json::encode(['output' => $output, 'message' => '']);
            }
            return $out;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HariKerja model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new HariKerja model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new HariKerja();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionGenerate()
    {
        $model = new HariKerja();

        $model->scenario = "generate-hari";
        if ($model->load(Yii::$app->request->post())) {

            $month = $model->bulan;
            if($month != ""){
                $year = date('Y');
                $days = cal_days_in_month( 0, $month, $year);

                $date = 1;
                while($date <= $days){
                    $mKerja = new HariKerja();

                    $tgl = $year."-".$month."-".$date;

                    $mKerja->tgl_kerja = $tgl;
                    $mKerja->status_hari = 'HARI KERJA';
                    $mKerja->deskripsi = " - ";
                    $mKerja->created_at = date('Y-m-d');
                    $mKerja->created_by = Yii::$app->user->id;
                    $mKerja->save();

                    $date++;
                }
            }
            

            return $this->redirect(['index']);
        }
  
        return $this->renderAjax('_form-generate', [
            'model' => $model,
        ]);
    }

    public function actionBagikan()
    {
        $model = new HariKerja();

        $model->scenario = "generate-hari";
        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['index']);
        }
  
        return $this->renderAjax('_form-generate', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing HariKerja model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing HariKerja model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HariKerja model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HariKerja the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HariKerja::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
