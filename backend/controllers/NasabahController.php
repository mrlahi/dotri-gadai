<?php

namespace backend\controllers;

use Yii;
use backend\models\NasabahExt;
use backend\models\NasabahExtSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;
use common\models\User;
use common\models\AuthAssignment;

/**
 * NasabahController implements the CRUD actions for NasabahExt model.
 */
class NasabahController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NasabahExt models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NasabahExtSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['akses' => 'defaultNasabah']);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NasabahExt model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NasabahExt model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NasabahExt();
        $modelUser = new User();
        $modelAuthAssignment = new AuthAssignment();

            $model->created = date('Y-m-d');
            $model->created_by = Yii::$app->user->identity->id;
            $model->akses = "defaultNasabah";
            $model->id_cabang = Yii::$app->session['id_cabang'];
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            /*
            $randomPass = Yii::$app->getSecurity()->generateRandomString(8);
            $modelUser->username = $model->email;
            $modelUser->password_hash = Yii::$app->getSecurity()->generatePasswordHash($randomPass);
            $modelUser->password_reset_token = Yii::$app->getSecurity()->generatePasswordHash($randomPass);
            $modelUser->auth_key = Yii::$app->getSecurity()->generatePasswordHash($randomPass);
            $modelUser->id = $model->id_user;
            $modelUser->email = $model->email;
            $modelUser->save(false);

            $modelAuthAssignment->item_name = "defaultNasabah";
            $modelAuthAssignment->user_id = $model->id_user;
            $modelAuthAssignment->created_at =  new Expression('NOW()');
            $modelAuthAssignment->save(false);

            Yii::$app->mailer->view->params['firstname'] = $model->nama;
            Yii::$app->mailer->view->params['randomPass'] = $randomPass;

            Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'kirimDetailLogin-html', 'text' => 'kirimDetailLogin-text'],
                    ['user' => $modelUser])
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo($modelUser->email)
                ->setSubject('Detail Login Aplikasi ' . Yii::$app->name)
                ->send();
            */
                
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new NasabahExt model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateLangsung()
    {
        $model = new NasabahExt();
        $modelUser = new User();
        $modelAuthAssignment = new AuthAssignment();

            $model->created_by = Yii::$app->user->identity->id;
            $model->akses = "defaultNasabah";
            $model->id_cabang = Yii::$app->session['id_cabang'];
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            /*
            $randomPass = Yii::$app->getSecurity()->generateRandomString(8);
            $modelUser->username = $model->email;
            $modelUser->password_hash = Yii::$app->getSecurity()->generatePasswordHash($randomPass);
            $modelUser->password_reset_token = Yii::$app->getSecurity()->generatePasswordHash($randomPass);
            $modelUser->auth_key = Yii::$app->getSecurity()->generatePasswordHash($randomPass);
            $modelUser->id = $model->id_user;
            $modelUser->email = $model->email;
            $modelUser->save(false);

            $modelAuthAssignment->item_name = "defaultNasabah";
            $modelAuthAssignment->user_id = $model->id_user;
            $modelAuthAssignment->created_at =  new Expression('NOW()');
            $modelAuthAssignment->save(false);

            Yii::$app->mailer->view->params['firstname'] = $model->nama;
            Yii::$app->mailer->view->params['randomPass'] = $randomPass;

            Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'kirimDetailLogin-html', 'text' => 'kirimDetailLogin-text'],
                    ['user' => $modelUser])
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo($modelUser->email)
                ->setSubject('Detail Login Aplikasi ' . Yii::$app->name)
                ->send();
            */
                
            return $this->redirect(['barang/create', 'id_nasabah' => $model->id_user]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing NasabahExt model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionBlacklist($id){
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->renderAjax('blacklist', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing NasabahExt model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NasabahExt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NasabahExt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NasabahExt::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
