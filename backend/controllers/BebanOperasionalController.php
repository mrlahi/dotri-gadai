<?php

namespace backend\controllers;

use Yii;
use common\models\BebanOperasional;
use common\models\BebanOperasionalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Profile;
use common\models\LogFinance;
use common\models\LogJurnal;

/**
 * BebanOperasionalController implements the CRUD actions for BebanOperasional model.
 */
class BebanOperasionalController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BebanOperasional models.
     * @return mixed
     */
    public function actionIndex()
    {
        $modelProfile = new Profile();
        $searchModel = new BebanOperasionalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(Yii::$app->user->can('defaultStaff') OR Yii::$app->user->can('Penafsir')){
            $dataProvider->query->andFilterWhere(['id_cabang' => Yii::$app->session['id_cabang']]);
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BebanOperasional model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BebanOperasional model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BebanOperasional();
        $modelLogFinance = new LogFinance();
        $mLogJurnal = new LogJurnal();

        $model->created_by = Yii::$app->user->identity->id;
        $model->id_cabang = Yii::$app->session['id_cabang'];
        $model->created_at = date('Y-m-d');
        if ($model->load(Yii::$app->request->post())) {
            $model->type_pencairan = $model->cekType($model->kas_bank_id);
            if($model->save()){
                $mLogJurnal->inOperasional($model->id_beban);
                //Pengeluaran masuk ke table log finance
                $modelLogFinance->jumlah_uang = $model->jumlah_pengeluaran;
                $modelLogFinance->status = "Operasional";
                $modelLogFinance->created_at = date('Y-m-d');
                $modelLogFinance->created_by = Yii::$app->user->identity->id;
                $modelLogFinance->id_cabang = $model->id_cabang;
                $modelLogFinance->id_barang = $model->id_beban;
                $modelLogFinance->type_pembayaran = $model->type_pencairan;
                $modelLogFinance->save(false);
                return $this->redirect(['index']);
            }
            return $this->render('create', [
                'model' => $model,
            ]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionFilterOperasional()
    {
        $model = new BebanOperasional();

        if ($model->load(Yii::$app->request->post())) {

            $date = $model->created_at;
            $id_cabang = $model->id_cabang;
            $date_explode=explode(" - ",$model->created_at);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);

            return $this->render('report-operasional', [
            'model' => $model,
            'date1' => $date1,
            'date2' => $date2,
            'id_cabang' => $id_cabang
        ]);
        }
        else{

        return $this->render('index-filter',[
            'model' => $model,
        ]);
        }
    }

    /**
     * Updates an existing BebanOperasional model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLogFinance = new LogFinance();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelLogFinance->updateAll(['jumlah_uang' => $model->jumlah_pengeluaran],['id_barang' => $id, 'status' => 'Operasional']);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BebanOperasional model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $modelLogFinance = new LogFinance();
        $modelLogFinance->deleteAll(['id_barang' => $id]);
        $this->findModel($id)->delete();
        
        $mLogJurnal = new LogJurnal();
        $mLogJurnal->deleteOperasional($id);

        return $this->redirect(['index']);
    }

    public function actionFixBeban(){
        $model = new BebanOperasional();
        $modelLogFinance = new LogFinance();

        $dataBeban = $model->find()->all();
        foreach ($dataBeban as $dataBebans){
            $id_barang = $dataBebans->id_beban;
            $modelLogFinance = new LogFinance();
            $dataLogFinance = $modelLogFinance->find()->where(['id_barang' => '$id_barang','status' => 'Operasional'])->one();
            if(empty($dataLogFinance->id_log_finance)){
                //Pengeluaran masuk ke table log finance
                $modelLogFinance->jumlah_uang = $dataBebans->jumlah_pengeluaran;
                $modelLogFinance->status = "Operasional";
                $modelLogFinance->created_at = $dataBebans->created_at;
                $modelLogFinance->created_by = $dataBebans->created_by;
                $modelLogFinance->id_cabang = $dataBebans->id_cabang;
                $modelLogFinance->id_barang = $dataBebans->id_beban;
                $modelLogFinance->type_pembayaran = $dataBebans->type_pencairan;
                $modelLogFinance->save(false);
            }
        }
    }

    /**
     * Finds the BebanOperasional model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BebanOperasional the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BebanOperasional::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
