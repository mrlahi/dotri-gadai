<?php

namespace backend\controllers;

use Yii;
use common\models\Barang;
use common\models\BarangSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use common\models\JenisBarang;
use common\models\Profile;
use common\models\LogStatus;
use common\models\Bayar;
use common\models\Jual;
use common\models\Cicil;
use common\models\Perpanjang;
use common\models\LogFinance;
use common\models\Cabang;
use common\models\LogReceipt;
use kartik\mpdf\Pdf;
use common\models\User;
use common\models\AuthAssignment;
use backend\models\NasabahExt;
use yii\db\Expression;
use common\models\TambahanJual;
use common\models\JualSearch;
use common\models\LogJurnal;
use common\models\LogJurnalPenjualan;
use common\models\LogTransaksi;
use yii\debug\models\search\Log;

/**
 * BarangController implements the CRUD actions for Barang model.
 */
class BarangController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Barang models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BarangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['status' => 'aktif']);

        $modelProfile = new Profile();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelProfile' => $modelProfile
        ]);
    }

    /**
     * Lists all Barang models.
     * @return mixed
     */
    public function actionAll()
    {

        $model = new Barang();

        if ($model->load(Yii::$app->request->post())) {

            $model->tgl_masuk;
            $date_explode=explode(" - ",$model->tgl_masuk);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);

            return $this->redirect(['index-all', 'date1' => $date1, 'date2' => $date2]);

        }
        else{

            return $this->render('index-filter',[
                'model' => $model,
            ]);
        }

    }

    public function actionIndexAll($date1,$date2){
        $searchModel = new BarangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['between','tgl_masuk',$date1,$date2]);

        $judulFile = "DataBarang(".$date1."-".$date2.")";

        $modelProfile = new Profile();
        return $this->render('index-all', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelProfile' => $modelProfile,
            'judulFile' => $judulFile
        ]);
    } 

    /**
     * Lists all Barang models.
     * @return mixed
     */
    public function actionHapus()
    {
        $searchModel = new BarangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $modelProfile = new Profile();

        return $this->render('index-hapus', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelProfile' => $modelProfile
        ]);
    }

    /**
     * Lists all Barang models.
     * @return mixed
     */
    public function actionHistory()
    {
        $searchModel = new BarangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $modelProfile = new Profile();

        return $this->render('index_history', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelProfile' => $modelProfile
        ]);
    }

     /**
     * Lists all Barang models.
     * @return mixed
     */
    public function actionHistoryByNasabah()
    {
        $searchModel = new BarangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->groupBy(['id_nasabah']);

        $modelProfile = new Profile();

        return $this->render('index_history', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelProfile' => $modelProfile
        ]);
    }

    /**
     * Lists all Barang models.
     * @return mixed
     */
    public function actionJatuhTempo()
    {
        $searchModel = new BarangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['status' => 'JatuhTempo']);
        $modelProfile = new Profile();

        return $this->render('index_jatuh_tempo', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelProfile' => $modelProfile
        ]);
    }

    /**
     * Lists all Barang models.
     * @return mixed
     */
    public function actionLunas()
    {
        $searchModel = new BarangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['status' => 'Lunas']);
        $modelProfile = new Profile();

        return $this->render('index_lunas', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelProfile' => $modelProfile
        ]);
    }

    /**
     * Lists all Barang models.
     * @return mixed
     */
    public function actionPasif()
    {
        $searchModel = new BarangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['status' => 'Pasif']);
        $modelProfile = new Profile();

        return $this->render('index_pasif', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelProfile' => $modelProfile
        ]);
    }

    /**
     * Lists all Barang models.
     * @return mixed
     */
    public function actionLelang()
    {
        $searchModel = new BarangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['status' => 'Lelang']);
        $modelProfile = new Profile();

        return $this->render('index_lelang', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelProfile' => $modelProfile
        ]);
    }

    public function actionBelumTerjual()
    {
        $searchModel = new BarangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['status' => 'Belum Terjual']);
        $modelProfile = new Profile();

        return $this->render('index_lelang', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelProfile' => $modelProfile
        ]);
    }

    /**
     * Lists all Barang models.
     * @return mixed
     */
    public function actionTerjual()
    {
        // $searchModel = new BarangSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->query->andFilterWhere(['status' => 'Terjual']);

        $searchModel = new JualSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $modelProfile = new Profile();

        return $this->render('index_terjual', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelProfile' => $modelProfile
        ]);
    }


    /**
     * Displays a single Barang model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single Barang model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionInvoice($id)
    {
        $model = $this->findModel($id);
        $jatuh_tempo = $model->jatuh_tempo;

        $lelang = date('Y-m-d',strtotime($model->tglJual(), strtotime($jatuh_tempo)));

        return $this->renderAjax('invoice', [
            'model' => $model,
            'tgl_lelang' => $lelang,
        ]);
    }



    /**
     * Creates a new Barang model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_nasabah = 0){
        $model = new Barang();
        $modelJenisBarang = new JenisBarang();
        $modelNasabah = new Profile();
        $modelLogStatus = new LogStatus();
        $modelCicil = new Cicil();
        $modelLogFinance = new LogFinance();
        $modelLogReceipt = new LogReceipt();
        $mLogJurnal = new LogJurnal();

        $dataNasabah = $modelNasabah->findOne($id_nasabah);

        if ($model->load(Yii::$app->request->post())) {
            $model->id_nasabah = $id_nasabah;
            $model->tgl_masuk = date('Y-m-d');
            $model->no_kontrak = $model->noKontrak();
            //$model->jangka = 30;
            $jangka = "+".$model->jangka." days";
            $jatuh_tempo = date('Y-m-d',strtotime($jangka));
            $model->jatuh_tempo = $jatuh_tempo;
            $model->created_by = Yii::$app->user->identity->id;
            $model->status = "Aktif";
            $model->id_cabang = Yii::$app->session['id_cabang'];
            $model->save(false);
            
            //Masuk ke table log status
            $modelLogStatus->inStatus($model->id_barang,"Aktif");

            ///Masuk ke table bayar 
            $jumlah_cicil = $model->nilai_pinjam * ($model->bunga/100)+$model->biaya_admin+$model->biaya_simpan;
            $modelCicil->inCicil($model->id_barang, $jumlah_cicil);

            //Pencairan pinjaman masuk ke table log finance
            $modelLogFinance = new LogFinance();
            $modelLogFinance->inLogFinance($model->id_barang,$model->nilai_pinjam,'Pinjaman',1);

            //Pemasukan Bunga masuk ke table log finance
            $modelLogFinance = new LogFinance();
            $bunga = $model->nilai_pinjam * ($model->bunga/100);
            $modelLogFinance->inLogFinance($model->id_barang,$bunga,'Bunga',1);

            //Pemasukan Biaya Admin masuk ke table log finance
            $modelLogFinance = new LogFinance();
            $bunga = $model->nilai_pinjam * ($model->bunga/100);
            $modelLogFinance->inLogFinance($model->id_barang,$model->biaya_admin,'Admin',1);

            //Print masuk ke table log receipt
            $modelLogReceipt->inReceipt($model->id_barang,$model->biaya_admin,$model->biaya_simpan,$bunga,'Gadai Baru','Cash',0);

            $mLogJurnal->inBarang($model->id_barang);

            return $this->redirect(['index', 'idnya' => $model->id_barang]);
        }

        return $this->render('create', [
            'model' => $model,
            'modelJenisBarang' => $modelJenisBarang,
            'modelNasabah' => $modelNasabah,
            'id_nasabah' => $id_nasabah,
            'dataNasabah' => $dataNasabah
        ]);
    }

    /**
     * Creates a new Barang model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreateLangsung($id_nasabah = 0)
    // {
    //     $model = new Barang();
    //     $modelJenisBarang = new JenisBarang();
    //     $modelNasabah = new Profile();
    //     $modelLogStatus = new LogStatus();
    //     $modelCicil = new Cicil();
    //     $modelLogFinance = new LogFinance();
    //     $modelLogReceipt = new LogReceipt();

    //     if ($model->load(Yii::$app->request->post())) {
    //         $tgl_masuk = $model->tgl_masuk;
    //         $created = $model->created_by;
    //         $cabang = $model->id_cabang;
    //         $model->no_kontrak = $model->noKontrakLangsung($tgl_masuk,$cabang);
    //         $model->jangka = 30;
    //         $jangka = "+".$model->jangka." days";
    //         $jatuh_tempo = date('d-m-Y', strtotime($tgl_masuk. $jangka));
    //         $model->jatuh_tempo = date_format(date_create($jatuh_tempo),'Y-m-d');
    //         $model->status = "Aktif";
    //         $model->save(false);
            
            
    //         //Masuk ke table log status
    //         $modelLogStatus->id_barang = $model->id_barang;
    //         $modelLogStatus->status = "Aktif";
    //         $modelLogStatus->created_at = $tgl_masuk;
    //         $modelLogStatus->created_by = $created;
    //         $modelLogStatus->save(false);

    //         ///Masuk ke table bayar 
    //         $jumlah_cicil = $model->nilai_pinjam * ($model->bunga/100)+$model->biaya_admin+$model->biaya_simpan;
    //         $modelCicil->id_barang = $model->id_barang;
    //         $modelCicil->jumlah_cicil = $jumlah_cicil;
    //         $modelCicil->created_at = $tgl_masuk;
    //         $modelCicil->created_by = $created;
    //         $modelCicil->save(false);

    //         //Pencairan pinjaman masuk ke table log finance
    //         $modelLogFinance = new LogFinance();
    //         $modelLogFinance->jumlah_uang = $model->nilai_pinjam;
    //         $modelLogFinance->status = 'Pinjaman';
    //         $modelLogFinance->created_at = $tgl_masuk;
    //         $modelLogFinance->created_by = $created;
    //         $modelLogFinance->id_cabang = $cabang;
    //         $modelLogFinance->id_barang = $model->id_barang;
    //         $modelLogFinance->type_pembayaran = 'Cash';
    //         $modelLogFinance->save(false);

    //         //Pemasukan Bunga masuk ke table log finance
    //         $modelLogFinance = new LogFinance();
    //         $bunga = $model->nilai_pinjam * ($model->bunga/100);
    //         $modelLogFinance->jumlah_uang = $bunga;
    //         $modelLogFinance->status = 'Bunga';
    //         $modelLogFinance->created_at = $tgl_masuk;
    //         $modelLogFinance->created_by = $created;
    //         $modelLogFinance->id_cabang = $cabang;
    //         $modelLogFinance->id_barang = $model->id_barang;
    //         $modelLogFinance->type_pembayaran = 'Cash';
    //         $modelLogFinance->save(false);

    //         //Pemasukan Biaya Admin masuk ke table log finance
    //         $modelLogFinance = new LogFinance();
    //         $modelLogFinance->jumlah_uang = $model->biaya_admin;
    //         $modelLogFinance->status = 'Admin';
    //         $modelLogFinance->created_at = $tgl_masuk;
    //         $modelLogFinance->created_by = $created;
    //         $modelLogFinance->id_cabang = $cabang;
    //         $modelLogFinance->id_barang = $model->id_barang;
    //         $modelLogFinance->type_pembayaran = 'Cash';
    //         $modelLogFinance->save(false);
            

    //         //Print masuk ke table log receipt
    //         $modelLogReceipt->id_barang = $model->id_barang;
    //         $modelLogReceipt->biaya_admin = $model->biaya_admin;
    //         $modelLogReceipt->biaya_simpan = $model->biaya_simpan;
    //         $modelLogReceipt->bunga = $bunga;
    //         $modelLogReceipt->denda = 0;
    //         $modelLogReceipt->angsuran = 0;
    //         $modelLogReceipt->type_receipt = 'Gadai Baru';
    //         $modelLogReceipt->created_at = $tgl_masuk;
    //         $modelLogReceipt->created_by = $created;
    //         $modelLogReceipt->save(false);

    //         return $this->redirect(['index', 'idnya' => $model->id_barang]);
            
    //     }

    //     return $this->render('create-langsung', [
    //         'model' => $model,
    //         'modelJenisBarang' => $modelJenisBarang,
    //         'modelNasabah' => $modelNasabah,
    //         'id_nasabah' => $id_nasabah
    //     ]);
    // }

    /**
     * Updates an existing Barang model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_barang]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdateLokasi($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->renderAjax('update-lokasi', [
            'model' => $model,
        ]);
    }

    public function actionPindahBelumTerjual($id){
        Barang::updateAll(['status' => 'Belum Terjual'], ['id_barang' => $id]);
        return $this->redirect(['lelang']);
    }

    public function actionPindahLelang($id){
        Barang::updateAll(['status' => 'Lelang'], ['id_barang' => $id]);
        return $this->redirect(['belum-terjual']);
    }
    

    public function actionUpdateNote($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['history']);
        }

        return $this->renderAjax('update-note', [
            'model' => $model,
        ]);
    }

    public function actionUpdateNoteLelang($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['lelang']);
        }

        return $this->renderAjax('update-note', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Barang model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $modelLogStatus = new LogStatus();
        $modelLogFinance = new LogFinance();
        $modelBayar = new Bayar();
        $modelCicil = new Cicil();
        $modelLogReceipt = new LogReceipt();
        $mLogJurnal = new LogJurnal();
        $mLogTransaksi = new LogTransaksi();

        $this->findModel($id)->delete();
        $modelLogStatus->deleteAll(['id_barang' => $id]);
        $modelLogFinance->deleteAll(['id_barang' => $id]);
        $modelCicil->deleteAll(['id_barang' => $id]);
        $modelLogReceipt->deleteAll(['id_barang' => $id]);
        $modelBayar->deleteAll(['id_barang' => $id]);

        $dataJurnal = $mLogJurnal->find()->where(['jenis_asal_jurnal' => 'BARANG', 'asal_id' => $id])->one();
        $log_transaksi_id = $dataJurnal->log_transaksi_id;
        $mLogTransaksi->deleteAll(['id_log_transaksi' => $log_transaksi_id]);

        return $this->redirect(['hapus']);
    }

    public function actionPerpanjang($id)
    {
        $model = $this->findModel($id);
        $modelBayar = new Bayar();
        $modelPerpanjang = new Perpanjang();
        $modelLogFinance = new LogFinance();
        $modelLogReceipt = new LogReceipt();
        $mLogJurnal = new LogJurnal();
        $modelCicil = new Cicil();

        $modelPerpanjang->id_barang = $id;
        $modelPerpanjang->created_at = date('Y-m-d');
        $modelPerpanjang->created_by = Yii::$app->user->identity->id;
        if($modelPerpanjang->load(Yii::$app->request->post()) && $modelPerpanjang->save()){

            $jatuh_tempo_old = $model->jatuh_tempo;
            $tempo = ' +'.$model->jangka.'  days';
            $jatuh_tempo_new = date('Y-m-d', strtotime($jatuh_tempo_old. $tempo));

             //Input ke table log finance

            $kas_bank_id = $_POST['kas_bank_id'];
            $modelLogFinance = new LogFinance();
            $modelLogFinance->inLogFinance($model->id_barang,$modelPerpanjang->bunga,'BungaPerpanjang',$kas_bank_id);

             //Input ke table log finance denda
            $modelLogFinance = new LogFinance();
            $modelLogFinance->inLogFinance($model->id_barang,$modelPerpanjang->denda,'DendaPerpanjang',$kas_bank_id);

             //Input ke table LogReceipt
            $modelLogReceipt->id_barang = $id;
            $modelLogReceipt->biaya_admin = 0;
            $modelLogReceipt->biaya_simpan = 0;
            $modelLogReceipt->bunga = $modelPerpanjang->bunga;
            $modelLogReceipt->denda = $modelPerpanjang->denda;
            $modelLogReceipt->angsuran = 0;
            $modelLogReceipt->type_receipt = "Perpanjang";
            $modelLogReceipt->created_at = date('Y-m-d H:m:i');
            $modelLogReceipt->created_by = Yii::$app->user->identity->id;
            $modelLogReceipt->type_pembayaran = $modelLogReceipt->typePembayaran($kas_bank_id);
            $modelLogReceipt->save(false);
            
            if($jatuh_tempo_new >= date('Y-m-d')){ //kalau setelah diperpanjang masih rugi
                $model->updateAll(['status' => 'Aktif','jatuh_tempo' => $jatuh_tempo_new],['id_barang' => $id]);
            }
            else{
                $model->updateAll(['jatuh_tempo' => $jatuh_tempo_new],['id_barang' => $id]);
            }

            $mLogJurnal->inPerpanjang($id,$modelPerpanjang->bunga,$modelPerpanjang->denda,$kas_bank_id);

            Yii::$app->getSession()->setFlash('success', [
                'type' => 'info',
                'duration' => 12000,
                'icon' => 'fa fa-like',
                'message' => Yii::t('app',Html::encode('Kontrak telah diperpanjang.')),
                'title' => Yii::t('app', Html::encode('Perpanjangan')),
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
            return $this->redirect(['/barang/','receipt' => 'yes','id_receipt' => $modelLogReceipt->id_log_receipt]);
        }
        else{
            return $this->renderAjax('perpanjang',[
                    'id' => $id,
                    'model' => $model,
                    'modelBayar' => $modelBayar,
                    'modelPerpanjang' => $modelPerpanjang,
                    'modelCicil' => $modelCicil,
                    'modelLogFinance' => $modelLogFinance
                ]);
        }
    }
    

    public function actionBayar($id){

        $model = $this->findModel($id);
        $modelLogStatus = new LogStatus();
        $modelBayar = new Bayar();
        $modelCicil = new Cicil();
        $modelLogFinance = new LogFinance();
        $modelLogReceipt = new LogReceipt();
        $mLogJurnal = new LogJurnal();

        if($modelCicil->load(Yii::$app->request->post())){
            $modelCicil->id_barang = $id;
            $modelCicil->created_at = date('Y-m-d');
            $modelCicil->created_by = Yii::$app->user->identity->id;
            $modelCicil->save(false);

            $kali_bunga = floor($modelBayar->myDenda($id)['telat']/30);//kalau telat sudah lebih dari 2 bulan
                if($kali_bunga == 0){
                    $kali_bunga = 1;
                }

            $bunganya = $modelBayar->myBunga($id)['bunga']*$kali_bunga;

            $sisabayar = $model->nilai_pinjam+$model->biaya_admin+$model->biaya_simpan+$bunganya-$modelBayar->myBunga($id)['diskon_bunga']+$modelBayar->myDenda($id)['denda'];

            if($modelCicil->myCicilan($id) == $sisabayar){
                 //update status tbl barang
                $model->updateAll(['status' => 'Lunas', 'tgl_status' => date('Y-m-d')],['id_barang' => $id]);
                
                //tambah data tbl bayar
                $modelBayar->inBayar($id,$model->id_nasabah);

                //tambah data tbl logstatus
                $modelLogStatus->inStatus($id,"Lunas");

                //Input ke table log finance Pokok
                $kas_bank_id = $_POST['kas_bank_id'];
                $modelLogFinance = new LogFinance();
                $jumlah_uang = $modelCicil->jumlah_cicil-$modelBayar->myDenda($id)['denda'];
                $modelLogFinance->inLogFinance($id,$jumlah_uang,'Pelunasan',$kas_bank_id);

                //Input jika ada kembali 5%
                if($modelBayar->myBunga($id)['diskon_bunga'] > 1){
                    $modelLogFinance = new LogFinance();
                    $jumlah_uang = $modelBayar->myBunga($id)['diskon_bunga'];
                    $status = $modelBayar->myBunga($id)['statusLogFinance'];
                    $modelLogFinance->inLogFinance($id,$jumlah_uang,$status,$kas_bank_id);
                }

                //Input ke table log finance DendaPelunasan
                $modelLogFinance = new LogFinance();
                $modelLogFinance->inLogFinance($id,$modelBayar->myDenda($id)['denda'],'DendaPelunasan',$kas_bank_id);


                //input type_receipt
                $modelLogReceipt->type_receipt = "Pelunasan";

                Yii::$app->getSession()->setFlash('success', [
                    'type' => 'success',
                    'duration' => 12000,
                    'icon' => 'fa fa-like',
                    'message' => Yii::t('app',Html::encode('Kontrak telah dilunasi.')),
                    'title' => Yii::t('app', Html::encode('Bayar Gadai')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);

                $cashback = $modelBayar->myBunga($id)['diskon_bunga'];
                $jumlah_uang2 = $modelCicil->jumlah_cicil-$modelBayar->myDenda($id)['denda'];
                $mLogJurnal->inPelunasan($id,$jumlah_uang2,$modelBayar->myDenda($id)['denda'],$cashback,$kas_bank_id);
            }
            else{
                $cashback = 0;
                if($model->status == "Aktif"){//supaya tidak ada double cicil & pelunasan
                    //input type_receipt
                    $modelLogReceipt->type_receipt = "Cicilan";

                    //Input ke table log finance
                    $kas_bank_id = $_POST['kas_bank_id'];
                    $modelLogFinance->inLogFinance($id,$modelCicil->jumlah_cicil,'Angsuran',$kas_bank_id);
                    $mLogJurnal->inAngsuran($id,$modelCicil->jumlah_cicil,$kas_bank_id);
                    
                }
                Yii::$app->getSession()->setFlash('success', [
                    'type' => 'success',
                    'duration' => 12000,
                    'icon' => 'fa fa-like',
                    'message' => Yii::t('app',Html::encode('Cicilan Sudah Disimpan.')),
                    'title' => Yii::t('app', Html::encode('Bayar Gadai')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }

            //Input ke table LogReceipt
            $modelLogReceipt->id_barang = $id;
            $modelLogReceipt->biaya_admin = 0;
            $modelLogReceipt->biaya_simpan = 0;
            $modelLogReceipt->bunga = 0;
            $modelLogReceipt->denda = $modelBayar->myDenda($id)['denda'];;
            $modelLogReceipt->angsuran = $modelCicil->jumlah_cicil-$modelBayar->myDenda($id)['denda'];
            $modelLogReceipt->created_at = date('Y-m-d H:m:i');
            $modelLogReceipt->created_by = Yii::$app->user->identity->id;
            $modelLogReceipt->cashback = $cashback;
            $modelLogReceipt->type_pembayaran = $modelLogReceipt->typePembayaran($kas_bank_id);
            $modelLogReceipt->save(false);

            return $this->redirect(['/barang/','receipt' => 'yes','id_receipt' => $modelLogReceipt->id_log_receipt]);
        }
        else{
            return $this->renderAjax('bayar',[
                    'id' => $id,
                    'model' => $model,
                    'modelBayar' => $modelBayar,
                    'modelCicil' => $modelCicil,
                    'modelLogFinance' => $modelLogFinance
                ]);
        }
    }

    public function actionLunaskan($id){

        $model = $this->findModel($id);
        $modelLogStatus = new LogStatus();
        $modelBayar = new Bayar();
        $modelCicil = new Cicil();
        $modelLogFinance = new LogFinance();
        $modelLogReceipt = new LogReceipt();
        $mLogJurnal = new LogJurnal();

        if($modelCicil->load(Yii::$app->request->post())){
            $modelCicil->id_barang = $id;
            $modelCicil->created_at = date('Y-m-d');
            $modelCicil->created_by = Yii::$app->user->identity->id;
            $modelCicil->save(false);

            $kali_bunga = floor($modelBayar->myDenda($id)['telat']/30);//kalau telat sudah lebih dari 2 bulan
                if($kali_bunga == 0){
                    $kali_bunga = 1;
                }

            $bunganya = $modelBayar->myBunga($id)['bunga']*$kali_bunga;

            $sisabayar = $model->nilai_pinjam+$model->biaya_admin+$model->biaya_simpan+$bunganya-$modelBayar->myBunga($id)['diskon_bunga']+$modelBayar->myDenda($id)['denda'];

            if($modelCicil->myCicilan($id) == $sisabayar){
                 //update status tbl barang
                $model->updateAll(['status' => 'Lunas', 'tgl_status' => date('Y-m-d')],['id_barang' => $id]);
                
                //tambah data tbl bayar
                $modelBayar->inBayar($id,$model->id_nasabah);

                //tambah data tbl logstatus
                $modelLogStatus->inStatus($id,"Lunas");

                //Input ke table log finance Pokok
                $type_pembayaran = $_POST['kas_bank_id'];
                $modelLogFinance = new LogFinance();
                $jumlah_uang = $modelCicil->jumlah_cicil-$modelBayar->myDenda($id)['denda'];
                $modelLogFinance->inLogFinance($id,$jumlah_uang,'Pelunasan',$type_pembayaran);

                //Input jika ada kembali 5%
                if($modelBayar->myBunga($id)['diskon_bunga'] > 1){
                    $modelLogFinance = new LogFinance();
                    $jumlah_uang = $modelBayar->myBunga($id)['diskon_bunga'];
                    $status = $modelBayar->myBunga($id)['statusLogFinance'];
                    $modelLogFinance->inLogFinance($id,$jumlah_uang,$status,$type_pembayaran);
                }

                //Input ke table log finance DendaPelunasan
                $type_pembayaran = $_POST['kas_bank_id'];
                $modelLogFinance = new LogFinance();
                $modelLogFinance->inLogFinance($id,$modelBayar->myDenda($id)['denda'],'DendaPelunasan',$type_pembayaran);


                //input type_receipt
                $modelLogReceipt->type_receipt = "Pelunasan";

                Yii::$app->getSession()->setFlash('success', [
                    'type' => 'success',
                    'duration' => 12000,
                    'icon' => 'fa fa-like',
                    'message' => Yii::t('app',Html::encode('Kontrak telah dilunasi.')),
                    'title' => Yii::t('app', Html::encode('Bayar Gadai')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);

                $cashback = $modelBayar->myBunga($id)['diskon_bunga'];
                $mLogJurnal->inPelunasan($id,$jumlah_uang,$modelBayar->myDenda($id)['denda'],$cashback,$type_pembayaran);
            }
            else{
                $cashback = 0;
                if($model->status == "Aktif"){//supaya tidak ada double cicil & pelunasan
                    //input type_receipt
                    $modelLogReceipt->type_receipt = "Cicilan";

                    //Input ke table log finance
                    $type_pembayaran = $_POST['kas_bank_id'];
                    $modelLogFinance->inLogFinance($id,$modelCicil->jumlah_cicil,'Angsuran',$type_pembayaran);
                    $mLogJurnal->inAngsuran($id,$modelCicil->jumlah_cicil,$type_pembayaran);
                }
                Yii::$app->getSession()->setFlash('success', [
                    'type' => 'success',
                    'duration' => 12000,
                    'icon' => 'fa fa-like',
                    'message' => Yii::t('app',Html::encode('Cicilan Sudah Disimpan.')),
                    'title' => Yii::t('app', Html::encode('Bayar Gadai')),
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }

            //Input ke table LogReceipt
            $type_pembayaran = $_POST['kas_bank_id'];
            $modelLogReceipt->id_barang = $id;
            $modelLogReceipt->biaya_admin = 0;
            $modelLogReceipt->biaya_simpan = 0;
            $modelLogReceipt->bunga = 0;
            $modelLogReceipt->denda = $modelBayar->myDenda($id)['denda'];;
            $modelLogReceipt->angsuran = $modelCicil->jumlah_cicil-$modelBayar->myDenda($id)['denda'];
            $modelLogReceipt->created_at = date('Y-m-d H:m:i');
            $modelLogReceipt->created_by = Yii::$app->user->identity->id;
            $modelLogReceipt->cashback = $cashback;
            $modelLogReceipt->type_pembayaran = $modelLogReceipt->typePembayaran($type_pembayaran);
            $modelLogReceipt->save(false);

            return $this->redirect(['/barang/','receipt' => 'yes','id_receipt' => $modelLogReceipt->id_log_receipt]);
        }
        else{
            return $this->renderAjax('lunaskan',[
                    'id' => $id,
                    'model' => $model,
                    'modelBayar' => $modelBayar,
                    'modelCicil' => $modelCicil,
                    'modelLogFinance' => $modelLogFinance
                ]);
        }
    }

    public function actionLunaskanPasif($id){

        $model = $this->findModel($id);
        $modelLogStatus = new LogStatus();
        $modelBayar = new Bayar();
        $modelCicil = new Cicil();
        $modelLogFinance = new LogFinance();
        $modelLogReceipt = new LogReceipt();
        $mLogJurnal = new LogJurnal();

        if($modelCicil->load(Yii::$app->request->post())){
            $modelCicil->id_barang = $id;
            $modelCicil->created_at = date('Y-m-d');
            $modelCicil->created_by = Yii::$app->user->identity->id;
            $modelCicil->save(false);

            $kali_bunga = floor($modelBayar->myDenda($id)['telat']/30);//kalau telat sudah lebih dari 2 bulan

            $bunganya = $modelBayar->myBunga($id)['bunga_baru'];

            $sisabayar = $model->nilai_pinjam+$model->biaya_admin+$model->biaya_simpan-$modelBayar->myBunga($id)['diskon_bunga']+$modelBayar->myDenda($id)['denda'];

            
            //update status tbl barang
            $model->updateAll(['status' => 'Lunas', 'tgl_status' => date('Y-m-d')],['id_barang' => $id]);
            
            //tambah data tbl bayar
            $modelBayar->inBayar($id,$model->id_nasabah);

            //tambah data tbl logstatus
            $modelLogStatus->inStatus($id,"Lunas");

            //Input ke table log finance Pokok
            $type_pembayaran = $_POST['kas_bank_id'];
            $modelLogFinance = new LogFinance();
            $jumlah_uang = $modelCicil->jumlah_cicil - $modelBayar->myDenda($id)['denda'] - $bunganya;
            $modelLogFinance->inLogFinance($id,$jumlah_uang,'Pelunasan',$type_pembayaran);

            //Input ke table log finance DendaPelunasan
            $type_pembayaran = $_POST['kas_bank_id'];
            $modelLogFinance = new LogFinance();
            $modelLogFinance->inLogFinance($id,$modelBayar->myDenda($id)['denda'],'DendaPelunasan',$type_pembayaran);

            //Input ke table log finance BungaPelunasan
            $type_pembayaran = $_POST['kas_bank_id'];
            $modelLogFinance = new LogFinance();
            $modelLogFinance->inLogFinance($id,$bunganya,'BungaPelunasanPasif',$type_pembayaran);


            //input type_receipt
            $modelLogReceipt->type_receipt = "Pelunasan";

            Yii::$app->getSession()->setFlash('success', [
                'type' => 'success',
                'duration' => 12000,
                'icon' => 'fa fa-like',
                'message' => Yii::t('app',Html::encode('Kontrak telah dilunasi.')),
                'title' => Yii::t('app', Html::encode('Bayar Gadai')),
                'positonY' => 'top',
                'positonX' => 'right'
            ]);

            $mLogJurnal->inPelunasanPasif($id,$jumlah_uang,$modelBayar->myDenda($id)['denda'],$bunganya,$type_pembayaran);
            
            //Input ke table LogReceipt
            $type_pembayaran = $_POST['kas_bank_id'];
            $modelLogReceipt->id_barang = $id;
            $modelLogReceipt->biaya_admin = 0;
            $modelLogReceipt->biaya_simpan = 0;
            $modelLogReceipt->bunga = 0;
            $modelLogReceipt->denda = $modelBayar->myDenda($id)['denda'];;
            $modelLogReceipt->angsuran = $modelCicil->jumlah_cicil-$modelBayar->myDenda($id)['denda'];
            $modelLogReceipt->created_at = date('Y-m-d H:m:i');
            $modelLogReceipt->created_by = Yii::$app->user->identity->id;
            $modelLogReceipt->cashback = 0;
            $modelLogReceipt->type_pembayaran = $modelLogReceipt->typePembayaran($type_pembayaran);
            $modelLogReceipt->save(false);

            return $this->redirect(['/barang/','receipt' => 'yes','id_receipt' => $modelLogReceipt->id_log_receipt]);
        }
        else{
            return $this->renderAjax('lunaskan-pasif',[
                    'id' => $id,
                    'model' => $model,
                    'modelBayar' => $modelBayar,
                    'modelCicil' => $modelCicil,
                    'modelLogFinance' => $modelLogFinance
                ]);
        }
    }

    public function actionJualPasif($id){
        $model = $this->findModel($id);
        $modelLogStatus = new LogStatus();
        $modelJual = new Jual();
        $modelLogFinance = new LogFinance();
        $modelTambahan = new TambahanJual();
        $mLogJurnal = new LogJurnal();

        if($modelJual->load(Yii::$app->request->post())){
            //update status tbl barang
            $model->updateAll(['status' => 'Terjual', 'tgl_status' => date('Y-m-d')],['id_barang' => $id]);

            //tambah data tbl jual
            $modelJual->id_barang = $id;
            $modelJual->id_nasabah = $model->id_nasabah;
            $modelJual->nilai_pinjam = $model->getSisapinjam2();
            $modelJual->harga_pasar = $model->harga_pasar;
            $modelJual->status = $modelJual->cekRugi($modelJual->harga_jual,$model->getSisapinjam2());
            $modelJual->created_by = Yii::$app->user->identity->id;
            $modelJual->created_at = date('Y-m-d');
            $modelJual->save(false);

            //Input ke table log finance
            $type_pembayaran = $_POST['kas_bank_id'];
            $modelLogFinance->inLogFinance($id,$modelJual->harga_jual,'Lelang',$type_pembayaran);

            //tambah data tbl logstatus
            $modelLogStatus->id_barang = $id;
            $modelLogStatus->status = "Terjual";
            $modelLogStatus->created_at = date('Y-m-d');
            $modelLogStatus->created_by = Yii::$app->user->identity->id;
            $modelLogStatus->save(false);

            //$mLogJurnal->inLogJual($id);

            Yii::$app->getSession()->setFlash('success', [
                'type' => 'success',
                'duration' => 12000,
                'icon' => 'fa fa-like',
                'message' => Yii::t('app',Html::encode('Barang Telah Dijual.')),
                'title' => Yii::t('app', Html::encode('Jual Barang')),
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
            return $this->redirect(['/barang/pasif']);
        }
        else{
            return $this->render('jual',[
                    'id' => $id,
                    'model' => $model,
                    'modelJual' => $modelJual,
                    'modelLogFinance' => $modelLogFinance,
                    'modelTambahan' => $modelTambahan
                ]);
        }
    }

    public function actionJualLelang($id){
        $model = $this->findModel($id);
        $modelLogStatus = new LogStatus();
        $modelJual = new Jual();
        $modelLogFinance = new LogFinance();
        $modelTambahan = new TambahanJual();
        $mLogJurnal = new LogJurnal();
        $mLogJurnalJual = new LogJurnalPenjualan();

        if($modelJual->load(Yii::$app->request->post())){
            //update status tbl barang
            $model->updateAll(['status' => 'Terjual', 'tgl_status' => date('Y-m-d')],['id_barang' => $id]);

            //tambah data tbl jual
            $modelJual->id_barang = $id;
            $modelJual->id_nasabah = $model->id_nasabah;
            $modelJual->nilai_pinjam = $model->getSisapinjam2();
            $modelJual->harga_pasar = $model->harga_pasar;
            $modelJual->status = $modelJual->cekRugi($modelJual->harga_jual,$model->getSisapinjam2());
            $modelJual->created_by = Yii::$app->user->identity->id;
            $modelJual->created_at = date('Y-m-d');
            $modelJual->save(false);

            //Input ke table log finance
            $type_pembayaran = $_POST['kas_bank_id'];
            $modelLogFinance->inLogFinance($id,$modelJual->harga_jual,'Lelang',$type_pembayaran);

            //tambah data tbl logstatus
            $modelLogStatus->id_barang = $id;
            $modelLogStatus->status = "Terjual";
            $modelLogStatus->created_at = date('Y-m-d');
            $modelLogStatus->created_by = Yii::$app->user->identity->id;
            $modelLogStatus->save(false);

            //$mLogJurnal->inLogJual($id);
            $kas_bank_id = $modelLogFinance->kas_bank_id;
            $mLogJurnalJual->generateLog($id,$kas_bank_id);

            Yii::$app->getSession()->setFlash('success', [
                'type' => 'success',
                'duration' => 12000,
                'icon' => 'fa fa-like',
                'message' => Yii::t('app',Html::encode('Barang Telah Dijual.')),
                'title' => Yii::t('app', Html::encode('Jual Barang')),
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
            return $this->redirect(['/log-jurnal-penjualan/index', 'barang_id' => $id]);
        }
        else{
            return $this->render('jual',[
                    'id' => $id,
                    'model' => $model,
                    'modelJual' => $modelJual,
                    'modelLogFinance' => $modelLogFinance,
                    'modelTambahan' => $modelTambahan
                ]);
        }
    }

    public function actionDetail($id){
        $model = $this->findModel($id);
        $modelLogStatus = new LogStatus();
            return $this->renderAjax('detail',[
                    'id' => $id,
                    'model' => $model,

                ]);
    }

    public function actionFind($sbg){
        $model = new Barang();
        $modelBayar = new Bayar();
        $modelPerpanjang = new Perpanjang();
        $modelCicil = new Cicil();
        $modelLogFinance = new LogFinance();
        $modelTambahan = new TambahanJual();
        return $this->render('find',[
            'model' => $model,
            'sbg' => $sbg,
            'modelBayar' => $modelBayar,
            'modelPerpanjang' => $modelPerpanjang,
            'modelCicil' => $modelCicil,
            'modelLogFinance' => $modelLogFinance,
            'modelTambahan' => $modelTambahan
        ]);
    }

    public function actionReceipt($id_receipt){
       $modelLogReceipt = new LogReceipt();
       $modelLogReceipt = $modelLogReceipt->find()->where(['id_log_receipt' => $id_receipt])->one();

        return $this->renderAjax('receipt',[
            'modelLogReceipt' => $modelLogReceipt
        ]);
    }

    public function actionLogReceipt($id){
        $model = new Barang();
        $modelLogReceipt = new LogReceipt();
        $modelLogReceipt = $modelLogReceipt->find()->where(['id_barang' => $id])->all();
        $modelCabang = new Cabang();

        return $this->renderAjax('log_receipt',[
            'modelLogReceipt' => $modelLogReceipt
        ]);
    }

     /**
     * Creates a new NasabahExt model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateNasabah()
    {
        $model = new NasabahExt();
        $modelUser = new User();
        $modelAuthAssignment = new AuthAssignment();

            $model->created_by = Yii::$app->user->identity->id;
            $model->akses = "defaultNasabah";
            $model->id_cabang = Yii::$app->session['id_cabang'];
       
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
             /*
            $randomPass = Yii::$app->getSecurity()->generateRandomString(8);
            $modelUser->username = $model->email;
            $modelUser->password_hash = Yii::$app->getSecurity()->generatePasswordHash($randomPass);
            $modelUser->password_reset_token = Yii::$app->getSecurity()->generatePasswordHash($randomPass);
            $modelUser->auth_key = Yii::$app->getSecurity()->generatePasswordHash($randomPass);
            $modelUser->id = $model->id_user;
            $modelUser->email = $model->email;
            $modelUser->save(false);

            $modelAuthAssignment->item_name = "defaultNasabah";
            $modelAuthAssignment->user_id = $model->id_user;
            $modelAuthAssignment->created_at =  new Expression('NOW()');
            $modelAuthAssignment->save(false);

            Yii::$app->mailer->view->params['firstname'] = $model->nama;
            Yii::$app->mailer->view->params['randomPass'] = $randomPass;

            Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'kirimDetailLogin-html', 'text' => 'kirimDetailLogin-text'],
                    ['user' => $modelUser])
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo($modelUser->email)
                ->setSubject('Detail Login Aplikasi ' . Yii::$app->name)
                ->send();
            */
                    
            return $this->redirect(['create', 'id_nasabah' => $model->id_user]);
        }

        return $this->renderAjax('create_nasabah', [
            'model' => $model,
        ]);
    }

    

    public function actionCekJatuhTempo(){
        $model = new Barang();

        //Recapt data yang jatuh_tempo
        $today = date('Y-m-d');
        $dataJatuhTempo = $model->find()->where(['<=','jatuh_tempo', $today])->andWhere(['status' => 'Aktif'])->all();
        foreach ($dataJatuhTempo as $dataJatuhTempos){
            echo $dataJatuhTempos->no_kontrak."<br>";
            echo $dataJatuhTempos->jatuh_tempo."<br>";
            //Tambahkan log perubahan status di tbl LogStatus
            $modelLogStatus = new LogStatus();//ini harus didalam foreach
            $modelLogStatus->id_barang = $dataJatuhTempos['id_barang'];
            $modelLogStatus->status = "JatuhTempo";
            $modelLogStatus->created_at = date('Y-m-d');
            $modelLogStatus->created_by = $dataJatuhTempos->jatuh_tempo;

            //Rubah dulu status di tbl Barang
            $model->updateAll(['status' => 'JatuhTempo'], ['id_barang'=>$dataJatuhTempos['id_barang']]);
            //cek jangan sampai ada data yg double
            if(empty($modelLogStatus->find()->andWhere(['id_barang' => $dataJatuhTempos['id_barang'],'status' => 'JatuhTempo'])->all())){
                //Tambah data di tbl log status
                $modelLogStatus->save(false);
            }
            
        }
        //End Recapt data yang jatuh_tempo
    }

    public function actionCekPasif(){
        $model = new Barang();

        //Recapt data masa tenggang bunga 1.2 & 5
        $jangka = "-10 days";
        $jatuh_tempo = date('Y-m-d',strtotime($jangka));
        $dataPasif = $model->find()->where(['<=','jatuh_tempo',$jatuh_tempo])->andWhere(['status' => 'JatuhTempo', 'jangka' => '15'])->all();
        foreach($dataPasif as $dataPasifs){
            echo $dataPasifs->no_kontrak."<br>";
            echo $dataPasifs->jatuh_tempo."<br>";
            //Tambahkan log perubahan status di tbl LogStatus
            $modelLogStatus = new LogStatus();//ini harus didalam foreach
            $modelLogStatus->id_barang = $dataPasifs['id_barang'];
            $modelLogStatus->status = "Pasif";
            $modelLogStatus->created_at = date('Y-m-d');
            $modelLogStatus->created_by = $dataPasifs['created_by'];

            //rubah dulu status di tbl barang
            $model->updateAll(['status' => "Pasif"], ['id_barang'=>$dataPasifs['id_barang']]);

            //cek jangan sampai ada data yg double
            if(empty($modelLogStatus->find()->andWhere(['id_barang' => $dataPasifs['id_barang'],'status' => 'Pasif'])->all())){
                //Tambah data di tbl log status
                $modelLogStatus->save(false); 
            }
            
        }
        //End Recapt data masa tenggang bunga 1.2 & 5

        //===========================================================================================

        //Recapt data masa tenggang bunga 2.4 & 10
        $jangka = "-16 days";
        $jatuh_tempo = date('Y-m-d',strtotime($jangka));
        $dataPasif = $model->find()->where(['<=','jatuh_tempo',$jatuh_tempo])->andWhere(['status' => 'JatuhTempo', 'jangka' => '30'])->all();
        foreach($dataPasif as $dataPasifs){
            echo $dataPasifs->no_kontrak."<br>";
            echo $dataPasifs->jatuh_tempo."<br>";
            //Tambahkan log perubahan status di tbl LogStatus
            $modelLogStatus = new LogStatus();//ini harus didalam foreach
            $modelLogStatus->id_barang = $dataPasifs['id_barang'];
            $modelLogStatus->status = "Pasif";
            $modelLogStatus->created_at = date('Y-m-d');
            $modelLogStatus->created_by = $dataPasifs['created_by'];

            //rubah dulu status di tbl barang
            $model->updateAll(['status' => "Pasif"], ['id_barang'=>$dataPasifs['id_barang']]);

            //cek jangan sampai ada data yg double
            if(empty($modelLogStatus->find()->andWhere(['id_barang' => $dataPasifs['id_barang'],'status' => 'Pasif'])->all())){
                //Tambah data di tbl log status
                $modelLogStatus->save(false); 
            }
            
        }
        //End Recapt data masa tenggang bunga 2.4 & 10
    }

    public function actionCekLelang(){
        $model = new Barang();

        //Recapt data masa tenggang
        $jangka1 = "-20 days";
        $jatuh_tempo1 = date('Y-m-d',strtotime($jangka1));
        $dataLelang = $model->find()->where(['<=','jatuh_tempo',$jatuh_tempo1])->andWhere(['status' => 'Pasif', 'jangka' => '30'])->all();
        foreach($dataLelang as $dataLelangs){
            echo $dataLelangs->no_kontrak."<br>";
            echo $dataLelangs->jatuh_tempo."<br>";
            //Tambahkan log perubahan status di tbl LogStatus
            $modelLogStatus = new LogStatus();//ini harus didalam foreach
            $modelLogStatus->id_barang = $dataLelangs['id_barang'];
            $modelLogStatus->status = "Lelang";
            $modelLogStatus->created_at = date('Y-m-d');
            $modelLogStatus->created_by = $dataLelangs['created_by'];

             //rubah dulu status di tbl barang
             $model->updateAll(['status' => "Lelang"], ['id_barang'=>$dataLelangs['id_barang']]);

            //cek jangan sampai ada data yg double
            if(empty($modelLogStatus->find()->andWhere(['id_barang' => $dataLelangs['id_barang'],'status' => 'Lelang'])->all())){
                //Tambah data di tbl log status
                $modelLogStatus->save(false); 
            }
            
        }

        $jangka2 = "-14 days";
        $jatuh_tempo2 = date('Y-m-d',strtotime($jangka2));
        $dataLelang = $model->find()->where(['<=','jatuh_tempo',$jatuh_tempo2])->andWhere(['status' => 'Pasif', 'jangka' => '15'])->all();
        foreach($dataLelang as $dataLelangs){
            echo $dataLelangs->no_kontrak."<br>";
            echo $dataLelangs->jatuh_tempo."<br>";
            //Tambahkan log perubahan status di tbl LogStatus
            $modelLogStatus = new LogStatus();//ini harus didalam foreach
            $modelLogStatus->id_barang = $dataLelangs['id_barang'];
            $modelLogStatus->status = "Lelang";
            $modelLogStatus->created_at = date('Y-m-d');
            $modelLogStatus->created_by = $dataLelangs['created_by'];

             //rubah dulu status di tbl barang
             $model->updateAll(['status' => "Lelang"], ['id_barang'=>$dataLelangs['id_barang']]);

            //cek jangan sampai ada data yg double
            if(empty($modelLogStatus->find()->andWhere(['id_barang' => $dataLelangs['id_barang'],'status' => 'Lelang'])->all())){
                //Tambah data di tbl log status
                $modelLogStatus->save(false); 
            }
            
        }
        //End Recapt data masa tenggang
    }

    public function actionFixJatuhTempo(){
        $model = new Barang();
        $modelLogStatus = new LogStatus;

        $dataBarang = $model->find()->where(['status' => 'JatuhTempo'])->all();
        $no = 1;
        foreach($dataBarang as $dataBarangs) {
            $modelLogStatus = new LogStatus;
            $dataLogStatus = $modelLogStatus->find()->andWhere(['id_barang' => $dataBarangs->id_barang, 'status' => 'Lunas'])->one();

            if(!empty($dataLogStatus->id_barang)){
                $model->updateAll(['status' => "Lunas"], ['id_barang'=>$dataBarangs->id_barang]);
                $modelLogStatus->deleteAll(['id_barang' =>$dataBarangs->id_barang, 'status' =>'JatuhTempo']);
                echo $no."- ".$dataLogStatus->id_barang."<br>";
                $no++;
            }
        }
    }

    public function actionPrintLabel($id){
        $model = $this->findModel($id);

        return $this->renderAjax('print-label', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Barang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Barang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Barang::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
