<?php

namespace backend\controllers;

use common\models\KoordinatorKunci;
use Yii;
use common\models\LoginToken;
use common\models\LoginTokenSearch;
use common\models\Profile;
//use PhpOffice\PhpSpreadsheet\Helper\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Cookie;

/**
 * LoginTokenController implements the CRUD actions for LoginToken model.
 */
class LoginTokenController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LoginToken models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LoginTokenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(!Yii::$app->user->can('admin')){
            $dataProvider->query->andFilterWhere(['cabang_id' => Profile::myProfile2()->cabang_id]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LoginToken model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LoginToken model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LoginToken();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LoginToken model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LoginToken model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionConfirmLogin(){

        $model = new LoginToken();

        $model->pegawai_id = Yii::$app->user->id;
        $model->created_at = date('Y-m-d');
        $model->cabang_id = Profile::myProfile2()->id_cabang;
        $model->token = Yii::$app->getSecurity()->generateRandomString(10);
        $this->layout = 'confirm-login';//supaya dia pakai layout confirm-login
        if ($model->load(Yii::$app->request->post())) {
            $dataKoordinator = KoordinatorKunci::find()->where(['key_code' => $model->key])->one();

            if($dataKoordinator){
                if($dataKoordinator->koordinator->id_cabang == Profile::myProfile2()->id_cabang){

                    $model->koordinator_id = $dataKoordinator->koordinator_id;

                    LoginToken::deleteAll(['pegawai_id' => Yii::$app->user->id, 'created_at' => date('Y-m-d')]);

                    if($model->save()){
                        Yii::$app->getSession()->setFlash('danger', [
                            'type' => 'success',
                            'duration' => 12000,
                            'icon' => 'fa fa-like',
                            'message' => Yii::t('app',Html::encode('Data Koordinator Ditemukan..!!')),
                            'title' => Yii::t('app', Html::encode('Autentikasi Berhasil.')),
                            'positonY' => 'top',
                            'positonX' => 'right',
                            'justLogin' => TRUE
                        ]);

                        $cookie = new Cookie ([
                            'name' => 'token',
                            'value' => $model->token,
                            'expire' => time() + 3600 * 24, // Waktu kedaluwarsa 24 jam
                        ]);
                        Yii::$app->response->cookies->add($cookie);

                        return $this->redirect(['/']);
                    }

                }
                else{
                    Yii::$app->getSession()->setFlash('danger', [
                        'type' => 'danger',
                        'duration' => 12000,
                        'icon' => 'fa fa-like',
                        'message' => Yii::t('app',Html::encode('Kartu Yang Digunakan Untuk Outlet Yang Berbeda..!!')),
                        'title' => Yii::t('app', Html::encode('Autentikasi Gagal.')),
                        'positonY' => 'top',
                        'positonX' => 'right',
                        'justLogin' => TRUE
                    ]);
                }
            }
            else{
                Yii::$app->getSession()->setFlash('danger', [
                    'type' => 'danger',
                    'duration' => 12000,
                    'icon' => 'fa fa-like',
                    'message' => Yii::t('app',Html::encode('Data Koordinator Tidak Ditemukan..!!')),
                    'title' => Yii::t('app', Html::encode('Autentikasi Gagal.')),
                    'positonY' => 'top',
                    'positonX' => 'right',
                    'justLogin' => TRUE
                ]);
            }
            
        }

        return $this->render('create', [
            'model' => $model,
        ]);
        
    }

    /**
     * Finds the LoginToken model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LoginToken the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LoginToken::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
