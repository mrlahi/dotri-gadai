<?php

namespace backend\controllers;

use Yii;
use common\models\LogUangKeluar;
use common\models\LogUangKeluarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Profile;
use common\models\LogFinance;
use common\models\Cabang;

/**
 * LogUangKeluarController implements the CRUD actions for LogUangKeluar model.
 */
class LogUangKeluarController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LogUangKeluar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogUangKeluarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $modelProfile = new Profile();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelProfile' => $modelProfile
        ]);
    }

    /**
     * Displays a single LogUangKeluar model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LogUangKeluar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LogUangKeluar();
        $modelProfile = new Profile();
        $modelLogFinance = new LogFinance();

        $model->created_at = date('Y-m-d');
        $model->created_by = Yii::$app->user->identity->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            //Pengeluaran masuk ke table log finance
            $modelLogFinance->jumlah_uang = $model->jumlah_pengeluaran;
            $modelLogFinance->status = "Pengeluaran";
            $modelLogFinance->created_at = date('Y-m-d');
            $modelLogFinance->created_by = Yii::$app->user->identity->id;
            $modelLogFinance->id_cabang = $model->id_cabang;
            $modelLogFinance->id_barang = $model->id_log_uang_keluar;
            $modelLogFinance->type_pembayaran = $model->type_pengeluaran;
            $modelLogFinance->save(false);

            return $this->redirect(['index']);
        }

        return $this->renderAjax('create', [
            'model' => $model,
            'modelProfile' => $modelProfile
        ]);
    }

    /**
     * Updates an existing LogUangKeluar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLogFinance = new LogFinance();
        $modelProfile = new Profile();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelLogFinance->updateAll(['jumlah_uang' => $model->jumlah_pengeluaran, 'created_at' => $model->created_at],['id_barang' => $id, 'status' => 'Pengeluaran']);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'modelProfile' => $modelProfile
        ]);
    }

    /**
     * Deletes an existing LogUangKeluar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $modelLogFinance = new LogFinance();
        $modelLogFinance->deleteAll(['id_barang' => $id]);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGenerateKasAll($first, $last, $step = '+1 day', $format = 'Y-m-d' ){
        $dates = [];
        $current = strtotime( $first );
        $last = strtotime( $last );

        while( $current <= $last ) {
            $tgl_now = date( $format, $current );
            $current = strtotime( $step, $current );
            
            $modelCabang = new Cabang();

            $listCabang = $modelCabang->find()->all();
            foreach($listCabang as $listCabangs){
                $id_cabang = $listCabangs->id_cabang;
                $model = new LogUangKeluar();
                $modelLogFinance = new LogFinance();
    
                
                if($listCabangs->nama_cabang != 'Akun Bank'){
    
                    $kas_masuk =  $modelLogFinance->find()->where(['id_cabang' => $id_cabang,'created_at' => $tgl_now,'type_pembayaran' => 'Cash'])->andWhere(['or',['status' => 'Modal'],['status' => 'Pelunasan'],['status' => 'Angsuran'],['status' => 'Bunga'],['status' => 'BungaPerpanjang'],['status' => 'DendaPelunasan'],['status' => 'DendaPerpanjang'],['status' => 'Admin'],['status' => 'KembaliBunga5Persen'],['status' => 'KembaliBunga1.2Persen'],['status' => 'Lelang']])->sum('jumlah_uang');
    
                    $kas_keluar = $modelLogFinance->find()->where(['id_cabang' => $id_cabang,'created_at' => $tgl_now,'type_pembayaran' => 'Cash'])->andWhere(['or',['status' => 'Operasional'],['status' => 'Pinjaman'],['status' => 'Pengeluaran'],['status' => 'KembaliBunga5Persen'],['status' => 'KembaliBunga1.2Persen']])->sum('jumlah_uang');

                    $model->nama_pengeluaran = "Tarik Kas Bos";
                    $model->jumlah_pengeluaran = $kas_masuk - $kas_keluar;
                    $model->type_pengeluaran = "Cash";
                    $model->id_cabang = $listCabangs->id_cabang;
                    $model->created_at = $tgl_now;
                    $model->created_by = Yii::$app->user->identity->id;
                    $model->save(false);

                    //Pengeluaran masuk ke table log finance
                    $modelLogFinance->jumlah_uang = $model->jumlah_pengeluaran;
                    $modelLogFinance->status = "Pengeluaran";
                    $modelLogFinance->created_at = $tgl_now;
                    $modelLogFinance->created_by = Yii::$app->user->identity->id;
                    $modelLogFinance->id_cabang = $model->id_cabang;
                    $modelLogFinance->id_barang = $model->id_log_uang_keluar;
                    $modelLogFinance->type_pembayaran = $model->type_pengeluaran;
                    $modelLogFinance->save(false);
                }
                else{
                    $kas_masuk =  $modelLogFinance->find()->where(['created_at' => $tgl_now,'type_pembayaran' => 'Transfer'])->andWhere(['or',['status' => 'Modal'],['status' => 'Pelunasan'],['status' => 'Angsuran'],['status' => 'Bunga'],['status' => 'BungaPerpanjang'],['status' => 'DendaPelunasan'],['status' => 'DendaPerpanjang'],['status' => 'Admin'],['status' => 'KembaliBunga5Persen'],['status' => 'KembaliBunga1.2Persen'],['status' => 'Lelang']])->sum('jumlah_uang');
    
                    $kas_keluar = $modelLogFinance->find()->where(['created_at' => $tgl_now,'type_pembayaran' => 'Transfer'])->andWhere(['or',['status' => 'Operasional'],['status' => 'Pinjaman'],['status' => 'Pengeluaran']])->sum('jumlah_uang');
    
                    $model->nama_pengeluaran = "Tarik Kas Bos";
                    $model->jumlah_pengeluaran = $kas_masuk - $kas_keluar;
                    $model->type_pengeluaran = "Transfer";
                    $model->id_cabang = $listCabangs->id_cabang;
                    $model->created_at = $tgl_now;
                    $model->created_by = Yii::$app->user->identity->id;
                    $model->save(false);

                    //Pengeluaran masuk ke table log finance
                    $modelLogFinance->jumlah_uang = $model->jumlah_pengeluaran;
                    $modelLogFinance->status = "Pengeluaran";
                    $modelLogFinance->created_at = $tgl_now;
                    $modelLogFinance->created_by = Yii::$app->user->identity->id;
                    $modelLogFinance->id_cabang = $model->id_cabang;
                    $modelLogFinance->id_barang = $model->id_log_uang_keluar;
                    $modelLogFinance->type_pembayaran = $model->type_pengeluaran;
                    $modelLogFinance->save(false);
                }
            }
        }

        //return $dates;
    }

    /**
     * Finds the LogUangKeluar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogUangKeluar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogUangKeluar::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
