<?php

namespace backend\controllers;

use common\models\PeriodePenggajianHonor;
use common\models\Profile;
use Yii;
use common\models\RekapitulasiAbsensiPegawai;
use common\models\RekapitulasiAbsensiPegawaiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * RekapitulasiAbsensiPegawaiController implements the CRUD actions for RekapitulasiAbsensiPegawai model.
 */
class RekapitulasiAbsensiPegawaiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RekapitulasiAbsensiPegawai models.
     * @return mixed
     */
    public function actionIndex($periode_penggajian_honor_id)
    {

        $dataPeriodePenggajianHonor = PeriodePenggajianHonor::findOne($periode_penggajian_honor_id);

        $pegawai_id = $dataPeriodePenggajianHonor->pegawai_id;
        $date1 = $dataPeriodePenggajianHonor->periode->start_date;
        $date2 = $dataPeriodePenggajianHonor->periode->end_date;

        $model = new RekapitulasiAbsensiPegawai();
        $model->rekapAbsen($pegawai_id,$date1,$date2);

        $searchModel = new RekapitulasiAbsensiPegawaiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['rekapitulasi_absensi_pegawai.pegawai_id' => $pegawai_id])->andFilterWhere(['between', 'tgl_kerja', $date1, $date2]);

        $dataPegawai = Profile::findOne($pegawai_id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'date1' => $date1,
            'date2' => $date2,
            'dataPegawai' => $dataPegawai,
            'periode_penggajian_honor_id' => $periode_penggajian_honor_id
        ]);
    }

    public function actionPrintRekapitulasi($periode_penggajian_honor_id){

        $dataPeriodePenggajianHonor = PeriodePenggajianHonor::findOne($periode_penggajian_honor_id);

        $dataHonor = $dataPeriodePenggajianHonor->honor;

        $pegawai_id = $dataPeriodePenggajianHonor->pegawai_id;
        $date1 = $dataPeriodePenggajianHonor->periode->start_date;
        $date2 = $dataPeriodePenggajianHonor->periode->end_date;

        $dataRekap = RekapitulasiAbsensiPegawai::find()->where(['pegawai_id' => $pegawai_id])->andWhere(['between', 'tgl_kerja', $date1, $date2])->orderBy(['tgl_kerja' => SORT_ASC])->all();
        $dataPegawai = Profile::findOne($pegawai_id);

        return $this->renderAjax('print-rekapitulasi', [
            'dataRekap' => $dataRekap,
            'dataPegawai' => $dataPegawai,
            'date1' => $date1,
            'date2' => $date2,
            'dataHonor' => $dataHonor,
        ]);

    }

    // public function actionFilterRekap(){
    //     $model = new RekapitulasiAbsensiPegawai();

    //     if ($model->load(Yii::$app->request->post())) {
    //         $date_explode=explode(" - ",$model->range_hari_kerja);
    //         $date1=trim($date_explode[0]);
    //         $date2=trim($date_explode[1]);

    //         $model->rekapAbsen();

    //         return $this->redirect(['index', 'pegawai_id' => $model->pegawai_id, 'date1' => $date1, 'date2' => $date2]);
    //     }
    //     else{
    //         return $this->render('filter-rekap', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    /**
     * Displays a single RekapitulasiAbsensiPegawai model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RekapitulasiAbsensiPegawai model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RekapitulasiAbsensiPegawai();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RekapitulasiAbsensiPegawai model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RekapitulasiAbsensiPegawai model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RekapitulasiAbsensiPegawai model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RekapitulasiAbsensiPegawai the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RekapitulasiAbsensiPegawai::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
