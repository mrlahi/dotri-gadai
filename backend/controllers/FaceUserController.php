<?php

namespace backend\controllers;

use Yii;
use common\models\FaceUser;
use common\models\FaceUserSearch;
use common\models\Profile;
use common\models\ProfileSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;

/**
 * FaceUserController implements the CRUD actions for FaceUser model.
 */
class FaceUserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FaceUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProfileSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FaceUser model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FaceUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FaceUser();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FaceUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionTrainFace($id)
    {
        $model = new FaceUser;
        $dataProfile = Profile::findOne($id);

        $model->profile_id = $id;
        if (Yii::$app->request->isPost) {
            $descriptor = Yii::$app->request->post('image_data');
            $imageUrl = Yii::$app->request->post('image_url');
            $model->face_decrypt = $descriptor;
            $model->created_at = date('Y-m-d');
            $model->created_by = Yii::$app->user->id;
            if ($model->save()) {

                 // Decode base64 image
            $imageUrl = str_replace('data:image/png;base64,', '', $imageUrl);
            $imageUrl = str_replace(' ', '+', $imageUrl);
            $decodedImageUrl = base64_decode($imageUrl);

             // Tentukan path untuk menyimpan file
            $filePath = Yii::getAlias('@backend/web/uploads/user_face_photos/');
            if (!file_exists($filePath)) {
                mkdir($filePath, 0777, true);
            }
            $fileName = $filePath . $model->id . '.png';

            file_put_contents($fileName, $decodedImageUrl);

            // Kompres gambar
            $compressedFileName = $filePath . $model->id . '_compressed.png';
            FaceUser::compressImage($fileName, $compressedFileName, 100 * 1024); // Target 100 KB

            // Hapus file asli jika kompresi berhasil
            if (file_exists($compressedFileName)) {
                unlink($fileName);
                $fileName = $compressedFileName;
            }

                Yii::$app->getSession()->setFlash('success', [
                    'type' => 'success',
                    'duration' => 3000,
                    'icon' => 'fa fa-check',
                    'message' => Yii::t('app',Html::encode('Face record success..')),
                    'title' => Yii::t('app', Html::encode('Face Record.')),
                    'positonY' => 'top',
                    'positonX' => 'right', 
                ]);

                return $this->redirect(['index']);

            } else {
                Yii::$app->getSession()->setFlash('success', [
                    'type' => 'danger',
                    'duration' => 3000,
                    'icon' => 'fa fa-close',
                    'message' => Yii::t('app',Html::encode('Face record not success..')),
                    'title' => Yii::t('app', Html::encode('Face Record.')),
                    'positonY' => 'top',
                    'positonX' => 'right', 
                ]);

                return $this->redirect(['index']);
            }
        }
        return $this->render('train-face', [
            'model' => $model,
            'dataProfile' => $dataProfile,
            'id' => $id
        ]);
    }

    public function actionDetailUser($id){
        $dataProfile = Profile::findOne($id);
        $dataFace = $dataProfile->getFaceUsers()->all();

        return $this->render('detail-user',[
            'dataProfile' => $dataProfile,
            'dataFace' => $dataFace
        ]);

    }

    /**
     * Deletes an existing FaceUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FaceUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FaceUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FaceUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
