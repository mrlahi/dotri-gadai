<?php

namespace backend\controllers;

use Yii;
use common\models\LogJurnal;
use common\models\LogJurnalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Akun;
use common\models\ListLaporan;
use yii\data\Pagination;

/**
 * LogJurnalController implements the CRUD actions for LogJurnal model.
 */
class LogJurnalController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LogJurnal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogJurnalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexInfinity()
    {
        $searchModel = new LogJurnalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-infinity', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LogJurnal model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LogJurnal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

     public function actionSaldoAwal()
     {
         $searchModel = new LogJurnalSearch();
         $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
         $dataProvider->query->andFilterWhere(['jenis_asal_jurnal' => 'SALDO AWAL']);
 
         return $this->render('index-saldo-awal', [
             'searchModel' => $searchModel,
             'dataProvider' => $dataProvider,
         ]);
     }

    public function actionCreateSaldoAwal()
    {
        $model = new LogJurnal();

        $model->scenario = "saldo_awal";
        $model->created_by = Yii::$app->user->id;
        $model->jenis_asal_jurnal = "SALDO AWAL";
        $model->keterangan = "SALDO AWAL";
        $model->log_transaksi_id = 1;//SEKALI BUAT TRANSAKSI SALDO AWAL
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['saldo-awal']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionCreatePenyesuaian($log_transaksi_id,$cabang_id)
    {
        $model = new LogJurnal();

        $model->scenario = "penyesuaian";
        $model->log_transaksi_id = $log_transaksi_id;
        $model->created_at = date('Y-m-d');
        $model->created_by = Yii::$app->user->id;
        $model->cabang_id = $cabang_id;
        $model->jenis_asal_jurnal = "PENYESUAIAN";
        $model->asal_id = $log_transaksi_id;
        $model->keterangan = "JURNAL PENYESUAIAN";
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/log-transaksi/jurnal-penyesuaian', 'id' => $log_transaksi_id]);
        }

        return $this->renderAjax('create-penyesuaian', [
            'model' => $model,
        ]);
    }

    public function actionFilterBukuBesar()
    {
        $model = new LogJurnal();

        if ($model->load(Yii::$app->request->post())) {

            return $this->redirect(['buku-besar', 'tgl' => $model->created_at, 'cabang_id' => $model->cabang_id, 'akun_kode' => $model->akun_kode]);
        }

        return $this->render('_form-filter-buku-besar', [
            'model' => $model,
        ]);
    }

    public function actionBukuBesar($tgl,$cabang_id,$akun_kode)
    {
        $model = new LogJurnal();

        
        
        if($cabang_id == 1000000){
            $query = $model->find()->where(['akun_kode' => $akun_kode])->andWhere(['<=', 'created_at', $tgl])->orderBy(['created_at' => SORT_ASC, 'id' => SORT_ASC]);;
        }
        else{
            $query = $model->find()->where(['akun_kode' => $akun_kode, 'cabang_id' => $cabang_id])->andWhere(['<=', 'created_at', $tgl])->orderBy(['created_at' => SORT_ASC]);
        }

        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count,'pageSize' => 100,]);

        $dataJurnal = $query->offset($pagination->offset)
        ->limit($pagination->limit)
        ->all();
        
        $akun = Akun::findOne($akun_kode);

        return $this->render('buku-besar', [
            'model' => $model,
            'dataJurnal' => $dataJurnal,
            'tgl' => $tgl,
            'cabang_id' => $cabang_id,
            'akun' => $akun,
            'pagination' => $pagination
        ]);
    }

    public function actionBukuBesarLinked($start,$end,$cabang_id,$akun_kode)
    {
        $model = new LogJurnal();

        
        
        if($cabang_id == 1000000){
            $dataJurnal = $model->find()->where(['akun_kode' => $akun_kode])->andWhere(['between', 'created_at', $start, $end])->orderBy(['created_at' => SORT_ASC, 'id' => SORT_ASC])->all();
        }
        else{
            $dataJurnal = $model->find()->where(['akun_kode' => $akun_kode, 'cabang_id' => $cabang_id])->andWhere(['between', 'created_at', $start, $end])->orderBy(['created_at' => SORT_ASC])->all();
        }
        
        $akun = Akun::findOne($akun_kode);

        return $this->render('buku-besar-linked', [
            'model' => $model,
            'dataJurnal' => $dataJurnal,
            'tgl' => $end,
            'cabang_id' => $cabang_id,
            'akun' => $akun,
        ]);
    }

    public function actionFilterBukuBesarLama()
    {
        $model = new LogJurnal();

        if ($model->load(Yii::$app->request->post())) {

            return $this->redirect(['buku-besar-lama', 'tgl' => $model->created_at, 'cabang_id' => $model->cabang_id, 'akun_kode' => $model->akun_kode]);
        }

        return $this->render('_form-filter-buku-besar', [
            'model' => $model,
        ]);
    }

    public function actionBukuBesarLama($tgl,$cabang_id,$akun_kode)
    {
        $model = new LogJurnal();

        
        
        if($cabang_id == 1000000){
            $query = $model->find()->where(['akun_kode' => $akun_kode])->andWhere(['<=', 'created_at', $tgl])->orderBy(['created_at' => SORT_ASC, 'id' => SORT_ASC]);
        }
        else{
            $query = $model->find()->where(['akun_kode' => $akun_kode, 'cabang_id' => $cabang_id])->andWhere(['<=', 'created_at', $tgl])->orderBy(['created_at' => SORT_ASC]);
        }

        $dataJurnal = $query->all();
        
        $akun = Akun::findOne($akun_kode);

        return $this->render('buku-besar-lama', [
            'model' => $model,
            'dataJurnal' => $dataJurnal,
            'tgl' => $tgl,
            'akun' => $akun,
        ]);
    }

    public function actionFilterNeracaSaldo()
    {
        $model = new LogJurnal();
        $mAkun = new Akun();

        if ($model->load(Yii::$app->request->post())) {
            $dataAkun = $mAkun->find()->all();

            return $this->render('neraca-saldo', [
                'tgl' => $model->created_at,
                'cabang_id' => $model->cabang_id,
                'model' => $model,
                'dataAkun' => $dataAkun
            ]);
        }

        return $this->render('_form-filter-neraca-saldo', [
            'model' => $model,
        ]);
    }

    public function actionFilterLabaRugi()
    {
        $model = new LogJurnal();
        $mAkun = new Akun();
        $mListLaporan = new ListLaporan();

        if ($model->load(Yii::$app->request->post())) {
            $tgl = explode(" - ", $model->created_at);
            $date1 = $tgl[0];
            $date2 = $tgl[1];
            return $this->render('laba-rugi', [
                'date1' => $date1,
                'date2' => $date2,
                'cabang_id' => $model->cabang_id,
                'model' => $model,
                'mAkun' => $mAkun,
                'mListLaporan' => $mListLaporan
            ]);
        }

        return $this->render('_form-filter-laba-rugi', [
            'model' => $model,
        ]);
    }

    public function actionFilterPosisiKeuangan()
    {
        $model = new LogJurnal();
        $mAkun = new Akun();
        $mListLaporan = new ListLaporan();

        if ($model->load(Yii::$app->request->post())) {
            $tgl = explode(" - ", $model->created_at);
            $date1 = $tgl[0];
            $date2 = $tgl[1];
            return $this->render('posisi-keuangan', [
                'date1' => $date1,
                'date2' => $date2,
                'cabang_id' => $model->cabang_id,
                'model' => $model,
                'mAkun' => $mAkun,
                'mListLaporan' => $mListLaporan
            ]);
        }

        return $this->render('_form-filter-posisi-keuangan', [
            'model' => $model,
        ]);
    }

    public function actionFilterArusKas()
    {
        $model = new LogJurnal();
        $mAkun = new Akun();
        $mListLaporan = new ListLaporan();

        if ($model->load(Yii::$app->request->post())) {
            $tgl = explode(" - ", $model->created_at);
            $date1 = $tgl[0];
            $date2 = $tgl[1];
            return $this->render('arus-kas', [
                'date1' => $date1,
                'date2' => $date2,
                'cabang_id' => $model->cabang_id,
                'model' => $model,
                'mAkun' => $mAkun,
                'mListLaporan' => $mListLaporan
            ]);
        }

        return $this->render('_form-filter-arus-kas', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LogJurnal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LogJurnal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['saldo-awal']);
    }

    public function actionDeleteJurnal($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionMutasiKas(){
        $model = new LogJurnal();
        $model->scenario = "mutasi";
        $model->created_at = date('Y-m-d');
        if ($model->load(Yii::$app->request->post())) {
            $model->mutasiKas();
            return $this->redirect(['mutasi-kas']);
        }

        return $this->render('_form-mutasi-kas', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the LogJurnal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogJurnal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogJurnal::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
