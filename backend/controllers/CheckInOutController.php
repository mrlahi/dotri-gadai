<?php

namespace backend\controllers;

use Yii;
use common\models\CheckInOut;
use common\models\CheckInOutSearch;
use common\models\FaceUser;
use common\models\Profile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Response;

/**
 * CheckInOutController implements the CRUD actions for CheckInOut model.
 */
class CheckInOutController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CheckInOut models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CheckInOutSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CheckInOut model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CheckInOut model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CheckInOut();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CheckInOut model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionAttendance(){
        return $this->render('attendance');
    }

    public function actionMatchFace()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        // Ambil data dari request
        $postData = Yii::$app->request->getRawBody();
        $data = json_decode($postData, true);

        // Debug untuk memeriksa data yang diterima
        Yii::info('Data yang diterima: ' . print_r($data, true));

        if (!isset($data['descriptor'])) {
            return ['success' => false, 'message' => 'Descriptor wajah tidak ditemukan'];
        }

        $descriptor = $data['descriptor'];

        // Ambil profil dari database dan bandingkan
        $profiles = FaceUser::find()->all();
        $matchedProfileId = null;
        foreach ($profiles as $profile) {
            $storedDescriptor = json_decode($profile->face_decrypt, true);

            // Pastikan descriptor yang disimpan tidak null
            if (!$storedDescriptor) {
                continue;
            }

            // Bandingkan descriptor wajah
            $distance = CheckInOut::calculateFaceDistance($descriptor, $storedDescriptor);
            if ($distance < 0.3) {  // Toleransi untuk kecocokan wajah
                $matchedProfileId = $profile->profile_id;
                break;
            }
        }

        if ($matchedProfileId) {
            return ['success' => true, 'profileId' => $matchedProfileId];

        }

        return ['success' => false, 'message' => 'Wajah tidak cocok atau tidak ditemukan'];
        
    }


    public function actionRecordAttendance()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        // Ambil data yang dikirim melalui POST
        $postData = Yii::$app->request->post();
        
        // Debug untuk memeriksa data yang diterima
        Yii::info('Data yang diterima: ' . print_r($postData, true));

        // Pastikan data yang diterima valid
        if (!isset($postData['profile_id'])) {

            Yii::$app->getSession()->setFlash('success', [
                'type' => 'danger',
                'duration' => 3000,
                'icon' => 'fa fa-close',
                'message' => Yii::t('app',Html::encode('Face Not Found.!')),
                'title' => Yii::t('app', Html::encode('Face Attendance.')),
                'positonY' => 'top',
                'positonX' => 'right', 
            ]);

            return $this->redirect(['attendance']);
        }
        
        if (!isset($postData['image_data'])) {

            Yii::$app->getSession()->setFlash('success', [
                'type' => 'danger',
                'duration' => 3000,
                'icon' => 'fa fa-close',
                'message' => Yii::t('app',Html::encode('Image Not Found.!')),
                'title' => Yii::t('app', Html::encode('Face Attendance.')),
                'positonY' => 'top',
                'positonX' => 'right', 
            ]);

            return $this->redirect(['attendance']);
        }

        // Ambil data profile_id dan image_data
        $profileId = $postData['profile_id'];
        $imageData = $postData['image_data'];
        $outletId = $postData['outlet_id'];

        // Proses lebih lanjut, seperti menyimpan ke database

        $dataPegawai = Profile::findOne($profileId);

        $attLog = new CheckInOut();
        $attLog->absen_id = $dataPegawai->absensi_id;
        $attLog->tgl_absen = date('Y-m-d');
        $attLog->jam_absen = date('H:i:s');
        $attLog->cabang_id = 'DOT'.$outletId;
        $attLog->created_at = date('Y-m-d');

        if ($attLog->save()) {

            // Decode base64 image
            $imageData = str_replace('data:image/png;base64,', '', $imageData);
            $imageData = str_replace(' ', '+', $imageData);
            $decodedData = base64_decode($imageData);

             // Tentukan path untuk menyimpan file
            $filePath = Yii::getAlias('@backend/web/uploads/attendance_photos/');
            if (!file_exists($filePath)) {
                mkdir($filePath, 0777, true);
            }
            $fileName = $filePath . $attLog->id . '.png';

            file_put_contents($fileName, $decodedData);

            // Kompres gambar
            $compressedFileName = $filePath . $attLog->id . '_compressed.png';
            CheckInOut::compressImage($fileName, $compressedFileName, 100 * 1024); // Target 100 KB

            // Hapus file asli jika kompresi berhasil
            if (file_exists($compressedFileName)) {
                unlink($fileName);
                $fileName = $compressedFileName;
            }

            Yii::$app->getSession()->setFlash('success', [
                'type' => 'success',
                'duration' => 3000,
                'icon' => 'fa fa-close',
                'message' => Yii::t('app',Html::encode('Log attendance success.!')),
                'title' => Yii::t('app', Html::encode('Face Attendance.')),
                'positonY' => 'top',
                'positonX' => 'right', 
            ]);

            return $this->redirect(['attendance-success', 'id' => $attLog->id]);

        } else {
            
            Yii::$app->getSession()->setFlash('success', [
                'type' => 'danger',
                'duration' => 3000,
                'icon' => 'fa fa-close',
                'message' => Yii::t('app',Html::encode('Log attendance failed.!')),
                'title' => Yii::t('app', Html::encode('Face Attendance.')),
                'positonY' => 'top',
                'positonX' => 'right', 
            ]);

            return $this->redirect(['attendance']);

        }
    }
    
    public function actionAttendanceSuccess($id){
        $dataLog = CheckInOut::findOne($id);
        return $this->render('attendance-success', [
            'dataLog' => $dataLog
        ]);
    }

    /**
     * Deletes an existing CheckInOut model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CheckInOut model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CheckInOut the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CheckInOut::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
