<?php

namespace backend\controllers;

use common\models\BebanOperasional;
use Yii;
use common\models\LogFinance;
use common\models\LogFinanceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\LogUangKeluar;
use common\models\LogModal;

/**
 * LogFinanceController implements the CRUD actions for LogFinance model.
 */
class LogFinanceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LogFinance models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new LogFinance();
        return $this->render('index', [
            'model' => $model
        ]);
    }

    public function actionFilter(){
        $model = new LogFinance();
        $mUangKeluar = new LogUangKeluar();
        $mModal = new LogModal();
        $mOperasional = new BebanOperasional();

        if ($model->load(Yii::$app->request->post())) {

            $id_cabang = $model->id_cabang;
            $date_explode=explode(" - ",$model->created_at);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);

            $model = new LogFinance();
            if($model->myCabang($id_cabang)->type_cabang == "CABANG BIASA"){
                return $this->render('index', [
                    'model' => $model,
                    'date1' => $date1,
                    'date2' => $date2,
                    'id_cabang' => $id_cabang,
                    'mOperasional' => $mOperasional
                ]);
            }else{

                $dataUangKeluar = $mUangKeluar->find()->where(['id_cabang' => $id_cabang])->andWhere(['between','created_at', $date1, $date2])->all();
                $dataModal = $mModal->find()->where(['id_cabang' => $id_cabang])->andWhere(['between','created_at', $date1, $date2])->all();

                return $this->render('index-kas', [
                    'model' => $model,
                    'date1' => $date1,
                    'date2' => $date2,
                    'id_cabang' => $id_cabang,
                    'dataUangKeluar' => $dataUangKeluar,
                    'dataModal' => $dataModal
                ]);
            }
        }
        else{

            return $this->render('index-filter',[
                'model' => $model,
            ]);
            }
    }

    public function actionFilterBaru(){
        $model = new LogFinance();
        $mUangKeluar = new LogUangKeluar();
        $mModal = new LogModal();

        if ($model->load(Yii::$app->request->post())) {

            $date = $model->created_at;
            $id_cabang = $model->id_cabang;
            $date_explode=explode(" - ",$model->created_at);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);

            $model = new LogFinance();
            if($model->myCabang($id_cabang)->type_cabang == "CABANG BIASA"){
                return $this->render('index-baru', [
                    'model' => $model,
                    'date1' => $date1,
                    'date2' => $date2,
                    'id_cabang' => $id_cabang
                ]);
            }else{

                $dataUangKeluar = $mUangKeluar->find()->where(['id_cabang' => $id_cabang])->andWhere(['between','created_at', $date1, $date2])->all();
                $dataModal = $mModal->find()->where(['id_cabang' => $id_cabang])->andWhere(['between','created_at', $date1, $date2])->all();

                return $this->render('index-kas-baru', [
                    'model' => $model,
                    'date1' => $date1,
                    'date2' => $date2,
                    'id_cabang' => $id_cabang,
                    'dataUangKeluar' => $dataUangKeluar,
                    'dataModal' => $dataModal
                ]);
            }
        }
        else{
            return $this->render('index-filter',[
                'model' => $model,
            ]);
        }
    }

    public function actionFilterPenjualan(){
        $model = new LogFinance();

        if ($model->load(Yii::$app->request->post())) {

            $date = $model->created_at;
            $id_cabang = $model->id_cabang;
            $date_explode=explode(" - ",$model->created_at);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);

            $model = new LogFinance();
            return $this->render('index-penjualan', [
            'model' => $model,
            'date1' => $date1,
            'date2' => $date2,
            'id_cabang' => $id_cabang
        ]);
        }
        else{

        return $this->render('index-filter-penjualan',[
            'model' => $model,
        ]);
        }
    }

    public function actionFilterLelang()
    {
        $model = new LogFinance();

        if ($model->load(Yii::$app->request->post())) {

            $date = $model->created_at;
            $id_cabang = $model->id_cabang;
            $date_explode=explode(" - ",$model->created_at);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);

            $model = new LogFinance();
            return $this->render('lelang', [
            'model' => $model,
            'date1' => $date1,
            'date2' => $date2,
            'id_cabang' => $id_cabang
        ]);
        }
        else{

        return $this->render('lelang-filter',[
            'model' => $model,
        ]);
        }
    }

    public function actionReport($start,$end)
    {
        $model = new LogFinance();
        $model = $model->find()->where(['between','created_at',$start,$end])->all();

        return $this->renderPartial('report',[
            'model' => $model,
            'start' => $start,
            'end' => $end
        ]);
    }

    public function actionExportReportExcel($date1,$date2,$id_cabang){
        $model = new LogFinance();
        return $this->renderAjax('export-report-excel',[
            'date1' => $date1,
            'date2' => $date2,
            'id_cabang' => $id_cabang,
            'model' => $model
        ]);

    }

    public function actionLaporanGadai(){
        $model = new LogFinance();
        if ($model->load(Yii::$app->request->post())) {
            $date = $model->created_at;
            $id_cabang = $model->id_cabang;
            $date_explode=explode(" - ",$model->created_at);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);

            if($id_cabang == 1000000){
                $dataGadai = $model->find()->where(['between', 'created_at', $date1, $date2])->andWhere(['status' => 'Pinjaman'])->all();
            }
            else{
                $dataGadai = $model->find()->where(['between', 'created_at', $date1, $date2])->andWhere(['status' => 'Pinjaman', 'id_cabang' => $id_cabang])->all();
            }

            

            return $this->render('laporan-gadai',[
                'dataGadai' => $dataGadai,
                'date1' => $date1,
                'date2' => $date2,
                'cabang_id' => $id_cabang
            ]);

        }
        else{
            return $this->render('index-filter-baru',[
                'model' => $model,
            ]);
        }
    }

    public function actionExportLaporanGadai($cabang_id,$date1,$date2){
        $model = new LogFinance();
        if($cabang_id == 1000000){
            $dataGadai = $model->find()->where(['between', 'created_at', $date1, $date2])->andWhere(['status' => 'Pinjaman'])->all();
        }
        else{
            $dataGadai = $model->find()->where(['between', 'created_at', $date1, $date2])->andWhere(['status' => 'Pinjaman', 'id_cabang' => $cabang_id])->all();
        }

        return $this->renderAjax('export-laporan-gadai',[
            'dataGadai' => $dataGadai,
            'date1' => $date1,
            'date2' => $date2,
            'cabang_id' => $cabang_id
        ]);

    }

    public function actionLaporanPerpanjang(){
        $model = new LogFinance();
        if ($model->load(Yii::$app->request->post())) {
            $date = $model->created_at;
            $id_cabang = $model->id_cabang;
            $date_explode=explode(" - ",$model->created_at);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);
            
            if($id_cabang == 1000000){
                $dataPerpanjang = $model->find()->where(['between', 'created_at', $date1, $date2])->andWhere(['status' => 'BungaPerpanjang'])->all();
            }
            else{
                $dataPerpanjang = $model->find()->where(['between', 'created_at', $date1, $date2])->andWhere(['status' => 'BungaPerpanjang', 'id_cabang' => $id_cabang])->all();
            }

            return $this->render('laporan-perpanjang',[
                'dataPerpanjang' => $dataPerpanjang,
                'date1' => $date1,
                'date2' => $date2,
                'cabang_id' => $id_cabang
            ]);

        }
        else{
            return $this->render('index-filter-baru',[
                'model' => $model,
            ]);
        }
    }

    public function actionExportLaporanPerpanjang($cabang_id,$date1,$date2){
        $model = new LogFinance();
        
        if($cabang_id == 1000000){
            $dataPerpanjang = $model->find()->where(['between', 'created_at', $date1, $date2])->andWhere(['status' => 'BungaPerpanjang'])->all();
        }
        else{
            $dataPerpanjang = $model->find()->where(['between', 'created_at', $date1, $date2])->andWhere(['status' => 'BungaPerpanjang', 'id_cabang' => $cabang_id])->all();
        }

        return $this->renderAjax('export-laporan-perpanjang',[
            'dataPerpanjang' => $dataPerpanjang,
            'date1' => $date1,
            'date2' => $date2,
            'cabang_id' => $cabang_id
        ]);

    }

    public function actionLaporanPelunasan(){
        $model = new LogFinance();
        if ($model->load(Yii::$app->request->post())) {
            $date = $model->created_at;
            $id_cabang = $model->id_cabang;
            $date_explode=explode(" - ",$model->created_at);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);
            
            if($id_cabang == 1000000){
                $dataPelunasan = $model->find()->where(['between', 'created_at', $date1, $date2])->andWhere(['status' => 'Pelunasan'])->orderBy(['created_at' => SORT_ASC])->all();
            }
            else{
                $dataPelunasan = $model->find()->where(['between', 'created_at', $date1, $date2])->andWhere(['status' => 'Pelunasan', 'id_cabang' => $id_cabang])->orderBy(['created_at' => SORT_ASC])->all();
            }

            return $this->render('laporan-pelunasan',[
                'dataPelunasan' => $dataPelunasan,
                'date1' => $date1,
                'date2' => $date2,
                'cabang_id' => $id_cabang
            ]);

        }
        else{
            return $this->render('index-filter-baru',[
                'model' => $model,
            ]);
        }
    }

    public function actionExportLaporanPelunasan($cabang_id,$date1,$date2){
        $model = new LogFinance();
        
        if($cabang_id == 1000000){
            $dataPelunasan = $model->find()->where(['between', 'created_at', $date1, $date2])->andWhere(['status' => 'Pelunasan'])->orderBy(['created_at' => SORT_ASC])->all();
        }
        else{
            $dataPelunasan = $model->find()->where(['between', 'created_at', $date1, $date2])->andWhere(['status' => 'Pelunasan', 'id_cabang' => $cabang_id])->orderBy(['created_at' => SORT_ASC])->all();
        }

        return $this->renderAjax('export-laporan-pelunasan',[
            'dataPelunasan' => $dataPelunasan,
            'date1' => $date1,
            'date2' => $date2,
            'cabang_id' => $cabang_id
        ]);

    }

    public function actionLaporanAngsuran(){
        $model = new LogFinance();
        if ($model->load(Yii::$app->request->post())) {
            $date = $model->created_at;
            $id_cabang = $model->id_cabang;
            $date_explode=explode(" - ",$model->created_at);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);
           
            if($id_cabang == 1000000){
                $dataAngsuran = $model->find()->where(['between', 'created_at', $date1, $date2])->andWhere(['status' => 'Angsuran'])->all();
            }
            else{
                $dataAngsuran = $model->find()->where(['between', 'created_at', $date1, $date2])->andWhere(['status' => 'Angsuran', 'id_cabang' => $id_cabang])->all();
            }

            return $this->render('laporan-angsuran',[
                'dataAngsuran' => $dataAngsuran,
                'date1' => $date1,
                'date2' => $date2,
                'cabang_id' => $id_cabang
            ]);

        }
        else{
            return $this->render('index-filter-baru',[
                'model' => $model,
            ]);
        }
    }

    public function actionExportLaporanAngsuran($cabang_id,$date1,$date2){
        $model = new LogFinance();
        
        if($cabang_id == 1000000){
            $dataAngsuran = $model->find()->where(['between', 'created_at', $date1, $date2])->andWhere(['status' => 'Angsuran'])->all();
        }
        else{
            $dataAngsuran = $model->find()->where(['between', 'created_at', $date1, $date2])->andWhere(['status' => 'Angsuran', 'id_cabang' => $cabang_id])->all();
        }

        return $this->renderAjax('export-laporan-angsuran',[
            'dataAngsuran' => $dataAngsuran,
            'date1' => $date1,
            'date2' => $date2,
            'cabang_id' => $cabang_id
        ]);

    }

    /**
     * Displays a single LogFinance model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LogFinance model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LogFinance();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_log_finance]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LogFinance model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_log_finance]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LogFinance model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LogFinance model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogFinance the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogFinance::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
