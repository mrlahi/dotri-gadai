<?php

namespace backend\controllers;

use common\models\TaksiranGrade;
use Yii;
use common\models\TaksiranHarga;
use common\models\TaksiranHargaSearch;
use common\models\TypeBarang;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaksiranHargaController implements the CRUD actions for TaksiranHarga model.
 */
class TaksiranHargaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaksiranHarga models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaksiranHargaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaksiranHarga model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TaksiranHarga model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new TaksiranHarga();
        $taksiranAktif = $model->find()->where(['type_barang_id' => $id, 'is_active' => true])->one();

        $mTaksiranGrade = new TaksiranGrade();
        $dataType = TypeBarang::findOne($id);

        $model->merk_id = $dataType->merk_barang_id;
        $model->type_barang_id = $id;
        $model->tgl_aktif = date('Y-m-d');
        $model->created_at = date('Y-m-d');
        $model->created_by = Yii::$app->user->id;
        $model->is_active = 1;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $mTaksiranGrade->generateGrade($model->id);

            if($taksiranAktif){
                $taksiranAktif->tgl_non_aktif = date('Y-m-d');
                $taksiranAktif->is_active = 0;
                $taksiranAktif->save();
            }

            return $this->redirect(['/type-barang/view', 'id' => $id]);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TaksiranHarga model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TaksiranHarga model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TaksiranHarga model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaksiranHarga the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TaksiranHarga::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
