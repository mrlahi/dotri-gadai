<?php

namespace backend\controllers;

use Yii;
use common\models\KasUmum;
use common\models\KasUmumSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\LogFinance;
use common\models\Cabang;

/**
 * KasUmumController implements the CRUD actions for KasUmum model.
 */
class KasUmumController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all KasUmum models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KasUmumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KasUmum model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new KasUmum model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KasUmum();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_kas_umum]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionGenerateKas2(){
        $modelCabang = new Cabang();
        $listCabang = $modelCabang->find()->all();
        $date = date('Y-m-d');
        $date = date( 'Y-m-d', strtotime( $date . ' -1 day' ) );


        foreach($listCabang as $listCabangs){
            $id_cabang = $listCabangs->id_cabang;
            $model = new KasUmum();
            $modelLogFinance = new LogFinance();

            $dataCekKas = $model->find()->where(['id_cabang' => $id_cabang, 'created_at' => $date])->one();
            
            if($listCabangs->nama_cabang != 'Akun Bank'){

                $kas_masuk =  $modelLogFinance->find()->where(['id_cabang' => $id_cabang,'created_at' => $date,'type_pembayaran' => 'Cash'])->andWhere(['or',['status' => 'Modal'],['status' => 'Pelunasan'],['status' => 'Angsuran'],['status' => 'Bunga'],['status' => 'BungaPerpanjang'],['status' => 'DendaPelunasan'],['status' => 'DendaPerpanjang'],['status' => 'Admin'],['status' => 'KembaliBunga5Persen'],['status' => 'KembaliBunga1.2Persen'],['status' => 'Lelang']])->sum('jumlah_uang');

                $kas_keluar = $modelLogFinance->find()->where(['id_cabang' => $id_cabang,'created_at' => $date,'type_pembayaran' => 'Cash'])->andWhere(['or',['status' => 'Operasional'],['status' => 'Pinjaman'],['status' => 'Pengeluaran'],['status' => 'KembaliBunga5Persen'],['status' => 'KembaliBunga1.2Persen'],])->sum('jumlah_uang');

                if(empty($dataCekKas->id_kas_umum)){
                    $model->jumlah_uang_masuk = $kas_masuk;
                    $model->jumlah_uang_keluar = $kas_keluar;
                    $model->id_cabang = $id_cabang;
                    $model->created_at = $date;
                    $model->created_by = '1';
                    $model->save(false);

                }
                else{
                    $model->updateAll(['jumlah_uang_masuk' => $kas_masuk, 'jumlah_uang_keluar' => $kas_keluar],['id_kas_umum' => $dataCekKas->id_kas_umum]);
                }
            }
            else{
                $kas_masuk =  $modelLogFinance->find()->where(['created_at' => $date,'type_pembayaran' => 'Transfer'])->andWhere(['or',['status' => 'Modal'],['status' => 'Pelunasan'],['status' => 'Angsuran'],['status' => 'Bunga'],['status' => 'BungaPerpanjang'],['status' => 'DendaPelunasan'],['status' => 'DendaPerpanjang'],['status' => 'Admin'],['status' => 'KembaliBunga5Persen'],['status' => 'KembaliBunga1.2Persen'],['status' => 'Lelang']])->sum('jumlah_uang');

                $kas_keluar = $modelLogFinance->find()->where(['created_at' => $date,'type_pembayaran' => 'Transfer'])->andWhere(['or',['status' => 'Operasional'],['status' => 'Pinjaman'],['status' => 'Pengeluaran'],['status' => 'KembaliBunga5Persen'],['status' => 'KembaliBunga1.2Persen']])->sum('jumlah_uang');

                if(empty($dataCekKas->id_kas_umum)){
                    $model->jumlah_uang_masuk = $kas_masuk;
                    $model->jumlah_uang_keluar = $kas_keluar;
                    $model->id_cabang = $id_cabang;
                    $model->created_at = $date;
                    $model->created_by = '1';
                    $model->save(false);

                }
                else{
                    $model->updateAll(['jumlah_uang_masuk' => $kas_masuk, 'jumlah_uang_keluar' => $kas_keluar],['id_kas_umum' => $dataCekKas->id_kas_umum]);
                }
            }
        }

        return $this->redirect(['index']);
    }

    public function actionGenerateKas(){
        $modelCabang = new Cabang();

        $listCabang = $modelCabang->find()->all();
        foreach($listCabang as $listCabangs){
            $id_cabang = $listCabangs->id_cabang;
            $model = new KasUmum();
            $modelLogFinance = new LogFinance();

            $dataCekKas = $model->find()->where(['id_cabang' => $id_cabang, 'created_at' => date('Y-m-d')])->one();
            
            if($listCabangs->nama_cabang != 'Akun Bank'){

                $kas_masuk =  $modelLogFinance->find()->where(['id_cabang' => $id_cabang,'created_at' => date('Y-m-d'),'type_pembayaran' => 'Cash'])->andWhere(['or',['status' => 'Modal'],['status' => 'Pelunasan'],['status' => 'Angsuran'],['status' => 'Bunga'],['status' => 'BungaPerpanjang'],['status' => 'DendaPelunasan'],['status' => 'DendaPerpanjang'],['status' => 'Admin'],['status' => 'KembaliBunga5Persen'],['status' => 'KembaliBunga1.2Persen'],['status' => 'Lelang'], ['status' => 'BungaPelunasanPasif']])->sum('jumlah_uang');

                $kas_keluar = $modelLogFinance->find()->where(['id_cabang' => $id_cabang,'created_at' => date('Y-m-d'),'type_pembayaran' => 'Cash'])->andWhere(['or',['status' => 'Operasional'],['status' => 'Pinjaman'],['status' => 'Pengeluaran'],['status' => 'KembaliBunga5Persen'],['status' => 'KembaliBunga1.2Persen'],])->sum('jumlah_uang');

                if(empty($dataCekKas->id_kas_umum)){
                    $model->jumlah_uang_masuk = $kas_masuk;
                    $model->jumlah_uang_keluar = $kas_keluar;
                    $model->id_cabang = $id_cabang;
                    $model->created_at = date('Y-m-d');
                    $model->created_by = '1';
                    $model->save(false);

                }
                else{
                    $model->updateAll(['jumlah_uang_masuk' => $kas_masuk, 'jumlah_uang_keluar' => $kas_keluar],['id_kas_umum' => $dataCekKas->id_kas_umum]);
                }
            }
            else{
                $kas_masuk =  $modelLogFinance->find()->where(['created_at' => date('Y-m-d'),'type_pembayaran' => 'Transfer'])->andWhere(['or',['status' => 'Modal'],['status' => 'Pelunasan'],['status' => 'Angsuran'],['status' => 'Bunga'],['status' => 'BungaPerpanjang'],['status' => 'DendaPelunasan'],['status' => 'DendaPerpanjang'],['status' => 'Admin'],['status' => 'KembaliBunga5Persen'],['status' => 'KembaliBunga1.2Persen'],['status' => 'Lelang']])->sum('jumlah_uang');

                $kas_keluar = $modelLogFinance->find()->where(['created_at' => date('Y-m-d'),'type_pembayaran' => 'Transfer'])->andWhere(['or',['status' => 'Operasional'],['status' => 'Pinjaman'],['status' => 'Pengeluaran'],['status' => 'KembaliBunga5Persen'],['status' => 'KembaliBunga1.2Persen']])->sum('jumlah_uang');

                if(empty($dataCekKas->id_kas_umum)){
                    $model->jumlah_uang_masuk = $kas_masuk;
                    $model->jumlah_uang_keluar = $kas_keluar;
                    $model->id_cabang = $id_cabang;
                    $model->created_at = date('Y-m-d');
                    $model->created_by = '1';
                    $model->save(false);

                }
                else{
                    $model->updateAll(['jumlah_uang_masuk' => $kas_masuk, 'jumlah_uang_keluar' => $kas_keluar],['id_kas_umum' => $dataCekKas->id_kas_umum]);
                }
            }
        }

        return $this->redirect(['index']);
    }


    public function actionGenerateKasAll($first, $last, $step = '+1 day', $format = 'Y-m-d' ){
        $dates = [];
        $current = strtotime( $first );
        $last = strtotime( $last );

        while( $current <= $last ) {
            $tgl_now = date( $format, $current );
            $current = strtotime( $step, $current );
            
            $modelCabang = new Cabang();

            $listCabang = $modelCabang->find()->all();
            foreach($listCabang as $listCabangs){
                $id_cabang = $listCabangs->id_cabang;
                $model = new KasUmum();
                $modelLogFinance = new LogFinance();
    
                $dataCekKas = $model->find()->where(['id_cabang' => $id_cabang, 'created_at' => $tgl_now])->one();
                
                if($listCabangs->nama_cabang != 'Akun Bank'){
    
                    $kas_masuk =  $modelLogFinance->find()->where(['id_cabang' => $id_cabang,'created_at' => $tgl_now,'type_pembayaran' => 'Cash'])->andWhere(['or',['status' => 'Modal'],['status' => 'Pelunasan'],['status' => 'Angsuran'],['status' => 'Bunga'],['status' => 'BungaPerpanjang'],['status' => 'DendaPelunasan'],['status' => 'DendaPerpanjang'],['status' => 'Admin'],['status' => 'KembaliBunga5Persen'],['status' => 'KembaliBunga1.2Persen'],['status' => 'Lelang']])->sum('jumlah_uang');
    
                    $kas_keluar = $modelLogFinance->find()->where(['id_cabang' => $id_cabang,'created_at' => $tgl_now,'type_pembayaran' => 'Cash'])->andWhere(['or',['status' => 'Operasional'],['status' => 'Pinjaman'],['status' => 'Pengeluaran'],['status' => 'KembaliBunga5Persen'],['status' => 'KembaliBunga1.2Persen']])->sum('jumlah_uang');
    
                    if(empty($dataCekKas->id_kas_umum)){
                        $model->jumlah_uang_masuk = $kas_masuk;
                        $model->jumlah_uang_keluar = $kas_keluar;
                        $model->id_cabang = $id_cabang;
                        $model->created_at = $tgl_now;
                        $model->created_by = Yii::$app->user->identity->id;
                        $model->save(false);
    
                    }
                    else{
                        $model->updateAll(['jumlah_uang_masuk' => $kas_masuk, 'jumlah_uang_keluar' => $kas_keluar],['id_kas_umum' => $dataCekKas->id_kas_umum]);
                    }
                }
                else{
                    $kas_masuk =  $modelLogFinance->find()->where(['created_at' => $tgl_now,'type_pembayaran' => 'Transfer'])->andWhere(['or',['status' => 'Modal'],['status' => 'Pelunasan'],['status' => 'Angsuran'],['status' => 'Bunga'],['status' => 'BungaPerpanjang'],['status' => 'DendaPelunasan'],['status' => 'DendaPerpanjang'],['status' => 'Admin'],['status' => 'KembaliBunga5Persen'],['status' => 'KembaliBunga1.2Persen'],['status' => 'Lelang']])->sum('jumlah_uang');
    
                    $kas_keluar = $modelLogFinance->find()->where(['created_at' => $tgl_now,'type_pembayaran' => 'Transfer'])->andWhere(['or',['status' => 'Operasional'],['status' => 'Pinjaman'],['status' => 'Pengeluaran'],['status' => 'KembaliBunga5Persen'],['status' => 'KembaliBunga1.2Persen']])->sum('jumlah_uang');
    
                    if(empty($dataCekKas->id_kas_umum)){
                        $model->jumlah_uang_masuk = $kas_masuk;
                        $model->jumlah_uang_keluar = $kas_keluar;
                        $model->id_cabang = $id_cabang;
                        $model->created_at = $tgl_now;
                        $model->created_by = Yii::$app->user->identity->id;
                        $model->save(false);
    
                    }
                    else{
                        $model->updateAll(['jumlah_uang_masuk' => $kas_masuk, 'jumlah_uang_keluar' => $kas_keluar],['id_kas_umum' => $dataCekKas->id_kas_umum]);
                    }
                }
            }
        }

        //return $dates;
    }

    /**
     * Updates an existing KasUmum model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_kas_umum]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing KasUmum model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KasUmum model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KasUmum the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KasUmum::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
