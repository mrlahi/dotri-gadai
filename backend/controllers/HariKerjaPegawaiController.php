<?php

namespace backend\controllers;

use common\models\HariKerja;
use Yii;
use common\models\HariKerjaPegawai;
use common\models\HariKerjaPegawaiSearch;
use common\models\Profile;
use common\models\ProfileSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * HariKerjaPegawaiController implements the CRUD actions for HariKerjaPegawai model.
 */
class HariKerjaPegawaiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all HariKerjaPegawai models.
     * @return mixed
     */
    public function actionHariKerja($id)
    {
        $searchModel = new HariKerjaPegawaiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['pegawai_id' => $id]);

        $dataPegawai = Profile::findOne($id);

        if(Yii::$app->request->post('hasEditable')){
            $hariId = Yii::$app->request->post('editableKey');
            $hari = HariKerjaPegawai::findOne($hariId);

            $post = [];
            $out = Json::encode(['output' => '', 'message' => '']);

            $posted = current($_POST['HariKerjaPegawai']);
            $post['HariKerjaPegawai'] = $posted;

            if($hari->load($post)){
                $hari->save();
                $output = '';
                $out = Json::encode(['output' => $output, 'message' => '']);
            }
            return $out;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataPegawai' => $dataPegawai
        ]);
    }

    public function actionIndexPegawai()
    {
        $searchModel = new ProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['<>','akses', 'defaultNasabah']);

        return $this->render('index-pegawai', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HariKerjaPegawai model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new HariKerjaPegawai model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($pegawai_id)
    {
        $model = new HariKerjaPegawai();

        if ($model->load(Yii::$app->request->post())) {

            $shift_id = $model->shift_id;

            $date_explode=explode(" - ",$model->range_tgl_kerja);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);
            $dataHari = HariKerja::find()->where(['between', 'tgl_kerja' , $date1, $date2])->all();

            foreach($dataHari as $hari){

                $mKerja = new HariKerjaPegawai();
                $mKerja->pegawai_id = $pegawai_id;
                $mKerja->hari_kerja_id = $hari->id;
                $mKerja->shift_id = $shift_id;
                $mKerja->status_absensi = $hari->status_hari;
                $mKerja->created_at = date('Y-m-d');
                $mKerja->created_by = Yii::$app->user->id;
                $mKerja->save();
            }

            return $this->redirect(['hari-kerja', 'id' => $pegawai_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing HariKerjaPegawai model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing HariKerjaPegawai model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HariKerjaPegawai model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HariKerjaPegawai the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HariKerjaPegawai::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
