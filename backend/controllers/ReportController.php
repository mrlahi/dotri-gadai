<?php

namespace backend\controllers;

use Yii;
use backend\models\BarangExt;
use backend\models\BarangExtSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Bayar;

/**
 * ReportController implements the CRUD actions for BarangExt model.
 */
class ReportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BarangExt models.
     * @return mixed
     */
    public function actionIndex($start,$end)
    {
        $searchModel = new BarangExtSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['between','tgl_masuk',$start,$end]);

        $modelBayar = new Bayar();

        $judul_file = "Data Outstanding (".date_format(date_create($start),'dmY')."-".date_format(date_create($end),'dmY').")";

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelBayar' => $modelBayar,
            'judulFile' => $judul_file
        ]);
    }

    /**
     * Lists all BarangExt models.
     * @return mixed
     */
    public function actionFilter()
    {
        $model = new BarangExt();

        if ($model->load(Yii::$app->request->post())) {

            $model->tgl_masuk;
            $date_explode=explode(" - ",$model->tgl_masuk);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);
            //$query->andFilterWhere(['between','only_date',$date1,$date2]);

            return $this->redirect(['index', 'start' => $date1, 'end' => $date2]);
        }
        else{

        return $this->render('index-filter',[
            'model' => $model,
        ]);
        }
    }

    /**
     * Displays a single BarangExt model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BarangExt model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BarangExt();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_barang]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing BarangExt model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_barang]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BarangExt model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BarangExt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BarangExt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BarangExt::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
