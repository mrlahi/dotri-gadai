<?php

namespace backend\controllers;

use Yii;
use common\models\LogStatusTemp;
use common\models\LogStatusTempSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\LogStatusExt;
use common\models\OutstandingManager;

/**
 * LogStatusTempController implements the CRUD actions for LogStatusTemp model.
 */
class LogStatusTempController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LogStatusTemp models.
     * @return mixed
     */
    public function actionIndex($outstanding_id)
    {
        $searchModel = new LogStatusTempSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['outstanding_id' => $outstanding_id]);

        $modelOutstanding = new OutstandingManager();
        $dataOutstanding = $modelOutstanding->findOne($outstanding_id);
        $start = $dataOutstanding->tgl_start;
        $end = $dataOutstanding->tgl_end;

        $judul_file = "Data Outstanding (".date_format(date_create($start),'dmY')."-".date_format(date_create($end),'dmY').")";

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'judulFile' => $judul_file,
            'start' => $start,
            'end' => $end
        ]);
    }

    public function actionFilter()
    {
        $model = new LogStatusExt();

        if ($model->load(Yii::$app->request->post())) {

            $model->created_at;
            $date_explode=explode(" - ",$model->created_at);
            $date1=trim($date_explode[0]);
            $date2=trim($date_explode[1]);
            //$query->andFilterWhere(['between','only_date',$date1,$date2]);

            return $this->redirect(['log-temp', 'start' => $date1, 'end' => $date2]);
        }
        else{

        return $this->render('index-filter',[
            'model' => $model,
        ]);
        }
    }

    public function actionLogTemp($start,$end)
    {
        $model = new LogStatusExt;
        $modelLogStatusTemp = new LogStatusTemp();

        $model = $model->find()->where(['between','created_at',$start,$end])->groupBy('id_barang')->all();

        $id_user = Yii::$app->user->identity->id;
        $modelLogStatusTemp->deleteAll();

        foreach ($model as $models){
            $modelLogStatusBaru = new LogStatusExt();
            $modelLogStatusTemp = new LogStatusTemp();
            
            $id_barang = $models->id_barang;
            $modelLogStatusBaru = $modelLogStatusBaru->find()->where(['id_barang' => $id_barang])->andWhere(['between','created_at',$start,$end])->orderBy(['id_status' => SORT_DESC])->one();

            if($modelLogStatusBaru->status != "Lunas" AND $modelLogStatusBaru->status != "Terjual"){
                $modelLogStatusTemp->id_status_temp = $modelLogStatusBaru->id_status;
                $modelLogStatusTemp->id_barang = $modelLogStatusBaru->id_barang;
                $modelLogStatusTemp->status = $modelLogStatusBaru->status;
                $modelLogStatusTemp->created_at = $modelLogStatusBaru->created_at;
                $modelLogStatusTemp->created_by = $id_user;
                $modelLogStatusTemp->save(false);
            }
        }
        return $this->redirect(['/log-status-temp/', 'start' => $start, 'end' => $end]);
    }

    /**
     * Displays a single LogStatusTemp model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LogStatusTemp model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LogStatusTemp();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_status_temp]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LogStatusTemp model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_status_temp]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LogStatusTemp model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LogStatusTemp model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogStatusTemp the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogStatusTemp::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
