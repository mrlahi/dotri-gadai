<?php

namespace backend\controllers;

use common\models\Inventory;
use common\models\LogJurnal;
use Yii;
use common\models\LogPenyusutanAset;
use common\models\LogPenyusutanAsetSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;

/**
 * LogPenyusutanAsetController implements the CRUD actions for LogPenyusutanAset model.
 */
class LogPenyusutanAsetController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LogPenyusutanAset models.
     * @return mixed
     */
    public function actionIndex($inventaris_id)
    {
        $searchModel = new LogPenyusutanAsetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['inventaris_id' => $inventaris_id]);

        $dataInventaris = Inventory::findOne($inventaris_id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataInventaris' => $dataInventaris
        ]);
    }

    public function actionSusutkan($id){
        $mLogJurnal = new LogJurnal();
        $mLogJurnal->inPenyusutanInventory($id);

        LogPenyusutanAset::updateAll(['is_penyusutan' => 1], ['id' => $id]);

        $dataLog = LogPenyusutanAset::findOne($id);

        Yii::$app->getSession()->setFlash('success', [
            'type' => 'success',
            'duration' => 12000,
            'icon' => 'fa fa-like',
            'message' => Yii::t('app',Html::encode('Penyusutan Berhasil.')),
            'title' => Yii::t('app', Html::encode('Penyusutan Barang')),
            'positonY' => 'top',
            'positonX' => 'right'
        ]);
        return $this->redirect(['index', 'inventaris_id' => $dataLog->inventaris->id]);
    }

    /**
     * Displays a single LogPenyusutanAset model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LogPenyusutanAset model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LogPenyusutanAset();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LogPenyusutanAset model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LogPenyusutanAset model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LogPenyusutanAset model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogPenyusutanAset the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogPenyusutanAset::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
