<?php

namespace backend\controllers;

use common\models\LogJurnal;
use Yii;
use common\models\LogTransaksi;
use common\models\LogTransaksiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LogTransaksiController implements the CRUD actions for LogTransaksi model.
 */
class LogTransaksiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-jurnal-penyesuaian' => ['POST'],
                    'delete-mutasi' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LogTransaksi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogTransaksiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexPenyesuaian()
    {
        $searchModel = new LogTransaksiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['jenis_transaksi' => 'PENYESUAIAN']);

        return $this->render('index-penyesuaian', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexMutasi()
    {
        $searchModel = new LogTransaksiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['jenis_transaksi' => 'MUTASI KAS']);

        return $this->render('index-mutasi', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionJurnalPenyesuaian($id){
        $model = new LogTransaksi();
        $date = date('Y-m-d');
        $dataTransaksi = $model->find()->where(['id_log_transaksi' => $id])->one();

        return $this->render('format-jurnal-penyesuaian', [
            'date' => $date,
            'model' => $model,
            'dataTransaksi' => $dataTransaksi,
        ]);

    }

    public function actionJurnal(){
        $model = new LogTransaksi();

        if ($model->load(Yii::$app->request->post())) {
            $date = explode(' - ',$model->created_at);
            $start = $date[0];
            $end = $date[1];

            $dataTransaksi = $model->find()->where(['between', 'created_at', $start, $end])->all();
            $model->created_at = "";

            return $this->render('format-jurnal', [
                'start' => $start,
                'end' => $end,
                'model' => $model,
                'dataTransaksi' => $dataTransaksi,
            ]);
        }
        else{
            $start = date('Y-m-d');
            $end = date('Y-m-d');
            $dataTransaksi = $model->find()->where(['between', 'created_at', $start, $end])->all();

            return $this->render('format-jurnal', [
                'start' => $start,
                'end' => $end,
                'model' => $model,
                'dataTransaksi' => $dataTransaksi,
            ]);
        }

    }

    public function actionExportJurnal($start,$end){
        $model = new LogTransaksi();
        $dataTransaksi = $model->find()->where(['between', 'created_at', $start, $end])->all();

        return $this->renderAjax('export-jurnal', [
            'start' => $start,
            'end' => $end,
            'model' => $model,
            'dataTransaksi' => $dataTransaksi,
        ]);
    }

    /**
     * Displays a single LogTransaksi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LogTransaksi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LogTransaksi();

        $model->scenario = "penyesuaian";
        $model->jenis_transaksi = "PENYESUAIAN";
        $model->created_at = date('Y-m-d');
        $model->created_by = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['jurnal-penyesuaian', 'id' => $model->id_log_transaksi]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LogTransaksi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = "penyesuaian";

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['jurnal-penyesuaian', 'id' => $model->id_log_transaksi]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LogTransaksi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeletePenyesuaian($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index-penyesuaian']);
    }

    public function actionDeleteMutasi($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index-mutasi']);
    }

    public function actionDeleteJurnalPenyesuaian($id,$transaksi_id)
    {
        LogJurnal::deleteAll(['id' => $id]);

        return $this->redirect(['jurnal-penyesuaian', 'id' => $transaksi_id]);
    }

    /**
     * Finds the LogTransaksi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogTransaksi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogTransaksi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
