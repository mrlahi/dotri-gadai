<?php
namespace backend\controllers;

use common\models\AksesKhusus;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\Profile;
use common\models\AuthAssignment;
use common\models\Barang;
use common\models\Cabang;
use common\models\LogModal;
use common\models\Cicil;
use common\models\Bayar;
use common\models\LogFinance;
use common\models\LoginToken;
use yii\helpers\Html;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(empty(Yii::$app->session['id_cabang'])){
            $modelProfile = new Profile();
            $modelCabang = new Cabang();
            $session = Yii::$app->session;
            $profile = $modelProfile->find()->where(['id_user' => Yii::$app->user->identity->id])->one();
            $modelCabang = $modelCabang->find()->where(['id_cabang' => $profile->id_cabang])->one();
    
            $session->set('nama', $profile->nama);
            $session->set('id_cabang', $profile->id_cabang);
            $session->set('nama_cabang', $modelCabang->nama_cabang);
        }
        

        $modelBarang = new Barang();
        $modelLogFinance = new LogFinance();
        $modelProfile = new Profile();
        $modelCabang = new Cabang();

        return $this->render('index',[
            'modelBarang' => $modelBarang,
            'modelLogFinance' => $modelLogFinance,
            'modelProfile' => $modelProfile,
            'modelCabang' => $modelCabang,
            'date' => date('Y-m-d'),
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $modelAuth = new AuthAssignment();
        $modelProfile = new Profile();
        $modelCabang = new Cabang();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $session = Yii::$app->session;
            $profile = $modelProfile->find()->where(['id_user' => Yii::$app->user->identity->id])->one();
            
            $modelCabang = $modelCabang->find()->where(['id_cabang' => $profile->id_cabang])->one();
            $session->set('nama', $profile->nama);
            $session->set('id_cabang', $profile->id_cabang);
            $session->set('nama_cabang', $modelCabang->nama_cabang);
            
            Yii::$app->getSession()->setFlash('success', [
                'type' => 'info',
                'duration' => 12000,
                'icon' => 'fa fa-like',
                'message' => Yii::t('app',Html::encode('Selamat Datang.')),
                'title' => Yii::t('app', Html::encode('Login Berhasil')),
                'positonY' => 'top',
                'positonX' => 'right',
                'justLogin' => TRUE
            ]);

            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
