<?php

namespace backend\modules\api\controllers;

use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;

class CheckInOutController extends ActiveController
{
    public $modelClass = 'common\models\CheckInOut';

    public function behaviors()

    {
        return [
            [
                'class' => \yii\filters\ContentNegotiator::className(),
                'only' => ['index', 'view'],
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actions()
    {
        $actions = parent::actions();

        return ArrayHelper::merge(parent::actions(), [
            'index' => [
                'pagination' => [
                    'pageSize' => false,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => SORT_DESC,
                    ],
                ],
                'dataFilter' => [
                    'class' => \yii\data\ActiveDataFilter::class,
                    'searchModel' => $this->modelClass,
                ],
            ],
        ]);

        return $actions;
    }

}

?>