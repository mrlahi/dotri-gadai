<?php

namespace console\controllers;
use yii\console\Controller;
use Yii;
use common\models\LogJurnal;


class FixCashbackController extends Controller
{

    public function actionIndex()
    {
        $dataJurnal = LogJurnal::find()->where(['keterangan' => 'CASHBACK PELUNASAN'])->all();
        foreach($dataJurnal as $jurnals){
            $bunga = $jurnals->barang->bunga. "\n";
            if($bunga == 10){
                $noAkun = 4013;
                $keterangan = "CASHBACK 5%"; 
            }
            else{
                $noAkun = 4014;
                $keterangan = "CASHBACK 1.2%";
            }
            $jurnal = new LogJurnal();
            $jurnal->updateAll(['akun_kode' => $noAkun, 'keterangan' => $keterangan],['id' => $jurnals->id]);
        }
    }

}
