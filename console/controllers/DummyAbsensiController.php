<?php
namespace console\controllers;

use common\models\CheckInOut;
use Yii;


class DummyAbsensiController extends \yii\console\Controller
{
    public function actionIndex($month,$year)
    {
        $days = cal_days_in_month( 0, $month, $year);

        $start = 1;
        while($start <= $days){
            if($start <= 15){
                $menit = rand(0,10);
                $jam_masuk = '08:'.$menit;
                $menit = rand(0,10);
                $jam_pulang = '16:'.$menit;
            }
            else{
                $menit = rand(0,10);
                $jam_masuk = '15:'.$menit;
                $menit = rand(0,10);
                $jam_pulang = '22:'.$menit;
            }
            
            $tgl = $year."-".$month."-".$start;

            $model = new CheckInOut();

            $model->absen_id = '2718';
            $model->tgl_absen = $tgl;
            $model->jam_absen = $jam_masuk;
            $model->cabang_id = "DOT1";
            $model->created_at = date('Y-m-d');
            $model->save();

            $model = new CheckInOut();

            $model->absen_id = '2718';
            $model->tgl_absen = $tgl;
            $model->jam_absen = $jam_pulang;
            $model->cabang_id = "DOT1";
            $model->created_at = date('Y-m-d');
            $model->save();

            $start++;
        }
    }
} 