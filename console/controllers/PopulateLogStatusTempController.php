<?php

namespace console\controllers;
use yii\console\Controller;
use Yii;
use backend\models\LogStatusExt;
use common\models\LogStatusTemp;
use common\models\OutstandingManager;


class PopulateLogStatusTempController extends Controller
{

    public function actionIndex()
    {   

        $modelOutstanding = new OutstandingManager();
        $dataOutstanding = $modelOutstanding->find()->where(['status_generator' => 'ONPROCESS'])->one();

        if(!empty($dataOutstanding)){

            $start = $dataOutstanding->tgl_start;
            $end = $dataOutstanding->tgl_end;
            $id_user = $dataOutstanding->created_by;
            $id_oustanding = $dataOutstanding->id_outstanding_manager;

            $model = new LogStatusExt;
            $modelLogStatusTemp = new LogStatusTemp();

            $model = $model->find()->where(['between','created_at',$start,$end])->groupBy('id_barang')->all();

            foreach ($model as $models){
                $modelLogStatusBaru = new LogStatusExt();
                $modelLogStatusTemp = new LogStatusTemp();
                
                $id_barang = $models->id_barang;
                $modelLogStatusBaru = $modelLogStatusBaru->find()->where(['id_barang' => $id_barang])->andWhere(['between','created_at',$start,$end])->orderBy(['id_status' => SORT_DESC])->one();

                if($modelLogStatusBaru->status != "Lunas" AND $modelLogStatusBaru->status != "Terjual"){
                    $modelLogStatusTemp->id_barang = $modelLogStatusBaru->id_barang;
                    $modelLogStatusTemp->status = $modelLogStatusBaru->status;
                    $modelLogStatusTemp->created_at = $modelLogStatusBaru->created_at;
                    $modelLogStatusTemp->created_by = $id_user;
                    $modelLogStatusTemp->outstanding_id = $id_oustanding;
                    $modelLogStatusTemp->save(false);

                    echo "Success for ".$modelLogStatusBaru->id_barang."\n";
                }
            }

            $modelOutstanding->updateAll(['status_generator' => 'EXPORTING'],['id_outstanding_manager' => $id_oustanding]);

        }
    }

    public function actionTest(){
        echo "Halo - halo";
    }

}
