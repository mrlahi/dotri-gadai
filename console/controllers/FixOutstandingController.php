<?php
namespace console\controllers;
use yii\console\Controller;
use Yii;

use yii2tech\spreadsheet\Spreadsheet;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use common\models\LogStatusTemp;
use common\models\LogStatusTempSearch;
use common\models\OutstandingManager;

class FixOutstandingController extends Controller{

    public function actionIndex(){

            $start = "2022-01-01";
            $end = "2022-02-28";

            $data = LogStatusTemp::find()->where(['outstanding_id' => 34])->all();
            $total = 0;
            foreach ($data as $datas){
                $sisaPinjam = $datas->sisaPinjam($datas->id_barang,$start,$end);
                $nilaiPinjam = $datas->barang->nilai_pinjam;
                $total += $nilaiPinjam;
                echo $nilaiPinjam."\n";
            }

            echo "=============="."\n";
            echo $total;
            // $exporter = new Spreadsheet([
            //     'dataProvider' => new ActiveDataProvider([
            //         'query' => LogStatusTemp::find()->where(['outstanding_id' => $outstanding_id]),
            //     ]),
            //     'columns' => [
            //         [
            //             'label' => 'No Kontrak',
            //             'value' => 'barang.no_kontrak'
            //         ],
            //         [
            //             'label' => 'Outlet',
            //             'value' => 'cabang.nama_cabang'
            //         ],
            //         [
            //             'label' => 'Tipe SBG',
            //             'value' => 'jenisBarang.nama_barang'
            //         ],
            //         [
            //             'label' => 'Tgl SBG',
            //             'value' => 'barang.tgl_masuk',
            //             'format' => 'date'
            //         ],
            //         [
            //             'label' => 'Jth Tempo',
            //             'value' => 'barang.jatuh_tempo',
            //             'format' => 'date'
            //         ],
            //         [
            //             'label' => 'Nasabah',
            //             'value' => 'nasabah.nama'
            //         ],
            //         [
            //             'label' => 'Nama Barang',
            //             'value' => 'barang.nama_barang'
            //         ],
            //         [
            //             'label' => 'Merk',
            //             'value' => 'barang.merk'
            //         ],
            //         [
            //             'label' => 'Tipe Barang',
            //             'value' => 'barang.tipe'
            //         ],
            //         [
            //             'label' => 'Kelengkapan',
            //             'value' => 'barang.kelengkapan'
            //         ],
            //         [
            //             'label' => 'Harga Taksir',
            //             'value' => 'barang.harga_taksir'
            //         ],
            //         [
            //             'label' => 'Nilai Pinjam',
            //             'value' => 'barang.nilai_pinjam'
            //         ],
            //         [
            //             'label' => 'Sisa Pinjam',
            //             'value' => function ($model) use ($start, $end) {
            //                 $id_barang = $model->id_barang;
            //                 $sisaPinjam = $model->sisaPinjam($id_barang,$start,$end);
            //                 return $sisaPinjam;
            //             },
            //         ],
            //         [
            //             'label' => 'Tgl Pelunasan',
            //             'value' => 'tglLunas',
            //             'format' => 'date'
            //         ],
            //         [
            //             'label' => 'Status',
            //             'value' => 'barang.status'
            //         ],
            //         [
            //             'label' => ' ',
            //             'value' => function(){ return "OUTSTANDING"; },
            //             'format' => 'raw'
            //         ]
            //     ],
            // ]);

            
    }

}