<?php

namespace console\controllers;
use yii\console\Controller;
use common\models\LogJurnal;
use common\models\LogTransaksi;

class FixDendaPelunasanController extends Controller
{

    public function actionIndex()
    {   
       $dataPelunasan = LogTransaksi::find()->where(['jenis_transaksi' => 'PELUNASAN'])->all();
       foreach($dataPelunasan as $transaksi){
        $dataKas = $transaksi->getLogJurnals()->where(['akun_kode' => 1101])->one();
        $dataPinjaman = $transaksi->getLogJurnals()->where(['akun_kode' => 1106])->one();

        if($dataKas and $dataPinjaman){
            $idKas = $dataKas->id;
            $idPinjam = $dataPinjaman->id;

            $debet = $dataKas->debet;
            $kredit = $dataPinjaman->kredit;

            if($debet < $kredit){
                LogJurnal::updateAll(['debet' => $dataPinjaman->kredit],['id' => $idKas]);
                LogJurnal::updateAll(['kredit' => $dataKas->debet],['id' => $idPinjam]);
            }
        }

       }
    }

    public function actionFixBri()
    {   
       $dataPelunasan = LogTransaksi::find()->where(['jenis_transaksi' => 'PELUNASAN'])->all();
       foreach($dataPelunasan as $transaksi){
        $dataKas = $transaksi->getLogJurnals()->where(['akun_kode' => 1103])->one();
        $dataPinjaman = $transaksi->getLogJurnals()->where(['akun_kode' => 1106])->one();

        if($dataKas and $dataPinjaman){
            $idKas = $dataKas->id;
            $idPinjam = $dataPinjaman->id;

            $debet = $dataKas->debet;
            $kredit = $dataPinjaman->kredit;

            if($debet < $kredit){
                LogJurnal::updateAll(['debet' => $dataPinjaman->kredit],['id' => $idKas]);
                LogJurnal::updateAll(['kredit' => $dataKas->debet],['id' => $idPinjam]);
            }
        }

       }
    }

    public function actionFixMandiri()
    {   
       $dataPelunasan = LogTransaksi::find()->where(['jenis_transaksi' => 'PELUNASAN'])->all();
       foreach($dataPelunasan as $transaksi){
        $dataKas = $transaksi->getLogJurnals()->where(['akun_kode' => 1104])->one();
        $dataPinjaman = $transaksi->getLogJurnals()->where(['akun_kode' => 1106])->one();

        if($dataKas and $dataPinjaman){
            $idKas = $dataKas->id;
            $idPinjam = $dataPinjaman->id;

            $debet = $dataKas->debet;
            $kredit = $dataPinjaman->kredit;

            if($debet < $kredit){
                LogJurnal::updateAll(['debet' => $dataPinjaman->kredit],['id' => $idKas]);
                LogJurnal::updateAll(['kredit' => $dataKas->debet],['id' => $idPinjam]);
            }
        }

       }
    }

    public function actionFixCashback()
    {   
       $dataPelunasan = LogTransaksi::find()->where(['jenis_transaksi' => 'PELUNASAN'])->all();
       foreach($dataPelunasan as $transaksi){
        $dataKas = $transaksi->getLogJurnals()->where(['akun_kode' => 1101])->one();
        $dataPinjaman = $transaksi->getLogJurnals()->where(['akun_kode' => 1106])->one();
        $dataCashback = $transaksi->getLogJurnals()->where(['akun_kode' => 4013])->one();

        if($dataKas and $dataPinjaman and $dataCashback){
            $idKas = $dataKas->id;
            $idPinjam = $dataPinjaman->id;

            $debet = $dataKas->debet;
            $kredit = $dataPinjaman->kredit;

            if($debet > $kredit){
                LogJurnal::updateAll(['debet' => $dataPinjaman->kredit],['id' => $idKas]);
                LogJurnal::updateAll(['kredit' => $dataKas->debet],['id' => $idPinjam]);
            }
        }

       }
    }

    public function actionFixCashback2() //UNTUK 1.2%
    {   
       $dataPelunasan = LogTransaksi::find()->where(['jenis_transaksi' => 'PELUNASAN'])->all();
       foreach($dataPelunasan as $transaksi){
        $dataKas = $transaksi->getLogJurnals()->where(['akun_kode' => 1101])->one();
        $dataPinjaman = $transaksi->getLogJurnals()->where(['akun_kode' => 1106])->one();
        $dataCashback = $transaksi->getLogJurnals()->where(['akun_kode' => 4014])->one();

        if($dataKas and $dataPinjaman and $dataCashback){
            $idKas = $dataKas->id;
            $idPinjam = $dataPinjaman->id;

            $debet = $dataKas->debet;
            $kredit = $dataPinjaman->kredit;

            if($debet > $kredit){
                LogJurnal::updateAll(['debet' => $dataPinjaman->kredit],['id' => $idKas]);
                LogJurnal::updateAll(['kredit' => $dataKas->debet],['id' => $idPinjam]);
            }
        }

       }
    }

    public function actionFixCashbackBri()
    {   
       $dataPelunasan = LogTransaksi::find()->where(['jenis_transaksi' => 'PELUNASAN'])->all();
       foreach($dataPelunasan as $transaksi){
        $dataKas = $transaksi->getLogJurnals()->where(['akun_kode' => 1103])->one();
        $dataPinjaman = $transaksi->getLogJurnals()->where(['akun_kode' => 1106])->one();
        $dataCashback = $transaksi->getLogJurnals()->where(['akun_kode' => 4013])->one();

        if($dataKas and $dataPinjaman and $dataCashback){
            $idKas = $dataKas->id;
            $idPinjam = $dataPinjaman->id;

            $debet = $dataKas->debet;
            $kredit = $dataPinjaman->kredit;

            if($debet > $kredit){
                LogJurnal::updateAll(['debet' => $dataPinjaman->kredit],['id' => $idKas]);
                LogJurnal::updateAll(['kredit' => $dataKas->debet],['id' => $idPinjam]);
            }
        }

       }
    }

    public function actionFixCashbackMandiri()
    {   
       $dataPelunasan = LogTransaksi::find()->where(['jenis_transaksi' => 'PELUNASAN'])->all();
       foreach($dataPelunasan as $transaksi){
        $dataKas = $transaksi->getLogJurnals()->where(['akun_kode' => 1104])->one();
        $dataPinjaman = $transaksi->getLogJurnals()->where(['akun_kode' => 1106])->one();
        $dataCashback = $transaksi->getLogJurnals()->where(['akun_kode' => 4013])->one();

        if($dataKas and $dataPinjaman and $dataCashback){
            $idKas = $dataKas->id;
            $idPinjam = $dataPinjaman->id;

            $debet = $dataKas->debet;
            $kredit = $dataPinjaman->kredit;

            if($debet > $kredit){
                LogJurnal::updateAll(['debet' => $dataPinjaman->kredit],['id' => $idKas]);
                LogJurnal::updateAll(['kredit' => $dataKas->debet],['id' => $idPinjam]);
            }
        }

       }
    }

    public function actionFixCashback2Bri() //UNTUK 1.2%
    {   
       $dataPelunasan = LogTransaksi::find()->where(['jenis_transaksi' => 'PELUNASAN'])->all();
       foreach($dataPelunasan as $transaksi){
        $dataKas = $transaksi->getLogJurnals()->where(['akun_kode' => 1103])->one();
        $dataPinjaman = $transaksi->getLogJurnals()->where(['akun_kode' => 1106])->one();
        $dataCashback = $transaksi->getLogJurnals()->where(['akun_kode' => 4014])->one();

        if($dataKas and $dataPinjaman and $dataCashback){
            $idKas = $dataKas->id;
            $idPinjam = $dataPinjaman->id;

            $debet = $dataKas->debet;
            $kredit = $dataPinjaman->kredit;

            if($debet > $kredit){
                LogJurnal::updateAll(['debet' => $dataPinjaman->kredit],['id' => $idKas]);
                LogJurnal::updateAll(['kredit' => $dataKas->debet],['id' => $idPinjam]);
            }
        }

       }
    }

    public function actionFixCashback2Mandiri() //UNTUK 1.2%
    {   
       $dataPelunasan = LogTransaksi::find()->where(['jenis_transaksi' => 'PELUNASAN'])->all();
       foreach($dataPelunasan as $transaksi){
        $dataKas = $transaksi->getLogJurnals()->where(['akun_kode' => 1104])->one();
        $dataPinjaman = $transaksi->getLogJurnals()->where(['akun_kode' => 1106])->one();
        $dataCashback = $transaksi->getLogJurnals()->where(['akun_kode' => 4014])->one();

        if($dataKas and $dataPinjaman and $dataCashback){
            $idKas = $dataKas->id;
            $idPinjam = $dataPinjaman->id;

            $debet = $dataKas->debet;
            $kredit = $dataPinjaman->kredit;

            if($debet > $kredit){
                LogJurnal::updateAll(['debet' => $dataPinjaman->kredit],['id' => $idKas]);
                LogJurnal::updateAll(['kredit' => $dataKas->debet],['id' => $idPinjam]);
            }
        }

       }
    }

}
