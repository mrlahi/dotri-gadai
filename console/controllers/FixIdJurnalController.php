<?php

namespace console\controllers;
use yii\console\Controller;
use Yii;
use common\models\LogJurnal;


class FixIdJurnalController extends Controller
{

    public function actionIndex()
    {   
       $dataJurnal = LogJurnal::find()->orderBy(['id' => SORT_DESC])->all();
       foreach($dataJurnal as $jurnal){
        $id_lama = $jurnal->id;
        $id_baru = $id_lama+100;
        echo "lama : ".$id_lama." - baru : ".$id_baru."\n";

        $mJurnal = new LogJurnal();
        $mJurnal->updateAll(['id' => $id_baru],['id' => $id_lama]);
       }
    }

}
