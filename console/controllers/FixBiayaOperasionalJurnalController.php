<?php

namespace console\controllers;
use yii\console\Controller;
use Yii;
use common\models\BebanOperasional;
use common\models\LogJurnal;

class FixBiayaOperasionalJurnalController extends Controller
{

    public function actionIndex()
    {   
        $dataBiaya = BebanOperasional::find()->where(['between', 'created_at', '2023-02-02', '2023-02-05'])->all();
        foreach($dataBiaya as $biaya){
            $mJurnal = new LogJurnal();
            $cekJurnal = $mJurnal->find()->where(['jenis_asal_jurnal' => 'OPERASIONAL', 'asal_id' => $biaya->id_beban])->one();
            if(!$cekJurnal){
                $mJurnal->inOperasionalFix($biaya->id_beban);
            }
        }
    }

}
