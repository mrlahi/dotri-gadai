<?php

namespace console\controllers;
use yii\console\Controller;
use Yii;
use common\models\BebanOperasional;
use common\models\Cabang;
use common\models\ListLaporan;
use common\models\LogJurnal;
use DateTime;

class LabaTahunBerjalanController extends Controller
{

    public function actionIndex()
    {
        $date = date('Y-m-d');
        $dataCabang = Cabang::find()->where(['<','id_cabang',1000])->all();
        foreach($dataCabang as $cabang){
            $t_pendapatan = 0;
            $t_biaya = 0;
            $mLogJurnal = new LogJurnal();
            $cabang_id = $cabang->id_cabang;
            $listPendapatan = ListLaporan::find()->where(['or',['jenis_laporan' => 'PENDAPATAN'],['jenis_laporan' => 'PENDAPATAN NON OPERASIONAL']])->orderBy(['akun_id' => SORT_ASC])->all();
            foreach($listPendapatan as $lapPendapatan){
                $pendapatan = $lapPendapatan->akun->saldoJurnalRange($date,$date,$cabang_id);
                $t_pendapatan += $pendapatan;
            }

            $listBiaya = ListLaporan::find()->where(['or', ['jenis_laporan' => 'BIAYA'], ['jenis_laporan' => 'BIAYA NON OPERASIONAL']])->all();
            foreach($listBiaya as $lapBiaya){
                $biaya = $lapBiaya->akun->saldoJurnalRange($date,$date,$cabang_id);
                $t_biaya += $biaya;
            }

            if($t_pendapatan >= $t_biaya){
                $jumlah = $t_pendapatan - $t_biaya;
                $mLogJurnal->labaTahunBerjalan($date,$jumlah,$cabang_id,"laba");
            }
            else{
                $jumlah = $t_biaya - $t_pendapatan;
                $mLogJurnal->labaTahunBerjalan($date,$jumlah,$cabang_id,"rugi");
            }
        }
    }

    public function actionFixAll(){
        $begin = new DateTime( "2023-01-01" );
        $end   = new DateTime( date('Y-m-d') );

        for($i = $begin; $i <= $end; $i->modify('+1 day')){
            $date = $i->format("Y-m-d");
            $this->generateLabaTahunBerjalan($date);
            echo $date." Fixed."."\n";
        }

    }

    public function actionFixByTgl($tanggal_start){
        $begin = new DateTime( $tanggal_start );
        $end   = new DateTime( date('Y-m-d') );

        for($i = $begin; $i <= $end; $i->modify('+1 day')){
            $date = $i->format("Y-m-d");
            $this->generateLabaTahunBerjalan($date);
            echo $date." Fixed."."\n";
        }

    }

    public function generateLabaTahunBerjalan($date){
        $dataCabang = Cabang::find()->where(['<','id_cabang',1000])->all();
        foreach($dataCabang as $cabang){
            $t_pendapatan = 0;
            $t_biaya = 0;
            $mLogJurnal = new LogJurnal();
            $cabang_id = $cabang->id_cabang;
            $listPendapatan = ListLaporan::find()->where(['or',['jenis_laporan' => 'PENDAPATAN'],['jenis_laporan' => 'PENDAPATAN NON OPERASIONAL']])->orderBy(['akun_id' => SORT_ASC])->all();
            foreach($listPendapatan as $lapPendapatan){
                $pendapatan = $lapPendapatan->akun->saldoJurnalRange($date,$date,$cabang_id);
                $t_pendapatan += $pendapatan;
            }

            $listBiaya = ListLaporan::find()->where(['or', ['jenis_laporan' => 'BIAYA'], ['jenis_laporan' => 'BIAYA NON OPERASIONAL']])->all();
            foreach($listBiaya as $lapBiaya){
                $biaya = $lapBiaya->akun->saldoJurnalRange($date,$date,$cabang_id);
                $t_biaya += $biaya;
            }

            if($t_pendapatan >= $t_biaya){
                $jumlah = $t_pendapatan - $t_biaya;
                $mLogJurnal->labaTahunBerjalan($date,$jumlah,$cabang_id,"laba");
            }
            else{
                $jumlah = $t_biaya - $t_pendapatan;
                $mLogJurnal->labaTahunBerjalan($date,$jumlah,$cabang_id,"rugi");
            }
        }
    }

}
