<?php

namespace console\controllers;
use yii\console\Controller;
use Yii;
use common\models\Perpanjang;
use common\models\LogFinance;


class FixPerpanjangBarangController extends Controller
{

    public function actionIndex($id_barang)
    {   
        $dataPerpanjang = Perpanjang::find()->where(['id_barang' => $id_barang])->all();
        foreach($dataPerpanjang as $perpanjang){
            echo "Denda : ".$perpanjang->denda." - Bunga : ".$perpanjang->bunga."\n";

            $mLog = new LogFinance();
            $bunga = $mLog->find()->where(['id_barang' => $id_barang, 'status' => 'BungaPerpanjang', 'created_at' => $perpanjang->created_at])->one();
            $denda = $mLog->find()->where(['id_barang' => $id_barang, 'status' => 'DendaPerpanjang', 'created_at' => $perpanjang->created_at])->one();

            $mLogBunga = new LogFinance();
            if(!$bunga){
                $mLogBunga->jumlah_uang = $perpanjang->bunga;
                $mLogBunga->status = "BungaPerpanjang";
                $mLogBunga->created_at = $perpanjang->created_at;
                $mLogBunga->created_by = $perpanjang->created_by;
                $mLogBunga->id_cabang = $perpanjang->barang->id_cabang;
                $mLogBunga->id_barang = $id_barang;
                $mLogBunga->type_pembayaran = "Cash";
                $mLogBunga->save();
            }

            $mLogDenda = new LogFinance();
            if(!$denda){
                $mLogDenda->jumlah_uang = $perpanjang->denda;
                $mLogDenda->status = "DendaPerpanjang";
                $mLogDenda->created_at = $perpanjang->created_at;
                $mLogDenda->created_by = $perpanjang->created_by;
                $mLogDenda->id_cabang = $perpanjang->barang->id_cabang;
                $mLogDenda->id_barang = $id_barang;
                $mLogDenda->type_pembayaran = "Cash";
                $mLogDenda->save();
            }
        }
    }

}
