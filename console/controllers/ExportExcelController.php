<?php
namespace console\controllers;
use yii\console\Controller;
use Yii;

use yii2tech\spreadsheet\Spreadsheet;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use common\models\LogStatusTemp;
use common\models\LogStatusTempSearch;
use common\models\OutstandingManager;

class ExportExcelController extends Controller{

    public function actionIndex(){

        $modelOutstanding = new OutstandingManager();
        $dataOutstanding = $modelOutstanding->find()->where(['status_generator' => 'EXPORTING'])->one();

        if(!empty($dataOutstanding)){
            $outstanding_id = $dataOutstanding->id_outstanding_manager;
            $start = $dataOutstanding->tgl_start;
            $end = $dataOutstanding->tgl_end;

            $exporter = new Spreadsheet([
                'dataProvider' => new ActiveDataProvider([
                    'query' => LogStatusTemp::find()->where(['outstanding_id' => $outstanding_id]),
                ]),
                'columns' => [
                    [
                        'label' => 'No Kontrak',
                        'value' => 'barang.no_kontrak'
                    ],
                    [
                        'label' => 'Outlet',
                        'value' => 'cabang.nama_cabang'
                    ],
                    [
                        'label' => 'Tipe SBG',
                        'value' => 'jenisBarang.nama_barang'
                    ],
                    [
                        'label' => 'Tgl SBG',
                        'value' => 'barang.tgl_masuk',
                        'format' => 'date'
                    ],
                    [
                        'label' => 'Jth Tempo',
                        'value' => 'barang.jatuh_tempo',
                        'format' => 'date'
                    ],
                    [
                        'label' => 'Nasabah',
                        'value' => 'nasabah.nama'
                    ],
                    [
                        'label' => 'Nama Barang',
                        'value' => 'barang.nama_barang'
                    ],
                    [
                        'label' => 'Merk',
                        'value' => 'barang.merk'
                    ],
                    [
                        'label' => 'Tipe Barang',
                        'value' => 'barang.tipe'
                    ],
                    [
                        'label' => 'Kelengkapan',
                        'value' => 'barang.kelengkapan'
                    ],
                    [
                        'label' => 'Harga Taksir',
                        'value' => 'barang.harga_taksir'
                    ],
                    [
                        'label' => 'Nilai Pinjam',
                        'value' => 'barang.nilai_pinjam'
                    ],
                    [
                        'label' => 'Sisa Pinjam',
                        'value' => function ($model) use ($start, $end) {
                            $id_barang = $model->id_barang;
                            $sisaPinjam = $model->sisaPinjam($id_barang,$start,$end);
                            return $sisaPinjam;
                        },
                    ],
                    [
                        'label' => 'Tgl Pelunasan',
                        'value' => 'tglLunas',
                    ],
                    [
                        'label' => 'Status',
                        'value' => 'barang.status'
                    ],
                    [
                        'label' => ' ',
                        'value' => function(){ return "OUTSTANDING"; },
                        'format' => 'raw'
                    ]
                ],
            ]);

            $simpan = $outstanding_id."-".$start."-".$end.".xls";
            //$exporter->save('backend/web/outstanding/'.$simpan);
            $exporter->save('/home/dotri/public_html/dotri-gadai/backend/web/outstanding/'.$simpan);

            $modelOutstanding->updateAll(['status_generator' => 'SUCCESS'],['id_outstanding_manager' => $outstanding_id]);
        }
    }

}