<?php

namespace console\controllers;

use common\models\Barang;
use common\models\NoteBarang;
use yii\console\Controller;
use Yii;


class FixNoteBarangController extends Controller
{

    public function actionIndex()
    {   
       $dataBarang = Barang::find()->where(['<>','note', ''])->all();

       foreach($dataBarang as $barang){
            $mNote = new NoteBarang();
            $mNote->barang_id = $barang->id_barang;
            $mNote->note = $barang->note;
            $mNote->created_at = $barang->tgl_masuk;
            $mNote->created_by = $barang->created_by;
            $mNote->save();
       }
    }

}
