<?php

namespace console\controllers;
use yii\console\Controller;
use Yii;
use common\models\LogJurnal;
use common\models\Barang;
use common\models\LogFinance;


class FixPelunasanJurnalController extends Controller
{

    public function actionIndex()
    {   
        $dataJurnal = LogJurnal::find()->where(['keterangan' => 'PELUNASAN'])->all();
        foreach($dataJurnal as $jurnal){
            $barang_id = $jurnal->asal_id;
            $dataFinance = LogFinance::find()->where(['status' => 'Pelunasan', 'id_barang' => $barang_id])->one();
            if($jurnal->debet != $dataFinance->jumlah_uang){
                echo "Berbeda : ".$jurnal->debet ."><".$dataFinance->jumlah_uang."\n";
                $jurnal->debet = $dataFinance->jumlah_uang;
                $jurnal->save();
            }
        }
    }

    public function actionFixPokok()
    {   
        $dataJurnal = LogJurnal::find()->where(['keterangan' => 'CASHBACK PELUNASAN'])->all();
        foreach($dataJurnal as $jurnal){
            $asal_id = $jurnal->asal_id;
            $dataPelunasan = LogJurnal::find()->where(['keterangan' => 'PELUNASAN', 'asal_id' => $asal_id])->one();
            $pelunasan = $dataPelunasan->debet;
            $cashback = $jurnal->kredit;
            $pokok = $pelunasan + $cashback;
            
            $dataPokok = LogJurnal::find()->where(['asal_id' => $asal_id, 'keterangan' => 'POKOK PINJAMAN'])->one();

            if($pokok != $dataPokok->kredit){
                echo "Berbeda : ". $pokok ." >< ".$dataPokok->kredit."\n";
                $dataPokok->kredit = $pokok;
                $dataPokok->save();
            }
            
        }
        
    }

    public function actionFixDenda(){
        $dataJurnal = LogJurnal::find()->where(['keterangan' => 'DENDA PELUNASAN'])->all();
        foreach($dataJurnal as $jurnal){
            $asal_id = $jurnal->asal_id;
            $dataPelunasan = LogJurnal::find()->where(['keterangan' => 'PELUNASAN', 'asal_id' => $asal_id])->one();
            $pelunasan = $dataPelunasan->debet;
            $denda = $jurnal->kredit;
            $pokok = $pelunasan + $denda;
            $dataPelunasan->debet = $pokok;
            $dataPelunasan->save();

            echo "Data Berhasil Diubah..!!"."\n";
        }
    }

}
