<?php

namespace console\controllers;
use yii\console\Controller;
use Yii;
use common\models\JenisBiayaOperasional;
use common\models\BebanOperasional;


class FixJenisOperasionalController extends Controller
{

    public function actionIndex()
    {   
      $dataBiaya = BebanOperasional::find()->groupBy(['type_operasional'])->all();
      foreach($dataBiaya as $biaya){
        $mJenis = new JenisBiayaOperasional();
        $mJenis->nama_jenis = $biaya->type_operasional;
        $mJenis->akun_id = 1101;
        $mJenis->created_at = date('Y-m-d');
        $mJenis->created_by = 1;
        $mJenis->save();
        echo $biaya->type_operasional."\n";
      }
    }

    public function actionChangeType(){
        $dataJenis = JenisBiayaOperasional::find()->all();
        foreach($dataJenis as $jenis){
            $id = $jenis->id;
            $jenis = $jenis->nama_jenis;
            $mBiaya = new BebanOperasional();

            $mBiaya->updateAll(['type_operasional' => $id],['type_operasional' => $jenis]);

            echo $jenis."\n";
        }
    }

}
