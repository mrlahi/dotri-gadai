<?php

namespace console\controllers;
use yii\console\Controller;
use Yii;
use common\models\Barang;
use common\models\LogStatus;

class AddTglLunasController extends Controller
{

    public function actionAddLunas()
    {
        $data = Barang::find()->where(['status' => 'Lunas', 'tgl_status' => NULL])->limit(10000)->all();
        foreach($data as $barang){
            $log = LogStatus::find()->where(['id_barang' => $barang->id_barang, 'status' => 'Lunas'])->one();
            if($log){
                Barang::updateAll(['tgl_status' => $log->created_at],['id_barang' => $barang->id_barang]);
                echo $barang->id_barang ."Success.!!"."\n";
            }
        }   
    }

    public function actionAddTerjual()
    {
        $data = Barang::find()->where(['status' => 'Terjual', 'tgl_status' => NULL])->limit(10000)->all();
        foreach($data as $barang){
            $log = LogStatus::find()->where(['id_barang' => $barang->id_barang, 'status' => 'Terjual'])->one();
            if($log){
                Barang::updateAll(['tgl_status' => $log->created_at],['id_barang' => $barang->id_barang]);
                echo $barang->id_barang ."Success.!!"."\n";
            }
        }   
    }

}
