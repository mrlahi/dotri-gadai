<?php

namespace console\controllers;
use yii\console\Controller;
use Yii;
use common\models\LogReceipt;
use common\models\LogFinance;

class FixLogReceiptController extends Controller
{

    public function actionIndex()
    {   
        $dataLog = LogReceipt::find()->where(['or',['type_receipt' => 'Cicilan'],['type_receipt' => 'Pelunasan'],['type_receipt' => 'Perpanjang']])->all();
        foreach($dataLog as $data){
            $type_pembayaran = $this->typePembayaran($data->id_barang,$data->type_receipt);
            if($data->type_receipt == "Pelunasan"){
                $cashBack = $this->cashBack($data->id_barang);
            }
            else{
                $cashBack = 0;
            }
            echo $data->id_log_receipt."-".$type_pembayaran."-".$cashBack."\n";

            if($type_pembayaran){
                $mLog = new LogReceipt();
                $mLog->updateAll(['type_pembayaran' => $type_pembayaran, 'cashback' => $cashBack],['id_log_receipt' => $data->id_log_receipt]);
            }
        }
    }

    public function typePembayaran($id_barang,$type){
        if($type == "Cicilan"){
            $cek = "Angsuran";
        }
        else if($type == "Pelunasan"){
            $cek = "Pelunasan";
        }
        else if($type == "Perpanjang"){
            $cek = "BungaPerpanjang";
        }

        $mFinance = new LogFinance();
        $data = $mFinance->find()->where(['id_barang' => $id_barang, 'status' => $cek])->one();

        if($data){
            return $data->type_pembayaran;
        }
        else{
            return "Cash";
        }
    }

    public function cashBack($id_barang){
        $mFinance = new LogFinance();
        $data = $mFinance->find()->where(['id_barang' => $id_barang, 'status' => 'KembaliBunga5Persen'])->one();
        if($data){
            return $data->jumlah_uang;
        }
        else{
            return 0;
        }
    }

}
