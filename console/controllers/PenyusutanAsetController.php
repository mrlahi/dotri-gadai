<?php

namespace console\controllers;

use common\models\LogJurnal;
use common\models\LogPenyusutanAset;
use yii\console\Controller;
use Yii;



class PenyusutanAsetController extends Controller
{

    public function actionIndex()
    {
        $dataLogPenyusutan = LogPenyusutanAset::find()->where(['is_penyusutan' => 0])->andWhere(['<=', 'tgl_penyusutan',  date('Y-m-d')])->all();

        foreach($dataLogPenyusutan as $penyusutan){
            $mLogJurnal = new LogJurnal();
            $mLogJurnal->inPenyusutanInventory($penyusutan->id);
            echo $penyusutan->id." Berhasil..!!"."\n";
        }

    }

}
