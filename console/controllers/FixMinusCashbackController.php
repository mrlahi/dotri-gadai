<?php

namespace console\controllers;
use yii\console\Controller;
use Yii;
use common\models\LogJurnal;


class FixMinusCashbackController extends Controller
{

    public function actionIndex()
    {   
       $dataJurnal = LogJurnal::find()->where(['like', 'keterangan', 'CASHBACK'])->all();
       foreach($dataJurnal as $jurnal){
        $model = new LogJurnal();
        $debet = $jurnal->debet * -1;
        $id = $jurnal->id;
        $model->updateAll(['debet' => $debet],['id' => $id]);

        echo "Berhasil Dirubah"."\n";

       }
    }

}
