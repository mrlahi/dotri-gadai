<?php
namespace console\controllers;

use Yii;
use common\models\Profile;

class FixPhoneController extends \yii\console\Controller
{
    public function actionBuangSpasi()
    {
        $dataNasabah = Profile::find()->where(['akses' => 'defaultNasabah'])->all();

        foreach($dataNasabah as $nasabah){
            $mNasabah = new Profile();

            $id_user = $nasabah->id_user;
            $oldPhone1 = $nasabah->no_hp;
            $newPhone1 = str_replace(' ', '', $oldPhone1);
            echo $oldPhone1." vs ".$newPhone1."\n";

            $mNasabah->updateAll(['no_hp' => $newPhone1], ['id_user' => $id_user]);

        }

    }

    public function actionSplitGaring()
    {
        $dataNasabah = Profile::find()->where(['akses' => 'defaultNasabah'])->all();

        foreach($dataNasabah as $nasabah){
            $mNasabah = new Profile();

            $id_user = $nasabah->id_user;
            $phoneNum = explode("/",$nasabah->no_hp);
            $phone1 = $phoneNum[0];
            $phone2 = "";
            if(array_key_exists(1,$phoneNum)){
                $phone2 = $phoneNum[1];
            }
            echo $phone1." dan ".$phone2."\n";
            $mNasabah->updateAll(['no_hp' => $phone1, 'no_hp2' => $phone2],['id_user' => $id_user]);

        }

    }

    public function actionSplitKoma()
    {
        $dataNasabah = Profile::find()->where(['akses' => 'defaultNasabah'])->all();

        foreach($dataNasabah as $nasabah){
            $mNasabah = new Profile();

            $id_user = $nasabah->id_user;
            $phoneNum = explode(",",$nasabah->no_hp);
            $phone1 = $phoneNum[0];
            $phone2 = "";
            if(array_key_exists(1,$phoneNum)){
                $phone2 = $phoneNum[1];
            }
            echo $phone1." dan ".$phone2."\n";
            $mNasabah->updateAll(['no_hp' => $phone1, 'no_hp2' => $phone2],['id_user' => $id_user]);

        }
    }

    public function actionBuangGaris()
    {
        $dataNasabah = Profile::find()->where(['akses' => 'defaultNasabah'])->all();

        foreach($dataNasabah as $nasabah){
            $mNasabah = new Profile();

            $id_user = $nasabah->id_user;
            $oldPhone1 = $nasabah->no_hp;
            $newPhone1 = str_replace('-', '', $oldPhone1);
            echo $oldPhone1." vs ".$newPhone1."\n";

            $mNasabah->updateAll(['no_hp' => $newPhone1], ['id_user' => $id_user]);

        }

    }

    public function actionBuangWa()
    {
        $dataNasabah = Profile::find()->where(['akses' => 'defaultNasabah'])->all();

        foreach($dataNasabah as $nasabah){
            $mNasabah = new Profile();

            $id_user = $nasabah->id_user;
            $oldPhone1 = $nasabah->no_hp;
            $newPhone1 = str_replace('(WA)', '', $oldPhone1);
            echo $oldPhone1." vs ".$newPhone1."\n";

            $mNasabah->updateAll(['no_hp' => $newPhone1], ['id_user' => $id_user]);

        }

    }

    public function actionBuangWa2()
    {
        $dataNasabah = Profile::find()->where(['akses' => 'defaultNasabah'])->all();

        foreach($dataNasabah as $nasabah){
            $mNasabah = new Profile();

            $id_user = $nasabah->id_user;
            $oldPhone1 = $nasabah->no_hp;
            $newPhone1 = str_replace('(wa)', '', $oldPhone1);
            echo $oldPhone1." vs ".$newPhone1."\n";

            $mNasabah->updateAll(['no_hp' => $newPhone1], ['id_user' => $id_user]);

        }

    }

    public function actionBuangWa3()
    {
        $dataNasabah = Profile::find()->where(['akses' => 'defaultNasabah'])->all();

        foreach($dataNasabah as $nasabah){
            $mNasabah = new Profile();

            $id_user = $nasabah->id_user;
            $oldPhone1 = $nasabah->no_hp;
            $newPhone1 = str_replace('WA', '', $oldPhone1);
            echo $oldPhone1." vs ".$newPhone1."\n";

            $mNasabah->updateAll(['no_hp' => $newPhone1], ['id_user' => $id_user]);

        }

    }

    public function actionNoDua()
    {
        $dataNasabah = Profile::find()->where(['akses' => 'defaultNasabah'])->all();

        foreach($dataNasabah as $nasabah){
            $mNasabah = new Profile();

            if($nasabah->no_hp2 != ""){
                echo $nasabah->no_hp2."\n";
            }
            // $id_user = $nasabah->id_user;
            // $oldPhone1 = $nasabah->no_hp;
            // $newPhone1 = str_replace('-', '', $oldPhone1);
            // echo $oldPhone1." vs ".$newPhone1."\n";

            // $mNasabah->updateAll(['no_hp' => $newPhone1], ['id_user' => $id_user]);

        }

    }
    
} 