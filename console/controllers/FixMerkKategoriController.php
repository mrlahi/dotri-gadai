<?php

namespace console\controllers;

use common\models\KategoriBarang;
use common\models\MerkBarang;
use common\models\TaksiranHargaPasar;
use yii\console\Controller;



class FixMerkKategoriController extends Controller
{

    public function actionIndex()
    {   
        $mTaksiran = new TaksiranHargaPasar();
        $dataTaksiran = $mTaksiran->find()->groupBy('merk_barang')->all();
        foreach($dataTaksiran as $taksiran){
            $mMerk = new MerkBarang();
            $dataKategori = KategoriBarang::find()->where(['nama_kategori' => $taksiran->kategori_barang])->all();

            $mMerk->nama_merek = $taksiran->merk_barang;
            $mMerk->deskripsi = '-';
            $mMerk->created_at = date('Y-m-d');
            $mMerk->created_by = 1;
            $mMerk->save();
        }
    }

}
