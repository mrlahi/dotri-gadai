<?php
namespace console\controllers;

use Yii;
use common\models\Barang;
use common\models\LogNotifikasi;

class GenerateNotifController extends \yii\console\Controller
{
    public function actionJatuhTempo()
    {
        $modelBarang = new Barang();
        $tagihNow = $modelBarang->listTagihNow2(30);
        foreach($tagihNow as $barang){
            $noHp1 = $barang->nasabah->pureHp(1);
            $noHp2 = $barang->nasabah->pureHp(2);

            if($barang->device){
                $device_id = $barang->device->id;
                $mLog = new LogNotifikasi();
                $mLog->barang_id = $barang->id_barang;
                $mLog->status_wa = "ONPROCESS";
                $mLog->created_at = date('Y-m-d');
                $mLog->jenis_notifikasi = "JATUH TEMPO";
                $mLog->device_id = $device_id;
                $mLog->message_id = "";
                $mLog->message = "";
                $mLog->no_hp_nasabah = $noHp1;
                $mLog->save();

                if(isset($noHp2)){
                    $mLog = new LogNotifikasi();
                    $mLog->barang_id = $barang->id_barang;
                    $mLog->status_wa = "ONPROCESS";
                    $mLog->created_at = date('Y-m-d');
                    $mLog->jenis_notifikasi = "JATUH TEMPO";
                    $mLog->device_id = $device_id;
                    $mLog->message_id = "";
                    $mLog->message = "";
                    $mLog->no_hp_nasabah = $noHp2;
                    $mLog->save();
                }

            }
        }
    }

    public function actionPasif(){
        $mBarang = new Barang();
        $dataLelang = $mBarang->listPasif(5);
        foreach($dataLelang as $barang){
            $noHp1 = $barang->nasabah->pureHp(1);
            $noHp2 = $barang->nasabah->pureHp(2);

            if($barang->device){
                $device_id = $barang->device->id;
                $mLog = new LogNotifikasi();
                $mLog->barang_id = $barang->id_barang;
                $mLog->status_wa = "ONPROCESS";
                $mLog->created_at = date('Y-m-d');
                $mLog->jenis_notifikasi = "PASIF";
                $mLog->device_id = $device_id;
                $mLog->message_id = "";
                $mLog->message = "";
                $mLog->no_hp_nasabah = $noHp1;
                $mLog->save();

                if(isset($noHp2)){
                    $mLog = new LogNotifikasi();
                    $mLog->barang_id = $barang->id_barang;
                    $mLog->status_wa = "ONPROCESS";
                    $mLog->created_at = date('Y-m-d');
                    $mLog->jenis_notifikasi = "PASIF";
                    $mLog->device_id = $device_id;
                    $mLog->message_id = "";
                    $mLog->message = "";
                    $mLog->no_hp_nasabah = $noHp2;
                    $mLog->save();
                }

            }
        }

        $mBarang = new Barang();
        $dataLelang = $mBarang->listPasif(10);
        foreach($dataLelang as $barang){
            $noHp1 = $barang->nasabah->pureHp(1);
            $noHp2 = $barang->nasabah->pureHp(2);

            if($barang->device){
                $device_id = $barang->device->id;
                $mLog = new LogNotifikasi();
                $mLog->barang_id = $barang->id_barang;
                $mLog->status_wa = "ONPROCESS";
                $mLog->created_at = date('Y-m-d');
                $mLog->jenis_notifikasi = "PASIF";
                $mLog->device_id = $device_id;
                $mLog->message_id = "";
                $mLog->message = "";
                $mLog->no_hp_nasabah = $noHp1;
                $mLog->save();

                if(isset($noHp2)){
                    $mLog = new LogNotifikasi();
                    $mLog->barang_id = $barang->id_barang;
                    $mLog->status_wa = "ONPROCESS";
                    $mLog->created_at = date('Y-m-d');
                    $mLog->jenis_notifikasi = "PASIF";
                    $mLog->device_id = $device_id;
                    $mLog->message_id = "";
                    $mLog->message = "";
                    $mLog->no_hp_nasabah = $noHp2;
                    $mLog->save();
                }

            }
        }

    }

    public function actionLelang(){
        $mBarang = new Barang();
        $dataLelang = $mBarang->listLelang(5);
        foreach($dataLelang as $barang){
            $noHp1 = $barang->nasabah->pureHp(1);
            $noHp2 = $barang->nasabah->pureHp(2);

            if($barang->device){
                $device_id = $barang->device->id;
                $mLog = new LogNotifikasi();
                $mLog->barang_id = $barang->id_barang;
                $mLog->status_wa = "ONPROCESS";
                $mLog->created_at = date('Y-m-d');
                $mLog->jenis_notifikasi = "LELANG";
                $mLog->device_id = $device_id;
                $mLog->message_id = "";
                $mLog->message = "";
                $mLog->no_hp_nasabah = $noHp1;
                $mLog->save();

                if(isset($noHp2)){
                    $mLog = new LogNotifikasi();
                    $mLog->barang_id = $barang->id_barang;
                    $mLog->status_wa = "ONPROCESS";
                    $mLog->created_at = date('Y-m-d');
                    $mLog->jenis_notifikasi = "LELANG";
                    $mLog->device_id = $device_id;
                    $mLog->message_id = "";
                    $mLog->message = "";
                    $mLog->no_hp_nasabah = $noHp2;
                    $mLog->save();
                }

            }
        }

        $mBarang = new Barang();
        $dataLelang = $mBarang->listLelang(10);
        foreach($dataLelang as $barang){
            $noHp1 = $barang->nasabah->pureHp(1);
            $noHp2 = $barang->nasabah->pureHp(2);

            if($barang->device){
                $device_id = $barang->device->id;
                $mLog = new LogNotifikasi();
                $mLog->barang_id = $barang->id_barang;
                $mLog->status_wa = "ONPROCESS";
                $mLog->created_at = date('Y-m-d');
                $mLog->jenis_notifikasi = "LELANG";
                $mLog->device_id = $device_id;
                $mLog->message_id = "";
                $mLog->message = "";
                $mLog->no_hp_nasabah = $noHp1;
                $mLog->save();

                if(isset($noHp2)){
                    $mLog = new LogNotifikasi();
                    $mLog->barang_id = $barang->id_barang;
                    $mLog->status_wa = "ONPROCESS";
                    $mLog->created_at = date('Y-m-d');
                    $mLog->jenis_notifikasi = "LELANG";
                    $mLog->device_id = $device_id;
                    $mLog->message_id = "";
                    $mLog->message = "";
                    $mLog->no_hp_nasabah = $noHp2;
                    $mLog->save();
                }

            }
        }

    }

    public function actionKirimJatuhTempo(){
        $dataLog = LogNotifikasi::find()->where(['jenis_notifikasi' => 'JATUH TEMPO', 'status_wa' => 'ONPROCESS'])->one();
        echo $dataLog->id;
        $tombolStatus = "STATUS ".$dataLog->barang->no_kontrak;
        $phone = $dataLog->no_hp_nasabah;
        $no_device = $dataLog->device->no_device;
        $token = $dataLog->device->token;
        $tgl_jatuh_tempo = date_format(date_create($dataLog->barang->jatuh_tempo), 'd M Y');

        $curl = curl_init();
        $data = [
            'phone' => $phone,
                // 'message'=> [
                //     'title' => [
                //         'type' => 'text',
                //         'content' => $dataLog->barang->cabang->nama_cabang,
                //     ],
                //     // 'buttons' => [
                //     //     'call' => [
                //     //         'display' => 'Info Lebih Lanjut',
                //     //         'phone' => $no_device,
                //     //     ],
                //     //     'quickReply' => [$tombolStatus],
                //     // ],
                //     'content' => 'NO. SBG : '.$dataLog->barang->no_kontrak."\n"."Halo Bapak/Ibu ".$dataLog->barang->nasabah->nama.", kami memberitahukan barang Bapak/Ibu akan jatuh tempo pada tgl ".$tgl_jatuh_tempo.".\n"."Segera lakukan pembayaran."."\n\n"."Terimakasih",
                //     // 'footer' => 'PT. DOTRI GADAI JAYA',
                // ],
            'message' => '*'.$dataLog->barang->cabang->nama_cabang.'*'."\n".'NO. SBG : '."\n".$dataLog->barang->no_kontrak."\n"."Halo Bapak/Ibu ".$dataLog->barang->nasabah->nama.", kami memberitahukan bahwa : ".
            "\n"."1. Barang jaminan gadai Bapak/Ibu (".$dataLog->barang->merk." ".$dataLog->barang->tipe.") akan jatuh tempo pada tgl ".$tgl_jatuh_tempo.".".
            "\n"."2. Segera tebus/perpanjang barang jaminan gadai bapak/ibu sebelum melewati tgl jatuh tempo karena jika melewati tgl jatuh tempo akan dikenakan denda Rp 5.000/hari.".
            "\n"."3. Perpanjangan barang jaminan gadai wajib membayar cicilan/angsuran minimal Rp 100.000 untuk mengurangi pinjaman Bapak/ibu.".
            "\n". "Jika Bapak/ibu tidak melakukan penebusan/perpanjangan barang jaminan gadai sampai pada tanggal jual maka barang jaminan gadai akan kami jual.".
            "\n\n"."Untuk penebusan/perpanjang masa gadai barang jaminan dapat dilakukan melalui transfer
            BCA : 8115777234 a/n Dotri Gadai Jaya PT".
            "\n"."ketentuan : ".
            "\n"."1. Wajib melakukan konfirmasi melalaui whatsapp Dotri Gadai untuk jumlah tagihan sebelum melakukan transfer.".
            "\n"."2. Wajib mengirim bukti transfer paling lama pukul 22.00 WIB. Jika mengirim bukti transfer melewati pukul 22.00 WIB transaksi diproses dan dihitung keesokan harinya.".
            "\n\n"."Terimakasih."."\n\n"."_PT DOTRI GADAI JAYA_",
        ];


        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(
                "Authorization: $token",
            )
        );
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_URL,  "https://deu.wablas.com/api/send-message");
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $result = curl_exec($curl);
        curl_close($curl);
        $lapor = json_decode($result);
        echo $dataLog->id;
        print_r($lapor);

        if(isset($lapor->data->messages[0]->id)){
            $status = $lapor->data->messages[0]->status;
            $message_id = $lapor->data->messages[0]->id;
        }else{
            $status = "REJECT";
            $message_id = "";
        }
        
        $message = $lapor->data->messages[0]->message;
        $dataLog->status_wa = $status;
        $dataLog->message_id = $message_id;
        $dataLog->message = $message;
        $dataLog->save();
    }

    public function actionKirimPasif(){
        $dataLog = LogNotifikasi::find()->where(['jenis_notifikasi' => 'PASIF', 'status_wa' => 'ONPROCESS'])->one();
        $tombolStatus = "STATUS ".$dataLog->barang->no_kontrak;
        $phone = $dataLog->no_hp_nasabah;
        $no_device = $dataLog->device->no_device;
        $token = $dataLog->device->token;
        $tgl_jual = date_format(date_create($dataLog->barang->forjual), 'd M Y');

        $curl = curl_init();
        $data = [
            'phone' => $phone,
                // 'message'=> [
                //     'title' => [
                //         'type' => 'text',
                //         'content' => $dataLog->barang->cabang->nama_cabang,
                //     ],
                //     // 'buttons' => [
                //     //     'call' => [
                //     //         'display' => 'Info Lebih Lanjut',
                //     //         'phone' => $no_device,
                //     //     ],
                //     //     'quickReply' => [$tombolStatus],
                //     // ],
                //     'content' => 'NO. SBG : '.$dataLog->barang->no_kontrak."\n"."Halo Bapak/Ibu ".$dataLog->barang->nasabah->nama.", kami memberitahukan barang Bapak/Ibu akan akan dijual pada tgl ".$tgl_jual.".\n"."Segera lakukan pembayaran SEBELUM TGL JUAL untuk menghindari barang TERJUAL."."\n\n"."Terimakasih",
                //     // 'footer' => 'PT. DOTRI GADAI JAYA',
                // ],
            'message' => '*'.$dataLog->barang->cabang->nama_cabang.'*'."\n".'NO. SBG : '.$dataLog->barang->no_kontrak."\n"."Halo Bapak/Ibu ".$dataLog->barang->nasabah->nama.", kami memberitahukan bahwa : ".
            "\n"."1. Barang Bapak/Ibu (".$dataLog->barang->merk." ".$dataLog->barang->tipe.") akan akan dijual pada tgl ".$tgl_jual.". Segera lakukan penebusan/perpanjangan barang jaminan gadai SEBELUM TGL JUAL untuk menghindari barang TERJUAL.".
            "\n". "2. Barang jaminan gadai yang melewati tgl jatuh tempo dikenakan denda Rp 5.000/hari.".
            "\n". "3. Perpanjangan barang jaminan gadai wajib membayar cicilan/angsuran minimal Rp 100.000 untuk mengurangi pinjaman Bapak/ibu.".
            "\n\n"."Untuk penebusan/perpanjang masa gadai barang jaminan dapat dilakukan melalui transfer
            BCA : 8115777234 a/n Dotri Gadai Jaya PT".
            "\n"."ketentuan : ".
            "\n"."1. Wajib melakukan konfirmasi melalaui whatsapp Dotri Gadai untuk jumlah tagihan sebelum melakukan transfer.".
            "\n"."2. Wajib mengirim bukti transfer paling lama pukul 22.00 WIB. Jika mengirim bukti transfer melewati pukul 22.00 WIB transaksi diproses dan dihitung keesokan harinya.".
            "\n\n"."Terimakasih"."\n".'_PT. DOTRI GADAI JAYA_'
            
        ];

        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(
                "Authorization: $token",
            )
        );
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_URL,  "https://deu.wablas.com/api/send-message");
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $result = curl_exec($curl);
        curl_close($curl);
        $lapor = json_decode($result);
        print_r($lapor);

        if(isset($lapor->data->messages[0]->id)){
            $status = $lapor->data->messages[0]->status;
            $message_id = $lapor->data->messages[0]->id;
        }else{
            $status = "REJECT";
            $message_id = "";
        }
        $message = $lapor->data->messages[0]->message;
        $dataLog->status_wa = $status;
        $dataLog->message_id = $message_id;
        $dataLog->message = $message;
        $dataLog->save();
    }

    public function actionKirimLelang(){
        $dataLog = LogNotifikasi::find()->where(['jenis_notifikasi' => 'LELANG', 'status_wa' => 'ONPROCESS'])->one();
        $tombolStatus = "STATUS ".$dataLog->barang->no_kontrak;
        $phone = $dataLog->no_hp_nasabah;
        $no_device = $dataLog->device->no_device;
        $token = $dataLog->device->token;
        $tgl_jual = date_format(date_create($dataLog->barang->forjual), 'd M Y');

        $curl = curl_init();
        $data = [
            'phone' => $phone,
                // 'message'=> [
                //     'title' => [
                //         'type' => 'text',
                //         'content' => $dataLog->barang->cabang->nama_cabang,
                //     ],
                //     // 'buttons' => [
                //     //     'call' => [
                //     //         'display' => 'Info Lebih Lanjut',
                //     //         'phone' => $no_device,
                //     //     ],
                //     //     'quickReply' => [$tombolStatus],
                //     // ],
                //     'content' => 'NO. SBG : '.$dataLog->barang->no_kontrak."\n"."Halo Bapak/Ibu ".$dataLog->barang->nasabah->nama.", kami memberitahukan bahwa Besok terakhir untuk pembayaran Perpanjang/Tebus barang Bapak/Ibu.\n"."Setelah itu barang akan dijual."."\n\n"."Terimakasih",
                //     //'footer' => 'PT. DOTRI GADAI JAYA',
                // ],
            'message' => '*'.$dataLog->barang->cabang->nama_cabang.'*'."\n".'NO. SBG : '.$dataLog->barang->no_kontrak."\n"."Halo Bapak/Ibu ".$dataLog->barang->nasabah->nama.", kami memberitahukan bahwa : ".
            "\n". "1. Besok adalah hari terakhir untuk perpanjang/tebus barang jaminan dan tanggal (".$tgl_jual.") barang Bapak/Ibu (".$dataLog->barang->merk." ".$dataLog->barang->tipe.") akan dijual.".
            "\n". "2. Barang jaminan gadai yang melewati tgl jatuh tempo dikenakan denda Rp 5.000/hari.".
            "\n". "3. Perpanjangan barang jaminan gadai wajib membayar cicilan/angsuran minimal Rp 100.000 untuk mengurangi pinjaman Bapak/ibu.".
            "\n\n"."Untuk penebusan/perpanjang masa gadai barang jaminan dapat dilakukan melalui transfer
            BCA : 8115777234 a/n Dotri Gadai Jaya PT".
            "\n"."ketentuan : ".
            "\n"."1. Wajib melakukan konfirmasi melalaui whatsapp Dotri Gadai untuk jumlah tagihan sebelum melakukan transfer.".
            "\n"."2. Wajib mengirim bukti transfer paling lama pukul 22.00 WIB. Jika mengirim bukti transfer melewati pukul 22.00 WIB transaksi diproses dan dihitung keesokan harinya.".
            "\n\n"."Terimakasih"."\n\n"."_PT. DOTRI GADAI JAYA_",
        ];

        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(
                "Authorization: $token",
            )
        );
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_URL,  "https://deu.wablas.com/api/send-message");
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $result = curl_exec($curl);
        curl_close($curl);
        $lapor = json_decode($result);
        print_r($lapor);

        if(isset($lapor->data->messages[0]->id)){
            $status = $lapor->data->messages[0]->status;
            $message_id = $lapor->data->messages[0]->id;
        }else{
            $status = "REJECT";
            $message_id = "";
        }

        $message = $lapor->data->messages[0]->message;
        $dataLog->status_wa = $status;
        $dataLog->message_id = $message_id;
        $dataLog->message = $message;
        $dataLog->save();
    }

    public function actionTestKirim(){
        $dataLog = LogNotifikasi::find()->one();
        $tombolStatus = "STATUS ".$dataLog->barang->no_kontrak;
        $phone = $dataLog->no_hp_nasabah;
        $no_device = $dataLog->device->no_device;
        $token = $dataLog->device->token;
        $tgl_jual = date_format(date_create($dataLog->barang->forjual), 'd M Y');
        $tgl_jatuh_tempo = date_format(date_create($dataLog->barang->jatuh_tempo), 'd M Y');

        $curl = curl_init();
        $token = $token;
        $data = [
        'phone' => '+6282136392052',
        //'message' => '*'.$dataLog->barang->cabang->nama_cabang.'*'."\n".'NO. SBG : '."\n".$dataLog->barang->no_kontrak."\n"."Halo Bapak/Ibu ".$dataLog->barang->nasabah->nama.", kami memberitahukan barang Bapak/Ibu akan jatuh tempo pada tgl ".$tgl_jatuh_tempo.".\n"."Segera lakukan pembayaran."."\n\n"."Terimakasih."."\n"."_PT DOTRI GADAI JAYA_",
        //'message' => '*'.$dataLog->barang->cabang->nama_cabang.'*'."\n".'NO. SBG : '.$dataLog->barang->no_kontrak."\n"."Halo Bapak/Ibu ".$dataLog->barang->nasabah->nama.", kami memberitahukan barang Bapak/Ibu akan akan dijual pada tgl ".$tgl_jual.".\n"."Segera lakukan pembayaran SEBELUM TGL JUAL untuk menghindari barang TERJUAL."."\n\n"."Terimakasih"."\n\n".'_PT. DOTRI GADAI JAYA'
        // 'message' => '*'.$dataLog->barang->cabang->nama_cabang.'*'."\n".'NO. SBG : '.$dataLog->barang->no_kontrak."\n"."Halo Bapak/Ibu ".$dataLog->barang->nasabah->nama.", kami memberitahukan bahwa Besok terakhir untuk pembayaran Perpanjang/Tebus barang Bapak/Ibu.\n"."Setelah itu barang akan dijual."."\n\n"."Terimakasih"."\n\n"."_PT. DOTRI GADAI JAYA_",
        'message' => 'https://google.com'
        ];
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(
                "Authorization: $token",
            )
        );
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_URL,  "https://deu.wablas.com/api/send-message");
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($curl);
        curl_close($curl);
        // echo "<pre>";
        // print_r($result);
        $lapor = json_decode($result);
        print_r($lapor);
    }

    public function actionDeviceInfo(){
        $curl = curl_init();
        $token = "tF9h1Mc5ybbvjn6KZYhtJziXHrTdT6Sma2Nij2I5baYdGBXrLFzU1FX0A2rEZsI1";
        curl_setopt($curl, CURLOPT_URL,  "https://deu.wablas.com/api/device/info?token=$token");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $result = curl_exec($curl);
        curl_close($curl);
        $lapor = json_decode($result);
        echo $lapor->data->name;
    }

    public function actionReportMessage(){
        $dataLog = LogNotifikasi::find()->where(['created_at' => date('Y-m-d')])->andWhere(['or', ['status_wa' => 'PENDING'], ['status_wa' => 'DELIVERED']])->limit(10)->all();
        
        foreach ($dataLog as $log){
            $mLog = new LogNotifikasi();
            $token = $this->pureToken($log->device->token);
            $message_id = $log->message_id;

            $curl = curl_init();
            $token = $token;
            $page = "";
            $limit = "10";
            $message_id = $message_id;
            curl_setopt($curl, CURLOPT_HTTPHEADER,
                array(
                    "Authorization: $token",
                )
            );
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_URL,  "https://deu.wablas.com/api/report-realtime?page=$page&message_id=$message_id&limit=$limit");
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

            $result = curl_exec($curl);
            curl_close($curl);
            $lapor = json_decode($result);
            print_r($lapor);
            
            $status = $lapor->data[0]->status;

            if($status == "PENDING"){
                $status = "DELIVERED";
            }
            
            if($status == "delivered"){
                $status = "SENT";
            }

            //echo $status;

            $mLog->updateAll(['status_wa' => $status],['id' => $log->id]);
        }
    }

    public function pureToken($token){
        $pureToken = explode('.', $token);
        return $pureToken[0];
    }
} 