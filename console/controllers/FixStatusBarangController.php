<?php

namespace console\controllers;
use yii\console\Controller;
use Yii;
use backend\models\LogStatusExt;
use common\models\LogStatusTemp;
use common\models\OutstandingManager;
use common\models\Barang;


class FixStatusBarangController extends Controller
{

    public function actionIndex()
    {   
        $mLogStatusTemp = new LogStatusTemp();
        $dataLog = $mLogStatusTemp->find()->all();
        foreach($dataLog as $dataLogs){
            $mBarang = new Barang();
            $dataBarang = $mBarang->findOne($dataLogs->id_barang);
            if(empty($dataBarang)){
                echo $dataLogs->id_barang;
                echo "\n";
            }
        }
    }

}
