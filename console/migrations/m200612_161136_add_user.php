<?php

use yii\db\Migration;

/**
 * Class m200612_161136_add_user
 */
class m200612_161136_add_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('user', [
            'id' => 1,
            'username'=>'admin',
            'email' => 'admin@localhost.com',
            'password_hash'=>\Yii::$app->getSecurity()->generatePasswordHash('admin'),
            'password_reset_token'=>\Yii::$app->getSecurity()->generatePasswordHash('admin'),
            'status'=>10,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s'),
            'auth_key'=>\Yii::$app->security->generateRandomString(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200612_161136_add_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200612_161136_add_user cannot be reverted.\n";

        return false;
    }
    */
}
